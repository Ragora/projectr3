//--------------------------------------
// Subspace Magnet
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(SMagFireSound)
{
   filename    = "fx/weapons/grenade_flash_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(SMagMountSound)
{
   filename    = "fx/misc/mine.deploy.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(SMagChargeSound)
{
   filename    = "fx/powered/turret_heavy_idle.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(SMagModeSound)
{
   filename    = "fx/weapons/grenade_camera_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(SMagFailSound)
{
   filename    = "fx/weapons/chaingun_off.wav";
   description = AudioDefault3d;
   preload = true;
};

// Projectiles
//

datablock LinearFlareProjectileData(FusionPulseSize1)
{
   explosion           = "BlasterExplosion";
   splash              = DiscSplash;

   hasDamageRadius     = true;
   damageRadius        = 2.0;
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::SubspaceMagnet;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = true;

   dryVelocity       = 175.0;
   wetVelocity       = 175.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 900;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "3.0 3.0 3.0";
   numFlares         = 30;
   flareColor        = "0.5 0.32 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/shrikeBoltCross";
};

datablock LinearFlareProjectileData(FusionPulseSize2) : FusionPulseSize1
{
   explosion           = "PlasmaBoltExplosion";

   hasDamageRadius     = true;
   damageRadius        = 4.0;
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::SubspaceMagnet;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   scale             = "6.0 6.0 6.0";
};

datablock LinearFlareProjectileData(FusionPulseSize3) : FusionPulseSize1
{
   explosion           = "PhaserExplosion";

   hasDamageRadius     = true;
   damageRadius        = 6.0;
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::SubspaceMagnet;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = true;

   scale              = "9.0 9.0 9.0";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(SubspaceCapacitor)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some subspace capacitor charges";
   cantPickup = true;

   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SubspaceMagnet)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_elf_large.dts";
   image = SubspaceMagnetImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a subspace magnet";
};

datablock ShapeBaseImageData(SMagDecalAImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   offset = "0.12 0.85 0";
   emap = true;
   rotation = "0 0 1 -90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
   
   subImage = true;
};

datablock ShapeBaseImageData(SMagDecalBImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   offset = "-0.12 0.85 0";
   emap = true;
   rotation = "0 0 1 90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
   
   subImage = true;
};

datablock ShapeBaseImageData(SMagDecalCImage)
{
   shapeFile = "pack_upgrade_cloaking.dts";
   offset = "0 0.6 0.32";
   emap = true;
   rotation = "1 0 0 90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";

   subImage = true;
};

datablock ShapeBaseImageData(SubspaceMagnetImage)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   item = SubspaceMagnet;
   ammo = SubspaceCapacitor;

   offset = "0 0.2 0.05";
   emap = true;
   usesEnergy = true;
   minEnergy = -1;

   enhancementSlots = 2;

   weaponDescName = "Subspace Magnet";
   defaultModeName = "Fusion Pulse";
   defaultModeDescription = "Charged ball of energy, hold down trigger for damage!";
   defaultModeSwitchSound = "SMagModeSound";
   defaultModeFireSound = "SMagFireSound";
   defaultModeFailSound = "SMagFailSound";

   subImage1 = SMagDecalAImage;
   subImage2 = SMagDecalBImage;
   subImage3 = SMagDecalCImage;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Deploy";
	stateSound[0]                    = SMagMountSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "Ready";

   stateName[1]                     = "Ready";
   stateTransitionOnTriggerDown[1]  = "Fire";

   stateName[2]                     = "Fire";
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateScript[2]                   = "onFire";
   stateSound[2]                    = SMagChargeSound;
   stateTransitionOnTriggerUp[2]    = "Deconstruction";

   stateName[3]                     = "Deconstruction";
   stateScript[3]                   = "onRelease";
   stateTimeoutValue[3]             = 0.5;
   stateTransitionOnTimeout[3]      = "Ready";
};

function SubspaceMagnetImage::hasAmmo(%data, %obj)
{
    %obj.smChargeLevel = %obj.getInventory(%data.ammo);
    %obj.setInventory(%data.ammo, 0);
    return true;
}

function SubspaceMagnetImage::getSubspaceChargeTime(%data, %obj)
{
    if(%obj.isDead)
        return 32;
        
    %subspaceModifier = %obj.subspacing ? 0.5 : 1;
    
    %time = mFloor(100 * %subspaceModifier);

    return %time;
}

function SubspaceMagnetImage::onFire(%data, %obj, %slot)
{
    if(%obj.weaponJammed == true)
    {
        if(%data.defaultModeFailSound !$= "")
            %obj.play3D(%data.defaultModeFailSound);

        messageClient(%obj.client, 'MsgWeaponJammed', '\c2Error: Interference from a nearby field, weapon jammed!');
        return;
    }

    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];

    if(isObject(%mode))
        if(%obj.fireTimeout[%data, %mode] > getSimTime())
            return;

    if(!%data.validateFire(%obj, %turret))
    {
        %data.misFire(%obj, %slot);
        return;
    }
    
    %obj.triggeredSM = true;
    %amount = 0;

    if(%obj.smPrecharge == true && %obj.getEnergyLevel() > 30)
    {
        %amount = 10;
        %obj.useEnergy(30);
    }
    
    %obj.setInventory(%data.ammo, %amount);
    %obj.smChargeLevel = %amount;
    
    %data.doSMCharge(%obj);
}

function SubspaceMagnetImage::doSMCharge(%data, %obj)
{
    if(%obj.isDead)
        return;

    if(%obj.triggeredSM)
    {
        if((%obj.getInventory(%data.ammo) / %obj.maxInventory(%data.ammo)) < 1)
            %obj.incInventory(%data.ammo, 1);

        %data.schedule(%data.getSubspaceChargeTime(%obj), "doSMCharge", %obj);
    }
}

// Re-validate in case they change modes or enter a jammer field
function SubspaceMagnetImage::spawnSMCharge(%data, %obj)
{
    if(%obj.weaponJammed == true)
    {
        if(%data.defaultModeFailSound !$= "")
            %obj.play3D(%data.defaultModeFailSound);

        messageClient(%obj.client, 'MsgWeaponJammed', '\c2Error: Interference from a nearby field, weapon jammed!');
        return;
    }

    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];

    if(!%data.validateFire(%obj, %turret))
    {
        %data.misFire(%obj, %slot);
        return;
    }
    
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %p = 0;
    %chargeLevel = %obj.smChargeLevel;
    
    if(isObject(%mode))
        %p = %mode.spawnProjectile(%data, %obj, %slot, %obj.getMuzzleVector(0), %obj.getMuzzlePoint(0));
    else
    {
        %projName = 0;
        
        if(%chargeLevel >= 30)
            %projName = 3;
        else if(%chargeLevel >= 20)
            %projName = 2;
        else if(%chargeLevel >= 10)
            %projName = 1;

        if(!%projName)
            return;
            
        %p = createProjectile("LinearFlareProjectile", "FusionPulseSize"@%projName, %obj.getMuzzleVector(0), %obj.getMuzzlePoint(0), %obj, 0, %obj);
    }
    
    %p.currentFireMode = %obj.selectedFireMode[%data.item];
    %p.smDamage = %chargeLevel * 5 * (%obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item]);

    if(%p.smElement $= "")
        %p.smElement = $DamageGroupMask::Energy;
//        %p.smKick = %chargeLevel * 15;

    %obj.lastProjectile = %p;
    %obj.client.projectile = %p;

    %fireSound = isObject(%mode) && %mode.fireSound !$= "" ? %mode.fireSound : %data.defaultModeFireSound;

//    %obj.applyKick(%chargeLevel * 10);
    
    if(%fireSound !$= "" || %fireSound !$= "n")
        %obj.play3D(%fireSound);
}

function SubspaceMagnetImage::onRelease(%data,%obj,%slot)
{
    %obj.triggeredSM = false;
     
    %data.spawnSMCharge(%obj);
}
