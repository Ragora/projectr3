//--------------------------------------
// MegaPulse Disruptor
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(MPDisruptorSwitchSound)
{
   filename    = "fx/weapons/sniper_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(MegaPulseHitSound)
{
   filename    = "fx/armor/med_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock ShockwaveData(MegaPulseShockwave)
{
   width = 0.5;
   numSegments = 16;
   numVertSegments = 5;
   velocity = 1;
   acceleration = 11;
   lifetimeMS = 600;
   height = 0.2;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1;

   colors[0] = "1.0 0.0 0.0 1";
   colors[1] = "1.0 0.25 0.25 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";
};

datablock ExplosionData(MegaPulseExplosion)
{
   soundProfile   = MegaPulseHitSound;
   shockwave      = MegaPulseShockwave;
   faceViewer     = true;

   sizes[0] = "0.1 0.1 0.1";
   sizes[1] = "0.1 0.1 0.1";
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleData(MegaPulseTrail)
{
   dragCoeffiecient     = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   colors[0]     = "0.9 0.1 0.1 0.5";
   colors[1]     = "0.6 0.05 0.05 0.2";
   colors[2]     = "0.4 0.0 0.0 0.1";
   colors[3]     = "0.4 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.5;
   sizes[2]      = 0.3;
   sizes[3]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(MegaPulseTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 40;
   overrideAdvance  = true;

   particles = "MegaPulseTrail";
};

datablock LinearFlareProjectileData(MPDisruptorBolt)
{
   directDamage        = 0.0;
   directDamageType    = $DamageType::MPDisruptor;

   scale              = "3.75 3.75 3.75";

   explosion          = "MegaPulseExplosion";
   baseEmitter        = MegaPulseTrailEmitter;

   hasDamageRadius     = true;
   indirectDamage      = 0.8;
   damageRadius        = 3.0;
   radiusDamageType    = $DamageType::MPDisruptor;

   // Multi-damage system for elemental damage attacks
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MPDisruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   dryVelocity       = 250.0;
   wetVelocity       = 250.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1750;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   numFlares         = 20;
   size              = 0.6;
   flareColor        = "1 0.25 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(MPDisruptorAImage)
{
   shapeFile = "weapon_mortar.dts";
   offset = "-0.075 1.1 0.075";
   emap = true;
   rotation = "1 0 0 180";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";

   subImage = true;
};

datablock ShapeBaseImageData(MPDisruptorBImage)
{
   shapeFile = "turret_elf_large.dts";
   emap = true;
   offset = "0 0 0";
   rotation = "1 0 0 0";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "deploy";

   subImage = true;
};

datablock ShapeBaseImageData(MPDisruptorImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   item = MPDisruptor;
   projectile = MPDisruptorBolt;
   projectileType = LinearFlareProjectile;

   offset = "0 0.475 -0.1075";
   rotation = "1 0 0 0";

   usesEnergy = true;
   fireEnergy = 10;
   minEnergy = 10;

   enhancementSlots = 2;
   
   subImage1 = "MPDisruptorAImage";
   subImage2 = "MPDisruptorBImage";

   weaponDescName = "MegaPulse Disruptor";
   defaultModeName = "MegaPulse Disruptor Blast";
   defaultModeDescription = "Bigger, badder, redder, and deader.";
   defaultModeSwitchSound = "BlasterSwitchSound";
   defaultModeFireSound = "AAFireSound";
   defaultModeFailSound = "BlasterDryFireSound";

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = MPDisruptorSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.5;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = MPDisruptorFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";
   
   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.2;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ItemData(MPDisruptor)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy.dts";
   image = MPDisruptorImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a MegaPulse Disruptor";
};
