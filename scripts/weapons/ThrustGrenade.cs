datablock ItemData(ThrusterGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlareGrenadeThrown;
	pickUpName = "some Thrusters";
	isGrenade = true;

   computeCRC = true;
};

function ThrusterGrenade::onUse(%this, %obj)
{
    if(Game.handInvOnUse(%this, %obj))
    {
        %time = getSimTime();

        if(%time > %obj.nextUseTime[%this]) // && %obj.getEnergyPct() >= 0.75)
        {
//            %obj.useEnergy(%obj.getDatablock().maxEnergy * 0.75);
//            commandToClient(%obj.client, 'setInventoryHudAmount', 0, 0);
//            %obj.decInventory(%this, 1);
            %obj.applyKick((%obj.getDatablock().mass + getRandom(5)) * 28.3291);
            %obj.play3D(MILFireSound);
            createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %obj.getMuzzleVector(0), %obj.position, 0, %obj);

            // miscellaneous grenade-throwing cleanup stuff
            %obj.lastThrowTime[%this] = %time;
            %obj.nextUseTime[%this] = %time + 6000;
            %obj.throwStrength = 0;
        }
        else //if(%time > %obj.nextUseTime[%this])
//            messageClient(%obj.client, 'MsgThrusterLowPower', "\c3Need at least 75\x25 energy for thruster.");
            messageClient(%obj.client, 'MsgThrusterRecharge', "\c3Thrusters recharging...");
    }
}
