//--------------------------------------------------------------------------
// Sniper rifle
// 
// 
//--------------------------------------------------------------------------
datablock AudioProfile(SniperRifleSwitchSound)
{
   filename    = "fx/weapons/sniper_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(SniperRifleFireSound)
{
   filename    = "fx/weapons/sniper_fire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleFireWetSound)
{
   filename    = "fx/weapons/sniper_underwater.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleProjectileSound)
{
   filename    = "fx/weapons/sniper_miss.wav";
   description = AudioClose3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData( SniperSplashParticle )
{

   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.03;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 600;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;


   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SniperSplashEmitter )
{
   ejectionPeriodMS = 25;
   ejectionOffset = 0.2;
   periodVarianceMS = 0;
   ejectionVelocity = 2.25;
   velocityVariance = 0.50;
   thetaMin         = 0.0;
   thetaMax         = 30.0;
   lifetimeMS       = 250;

   particles = "SniperSplashParticle";
};

datablock SplashData( SniperSplash )
{
   numSegments = 5;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = SniperSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock AudioProfile(sniperExpSound)
{
   filename    = "fx/weapons/sniper_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock ParticleData(SniperExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.0625;
   sizes[1]      = 0.2;
};

datablock ParticleEmitterData(SniperExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "SniperExplosionParticle1";
};

datablock ExplosionData(SniperExplosion)
{
   explosionShape = "energy_explosion.dts";
   soundProfile   = sniperExpSound;

   particleEmitter = SniperExplosionEmitter;
   particleDensity = 150;
   particleRadius = 0.25;

   faceViewer           = false;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(BasicSniperShot)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 1000;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(PulseLaser)
{
   directDamage        = 0.25;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 75;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 69;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 69;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;
   
   hasFalloff = true;
   optimalRange = 50;
   falloffRange = 75;
   falloffDamagePct = 0.1;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(PulseLaserExtended) : PulseLaser
{
   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 69;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 69;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;
   
   hasFalloff = true;
   optimalRange = 150;
   falloffRange = 300;
   falloffDamagePct = 0.1;
};

//--------------------------------------------------
// Vehicle beams

datablock SniperProjectileData(VMBeamLaser)
{
   directDamage        = 1.0;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 80;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 80;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;
   maxRifleRange       = 200;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   hasFalloff = true;
   optimalRange = 200;
   falloffRange = 150;
   falloffDamagePct = 0.333;
   
   startBeamWidth		  = 0.75;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 0.75;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 7.0;
   pulseLength         = 0.3;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(VMPulseLaser) : PulseLaser
{
   directDamage        = 0.4;
   maxRifleRange       = 100;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 35;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 35;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   hasFalloff = true;
   optimalRange = 60;
   falloffRange = 100;
   falloffDamagePct = 0.1;
   
   startBeamWidth		  = 0.75;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 0.75;
   pulseSpeed          = 7.0;
   pulseLength         = 0.3;
};

datablock SniperProjectileData(VLLaserStream) : PulseLaser
{
   directDamage        = 0.2;
   maxRifleRange       = 150;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 10;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   hasFalloff = true;
   optimalRange = 75;
   falloffRange = 150;
   falloffDamagePct = 0.1;
   
   startBeamWidth		  = 1.0;
   endBeamWidth 	     = 0.6;
   pulseBeamWidth 	  = 1.5;
   pulseSpeed          = 16.0;
   pulseLength         = 0.6;
};

datablock SniperProjectileData(VSBeamLaser) : VMBeamLaser
{
   directDamage        = 0.5;
   maxRifleRange       = 50;
   flags               = $Projectile::CanHeadshot;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 0.65;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 25;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   hasFalloff = true;
   optimalRange = 25;
   falloffRange = 50;
   falloffDamagePct = 0.333;
};

datablock SniperProjectileData(VLBeamLaser) : VMBeamLaser
{
   directDamage        = 2.0;
   maxRifleRange       = 250;
   flags               = $Projectile::CanHeadshot;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.25;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 130;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 130;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;
   
   hasFalloff = true;
   optimalRange = 150;
   falloffRange = 250;
   falloffDamagePct = 0.333;
   
   startBeamWidth		  = 0.75;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   pulseSpeed          = 8.0;
   pulseLength         = 0.45;
};

//--------------------------------------
// Rifle and item...
//--------------------------------------
datablock ItemData(SniperRifle)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_sniper.dts";
   image        = SniperRifleImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a Laser Assault Rifle";

   computeCRC = true;
};

datablock ShapeBaseImageData(SniperRifleImage)
{
	className = WeaponImage;
   shapeFile = "weapon_sniper.dts";
   item = SniperRifle;
   projectile = PulseLaser;
   projectileType = SniperProjectile;
	armThread = looksn;

   enhancementSlots = 2;

   weaponDescName = "Laser Assault Rifle";
   defaultModeName = "Pulse Laser";
   defaultModeDescription = "3-burst of 20kj laser pulses, 50m effective range, 75m falloff range";
   defaultModeSwitchSound = "SniperRifleSwitchSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "SniperRifleDryFireSound";

   usesEnergy = true;
   fireEnergy = 5;
   minEnergy = 5;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateSound[0]                    = SniperRifleSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.05;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";

   stateName[4]                     = "Reload";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;

   stateName[5]                     = "CheckWet";
   stateTransitionOnWet[5]          = "DryFire";
   stateTransitionOnNotWet[5]       = "Fire";
   
   stateName[6]                     = "NoAmmo";
   stateTransitionOnAmmo[6]         = "Reload";
   stateTransitionOnTriggerDown[6]  = "DryFire";
   stateSequence[6]                 = "NoAmmo";
   
   stateName[7]                     = "DryFire";
   stateSound[7]                    = SniperRifleDryFireSound;
   stateTimeoutValue[7]             = 0.5;
   stateTransitionOnTimeout[7]      = "Ready";
};

function SniperRifleImage::validateFireMode(%data, %obj)
{
    %ammoUse = 0;
    %gyanUse = 0;
    %energyUse = 60;

    if(%obj.getEnergyLevel() < %energyUse)
        return "f";

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function SniperRifleImage::spawnProjectile(%data, %obj, %slot, %mode)
{
    %vector = %obj.getMuzzleVector(%slot);

    if(%mode !$= "")
        %proj = %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
    else
    {
        if(%obj.laserFocus)
            %proj = createProjectile(%data.projectileType, %data.projectile@"Extended", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
        else
            %proj = createProjectile(%data.projectileType, %data.projectile, %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    }
    
    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];

    if(%data.deleteLastProjectile)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        %obj.deleteLastProjectile = %data.deleteLastProjectile;
    }

    %obj.lastProjectile = %proj;

    // AI hook
    if(%obj.client)
        %obj.client.projectile = %proj;

    return %proj;
}

function SniperRifleImage::onFire(%data,%obj,%slot)
{
    %mode = %obj.client.weaponMode[%data.item, %obj.client.selectedFireMode[%data.item]];
    %time = getSimTime();

    if(%obj.rateOfFire[%data.item] $= "")
        %obj.rateOfFire[%data.item] = 1.0;
    
    if(isObject(%mode))
    {
        if(%obj.fireTimeout[%data, %mode] > %time)
            return;
    }
    else
    {
        if(%obj.fireTimeout[%data, %data.item] > %time)
            return;
    }

    if(!isObject(%mode))
    {
        if(%data.validateFire(%obj, %slot, %mode))
        {
            for(%i = 0; %i < 3; %i++)
                %data.schedule(%i * 200, "burstFire", %obj, %slot);
        }
        
        %obj.fireTimeout[%data, %data.item] = %time + (1500 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %p = Parent::onFire(%data,%obj,%slot);
        
        if(isObject(%p))
            %p.setEnergyPercentage(0.85);
    }
}

function SniperRifleImage::burstFire(%data, %obj, %slot)
{
    %p = %data.spawnProjectile(%obj, %slot, "");
    %p.setEnergyPercentage(0.5);
    %obj.play3D(SniperRifleProjectileSound);
}
