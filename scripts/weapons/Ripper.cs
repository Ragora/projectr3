//--------------------------------------
// Ripper
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(RipperFireSound)
{
   filename    = "fx/weapons/TR2spinfusor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(RipperMountSound)
{
   filename    = "fx/vehicles/bomber_turret_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(RipperAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some heavy discs";
   cantPickup = true;

   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Ripper)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_disc.dts";
   image = RipperImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Heavy Disc Launcher";
};

datablock ShapeBaseImageData(RipperDecalAImage)
{
   shapeFile = "pack_upgrade_satchel.dts";
   offset = "0 0.65 0.15";
   emap = true;
   rotation = "1 0 0 90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
   
   subImage = true;
};

datablock ShapeBaseImageData(RipperDecalBImage)
{
   shapeFile = "pack_barrel_aa.dts";
   offset = "0 0.35 -0.25";
   emap = true;
   rotation = "1 0 0 90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";

   subImage = true;
};

datablock ShapeBaseImageData(RipperImage)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_disc.dts";
   item = Ripper;
   ammo = RipperAmmo;

   offset = "0 0.2 -0.25";
   emap = true;

   enhancementSlots = 2;

   weaponDescName = "Heavy Disc Launcher";
   defaultModeName = "Power Disc";
   defaultModeDescription = "Densely packed HE maglev disc";
   defaultModeSwitchSound = "DiscReloadSound";
   defaultModeFireSound = "RipperFireSound";
   defaultModeFailSound = "DiscDryFireSound";

   projectile = PowerDiscProjectile;
   projectileType = LinearProjectile;

   subImage1 = RipperDecalAImage;
   subImage2 = RipperDecalBImage;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = RipperMountSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.75; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function RipperImage::spawnProjectile(%data, %obj, %slot, %mode) // I think T2 clears the datablock's projectile field if it does not exist at the time of compilation, which is retarded
{
    %vector = %data.projectileSpread > 0 ? vectorSpread(%obj.getMuzzleVector(%slot), %data.projectileSpread * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item])) : %obj.getMuzzleVector(%slot);

    if(%mode !$= "")
        %proj = %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
    else
        %proj = createProjectile(%data.projectileType, "PowerDiscProjectile", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);

    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
    %obj.lastProjectile = %proj;

    // AI hook
    if(%obj.client)
        %obj.client.projectile = %proj;

    return %proj;
}
