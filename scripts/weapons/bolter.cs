//--------------------------------------
// Bolter
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(BolterSwitchSound)
{
   filename    = "fx/powered/turret_sentry_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(BolterFireSound)
{
   filename    = "fx/vehicles/tank_chaingun.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//------------------------------------------------------------------------------

datablock TracerProjectileData(BolterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.2;
   directDamageType    = $DamageType::Bolter;
   explosion           = "ChaingunExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bolter;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 350.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   tracerLength    = 12.0;
   tracerAlpha     = false;
   tracerMinPixels = 8;
   tracerColor     = 224.0/255.0 @ " " @ 227.0/255.0 @ " " @ 159.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.45;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function BolterBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    %snd = %projectile.isWet() ? getRandomChaingunWetSound() : getRandomChaingunSound();
    %projectile.play3D(%snd);
    
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(BolterAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some bolter ammo";

   computeCRC = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(BolterImage) : TriggerProxyImage
{
   item      = Bolter;
   ammo 	 = BolterAmmo;

   shapeFile = "weapon_mortar.dts";
   rotation = "0 0 1 180";
   offset = "0.25 0.5 0";

   enhancementSlots = 3;

   weaponDescName = "Bolter";
   defaultModeName = "Depleted Uranium Bolt";
   defaultModeDescription = "Shell-shaped bolt formed out depleted uranium";
   defaultModeSwitchSound = "BolterSwitchSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "ChaingunDryFireSound";
   
   subImage2 = "BolterAmmoImage";
};

datablock ShapeBaseImageData(BolterAmmoImage) : TriggerProxyImage
{
   item      = Bolter;
   ammo 	 = BolterAmmo;
   subImage = true;
   
   shapeFile = "ammo_chaingun.dts";
   rotation = "1 0 0 90";
   offset = "0.115 0.8 0.24";
};

datablock ShapeBaseImageData(BolterGunImage)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_chaingun.dts";
   item      = Bolter;
   ammo 	 = BolterAmmo;
   projectile = BolterBolt;
   projectileType = TracerProjectile;
   emap = true;

   offset = "0 0.55 0";

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 5.0;

   subImage = true;
   projectileSpread = 6.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = BolterSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.25;
   stateWaitForTimeout[3]        = true;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = BolterFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.2;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.5;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(BolterGunTurboImage) : BolterGunImage
{
   shellVelocity       = 9.0;

   stateTimeoutValue[3]          = 0.1;
   stateTimeoutValue[4]          = 0.1;
   stateTimeoutValue[5]            = 0.25;
};

datablock ItemData(Bolter)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_chaingun.dts";
   image        = BolterImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "a Bolter";

   computeCRC = true;
   emap = true;
};

function BolterImage::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);

    %mode = %obj.client.weaponMode[%this.item, %obj.client.selectedFireMode[%this.item]];
    %subImage = "BolterGunImage";

    if(isObject(%mode) && %mode.subImage !$= "")
        %subImage = %mode.subImage;
    else if(%obj.rateOfFire[%this.item] > 0)
        %subImage = "BolterGunTurboImage";

    %obj.mountImage(%subImage, $WeaponSubImage1);
}

function BolterImage::onUnmount(%this,%obj,%slot)
{
    %obj.unmountImage($WeaponSubImage1);

    Parent::onUnmount(%this, %obj, %slot);
}

function BolterGunTurboImage::validateFireMode(%data, %obj)
{
    %ammoUse = 1;
    %energyUse = %obj.rateOfFire[%obj.getMountedImage(0).item] > 0 ? 2 : 0;
    
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energyUse)
        return "f";

    %gyanUse = 0;

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function Bolter::onChangeMode(%weapon, %obj, %mode)
{
    %subImage = "BolterGunImage";

    if(isObject(%mode) && %mode.subImage !$= "")
        %subImage = %mode.subImage;
    else if(%obj.rateOfFire[%weapon] > 0)
        %subImage = "BolterGunTurboImage";

    %obj.mountImage(%subImage, $WeaponSubImage1);
}
