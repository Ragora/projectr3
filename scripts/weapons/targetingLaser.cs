//--------------------------------------------------------------------------
// Targeting laser
// 
//--------------------------------------------------------------------------
datablock AudioProfile(TargetingLaserSwitchSound)
{
   filename    = "fx/weapons/generic_switch.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(TargetingLaserPaintSound)
{
   filename    = "fx/weapons/targetinglaser_paint.wav";
   description = CloseLooping3d;
   preload = true;
};


//--------------------------------------
// Projectile
//--------------------------------------
datablock TargetProjectileData(BasicTargeter)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 1000;
   beamColor           	= "0.1 1.0 0.1";
								
   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = true;
};


//--------------------------------------
// Rifle and item...
//--------------------------------------
datablock ItemData(TargetingLaser)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_targeting.dts";
   image        = TargetingLaserImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "an Armor Ability";

   dontCycleTo = true;
   computeCRC = true;
};

datablock ShapeBaseImageData(TargetingLaserImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = TargetingLaser;
   offset = "0 0 0";

   projectile = BasicTargeter;
   projectileType = TargetProjectile;
   deleteLastProjectile = true;

   enhancementSlots = 0;

   weaponDescName = "Armor Ability";
   defaultModeName = "Targeting Laser";
   defaultModeDescription = "Paint targets by holding down the fire button, works with mortars.";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "BlasterFireSound";
   defaultModeFailSound = "BlasterDryFireSound";
   
	usesEnergy = true;
	minEnergy = 3;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
	stateEnergyDrain[3]              = 3;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

datablock ShapeBaseImageData(TargetingLaserFakeImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = TargetingLaser;
   offset = "0 0 0";

   projectile = BasicTargeter;
   projectileType = TargetProjectile;
   deleteLastProjectile = true;

   enhancementSlots = 0;

   weaponDescName = "Armor Ability";
   defaultModeName = "Targeting Laser";
   defaultModeDescription = "Paint targets by holding down the fire button, works with mortars.";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "BlasterDryFireSound";
   
	usesEnergy = true;
	minEnergy = 3;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
	stateEnergyDrain[3]              = 3;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

datablock ShapeBaseImageData(ArmorSpecialImage)
{
   className = WeaponImage;

   shapeFile = "repair_patch.dts";
   item = TargetingLaser;
   offset = "0 0 0";

   projectile = "";
   projectileType = "";
   deleteLastProjectile = true;

   enhancementSlots = 0;

   weaponDescName = "Armor Ability";
   defaultModeName = "Special Function";
   defaultModeDescription = "Press fire to activate your armor's special ability.";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "BlasterFireSound";
   defaultModeFailSound = "BlasterDryFireSound";

	usesEnergy = true;
	minEnergy = -1;
    fireEnergy = -1;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
	stateEnergyDrain[3]              = 0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
//   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

// Wish I didn't have to do this statically, ah well
function ArmorSpecialImage::onFire(%data, %obj, %slot)
{
    %pData = %obj.getDatablock();
    %obj.schedule(0, "setImageTrigger", 0, false);
    %obj.schedule(0, "umountImage", 0);
    
    switch(%pData.armorType)
    {
        case $ArmorType::Plugsuit:
            triggerSpecialPlugsuit(%pData, %obj, %imageData);
            return;

        case $ArmorType::Scout:
            triggerSpecialScout(%pData, %obj, %imageData);
            return;
            
        case $ArmorType::Gearhead:
            triggerSpecialGearhead(%pData, %obj, %imageData);
            return;
            
        case $ArmorType::Assault:
            triggerSpecialAssault(%pData, %obj, %imageData);
            return;
            
        case $ArmorType::MagneticIon:
            triggerSpecialMagneticIon(%pData, %obj, %imageData);
            return;
            
        default:
            return;
    }
}

datablock ShapeBaseImageData(BarrelSwapperImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = TargetingLaser;
   offset = "0 0 0";

   projectile = "";
   projectileType = "";
   customModeChange = true;

   enhancementSlots = 0;

   weaponDescName = "Barrel Swapper";
   defaultModeName = "Barrel Swapper";
   defaultModeDescription = "Point and fire to change the turret's barrel, mode to change barrels.";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "BlasterFireSound";
   defaultModeFailSound = "BlasterDryFireSound";
   
    barrelCount = 4;

    barrelName[0] = "Plasma Barrel";
    barrelDesc[0] = "Shoots white-hot plasma blasts. 105 HP Plasma damage.";
    barrelImage[0] = "PlasmaBarrelLarge";
    
    barrelName[1] = "Anti-Air Blaster Barrel";
    barrelDesc[1] = "Fires blaster bolts tuned to do 250% damage to flying vehicles. 75 HP Electromagnetic damage.";
    barrelImage[1] = "AABarrelLarge";
    
    barrelName[2] = "Arc Welder Barrel";
    barrelDesc[2] = "Energy whip, drains power and does damage. 16 HP Electromagnetic damage/sec.";
    barrelImage[2] = "ELFBarrelLarge";
    
    barrelName[3] = "Missile Launcher Barrel";
    barrelDesc[3] = "Locks onto flying targets and fires a missile. 200 HP Explosive damage.";
    barrelImage[3] = "MissileBarrelLarge";
    
    barrelName[4] = "Mortar Launcher Barrel";
    barrelDesc[4] = "Lobs Mortar shells with an 8 second fuse. 250 HP Explosive damage.";
    barrelImage[4] = "MortarBarrelLarge";

	usesEnergy = true;
	minEnergy = -1;
    fireEnergy = -1;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
	stateEnergyDrain[3]              = 0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
//   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

function BarrelSwapperImage::selectFireMode(%this, %obj)
{
    %obj.selectedBarrel++;
    
    if(%obj.selectedBarrel > %this.barrelCount)
        %obj.selectedBarrel = 0;

    bottomPrint(%obj.client, "<color:ffffff>[Turret Barrel "@%obj.selectedBarrel+1@"/"@%this.barrelCount+1@"] <color:42dbea>"@%this.barrelName[%obj.selectedBarrel] NL %this.barrelDesc[%obj.selectedBarrel], $WeaponMode::DisplayTime, 2);
}

function BarrelSwapperImage::onFire(%this, %obj, %slot)
{
    %time = getSimTime();
    
    if(%obj.lastBarrelSwap > getSimTime())
        return;
        
    %hit = castRay(%obj.getMuzzlePoint(%slot), %obj.getMuzzleVector(%slot), 10, $TypeMasks::TurretObjectType);

    if(%hit)
    {
        if(%hit.hitObj.getDatablock().shapeFile $= "turret_base_large.dts")
        {
            if(%hit.hitObj.team == %obj.client.team)
            {
                if(%hit.hitObj.getDamageState() $= "Enabled")
                {
                    %hit.hitObj.clearTarget();
                    %hit.hitObj.unmountImage(0);
                    %hit.hitObj.mountImage(%this.barrelImage[%obj.selectedBarrel], 0, true);
                    %hit.hitObj.play3D("TurretPackActivateSound");

                    messageClient(%obj.client, 'MsgTurretMount', "\c2Mounted turret barrel.");
                }
                else
                    messageClient(%obj.client, 'MsgBaseDestroyed', "\c2Turret base is disabled, cannot mount barrel.");
            }
            else
                messageClient(%obj.client, 'MsgEnemyTurretMount', "\c2Cannot mount a barrel on an enemy turret.");
        }
        else
            messageClient(%obj.client, 'MsgTurretNotMountable', "\c2This object cannot be reconfigured.");
    }
    else
        messageClient(%obj.client, 'MsgNoTurretBase', "\c2No turret found within scan range.");
        
    %obj.lastBarrelSwap = %time + 500;
    %obj.setImageTrigger(%slot, false);
}
