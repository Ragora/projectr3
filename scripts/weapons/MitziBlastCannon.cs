//---------------------------------------
// Mitzi Blast Cannon v2.0
// First major revision since leaving MD2
//---------------------------------------

datablock AudioProfile(MitziFireSound)
{
   filename    = "fx/misc/gridjump.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(MitziModeSwitch)
{
   filename    = "gui/launchMenuOver.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(MBExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 0.1 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MBExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MBExplosionParticle";
};

datablock ExplosionData(MitziBlastExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MBExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ExplosionData(UMitziBlastExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MBExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;

   bubbleEmitter = "DiscExplosionBubbleEmitter";
};

//--------------------------------------------------------------------------
// Trail
//--------------------------------------

datablock ParticleData(MitziTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.5 0.5 0.1 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MitziTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MitziTrailParticle";
};

datablock ExplosionData(MitziShotgunExplosion) : FireballAtmosphereBoltExplosion
{
   shakeCamera = false;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(MitziBlast)
{
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0.6;
   directDamageType    = $DamageType::MitziBlast;
   hasDamageRadius     = true;
   indirectDamage      = 0.45;
   damageRadius        = 14.0;
   kickBackStrength    = 1000;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[0]   = 60;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 60;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "MitziBlastExplosion";
   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MitziTrailEmitter;

   dryVelocity       = 100.0;
   wetVelocity       = 200.0;       // faster uw
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 4000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

datablock ShockwaveData(MitziBoosterShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(BoosterShockwaveExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = MitziBoosterShockwave;

   faceViewer = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MitziCapacitor)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "extra mitzi capacitor charges";
   cantPickup = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MitziBlastCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = MitziImage;
   ammo = MitziCapacitor;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Mitzi Blast Cannon";

   emap = true;
};

datablock ShapeBaseImageData(MBChargerAImage)
{
   shapeFile = "weapon_elf.dts";
   offset = "0.055 0.225 0.075";
   emap = true;
   
   stateName[0] = "Activate";
   stateSequence[0] = "Activate";
   
   subImage = true;
};

datablock ShapeBaseImageData(MBChargerBImage) : MBChargerAImage
{
   offset = "0.055 0.225 -0.01";
   rotation = "0 1 0 135";
};

datablock ShapeBaseImageData(MBChargerCImage) : MBChargerAImage
{
   offset = "0.14 0.225 0.025";
   rotation = "0 1 0 -135";
};

datablock ShapeBaseImageData(MitziImage)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   item = MitziBlastCannon;
   ammo = MitziCapacitor;
   offset = "0 0 0";
   emap = true;

   projectile = MitziBlast;
   projectileType = LinearFlareProjectile;

   enhancementSlots = 4;

   weaponDescName = "Mitzi Blast Cannon";
   defaultModeName = "Mitzi Blast";
   defaultModeDescription = "The one weapon to rule them all";
   defaultModeSwitchSound = "MitziModeSwitch";
   defaultModeFireSound = "MitziFireSound";
   defaultModeFailSound = "MortarDryFireSound";
   
   subImage1 = MBChargerAImage;
   subImage2 = MBChargerBImage;
   subImage3 = MBChargerCImage;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateSequence[3] = "Recoil";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.8;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.75;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = DiscDryFireSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateScript[5] = "onCharge";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function MitziImage::hasAmmo(%data, %obj)
{
    return true;
}

function MitziImage::validateFireMode(%data, %obj)
{
    %ammoUse = 25;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    %gyanUse = 0;
    %energyUse = 0;

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function getMitziTime(%obj)
{
    %energyModifier = %obj.getEnergyPct() == 1 ? 0.5 : 1;
    %chargeModifier = %obj.getInventory(MitziCapacitor) / 100;
    %subspaceModifier = %obj.subspacing ? 0.5 : 1;
    
    %time = mCeil(4000 * %energyModifier * %chargeModifier * %subspaceModifier);

    if(%time < 100)
        %time = 100;
        
    return %time;
}

function updateMitziCharge(%obj)
{
    if((%obj.getInventory(MitziCapacitor) / %obj.maxInventory(MitziCapacitor)) < 1)
        %obj.incInventory(MitziCapacitor, 1);

    if(%obj.mbcCharge)
        schedule(getMitziTime(%obj), %obj, "updateMitziCharge", %obj);
}

// todo:: item::onInventory seems unreliable?
function MitziBlastCannon::onInventory(%this, %player, %value)
{
    if(%player.isPlayer())
    {
        %player.mbcCharge = %value;

        if(%value)
            updateMitziCharge(%player);
    }

    Weapon::onInventory(%this, %player, %value);
}
