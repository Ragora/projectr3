//--------------------------------------
// Plasma Cannon
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(PlasmaCannonSwitchSound)
{
   filename    = "fx/powered/sensor_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaCannonFireSound)
{
   filename    = "fx/powered/turret_plasma_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(PlasmaCannonBolt) : PlasmaBarrelBolt
{
   directDamageType    = $DamageType::PlasmaCannon;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 14.0;
   kickBackStrength    = 0;
   radiusDamageType    = $DamageType::PlasmaCannon;
   explosion           = PlasmaBarrelBoltExplosion;
   splash              = PlasmaSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PlasmaCannon;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 160;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.333;
   statusEffectMask    = $TypeMasks::PlayerObjectType;

   dryVelocity       = 250.0;
   fizzleTimeMS      = 1300;
   lifetimeMS        = 1500;
   
   baseEmitter         = DiscMistEmitter;
};

datablock ShockwaveData(PCFireShockwave)
{
   width = 5;
   numSegments = 24;
   numVertSegments = 7;
   velocity = 1;
   acceleration = -90.0;
   lifetimeMS = 250;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.3 1.0 1.0";
   colors[1] = "0.6 0.6 1.0 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";
};

datablock ExplosionData(PCDisplayExplosion)
{
   soundProfile   = PBCShockwaveSound;
   shockwave = PCFireShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;

   explosion           = "PCDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PlasmaCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some plasma cannon canisters";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(PlasmaCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_plasma.dts";
   image = PlasmaCannonTrigger;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Plasma Cannon";
};

datablock ShapeBaseImageData(PlasmaCannonTrigger) : TriggerProxyImage
{
   item = PlasmaCannon;
   ammo = PlasmaCannonAmmo;

   shapeFile = "weapon_mortar.dts";
   rotation = "0 0 1 180";
   offset = "0.09 1.4 -0.025";
   
   enhancementSlots = 2;

   weaponDescName = "Plasma Cannon";
   defaultModeName = "Blue-hot Fireball";
   defaultModeDescription = "Toasty!";
   defaultModeSwitchSound = "PlasmaReloadSound";
   defaultModeFireSound = "";
   defaultModeFailSound = "PlasmaDryFireSound";
};

datablock ShapeBaseImageData(PlasmaCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
   item = PlasmaCannon;
   ammo = PlasmaCannonAmmo;
   offset = "0 0 0";

   subImage = true;
   defaultModeFireSound = "PlasmaCannonFireSound";
   
   projectile = PlasmaCannonBolt;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Deploy";
   stateSound[0] = PlasmaCannonSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.1;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitterTime[3] = 0.2;
//   stateSound[3] = PlasmaFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 1.15;
   stateAllowImageChange[4] = false;
//   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

function PlasmaCannonImage::validateFireMode(%data, %obj)
{
    %ammoUse = 10;
    %gyanUse = 0;
    %energyUse = 0;
    
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

// keen - 0.1? floating point fail
function PlasmaCannonTrigger::onMount(%this,%obj,%slot)
{
    Parent::onMount(%this,%obj,%slot);
    
    %mode = %obj.client.weaponMode[%this.item, %obj.client.selectedFireMode[%this.item]];
    %subImage = "PlasmaCannonImage";
    
    if(isObject(%mode) && %mode.subImage !$= "")
        %subImage = %mode.subImage;

    %obj.mountImage(%subImage, $WeaponSubImage1);
}

function PlasmaCannonTrigger::onUnmount(%this,%obj,%slot)
{
    %obj.unmountImage($WeaponSubImage1);
    
    Parent::onUnmount(%this, %obj, %slot);
}

function PlasmaCannonImage::spawnProjectile(%data, %obj, %slot, %mode)
{
     %vector = %obj.getMuzzleVector(%slot);

     if(%mode !$= "")
          %proj = %mode.spawnProjectile(%data, %obj, %slot, %vector, %obj.getMuzzlePoint(%slot));
     else
     {
          %proj = createProjectile("LinearFlareProjectile", "PCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
          %obj.play3D("PBCShockwaveSound");
          %data.schedule(250, "PCDelayFire", %obj, %slot);
     }

    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];

    return %proj;
}

function PlasmaCannonImage::PCDelayFire(%data, %obj, %slot)
{
     %proj = createProjectile(%data.projectileType, %data.projectile, %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
     %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
}

function PlasmaCannon::onChangeMode(%weapon, %obj, %mode)
{
    %subImage = "PlasmaCannonImage";

    if(isObject(%mode) && %mode.subImage !$= "")
        %subImage = %mode.subImage;

    %obj.mountImage(%subImage, $WeaponSubImage1);
    commandToClient(%obj.client, 'setRepairReticle');
}
