// Napalm Grenade
// ------------------------------------------------------------------------

datablock ParticleData(NapalmDebrisSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.01;

   lifetimeMS           = 2250;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "flarebase";

   useInvAlpha =     false;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "1.0 0.775 0.3 1.0";
   colors[1]     = "0.4 0.2 0.05 0.75";
   colors[2]     = "0.3 0.3 0.3 0.5";
   colors[3]     = "0.05 0.05 0.01 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.625;
   sizes[2]      = 0.7;
   sizes[3]      = 0.49;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.7;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(NapalmDebrisSmokeEmitter)
{
   ejectionPeriodMS = 16;
   periodVarianceMS = 6;

   ejectionVelocity = 0.5;  // A little oomph at the back end
   velocityVariance = 0.1;

   thetaMin         = 1.0;
   thetaMax         = 8.0;

   particles = "NapalmDebrisSmokeParticle";
};

datablock ParticleData(NapalmExplosionSmoke)
{
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = 0;   // rises slowly
   inheritedVelFactor   = 0;
   constantAcceleration = -0.35;

   lifetimeMS           = 1750;
   lifetimeVarianceMS   = 350;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 0.3 0.0";
   colors[1]     = "0.95 0.3 0.2 1.0";
   colors[2]     = "0.5 0.2 0.1 0.75";
   colors[3]     = "0.2 0.15 0.2 0.0";
   colors[4]     = "0.1 0.1 0.1 0.0";
   colors[5]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 5.5;
   sizes[2]      = 6.1;
   sizes[3]      = 8.5;
   sizes[4]      = 10.25;
   sizes[5]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 0.35547;
   times[3]      = 0.5235;
   times[4]      = 0.83547;
   times[5]      = 1.0;
};

datablock ParticleEmitterData(NapalmExplosionSmokeEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 4;

   ejectionOffset = 1.0;

   ejectionVelocity = 7;
   velocityVariance = 2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 1000;

   particles = "NapalmExplosionSmoke";
};

datablock DebrisData(NapalmExplosionDebris)
{
   emitters[0] = NapalmDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 5.0;
   lifetimeVariance = 2.0;

  numBounces = 3;
   bounceVariance = 2;
};

datablock ExplosionData(NapalmExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "1.75 1.75 1.75";
   sizes[1] = "1.75 1.75 1.75";
   times[0] = 0.0;
   times[1] = 1.0;

   debris = NapalmExplosionDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 45;
   debrisNum = 6;
   debrisNumVariance = 2;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 5.0;

   emitter[0] = NapalmExplosionSmokeEmitter;
};

datablock ItemData(NapalmGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = NapalmExplosion;
   indirectDamage      = 0.4;
   damageRadius        = 6;
   radiusDamageType    = $DamageType::Burn;
   kickBackStrength    = 500;
   
   // MD3 radial status effect code
   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.5;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Burn;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
};

datablock ItemData(NapalmGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = NapalmGrenadeThrown;
	pickUpName = "some Napalm grenades";
	isGrenade = true;
};

function NapalmGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}

function NapalmGrenadeThrown::onRPGHit(%this, %gren)
{
   AIGrenadeThrown(%gren);
   detonateGrenade(%gren);
}
