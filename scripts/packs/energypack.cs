// ------------------------------------------------------------------
// ENERGY PACK
// can be used by any armor type
// does not have to be activated
// increases the user's energy recharge rate

datablock ShapeBaseImageData(EnergyPackImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   item = EnergyPack;
   mountPoint = 1;
   offset = "0 0 0";
   rechargeRateBoost = 0.15;

   enhancementSlots = 1;
   
	stateName[0] = "default";
	stateSequence[0] = "activation";
};

datablock ItemData(EnergyPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_energy.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "EnergyPackImage";
	pickUpName = "a power core";

   computeCRC = true;
};

function EnergyPackImage::onMount(%data, %obj, %node)
{
    %boost = 1;
    %obj.hasEnergyPack = true;
   
    if(%obj.energyPackOverdrive)
    {
        %boost = 2;
        schedule(0, %obj, "ePackOverdrive", %obj);
    }

    if(%obj.turbocharger)
    {
        %boost = 0;
        %obj.damageBuffFactor += 0.2;
    }
        
    %obj.energyPackBonus = (%data.rechargeRateBoost / %obj.getDatablock().rechargeRate) * %boost;
    %obj.rechargeRateFactor += %obj.energyPackBonus;
    %obj.setRechargeRate(%obj.getDatablock().rechargeRate * %obj.rechargeRateFactor);
}

function ePackOverdrive(%obj)
{
    if(%obj.hasEnergyPack == true)
    {
        %obj.setHeat(1.0);
        schedule(500, %obj, "ePackOverdrive", %obj);
    }
}

function EnergyPackImage::onUnmount(%data, %obj, %node)
{
    if(%obj.turbocharger)
        %obj.damageBuffFactor -= 0.2;
    
    %obj.rechargeRateFactor -= %obj.energyPackBonus;
    
    %obj.setRechargeRate(%obj.getDatablock().rechargeRate * %obj.rechargeRateFactor);
    %obj.hasEnergyPack = "";
}

function EnergyPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
