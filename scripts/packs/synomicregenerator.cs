// ------------------------------------------------------------------
// Synomic Regenerator

datablock AudioProfile(SynomiumIdle)
{
	filename = "fx/misc/nexus_idle.wav";
	description = ClosestLooping3d;
    preload = true;
};

datablock AudioProfile(SynomiumOn)
{
	filename = "fx/weapons/chaingun_start.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock AudioProfile(SynomiumOff)
{
	filename = "fx/weapons/chaingun_off.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock ShapeBaseImageData(SynomicRegeneratorImage)
{
   shapeFile = "ammo_plasma.dts";
   item = SynomicRegenerator;
   mountPoint = 1;
   offset = "0 -0.12 -0.5";

   enhancementSlots = 1;
   
	stateName[0] = "default";
	stateSequence[0] = "activation";
	stateSound[0] = SynomiumIdle;
};

datablock ItemData(SynomicRegenerator)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "SynomicRegeneratorImage";
	pickUpName = "a synomic regenerator";

   computeCRC = true;
};

function SynomicRegeneratorImage::onMount(%data, %obj, %node)
{
    if(isEventPending(%obj.subspaceThread))
        cancel(%obj.subspaceThread);

    %obj.subspacing = true;
    %obj.subspaceTickCount = 0;
    %obj.subspaceThread = %data.schedule(100, "onTick", %obj);
    
    %obj.play3d(SynomiumOn);
}

function SynomicRegeneratorImage::onTick(%data, %obj, %node)
{
    if(%obj.isDead || !%obj.subspacing)
        return;
        
    if(%obj.subspaceTickCount % 4 == 0)
    {
        zapEffect(%obj, "FXZap");

        if(getRandom() >  0.8)
            zapEffect(%obj, "FXPulse");
    }
    
    if(%obj.subspaceTickCount % 20 == 0)
        %obj.incHP(1);
    
    %obj.subspaceTickCount++;
    %obj.subspaceThread = %data.schedule(100, "onTick", %obj);
}

function SynomicRegeneratorImage::onUnmount(%data, %obj, %node)
{
    %obj.subspacing = false;
    %obj.play3d(SynomiumOff);
    
    if(isEventPending(%obj.subspaceThread))
        cancel(%obj.subspaceThread);
}

function SynomicRegenerator::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
