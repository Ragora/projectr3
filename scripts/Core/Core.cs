//==============================================================================
// MD3 Core
setPerfCounterEnable(0); // Set to 1 if only 1 core is allocated to server process
setLogMode(1); // Logging console for development purposes

$Host::CRCTextures = false;               // This is disabled for some important reason (causes some players to randomly disconnect based on their GPU architecture?)
$Pref::useImmersion = "1";                // Used for C++ interface
loadMod("TSExtension");

//==============================================================================
// System object - Runtime environment for all MD3 objects
if(!isObject(System))
{
   new ScriptObject(System)
   {
      class = System;
      Version = "3.0.0";
      subClassCount = 0;
      debugMode = false;
   };
}

function System::deleteClass(%this)
{
   for(%i = 0; %i < System.subClassCount; %i++)
   {
      System.subClass[%i].__destruct();
      eval("System.subClass["@%i@"] = \"\";");
   }
}

function System::addClass(%this, %className)
{
    if(isObject(%className))
        return;

   %class = new ScriptObject(%className)
   {
      class = %className;
//      superClass = System;
   };

   System.subClass[System.subClassCount] = %className;
   System.subClassCount++;
   
   %class.__construct();
}

// External network layer and HTTP Client Support
exec("scripts/Core/Net.cs");

function tsExtensionLoop()
{
    tsExtensionUpdate();
    schedule(32, 0, "tsExtensionLoop");
}

//==============================================================================
// Miscellaneous immediate-override functions relevant to server startup
function GameConnection::dataBlocksDone( %client, %missionSequence )
{
   echo("Client "@%client@" ("@%client.namebase@") finished datablock load.");

   if(%missionSequence != $missionSequence)
      return;

   if(%client.currentPhase != 1)
      return;
      
   %client.currentPhase = 2;

   // only want to set this once... (targets will not be updated/sent until a
   // client has this flag set)
   if(!%client.getReceivedDataBlocks())
   {
      %client.setReceivedDataBlocks(true);
      sendTargetsToClient(%client);
   }

   commandToClient(%client, 'MissionStartPhase2', $missionSequence);
}

function datablockInfo() // datablockInfo
{
   %blocks = DataBlockGroup.getCount();
   echo("Number of Datablocks:");
   echo(%blocks);
   echo("Having more than 1975 datablocks is not recommended. Degraded performance, likelyhood to crash, map incompatibility... ");
   echo("Current Datablock Capacity:");
   echo(mCeil((%blocks / 2048)*100)@"%");

   if(%blocks <= 1975)
      echo("The mod is safe and should be compatible with most maps.");
   else
      echo("The mod is not safe and map compatibility is starting to lack. Remove some datablocks.");

   echo("After 1950 datablocks, you cannot use the reload() command in an online server or clients will drop.");
   echo("Once you hit 100% (2048 datablocks), the server will UE on start.");
}

function displayConnectionInfo(%client)
{
   echo("==================================== Client Info");
   echo("Assigned Client ID: " @ %client @ " IP Address: " @ %client.getAddress());
   echo("Connection Name: "@getTaggedString(%client.name)@" Real Name: "@%client.realname@" GUID: "@%client.guid@"\nSkin: "@getTaggedString(%client.skin)@" Voice: "@getTaggedString(%client.voiceTag)@" Pitch: "@%client.voicePitch);
   echo("Auth Info: \n"@%client.getAuthInfo());
   echo("==========================================================");
}

//==============================================================================
// Server load
function CreateServer(%mission, %missionType)
{
    if($pref::Net::PacketRateToClient < Net.packetRate)
        $pref::Net::PacketRateToClient = Net.packetRate;
        
    if($pref::Net::PacketSize < Net.packetSize)
        $pref::Net::PacketSize = Net.packetSize;
        
    if($pref::Net::PacketRateToServer < Net.packetRate)
        $pref::Net::PacketRateToServer = Net.packetRate;

   DestroyServer();

   // z0dd - ZOD, 3/27/02. Automatically reboot the server after a specified time.
   $AutoRestart = 0; // Paranoia
   if($Host::ClassicAutoRestartServer == 1)
      schedule($Host::ClassicRestartTime * 3600000, 0, "AutoRestart");

//   if($Host::ClassicTelnet)
//      telnetsetparameters($Host::ClassicTelnetPort, $Host::ClassicTelnetPassword, $Host::ClassicTelnetListenPass);

   // Load server data blocks
   exec("scripts/commanderMapIcons.cs");
   exec("scripts/markers.cs");
   exec("scripts/serverAudio.cs");
   exec("scripts/Server/SoundEffects.cs");      // keen
   exec("scripts/damageTypes.cs");
   exec("scripts/deathMessages.cs");
   exec("scripts/inventory.cs");
   exec("scripts/camera.cs");
   exec("scripts/particleEmitter.cs");    // Must exist before item.cs and explosion.cs
   exec("scripts/particleDummies.cs");
   exec("scripts/weather.cs");
   exec("scripts/Server/VisualEffects.cs"); // keen
   exec("scripts/projectiles.cs");        // Must exits before item.cs
   exec("scripts/player.cs");
   exec("scripts/Server/Armor.cs");             // keen
   exec("scripts/gameBase.cs");
   exec("scripts/staticShape.cs");
   exec("scripts/Server/itemUtils.cs");             // keen
   exec("scripts/weapons.cs");
   exec("scripts/turret.cs");
   exec("scripts/weapTurretCode.cs");
   exec("scripts/pack.cs");
   exec("scripts/vehicles/vehicle_spec_fx.cs");    // Must exist before other vehicle files or CRASH BOOM
   exec("scripts/vehicles/serverVehicleHud.cs");
   exec("scripts/vehicles/vehicle_shrike.cs");
   exec("scripts/vehicles/vehicle_bomber.cs");
   exec("scripts/vehicles/vehicle_havoc.cs");
   exec("scripts/vehicles/vehicle_bulldog.cs");
   exec("scripts/vehicles/vehicle_wildcat.cs");
   exec("scripts/vehicles/vehicle_tank.cs");
   exec("scripts/vehicles/vehicle_outlaw.cs");
   exec("scripts/vehicles/vehicle_mpb.cs");
   exec("scripts/vehicles/vehicle.cs");            // Must be added after all other vehicle files or EVIL BAD THINGS
   exec("scripts/Walkers/Walker.cs");            // keen
   exec("scripts/ai.cs");
   exec("scripts/item.cs");
   exec("scripts/station.cs");
   exec("scripts/simGroup.cs");
   exec("scripts/trigger.cs");
   exec("scripts/forceField.cs");
   exec("scripts/lightning.cs");
   exec("scripts/deployables.cs");
   exec("scripts/stationSetInv.cs");
   exec("scripts/navGraph.cs");
   exec("scripts/targetManager.cs");
   exec("scripts/serverCommanderMap.cs");
   exec("scripts/environmentals.cs");
   exec("scripts/power.cs");
   exec("scripts/supportClassic.cs"); // z0dd - ZOD, 5/13/02. Execute the support functions.
   exec("scripts/practice.cs"); // z0dd - ZOD, 3/13/02. Execute practice mode server functions.
   exec("scripts/serverTasks.cs");
   exec("scripts/admin.cs");
   exec("prefs/banlist.cs");

   // Misc MD3 scripts that can't be autoexec'd for some reason?
   exec("scripts/Server/StatusEffect.cs");
   exec("scripts/Server/Aura.cs");
   exec("scripts/vehicles/VehicleParts.cs");

   // ---------------------------------------------------
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   // keen: optimized loop
   %search = "scripts/*Game.cs";
   
   for(%file = findFirstFile(%search); %file !$= ""; %file = findNextFile(%search))
       exec("scripts/" @ fileBase(%file) @ ".cs");

   // ---------------------------------------------------
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // keen: MD3 Core - Server scripts
   exec("scripts/server/GameExtensions.cs");
   execDir("ServerAuto");
   exec("scripts/Enhancements/Enhancements.cs");

   $missionSequence = 0;
   $CurrentMissionType = %missionType;
   $HostGameBotCount = 0;
   $HostGamePlayerCount = 0;
   if ( $HostGameType !$= "SinglePlayer" )
      allowConnections(true);
   $ServerGroup = new SimGroup (ServerGroup);
   if(%mission $= "")
   {
      %mission = $HostMissionFile[$HostMission[0,0]];
      %missionType = $HostTypeName[0];
   }

   if ( ( $HostGameType $= "Online" && $pref::Net::DisplayOnMaster !$= "Never" ) ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
      schedule(0,0,startHeartbeat);

   // setup the bots for this server
   if( $Host::BotsEnabled ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
      initGameBots( %mission, %missionType );

   datablockInfo();

   echo("Server packet rate data:" SPC $pref::Net::PacketRateToClient SPC $pref::Net::PacketRateToServer SPC $pref::Net::PacketSize);
   // z0dd - ZOD, 9/13/02. For TR2 compatability
   // This is a failsafe way of ensuring that default gravity is always restored
   // if a game type (such as TR2) changes it.  It is placed here so that listen
   // servers will work after opening and closing different gametypes.
   $DefaultGravity = getGravity();

   // Login to stats server
   %socket = Net.createConnection();
   %loginPacket = %socket.createHTTPPacket(true, "/login.php");
   %loginPacket.setPOSTField("login", 1);
   %loginPacket.setPOSTField("sid", $Host::ServerID);
   %loginPacket.setPOSTField("uid", $Host::ServerLogin);
   %loginPacket.setPOSTField("pwd", $Host::ServerPassword);
   %loginPacket.setPOSTField("submit", "test"); // required: for some reason, last field has some weird issue with it
   Net.sendPacket(%socket, %loginPacket);

   // load the mission...
   loadMission(%mission, %missionType, true);
}

function initGameBots( %mission, %mType )
{
   echo("Core: creating AI...");

   AISystemEnabled( false );
   if ( $Host::BotCount > 0 && %mType !$= "SinglePlayer" )
   {
      // Make sure this mission is bot enabled:
      for ( %idx = 0; %idx < $HostMissionCount; %idx++ )
      {
         if ( $HostMissionFile[%idx] $= %mission )
            break;
      }

      // keen: removed bot count check here
      if ( $BotEnabled[%idx] )
      {
            $HostGameBotCount = $Host::BotCount;

         if ( $Host::BotCount > $Host::MaxPlayers - 1 )
            $HostGameBotCount = $Host::MaxPlayers - 1;

         //set the objective reassessment timeslice var
         $AITimeSliceReassess = 0;
         aiConnectMultiple( $HostGameBotCount, $Host::MinBotDifficulty, $Host::MaxBotDifficulty, -1 );
      }
      else
      {
         $HostGameBotCount = 0;
      }
   }
}

function DestroyServer()
{
   $missionRunning = false;
   allowConnections(false);
   stopHeartbeat();
   if ( isObject( MissionGroup ) )
      MissionGroup.delete();
   if ( isObject( MissionCleanup ) )
      MissionCleanup.delete();
   if(isObject(game))
   {
      game.deactivatePackages();
      game.delete();
   }
   if(isObject($ServerGroup))
      $ServerGroup.delete();

   // delete all the connections:
   while(ClientGroup.getCount())
   {
      %client = ClientGroup.getObject(0);
      if (%client.isAIControlled())
         %client.drop();
      else
         %client.delete();
   }

   // delete all the data blocks...
   // this will cause problems if there are any connections
   deleteDataBlocks();

   // reset the target manager
   resetTargetManager();

   echo( "exporting server prefs..." );
   export( "$Host::*", "prefs/ServerPrefs.cs", false );
   purgeResources();

   // z0dd - ZOD, 9/13/02. For TR2 compatability.
   // This is a failsafe way of ensuring that default gravity is always restored
   // if a game type (such as TR2) changes it.  It is placed here so that listen
   // servers will work after opening and closing different gametypes.
   if ($DefaultGravity !$= "")
      setGravity($DefaultGravity);
}

//==============================================================================
// Client connections

function GameConnection::onConnect( %client, %name, %raceGender, %skin, %voice, %voicePitch )
{
   %client.setMissionCRC($missionCRC);
   sendLoadInfoToClient( %client );

   //%client.setSimulatedNetParams(0.1, 30);

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // ---------------------------------------------------
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // if hosting this server, set this client to superAdmin
   if(%client.getAddress() $= "Local")
   {
      %client.isAdmin = true;
      %client.isSuperAdmin = true;
   }
   // Get the client's unique id:
   %authInfo = %client.getAuthInfo();
   %client.guid = getField( %authInfo, 3 );

   // check admin and super admin list, and set status accordingly
   if ( !%client.isSuperAdmin )
   {
      if ( isOnSuperAdminList( %client ) )
      {
         %client.isAdmin = true;
         %client.isSuperAdmin = true;
      }
      else if( isOnAdminList( %client ) )
      {
         %client.isAdmin = true;
      }
   }

   // Sex/Race defaults
   switch$ ( %raceGender )
   {
      case "Human Male":
         %client.sex = "Male";
         %client.race = "Human";
      case "Human Female":
         %client.sex = "Female";
         %client.race = "Human";
      case "Bioderm":
         %client.sex = "Male";
         %client.race = "Bioderm";
      default:
         error("Invalid race/gender combo passed: " @ %raceGender);
         %client.sex = "Male";
         %client.race = "Human";
   }
   %client.armor = "Light";

   // Override the connect name if this server does not allow smurfs:
   %realName = getField( %authInfo, 0 );
   if ( $PlayingOnline && $Host::NoSmurfs )
      %name = %realName;

   if ( strcmp( %name, %realName ) == 0 )
   {
      %client.isSmurf = false;

      //make sure the name is unique - that a smurf isn't using this name...
      %dup = -1;
      %count = ClientGroup.getCount();
      for (%i = 0; %i < %count; %i++)
      {
         %test = ClientGroup.getObject( %i );
         if (%test != %client)
         {
	    %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
            if (%realName $= %rawName)
            {
               %dup = %test;
               %dupName = %rawName;
               break;
            }
         }
      }

      //see if we found a duplicate name
      if (isObject(%dup))
      {
         //change the name of the dup
         %isUnique = false;
         %suffixCount = 1;
         while (!%isUnique)
         {
            %found = false;
            %testName = %dupName @ "." @ %suffixCount;
            for (%i = 0; %i < %count; %i++)
            {
               %cl = ClientGroup.getObject(%i);
	       %rawName = stripChars( detag( getTaggedString( %cl.name ) ), "\cp\co\c6\c7\c8\c9" );
               if (%rawName $= %testName)
               {
                  %found = true;
                  break;
               }
            }

            if (%found)
               %suffixCount++;
            else
               %isUnique = true;
         }

         //%testName will now have the new unique name...
         %oldName = %dupName;
         %newName = %testName;

         MessageAll( 'MsgSmurfDupName', '\c2The real \"%1\" has joined the server.', %dupName );
         MessageAll( 'MsgClientNameChanged', '\c2The smurf \"%1\" is now called \"%2\".', %oldName, %newName, %dup );

         %dup.name = addTaggedString(%newName);
         setTargetName(%dup.target, %dup.name);
      }

      // Add the tribal tag:
      %tag = getField( %authInfo, 1 );
      %append = getField( %authInfo, 2 );
      if ( %append )
         %name = "\cp\c6" @ %name @ "\c7" @ %tag @ "\co";
      else
         %name = "\cp\c7" @ %tag @ "\c6" @ %name @ "\co";

      %client.sendGuid = %client.guid;
   }
   else
   {
      %client.isSmurf = true;
      %client.sendGuid = 0;
      %name = stripTrailingSpaces( strToPlayerName( %name ) );
      if ( strlen( %name ) < 3 )
         %name = "Poser";

      // Make sure the alias is unique:
      %isUnique = true;
      %count = ClientGroup.getCount();
      for ( %i = 0; %i < %count; %i++ )
      {
         %test = ClientGroup.getObject( %i );
         %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
         if ( strcmp( %name, %rawName ) == 0 )
         {
            %isUnique = false;
            break;
         }
      }

      // Append a number to make the alias unique:
      if ( !%isUnique )
      {
         %suffix = 1;
         while ( !%isUnique )
         {
            %nameTry = %name @ "." @ %suffix;
            %isUnique = true;

            %count = ClientGroup.getCount();
            for ( %i = 0; %i < %count; %i++ )
            {
               %test = ClientGroup.getObject( %i );
               %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
               if ( strcmp( %nameTry, %rawName ) == 0 )
               {
                  %isUnique = false;
                  break;
               }
            }

            %suffix++;
         }

         // Success!
         %name = %nameTry;
      }

      %smurfName = %name;
      // Tag the name with the "smurf" color:
      %name = "\cp\c8" @ %name @ "\co";
   }

   %client.name = addTaggedString(%name);
   if(%client.isSmurf)
      %client.nameBase = %smurfName;
   else
      %client.nameBase = %realName;

   // Make sure ranking doesn't fail on login
   %client.rankName = %client.nameBase;
   
   // Make sure that the connecting client is not trying to use a bot skin:
   %temp = detag( %skin );
   if ( %temp $= "basebot" || %temp $= "basebbot" )
      %client.skin = addTaggedString( "base" );
   else
      %client.skin = addTaggedString( %skin );

   %client.voice = %voice;
   %client.voiceTag = addtaggedString(%voice);

   //set the voice pitch based on a lookup table from their chosen voice
   %client.voicePitch = getValidVoicePitch(%voice, %voicePitch);
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   // ---------------------------------------------------

   %client.justConnected = true;
   %client.isReady = false;

   // full reset of client target manager
   clientResetTargets(%client, false);

   %client.target = allocClientTarget(%client, %client.name, %client.skin, %client.voiceTag, '_ClientConnection', 0, 0, %client.voicePitch);
   %client.score = 0;
   %client.team = 0;

   $instantGroup = ServerGroup;
   $instantGroup = MissionCleanup;

   echo("CADD: " @ %client @ " " @ %client.getAddress());

   %count = ClientGroup.getCount();
   for(%cl = 0; %cl < %count; %cl++)
   {
      %recipient = ClientGroup.getObject(%cl);
      if((%recipient != %client))
      {
         // These should be "silent" versions of these messages...
         messageClient(%client, 'MsgClientJoin', "",
               %recipient.name,
               %recipient,
               %recipient.target,
               %recipient.isAIControlled(),
               %recipient.isAdmin,
               %recipient.isSuperAdmin,
               %recipient.isSmurf,
               %recipient.sendGuid);

         messageClient(%client, 'MsgClientJoinTeam', "", %recipient.name, $teamName[%recipient.team], %recipient, %recipient.team );
      }
   }

//   commandToClient(%client, 'getManagerID', %client);

   commandToClient(%client, 'setBeaconNames', "Target Beacon", "Marker Beacon", "Bomb Target");

   if ( $CurrentMissionType !$= "SinglePlayer" )
   {
      clearBottomPrint(%client);
      commandToClient(%client, 'CenterPrint', " Meltdown 3 - http://forums.radiantage.net - THIS IS A WORK IN PROGRESS\nPress F2 to configure your account.", 0, 3); // @$Host::MOTD
      
      // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
      messageClient(%client, 'MsgClientJoin', '\c2Welcome to Meltdown 3 %1.',
                    %client.name,
                    %client,
                    %client.target,
                    false,   // isBot
                    %client.isAdmin,
                    %client.isSuperAdmin,
                    %client.isSmurf,
                    %client.sendGuid );
       // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

      messageAllExcept(%client, -1, 'MsgClientJoin', '\c1%1 joined the game.',
                       %client.name,
                       %client,
                       %client.target,
                       false,   // isBot
                       %client.isAdmin,
                       %client.isSuperAdmin,
                       %client.isSmurf,
                       %client.sendGuid );
   }
   else
      messageClient(%client, 'MsgClientJoin', "\c0Mission Insertion complete...",
            %client.name,
            %client,
            %client.target,
            false,   // isBot
            false,   // isAdmin
            false,   // isSuperAdmin
            false,   // isSmurf
            %client.sendGuid );

   //Game.missionStart(%client);
   setDefaultInventory(%client);

   if($missionRunning)
      %client.startMission();
   $HostGamePlayerCount++;
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // z0dd - ZOD 4/29/02. Activate the clients Classic Huds
   // and start off with 0 SAD access attempts.
   %client.SadAttempts = 0;
   messageClient(%client, 'MsgBomberPilotHud', ""); // Activate the bomber pilot hud

   // z0dd - ZOD, 8/10/02. Get player hit sounds etc.
   commandToClient(%client, 'GetClassicModSettings', 1);


   //---------------------------------------------------------
   // z0dd - ZOD, 7/12/02. New AutoPW server function. Sets
   // server join password when server reaches x player count.
   if($Host::ClassicAutoPWEnabled)
   {
      if(($Host::ClassicAutoPWPlayerCount != 0 && $Host::ClassicAutoPWPlayerCount !$= "") && ($HostGamePlayerCount >= $Host::ClassicAutoPWPlayerCount))
         AutoPWServer(1);
   }

   commandToClient(%client, 'RegisterSCClient');
   
   %client.loadAccountData();
   schedule(0, 0, "updatePlayerCounts");
}

function GameConnection::onDrop(%client, %reason)
{
   if(isObject(Game))
      Game.onClientLeaveGame(%client);
      
   %client.saveAccountData();

   // make sure that tagged string of player name is not used
   if ( $CurrentMissionType $= "SinglePlayer" )
      messageAllExcept(%client, -1, 'MsgClientDrop', "", getTaggedString(%client.name), %client);
   else
      messageAllExcept(%client, -1, 'MsgClientDrop', '\c1%1 has left the game.', getTaggedString(%client.name), %client);

   if ( isObject( %client.camera ) )
      %client.camera.delete();

   // z0dd - ZOD, 6/19/02. Strip the hit sound tags
   removeTaggedString(%client.playerHitWav);
   removeTaggedString(%client.vehicleHitWav);

   removeTaggedString(%client.name);
   removeTaggedString(%client.voiceTag);
   removeTaggedString(%client.skin);
   freeClientTarget(%client);

   echo("CDROP: " @ %client @ " " @ %client.getAddress());
   $HostGamePlayerCount--;

   //---------------------------------------------------------
   // z0dd - ZOD, 7/12/02. New AutoPW server function. Sets
   // server join password when server reaches x player count.
   if($Host::ClassicAutoPWEnabled)
   {
      if($HostGamePlayerCount < $Host::ClassicAutoPWPlayerCount)
         AutoPWServer(0);
   }
   // reset the server if everyone has left the game
   //if( $HostGamePlayerCount - $HostGameBotCount == 0 && $Host::Dedicated && !$resettingServer && !$LoadingMission )
   //   schedule(0, 0, "resetServerDefaults");

   // ------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Reset the server if everyone has left the game and set this mission as startup mission.
   // This helps with $Host::ClassicRandomMissions to keep the random more random.
//   if( $HostGamePlayerCount - $HostGameBotCount == 0 && $Host::Dedicated && !$resettingServer && !$LoadingMission )
//   {
//      $Host::Map = $CurrentMission;
//      export("$Host::*", "prefs/ServerPrefs.cs", false);
//      $Host::MissionType = $CurrentMissionType;
//      export("$Host::*", "prefs/ServerPrefs.cs", false);
//      schedule(10, 0, "resetServerDefaults");
//   }
   // ------------------------------------------------------------------------------------------------------------
   schedule(0, 0, "updatePlayerCounts");
}

function serverCmdSCClientRegister(%client, %ver)
{
    if(%ver > 0)
    {
        %client.scClientVer = %ver;
        Enhancement.enumerateClient(%client);
        VehiclePart.enumerateClient(%client);
    }
}

function loadMissionStage1(%missionName, %missionType, %firstMission)
{
   // if a mission group was there, delete prior mission stuff
   if(isObject(MissionGroup))
   {
      // clear out the previous mission paths
      for(%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
      {
         // clear ghosts and paths from all clients
         %cl = ClientGroup.getObject(%clientIndex);
         %cl.resetGhosting();
         %cl.clearPaths();
         %cl.isReady = "";
         %cl.matchStartReady = false;
         %cl.scoreHudMenuState = $MenuState::Default;
         %cl.scoreHudMenu = $Menu::Main; //%cl.defaultMenu;
      }
      Game.endMission();
      $lastMissionTeamCount = Game.numTeams;

      MissionGroup.delete();
      MissionCleanup.delete();
      Game.deactivatePackages();
      Game.delete();
      $ServerGroup.delete();
      $ServerGroup = new SimGroup(ServerGroup);
   }

   $CurrentMission = %missionName;
   $CurrentMissionType = %missionType;

   createInvBanCount();
   echo("LOADING MISSION: " @ %missionName);

   // increment the mission sequence (used for ghost sequencing)
   $missionSequence++;

   // if this isn't the first mission, allow some time for the server
   // to transmit information to the clients:

// jff: $currentMission  already being used for this purpose, used in 'finishLoadMission'
   $MissionName = %missionName;
   $missionRunning = false;

   if(!%firstMission)
      schedule(15000, ServerGroup, loadMissionStage2);
   else
      loadMissionStage2();
}

function loadMissionStage2()
{
   // create the mission group off the ServerGroup
   echo("Mission Load - Stage 2");
   $instantGroup = ServerGroup;

   new SimGroup (MissionCleanup);

   if($CurrentMissionType $= "")
   {
      new ScriptObject(Game) {
         class = DefaultGame;
      };
   }
   else
   {
      new ScriptObject(Game) {
         class = $CurrentMissionType @ "Game";
         superClass = DefaultGame;
      };
   }
   // allow the game to activate any packages.
   Game.activatePackages();

   // reset the target manager
   resetTargetManager();

   %file = "missions/" @ $missionName @ ".mis";
   if(!isFile(%file))
      return;

   // send the mission file crc to the clients (used for mission lighting)
   $missionCRC = getFileCRC(%file);
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %client = ClientGroup.getObject(%i);
      if(!%client.isAIControlled())
         %client.setMissionCRC($missionCRC);
   }

   $countDownStarted = false;
   exec(%file);
   $instantGroup = MissionCleanup;

   // pre-game mission stuff
   if(!isObject(MissionGroup))
   {
      error("No 'MissionGroup' found in mission \"" @ $missionName @ "\".");
      schedule(3000, ServerGroup, CycleMissions);
      return;
   }

   MissionGroup.cleanNonType($CurrentMissionType);

   // construct paths
   pathOnMissionLoadDone();

   $ReadyCount = 0;
   $MatchStarted = false;
   $CountdownStarted = false;
   AISystemEnabled( false );

   // Set the team damage here so that the game type can override it:

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   if ( $Host::TournamentMode )
      $TeamDamage = 1;
   else
      $TeamDamage = $Host::TeamDamageOn;
   // ----------------------------------------

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // z0dd - ZOD, 8/4/02. Gravity change
   if(getGravity() !$= $Classic::gravSetting)
      setGravity($Classic::gravSetting);
   // ---------------------------------------------

   Game.missionLoadDone();

   // Run initialization tasks for Sol Conflict
   System.processNewMission();
   
   // start all the clients in the mission
   $missionRunning = true;
   for(%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
      ClientGroup.getObject(%clientIndex).startMission();

   if(!$MatchStarted && $LaunchMode !$= "NavBuild" && $LaunchMode !$= "SpnBuild" )
   {
      if( $Host::TournamentMode ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         checkTourneyMatchStart();
      else if( $currentMissionType !$= "SinglePlayer" )
         checkMissionStart();
   }

   // offline graph builder...
   if( $LaunchMode $= "NavBuild" )
      buildNavigationGraph( "Nav" );

   if( $LaunchMode $= "SpnBuild" )
      buildNavigationGraph( "Spn" );
      
   purgeResources();
   disableCyclingConnections(false);
   $LoadingMission = false;
}

function updatePlayerCounts()
{
    $HostGamePlayerCount = ClientGroup.getCount();
}

// Fix provided by DarkDragonDX - allows BioDerm meshes to use PDA
function serverCmdSetPDAPose(%client, %val)
{
   if(!isObject(%client.player))
      return;

   // if client is in a vehicle, return
   if(%client.player.isMounted())
      return;

   if(%val)
   {
      // play "PDA" animation thread on player
      if (%client.race $="Bioderm") //PDA pose hackery to allow the Bioderm PDA anim
      %client.player.setActionThread("IdlePDA", true);
      else
      %client.player.setActionThread("PDA", true);
   }
   else
   {
      // cancel PDA animation thread with another one.
     %client.player.setActionThread("light_recoil", false);
   }
}

//==============================================================================
// File loading functions
function execDir(%dir, %ext)
{
    %count = 0;

    if(%ext $= "")
       %ext = "cs";

    %path = "scripts/" @ %dir @ "/*." @ %ext;

    for(%file = findFirstFile(%path); %file !$= ""; %file = findNextFile(%path))
        exec(%file);
}

function execDirBase(%dir, %ext)
{
    if(%ext $= "")
       %ext = "cs";

    %path = %dir @ "/*." @ %ext;

    for(%file = findFirstFile(%path); %file !$= ""; %file = findNextFile(%path))
        exec(%file);
}

function reload(%script)
{
    bottomprintall("<color:00FF00>Server is sending datablocks for "@%script, 10);
    exec(%script);
    
    %count = ClientGroup.getCount();

    for(%i = 0; %i < %count; %i++)
    {
        %cl = ClientGroup.getObject(%i);

        if(!%cl.isAIControlled()) // no sending bots datablocks
            %cl.transmitDataBlocks(0);
    }
}

execDir("CoreAuto");
//tsExtensionLoop();

echo("");
echo("Meltdown 3 Core Version: v"@System.Version);
echo("Meltdown 3 Boot Time: "@formatTimeString("H:nn:ss"));
echo("");
echo("=====================================================================");
echo("=                     Meltdown 3 BASE CODE INIT                     =");
echo("=====================================================================");
echo("");
