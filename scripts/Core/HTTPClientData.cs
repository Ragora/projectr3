function UNIQUESOCKET::onLine(%this, %line)
{
	%line = strReplace(%line, "\t", "*TAB*");
	%this.owner.receiveBuffer = %this.owner.receiveBuffer @ %line @ "\t";
	%this.owner.onLine(%line);

	if (isEventPending(%this.disconnectScheduler))
		cancel(%this.disconnectScheduler);

	%this.disconnectScheduler = %this.schedule($HTTPClient::TimeoutMS, "onDisconnected");
	return true;
}

function UNIQUESOCKET::onDisconnected(%this)
{
	%this.owner.isConnected = false;
	%this.owner.onDisconnected();

	%this.owner.connectedAddress = "";
	%this.owner.connectedPort = 0;

	%this.disconnect();

	%packet = new ScriptObject(){ class = "HTTPResponsePacket"; };
	%packet.setStatusCode(getWord(getField(%this.owner.receiveBuffer, 0), 1));

	%payloadBuffer = "";
	%processingPayload = false;
	for (%i = 1; %i < getFieldCount(%this.owner.receiveBuffer); %i++)
	{
		%currentField = getField(%this.owner.receiveBuffer, %i);
		if (%currentField $= "")
			%processingPayload = true;

		// TODO: Make this more efficient by using getSubStr
		// to retrieve the rest of the payload
		if (!%processingPayload)
		{
			%splitLoc = strStr(%currentField, ":");
			%name = getSubStr(%currentField, 0, %splitLoc);
			%data = getSubStr(%currentField, %splitLoc + 1, strLen(%currentField));
			%packet.setHeader(%name, trim(%data));
		}
		else
			%payloadBuffer = %payloadBuffer @ strReplace(%currentField, "*TAB*", "\t");
	}

	%packet.setPayload(%payloadBuffer);
	%this.owner.onReceivedPacket(%packet);

	return true;
}

function UNIQUESOCKET::onConnected(%this)
{
	%this.owner.isConnected = true;
	%this.owner.onConnected();

	%this.disconnectScheduler = %this.schedule($HTTPClient::TimeoutMS, "onDisconnected");
	return true;
}
