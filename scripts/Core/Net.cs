// Sol Conflict Network Layer

if(!isObject(Net))
{
   System.Net = new ScriptObject(Net)
   {
      class = Net;

      // Stats tracking
      isStatTracking = false;
      login = "login.dividedsol.com";

      // Network prefs
      packetRate = 32;
      packetSize = 600;
   };
   
   Net.connections = new SimSet();
   Net.schedule(60000, "checkConnections");
}

$pref::Net::PacketRateToClient = Net.packetRate;
$pref::Net::PacketSize = Net.packetSize;
$pref::Net::PacketRateToServer = Net.packetRate;

exec("scripts/Core/HTTPClient.cs");

// Garbage collection
function Net::checkConnections(%this)
{
    for(%c = 0; %c < %this.connections.getCount(); %c++)
    {
        %conn = %this.connections.getObject(%c);
        
        if(%conn.marked < 1)
        {
            %conn.marked = 1;
            continue;
        }

        %this.connections.remove(%conn);
        %conn.delete();
    }
    
    %this.schedule(60000, "checkConnections");
    //error("Net::checkConnections tick" SPC getSimTime());
}

function Net::registerObject(%this, %obj)
{
    if(%this.connections.isMember(%obj))
        return;
        
    %this.connections.add(%obj);
}

function Net::get(%this, %addr)
{
    %conn = new ScriptObject() { class = "HTTPClient"; };

    Net.registerObject(%conn);
    %conn.get(%addr);
}

// Generates random socket IDs
function Net::generateSocketID(%this)
{
    %id = "S"@generateRandomString(5);

    if(isObject(%id))
        return %this.generateSocketID();
    else
        return %id;
}

function Net::createConnection(%this, %addr)
{
    %conn = new ScriptObject() { class = "HTTPClient"; };

    %conn.connect(%addr);
    Net.registerObject(%conn);
    
    return %conn;
}

function Net::sendPacket(%this, %conn, %packet)
{
    %conn.sendPacket(%packet);
}

function Net::processPacket(%this, %packet)
{
    %pcmd = getField(%packet.payload, 0);
    Net.registerObject(%packet);
    
    switch$(%pcmd)
    {
        case "OK":
            return;
            
        case "DATA":
            onLoadAccount(%packet.payload);

        case "LOGIN":
            %status = getField(%packet.payload, 1);

            if(%status $= "OK")
            {
                %this.isStatTracking = true;
                error("Logged into Sol Conflict system.");
            }
            else
            {
                %this.isStatTracking = false;
                error("Sol Conflict master rejected login:" SPC %packet.payload);
            }

        case "ERROR":
            %this.isStatTracking = false;
            error("Sol Conflict master server error, stats not being tracked. Please try again in a few moments.");

        case "ISSUE":
            error("Sol Conflict master server issue:" SPC getField(%packet.payload, 1));

        case "STATUS":
            warn(%packet.payload);

        default:
            echo(%packet.payload);
    }
}
