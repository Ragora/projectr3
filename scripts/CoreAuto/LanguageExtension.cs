function stringExplode(%delimiter, %string)
{
 // getword-style magic here, except with a chooseable delimiter
 // or try ruby, perhaps find a way to define a TS-callable function in ruby - t2scri?
}

function stringImplode(%this, %delimiter, %array)
{
}

// base64_encode and decode exist as part of t2scri
// JSON in a separate library

// list functionality
// in background: list creates an array based on input, and array length counter - perhaps tab delimited?
// listLength, forEachInList, getListItem, setListItem, listPush, listPop

function foreachInGroup(%group, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    if(isObject(%group) && %group.getCount() > 1)
    {
        %count = %group.getCount();

        for(%i = 0; %i < %count; %i++)
        {
            %obj = %group.getObject(%i);

            if(isObject(%obj))
                eval(%function@"("@%obj@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");
        }
    }
}

function foreachInArray(%arrayName, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    if(%arrayName !$= "")
    {
        %index = 0;

        while(%arrayName[%index] !$= "")
        {
            %obj = %arrayName[%index];

            if(isObject(%obj))
                eval(%function@"("@%obj@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");

            %index++;
        }
    }
}

function foreachInArea(%pos, %radius, %mask, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    InitContainerRadiusSearch(%pos, %radius, %mask);

    // Add all items found to the set, it has it's own internal counting and shelfkeeping mechanisms
    while((%int = ContainerSearchNext()) != 0)
        eval(%function@"("@%int@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");
}
