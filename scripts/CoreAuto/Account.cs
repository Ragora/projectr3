//-----------------------------------------------------------------------------
// Account Handler
//--------------------------------------

// List of tracked stats
$SCStats[0] = "pointsthismatch";
$SCStats[1] = "kills";
$SCStats[2] = "deaths";
$SCStats[3] = "assists";
$SCStats[4] = "suicides";
$SCStats[5] = "teamkills";
$SCStats[6] = "makills";
$SCStats[7] = "pilotkills";
$SCStats[8] = "vehiclekills";
$SCStats[9] = "vehiclesdestroyed";
$SCStats[10] = "vehiclecrashes";
$SCStats[11] = "gamesplayed";
$SCStats[12] = "gpcollected";
$SCStatsCount = 13;

function GameConnection::loadAccountData(%client)
{
    if(%client.accountLoaded == true)
        return;
        
    $SCEnhCount[%client.guid] = 0;

    for(%i = 0; %i < $SCStatsCount; %i++)
        $SCAccountData[%client.guid, $SCStats[%i]] = 0;
        
    $SCAccountData[%client.guid, "totalpoints"] = 0;

    // Send request for player data
    Net.get("/login.php?playerdata=1&guid="@%client.guid@"&name="@Base64_Encode(%client.nameBase));
    
    // Send request for loadouts
    //Net.get("/login.php?enhloadouts="@%client.guid);
    
    // Send request for vehicles
    //Net.get("/login.php?vehloadouts="@%client.guid);
}

// [Net.cs->Net::processPacket] onLoadAccount()
function onLoadAccount(%acc)
{
    %guid = getField(%acc, 1);
    %datacounts = getField(%acc, 2);
    $SCAccountData[%guid, "totalpoints"] = getField(%acc, 3);
    %numenh = getWord(%datacounts, 0);
    %numvenh = getWord(%datacounts, 1);
    %totaldata = getFieldCount(%acc); // stridelen(3) * numstrides(9) + header(3) = 27 tabs per enh, 3 + tabs len total data - could potentially get this down to pure key->value? 18 fields per vs 27
    %stride = 0;
    %stdata = "";
    %type = 0;
    %value = "";

//    error("Parsing data" SPC %guid SPC %totaldata SPC %numenh SPC %numvenh SPC $SCAccountData[%guid, "totalpoints"]);

    %dirtyitemcount = 0;
    %dirtyguns = "";
    %dirtyvehcount = 0;
    %dirtyvehs = "";

    for(%idx = 4; %idx < %totaldata; %idx++)
    {
        %block = getField(%acc, %idx);
        
//        if(%block $= "END")
//            break;
        
        switch(%stride)
        {
            case 0:
                %type = %block;

            case 1:
                %stdata = %block;

            case 2:
                %value = %block;

            default:
                    error("Unknown data:" SPC %stride SPC %block);
                    %stride = 0;
                continue;
        }

        if(%stride == 2)
        {
//            error("processed block:" SPC %idx SPC %totaldata SPC %type SPC %stdata SPC %value);
            
            switch$(%type)
            {
                case "enhtype":
                    $SCEnhData[%stdata, "type"] = %value;

                case "enhname":
                    $SCEnhName[%stdata] = %value;
                    $SCEnhNameToID[%guid, %value] = %stdata;

                case "enhitem":
                    $SCEnhData[%stdata, "item"] = %value;

//                    echo("dirty bit:" SPC %guid SPC %value SPC $SCEnhDirty[%guid, %value]);
                    if($SCEnhTotal[%guid, %value] $= "" || $SCEnhDirty[%guid, %value] == 1)
                    {
                        $SCEnhTotal[%guid, %value] = 0;
                        $SCEnhDirty[%guid, %value] = 0;
                    }

                    %dirtyguns[%dirtyitemcount] = %value;
                    %dirtyitemcount++;

                    $SCEnhLookup[%guid, %value, $SCEnhTotal[%guid, %value]] = %stdata;
                    $SCEnhTotal[%guid, %value]++;

                case "eslot0":
                    $SCEnhData[%stdata, 0] = %value;

                case "eslot1":
                    $SCEnhData[%stdata, 1] = %value;

                case "eslot2":
                    $SCEnhData[%stdata, 2] = %value;

                case "eslot3":
                    $SCEnhData[%stdata, 3] = %value;

                case "eslot4":
                    $SCEnhData[%stdata, 4] = %value;

                case "eslot5":
                    $SCEnhData[%stdata, 5] = %value;

                case "vehmask":
                    $SCVehData[%stdata, "firemask"] = %value;

                case "vehname":
                    $SCVehName[%stdata] = %value;
                    $SCVehNameToID[%guid, %value] = %stdata;

                case "vehitem":
                    $SCVehData[%stdata, "item"] = %value;

//                    echo("dirty bit:" SPC %guid SPC %value SPC $SCVehDirty[%guid, %value]);
                    if($SCVehTotal[%guid, %value] $= "" || $SCVehDirty[%guid, %value] == 1)
                    {
                        $SCVehTotal[%guid, %value] = 0;
                        $SCVehDirty[%guid, %value] = 0;
                    }

                    %dirtyvehs[%dirtyvehcount] = %value;
                    %dirtyvehcount++;

                    $SCVehLookup[%guid, %value, $SCVehTotal[%guid, %value]] = %stdata;
                    $SCVehTotal[%guid, %value]++;

                case "vslot0":
                    $SCVehData[%stdata, 0] = %value;

                case "vslot1":
                    $SCVehData[%stdata, 1] = %value;

                case "vslot2":
                    $SCVehData[%stdata, 2] = %value;

                case "vslot3":
                    $SCVehData[%stdata, 3] = %value;

                case "vslot4":
                    $SCVehData[%stdata, 4] = %value;

                case "vslot5":
                    $SCVehData[%stdata, 5] = %value;

                case "vslot6":
                    $SCVehData[%stdata, 6] = %value;

                case "vslot7":
                    $SCVehData[%stdata, 7] = %value;

                case "venh0":
                    $SCVehEnhData[%stdata, 0] = %value;

                case "venh1":
                    $SCVehEnhData[%stdata, 1] = %value;

                case "venh2":
                    $SCVehEnhData[%stdata, 2] = %value;

                case "venh3":
                    $SCVehEnhData[%stdata, 3] = %value;

                case "venh4":
                    $SCVehEnhData[%stdata, 4] = %value;

                case "venh5":
                    $SCVehEnhData[%stdata, 5] = %value;

                default:
                    error("Invalid data in stride:" SPC %stride SPC %block);
                    %stride = 0;
                    continue;
            }

            %stride = 0;
            %stdata = "";
            %type = 0;
            %value = "";
        }
        else
            %stride++;
    }

    for(%d = 0; %d < %dirtyitemcount; %d++)
    {
        %g = %dirtyguns[%d];
        $SCEnhDirty[%guid, %g] = true;
    }

    $SCEnhCount[%guid] = %numenh;

    for(%e = 0; %e < %dirtyvehcount; %e++)
    {
        %h = %dirtyvehs[%e];
        $SCVehDirty[%guid, %h] = true;
    }

    $SCVehCount[%guid] = %numvenh;

    %client = GUIDToClientID(%guid);
    calculateRank(%guid, true, %client);
    messageClient(%client, 'MsgLoadedAccounts', '\c3[SYSTEM] Successfully synced account data.');
    
    %client.accountLoaded = true;
}

function System::processGameOver(%this)
{
    // Get score data here before gameOver() is sent

    for(%x = 0; %x < ClientGroup.getCount(); %x++)
    {
        %cl = ClientGroup.getObject(%x);

        if(%cl.isAIControlled())
            continue;
            
        // reset F2 menu on mission end
        %cl.scoreHudMenuState = $MenuState::Default;
        %cl.scoreHudMenu = $Menu::Main;

        $SCAccountData[%cl.guid, "totalpoints"] += %cl.score; // move this to individual scoring mechanisms
        $SCAccountData[%cl.guid, "pointsthismatch"] = %cl.score;
        $SCAccountData[%cl.guid, "gamesplayed"]++;
    }
}

function System::processNewMission(%this)
{
    for(%x = 0; %x < ClientGroup.getCount(); %x++)
    {
        %cl = ClientGroup.getObject(%x);

        if(!%cl.isAIControlled())
            %cl.saveAccountData();
    }
    
    for(%i = 0; %i < 8; %i++)
    {
        $Satellite[%i] = 0;
        $BaseCore[%i] = 0;
        $ResourceCount[%i] = 0;
        $ResourceMax[%i] = 15000;
    }
    
    // keen: Set flight ceiling after mission countdown
    schedule(1000, ServerGroup, "setFlightCeiling", 2000);
}

function updateStat(%guid, %type, %delta)
{
     // Verify we have a GUID and type
     if(%guid $= "" || %type $= "" || %delta $= "")
          return;

     // Initialize any empty stats
     if($SCAccountData[%guid, %type] $= "")
          $SCAccountData[%guid, %type] = 0;

     %val = $SCAccountData[%guid, %type] + %delta;

     // Overflow checks
     if(%val < -2147483648)
          %val = -2147483648;

     if(%val > 2147483647)
        %val = 2147483647;

     // All good here, modify this stat
     $SCAccountData[%guid, %type] = %val;

     // Send this update through the list to see if there's any dependancies
     statTrackUpdate(%guid, %type, %val, %delta);
}

function statTrackUpdate(%guid, %type, %current, %delta)
{
    switch$(%type)
    {
        case "totalpoints":
            calculateRank(%guid, false, "");
            
        default:
            return;
    }
}

$SCRankCount = 0;

$SCRanks[$SCRankCount, "name"] = "Rookie";
$SCRanks[$SCRankCount, "points"] = "0";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Reservist";
$SCRanks[$SCRankCount, "points"] = "100";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Militant";
$SCRanks[$SCRankCount, "points"] = "675";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Private";
$SCRanks[$SCRankCount, "points"] = "2250";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Specialist";
$SCRanks[$SCRankCount, "points"] = "5100";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Lance Corporal";
$SCRanks[$SCRankCount, "points"] = "7400";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Corporal";
$SCRanks[$SCRankCount, "points"] = "9000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Sergeant";
$SCRanks[$SCRankCount, "points"] = "13500";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "2nd Lieutenant";
$SCRanks[$SCRankCount, "points"] = "26000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "1st Lieutenant";
$SCRanks[$SCRankCount, "points"] = "52000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Captain";
$SCRanks[$SCRankCount, "points"] = "110000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Major";
$SCRanks[$SCRankCount, "points"] = "250000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Lieutenant Colonel";
$SCRanks[$SCRankCount, "points"] = "550000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Colonel";
$SCRanks[$SCRankCount, "points"] = "1200000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Lieutenant Commander";
$SCRanks[$SCRankCount, "points"] = "2750000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Commander";
$SCRanks[$SCRankCount, "points"] = "6000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "General";
$SCRanks[$SCRankCount, "points"] = "10000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Warlord";
$SCRanks[$SCRankCount, "points"] = "18000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "High Warlord";
$SCRanks[$SCRankCount, "points"] = "42500000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Overlord";
$SCRanks[$SCRankCount, "points"] = "125000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Dominator";
$SCRanks[$SCRankCount, "points"] = "500000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Master";
$SCRanks[$SCRankCount, "points"] = "1000000000";
$SCRankCount++;

$SCRanks[$SCRankCount, "name"] = "Radiant";
$SCRanks[$SCRankCount, "points"] = "2147483647"; // this should more or less be unobtainable, and moreso for show
$SCRankCount++;

function calculateRank(%guid, %connecting, %client)
{
    if(%client $= "")
        %client = GUIDToClientID(%guid);

    if($SCAccountData[%guid, "totalpoints"] < $SCRanks[1, "points"])
    {
        %client.currentRank = 0;
        
        if(%connecting)
            %client.rankName = addTaggedString($SCRanks[0, "name"] SPC getTaggedString(%client.name));
        
        return;
    }

    %rank = 0;
    
    // suggested by DarkDragonDX
    while(%rank < $SCRankCount && $SCAccountData[%guid, "totalpoints"] >= $SCRanks[%rank, "points"] && $SCAccountData[%guid, "totalpoints"] >= $SCRanks[(%rank + 1), "points"])
        %rank++;
        
//    while(%rank < $SCRankCount)
//    {
//        if($SCRanks[%rank, "points"] <= $SCAccountData[%guid, "totalpoints"])
//            %rank++;
//        else
//            break;
//    }

    if(%connecting)
        %client.rankName = addTaggedString($SCRanks[%rank, "name"] SPC getTaggedString(%client.name));
    else if(%rank != %client.currentRank && %client > 0)
    {
        if(%rank > %client.currentRank)
        {
            messageClient(%client, 'MsgRankup', '\c2Congratulations, your rank is now %1 (%2 points)!', $SCRanks[%rank, "name"], $SCRanks[%rank, "points"]);
            chatMessageAll(%client, '\c4%1 been promoted to a "%2"! (%3 points)', %client.nameBase, $SCRanks[%rank, "name"], $SCRanks[%rank, "points"]);
        }
        else
        {
            messageClient(%client, 'MsgRankdown', '\c2You have been demoted due to bad behavior, your rank is now %1 (%2 points)', $SCRanks[%rank, "name"], $SCRanks[%rank, "points"]);
            chatMessageAll(%client, '\c4%1 been demoted to a "%2" due to bad behavior! (%3 points)', %client.nameBase, $SCRanks[%rank, "name"], $SCRanks[%rank, "points"]);
        }
        
        removeTaggedString(%client.rankName);
        
        %client.rankName = addTaggedString($SCRanks[%rank, "name"] SPC getTaggedString(%client.name));
    }

    // Apply rank bonuses to client here
    
    %client.currentRank = %rank;
}

function GameConnection::saveAccountData(%client)
{
    %socket = Net.createConnection();
    %player = %socket.createHTTPPacket(true, "/login.php");
    %player.setPOSTField("updatestats", 1);
    %player.setPOSTField("sid", $Host::ServerID);
    %player.setPOSTField("guid", %client.guid);

    for(%i = 0; %i < $SCStatsCount; %i++)
        %player.setPOSTField($SCStats[%i], $SCAccountData[%client.guid, $SCStats[%i]]);

    %player.setPOSTField("enh", %client.exportEnhancements());
    %player.setPOSTField("veh", %client.exportVehicles());
    %player.setPOSTField("use", 1);
    
    Net.sendPacket(%socket, %player);
    
    // Clear stats (mission end/player left)
    for(%i = 0; %i < $SCStatsCount; %i++)
        $SCAccountData[%client.guid, $SCStats[%i]] = 0;
        
    //echo("[STATS] Sent account data to Sol Conflict master for" SPC %client.guid SPC %player.payload);
}

function GameConnection::exportEnhancements(%client)
{
    %enharray = JSON_createInstance();
    %enharray.setType($JSONType::Array);
    %total = $SCEnhCount[%client.guid]; // + 1;

    for(%i = 0; %i < %total; %i++)
    {
        %trueid = %client.guid @ %i;
        
        if($SCEnhName[%trueid] $= "")
            $SCEnhName[%trueid] = "New Loadout";
//            continue;

        %enh = JSON_createInstance();
        %enh.setType($JSONType::Object);
        %enh.push($JSONType::Number, "enhid", %i);
        %enh.push($JSONType::String, "name", Base64_Encode($SCEnhName[%trueid]));
        %enh.push($JSONType::String, "item", $SCEnhData[%trueid, "item"]);
        %enh.push($JSONType::String, "type", $SCEnhData[%trueid, "type"]);

        for(%k = 0; %k < 6; %k++)
        {
            if($SCEnhData[%trueid, %k] $= "")
            {
                switch($SCEnhData[%trueid, "type"])
                {
                    case $EnhancementType::Weapon:
                        $SCEnhData[%trueid, %k] = "DefaultWeaponEnh";
                        
                    case $EnhancementType::Armor:
                        $SCEnhData[%trueid, %k] = "DefaultArmorEnh";
                        
                    case $EnhancementType::Pack:
                        $SCEnhData[%trueid, %k] = "DefaultPackEnh";
                        
                    case $EnhancementType::Vehicle:
                        $SCEnhData[%trueid, %k] = "DefaultVehicleEnh";
                }
            }
            
            %enh.push($JSONType::String, "slot"@%k, $SCEnhData[%trueid, %k]);
        }
        
        %enharray.push($JSONType::Object, "", %enh);
    }
    
    return %enharray.render();
}

function GameConnection::exportVehicles(%client)
{
    %varray = JSON_createInstance();
    %varray.setType($JSONType::Array);
    %total = $SCVehCount[%client.guid];

    for(%i = 0; %i < %total; %i++)
    {
        %trueid = %client.guid @ %i;
        
        if($SCVehName[%trueid] $= "")
            $SCVehName[%trueid] = "New Loadout";
        
        %Veh = JSON_createInstance();
        %Veh.setType($JSONType::Object);
        %Veh.push($JSONType::Number, "vehid", %i);
        %Veh.push($JSONType::String, "name", Base64_Encode($SCVehName[%trueid]));
        %Veh.push($JSONType::String, "vdata", $SCVehData[%trueid, "item"]);
        %Veh.push($JSONType::String, "mask", $SCVehData[%trueid, "firemask"]);

        for(%k = 0; %k < 8; %k++)
        {
            if($SCVehData[%trueid, %k] $= "")
                $SCVehData[%trueid, %k] = "XEmptyHardpoint";

            if($SCVehEnhData[%trueid, %k] $= "")
                $SCVehEnhData[%trueid, %k] = "WalkerArmorPlate";

            %Veh.push($JSONType::String, "slot"@%k, $SCVehData[%trueid, %k]);

            if(%k < 6)
                %Veh.push($JSONType::String, "enh"@%k, $SCVehEnhData[%trueid, %k]);
        }

        %varray.push($JSONType::Object, "", %Veh);
    }

    return %varray.render();
}

function GameConnection::generateEnhID(%client)
{
    if($SCEnhCount[%client.guid] $= "")
        $SCEnhCount[%client.guid] = 0;
        
    $SCEnhID[%client.guid, $SCEnhCount[%client.guid]] = $SCEnhCount[%client.guid];
    $SCEnhCount[%client.guid]++;
    
    return $SCEnhCount[%client.guid];
}

function GameConnection::loadEnhancementID(%client, %enhID)
{
    %trueid = %client.guid @ %enhID;
    
    if($SCEnhData[%trueid, "item"] $= "")
    {
        error(%trueid SPC "NOT FOUND");
        return;
    }
    
    %type = $SCEnhData[%trueid, "type"];
    %item = $SCEnhData[%trueid, "item"];
    
    for(%i = 0; %i < 6; %i++)
        %client.pendingEnhancement[%type, %item, %i] = $SCEnhData[%trueid, %i];
}

function GameConnection::setLoadoutName(%client, %enhID, %name)
{
    %trueid = %client.guid @ %enhID;
    
    if($SCEnhData[%enhID, "item"] $= "")
    {
        messageClient(%client, 'MsgEnhNotFound', '\c2Enhancement ID not found.');
        return;
    }
    
    $SCEnhName[%enhID] = %name;
    $SCEnhNameToID[%client.guid, %name] = %enhID;
}

function GameConnection::setVLoadoutName(%client, %vehID, %name)
{
    %trueid = %client.guid @ %vehID;

    if($SCVehData[%vehID, "item"] $= "")
    {
        messageClient(%client, 'MsgVehNotFound', '\c2Vehicle ID not found.');
        return;
    }

    $SCVehName[%vehID] = %name;
    $SCVehNameToID[%client.guid, %name] = %vehID;
    %client.pendingVehicleName[$VehicleListID[$SCVehData[%vehID, "item"]]] = %name;
}

function GameConnection::getEnhName(%client, %enhID)
{
    return $SCEnhName[%client.guid @ %enhID];
}

function GameConnection::getEnhNameToID(%client, %name)
{
    return $SCEnhNameToID[%client.guid, %name];
}

// Legacy functions here, for using when constructing new account functions
function loadClientAccountData(%client)
{
     %guid = %client.guid;

     // Impress defaults first...
     $MDAccountData[%guid, "spawnLoadout"] = 0;
     $MDAccountData[%guid, "spawnPack"] = "RepairPack";
     $MDAccountData[%guid, "mitziAutoMode"] = false;
     $MDAccountData[%guid, "modekeys"] = false;
     $MDAccountData[%guid, "allowVehicleHud"] = true ;	// false; -soph
     $MDAccountData[%guid, "playEnemyHitSound"] = true;
     $MDAccountData[%guid, "showRank"] = true;
     $MDAccountData[%guid, "defaultScoreMenu"] = 0;
     $MDAccountData[%guid, "defaultLoadScreen"] = 0;
     $MDAccountData[%guid, "grenadeTimer"] = 2;
     $MDAccountData[%guid, "voice"] = %client.voice;
     $MDAccountData[%guid, "voicePitch"] = %client.voicePitch;

    // MECH LOADING CODE PT1
     $MDMechData[ %guid , "mechSlot" ] = 0 ;		// +[soph]
     $MDMechData[ %guid , "mechWeight" ] = 100 ;	// + these are the only two that need defaults
							// +[/soph]
     // Add to lookup table for faster access times
     %nameBase = %client.nameBase;
     addToGUIDLookup(%guid, %nameBase);

     // Then if they have an account, overwrite with their prefs
     if(!readAccountData(%guid))
          echo("[AccountManager] Loading default pref template for GUID" SPC %guid);

     // Remove voice tag before adding a new one - possible memory leak/crash fix
     removeTaggedString(%client.voiceTag);

     // This way, if any new account settings come into play, they are immediately up to date
     %client.spawnLoadout = $MDAccountData[%guid, "spawnLoadout"];
     %client.spawnPack = $MDAccountData[%guid, "spawnPack"];
     %client.mitziAutoMode = $MDAccountData[%guid, "mitziAutoMode"];
     %client.modekeys = $MDAccountData[%guid, "modekeys"];
     %client.playEnemyHitSound = $MDAccountData[%guid, "playEnemyHitSound"];
     %client.showRank = $MDAccountData[%guid, "showRank"];
     %client.defaultMenu = $MDAccountData[%guid, "defaultScoreMenu"];
     %client.defaultLoadScreen = $MDAccountData[%guid, "defaultLoadScreen"];
     %client.grenadeTimer = $MDAccountData[%guid, "grenadeTimer"];
     %client.voice = $MDAccountData[%guid, "voice"];
     %client.voiceTag = addtaggedString($MDAccountData[%guid, "voice"]);
     %client.voicePitch = $MDAccountData[%guid, "voicePitch"];
//     setTargetVoice(%client.target, %client.voiceTag);

    // MECH LOADING CODE PT2				// +[soph]
     %client.currentMechSlot = $MDMechData[ %guid , "mechSlot" ] ;
     %client.mechWeight = $MDMechData[ %guid , "mechWeight" ] ;
     for( %j = 0 ; %j < $MechCount ; %j++ )		// + iterate through mech slots
     {							// +
          %client.modArmorVal[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "armor" ] ;
          %client.modHeatsinkVal[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "heat" ] ;
          %numWeapons = $MechMaxWeapons[ %j ] ;		// +
          for( %i = 2 ; %i < 2 + %numWeapons ; %i++ )	// + iterate through hardpoints
          {						// +
               %client.mechWeaponSlot[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlot" @ %i ] ;
               %idxNum = %i - 2 ;			// +
               %client.mechWeaponSlotID[ %j , %idxNum ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlotID" @ %idxNum ] ;
               %client.mechSlotFireGroup[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlotGroup" @ %i ] ;
          }						// +
         // DEPRECIATED
//          %client.mModule[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "module" ] ;
//          %client.mEquip[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "equip" ] ;
//          %client.mechOverweight[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "overweight" ] ;
         // NEW EQUIP SYS
          for( %i = 0 ; %i < $MechEquipCount ; %i++ )
               %client.mEquip[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "equip" @ %i ] ;
     }							// +
							// +
    // VEHICLE LOADING CODE				// +
     for( %j = 0 ; %j < $VehicleCount ; %j++ )		// + iterate through vehicles
     {							// +
          %data = $VehicleData[ %j ] ;			// +
          %client.vModPriWeapon[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "primary" ] ;
          %client.vModSecWeapon[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "secondary" ] ;
          %client.vModule[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "module" ] ;
          %client.vModShield[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "shield" ] ;
          %client.vModArmor[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "armor" ] ;
     }							// +[/soph]
}

function saveClientAccountData(%client)
{
     %guid = %client.guid;

     $MDAccountData[%guid, "spawnLoadout"] = %client.spawnLoadout;
     $MDAccountData[%guid, "spawnPack"] = %client.spawnPack;
     $MDAccountData[%guid, "mitziAutoMode"] = %client.mitziAutoMode;
     $MDAccountData[%guid, "modekeys"] = %client.modekeys;
     $MDAccountData[%guid, "playEnemyHitSound"] = %client.playEnemyHitSound;
     $MDAccountData[%guid, "showRank"] = %client.showRank;
     $MDAccountData[%guid, "defaultScoreMenu"] = %client.defaultMenu;
     $MDAccountData[%guid, "defaultLoadScreen"] = %client.defaultLoadScreen;
     $MDAccountData[%guid, "grenadeTimer"] = %client.grenadeTimer;
     $MDAccountData[%guid, "lastName"] = %client.nameBase;
     $MDAccountData[%guid, "voice"] = %client.voice;
     $MDAccountData[%guid, "voicePitch"] = %client.voicePitch;

    // MECH SAVING CODE 			// +[soph]
     $MDMechData[ %guid , "mechSlot" ] = %client.currentMechSlot ;
     $MDMechData[ %guid , "mechWeight" ] = %client.mechWeight ;
     for( %j = 0 ; %j < $MechCount ; %j++ )	// three slots
     {
          $MDMechData[ %guid , "mech" @ %j @ "armor" ] = %client.modArmorVal[ %j ] ;
          $MDMechData[ %guid , "mech" @ %j @ "heat" ] = %client.modHeatsinkVal[ %j ] ;
          %numWeapons = $MechMaxWeapons[ %j ] ;
          for( %i = 2 ; %i < 2 + %numWeapons ; %i++ )
          {
               $MDMechData[ %guid , "mech"@%j@"weaponSlot" @ %i ] = %client.mechWeaponSlot[ %j , %i ] ;
               %idxNum = %i - 2 ;
               $MDMechData[ %guid , "mech" @ %j @ "weaponSlotID" @ %idxNum ] = %client.mechWeaponSlotID[ %j , %idxNum ] ;
               $MDMechData[ %guid , "mech" @ %j @ "weaponSlotGroup" @ %i ] = %client.mechSlotFireGroup[ %j , %i ] ;
          }
         // DEPRECIATED
//          $MDMechData[ %guid , "mech" @ %j @ "module" ] = %client.mModule[ %j ] ;
//          $MDMechData[ %guid , "mech" @ %j @ "equip" ] = %client.mEquip[ %j ] ;
//          $MDMechData[ %guid , "mech" @ %j @ "overweight" ] = %client.mechOverweight[ %j ] ;
         // NEW EQUIP SYS
          for( %i = 0 ; %i < $MechEquipCount ; %i++ )
               $MDMechData[ %guid , "mech" @ %j @ "equip" @ %i ] = %client.mEquip[ %j , %i ] ;
     }
    // VEHICLE SAVING CODE
     for( %j = 0 ; %j < $VehicleCount ; %j++ )
     {
          %data = $VehicleData[ %j ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "primary" ] = %client.vModPriWeapon[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "secondary" ] = %client.vModSecWeapon[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "module" ] = %client.vModule[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "shield" ] = %client.vModShield[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "armor" ] = %client.vModArmor[ %data ] ;
     }						// +[/soph]

     writeAccountData(%guid);
}

function addStatTrack(%guid, %type, %delta)
{
     // Verify we have a GUID and type
     if(%guid $= "" || %type $= "" || %delta $= "")
          return;

     // Initialize any empty stats
     if($MDStat[%guid, %type] $= "")
          $MDStat[%guid, %type] = 0;

     // This GUID has a ban on this particular stat
     if($MDStat[%guid, %type] == -1)
          return;

     %val = $MDStat[%guid, %type] + %delta;

     // Overflow check
     if(%val < -1)
          %val = 1 << 31;

     // All good here, modify this stat
     $MDStat[%guid, %type] = %val;

     // Send this update through the list to see if there's any dependancies
     statTrackUpdate(%guid, %type, %val, %delta);
}

function getTrackedStat(%guid, %type)
{
     // Verify we have a GUID and type
     if(%guid $= "" || %type $= "")
          return;

     %stat = $MDStat[%guid, %type];

     // Make sure the stat isn't blank
     if(%stat $= "")
          %stat = 0;

     // Return it!
     return %stat;
}

function exportMD2Stats()
{
//     echo(formatTimeString("H:nn:ss") SPC "Saving Meltdown 2 user database...");

     %count = ClientGroup.getCount();

     for(%i = 0; %i < %count; %i++)
     {
          %client = ClientGroup.getObject(%i);

          if(!%client.isAIControlled())
               saveClientAccountData(%client);
     }

     writeAccountData(0);

     export("$MDGUIDList*", "mdb/serverGUIDList.dat", false);
     export("$MDGUIDLookup*", "mdb/serverGUIDLookup.dat", false);
     export("$AutoBanList*", "autobanlist.cs", false);
}

function addToGUIDLookup(%guid, %name)
{
     for(%index = 0; %index < $MDGUIDListCount; %index++)
          if($MDGUIDList[%index] == %guid)
               return;

     $MDGUIDList[%index] = %guid;
     $MDGUIDLookup[%guid] = %name;
     $MDGUIDListCount++;
}

function findOfflineAccount(%name)
{
     for(%index = 0; %index < $MDGUIDListCount; %index++)
     {
          %guid = $MDGUIDList[%index];

          if($MDGUIDLookup[%guid] $= %name)
               return %guid;
     }

     for(%index = 0; %index < $MDGUIDListCount; %index++)
     {
          %guid = $MDGUIDList[%index];

          if(strstr(strlwr($MDGUIDLookup[%guid]), strlwr(%name)) != -1)
               return %guid;
     }

     return 0;
}

function readAccountData(%guid)
{
     if(!%guid)
     {
          exec("mdb/serverStats.dat");
          exec("mdb/serverGUIDList.dat");
          exec("mdb/serverGUIDLookup.dat");
          exec("autobanlist.cs");
          echo("Loaded server-wide statistic data.");
     }
     else
     {
          %found = isFile("mdb/"@%guid@"/stats.dat");

          if(%found)
          {
               exec("mdb/"@%guid@"/stats.dat");
               exec("mdb/"@%guid@"/account.dat");
               exec( "mdb/"@%guid@"/mech.dat" ) ;	// +soph
               exec( "mdb/"@%guid@"/vehicle.dat" ) ;	// +soph

               echo("[AccountManager] Loaded account data for:" SPC %guid SPC $MDAccountData[%guid, "account"]);

               $MDAccountData[%guid, "account"] = %client.realName;
               $MDAccountData[%guid, "lastLogin"] = formatTimeString("MM d H:nn:ss");

               return true;
          }
          else
          {
               $MDAccountData[%guid, "account"] = %client.realName;
               $MDAccountData[%guid, "lastLogin"] = formatTimeString("MM d H:nn:ss");

               echo("[AccountManager] Data not found for:" SPC %guid SPC "- using blank profile.");

               return false;
          }
     }
}

function writeAccountData(%guid)
{
     if(!%guid)
          export("$MDStat"@%guid@"*", "mdb/serverStats.dat", false);
     else
     {
          export("$MDStat"@%guid@"*", "mdb/"@%guid@"/stats.dat", false);
          export("$MDAccountData"@%guid@"*", "mdb/"@%guid@"/account.dat", false);
          export( "$MDMechData" @ %guid @ "*" , "mdb/" @ %guid @ "/mech.dat" , false ) ;	// +soph
          export( "$MDVehicleData" @ %guid @ "*" , "mdb/" @ %guid @ "/vehicle.dat" , false ) ;	// +soph
     }
}
