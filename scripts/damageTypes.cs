//--------------------------------------------------------------------------
// TYPES OF ALLOWED DAMAGE
//--------------------------------------------------------------------------

$DamageType::Default    		= 0;
$DamageType::Blaster			= 1;
$DamageType::Plasma     		= 2;
$DamageType::Bullet     		= 3;
$DamageType::Disc				= 4;
$DamageType::Grenade			= 5;
$DamageType::Laser				= 6;     // NOTE: This value is referenced directly in code.  DO NOT CHANGE!
$DamageType::ELF				= 7;
$DamageType::Mortar				= 8;
$DamageType::Missile			= 9;
$DamageType::ShockLance			= 10;
$DamageType::Mine				= 11;
$DamageType::Explosion			= 12;
$DamageType::Impact				= 13;	// Object to object collisions
$DamageType::Ground				= 14;	// Object to ground collisions
$DamageType::Turret				= 15;

$DamageType::PlasmaTurret 		= 16;
$DamageType::AATurret	 		= 17;
$DamageType::ElfTurret 			= 18;
$DamageType::MortarTurret 		= 19;
$DamageType::MissileTurret 		= 20;
$DamageType::IndoorDepTurret	= 21;
$DamageType::OutdoorDepTurret	= 22;
$DamageType::SentryTurret		= 23;

$DamageType::OutOfBounds		= 24;
$DamageType::Lava				= 25;

$DamageType::ShrikeBlaster		= 26;
$DamageType::BellyTurret		= 27;
$DamageType::BomberBombs		= 28;
$DamageType::TankChaingun		= 29;
$DamageType::TankMortar			= 30;
$DamageType::SatchelCharge		= 31;
$DamageType::MPBMissile			= 32;
$DamageType::Lightning        = 33;
$DamageType::VehicleSpawn     = 34;
$DamageType::ForceFieldPowerup = 35;
$DamageType::Crash            = 36;

// MD3 Damage Types
$DamageType::MitziBlast         = 37;
$DamageType::PBW                = 38;
$DamageType::Sniper             = 39;
$DamageType::Annihalator        = 40;
$DamageType::EMP                = 41;
$DamageType::Burn               = 42;
$DamageType::Poison             = 43;
$DamageType::BlueBlaster        = 44;
$DamageType::GreenBlaster       = 45;
$DamageType::Flak               = 46;
$DamageType::AutoCannon         = 47;
$DamageType::Flamethrower       = 48;
$DamageType::StarHammer         = 49;
$DamageType::MeteorCannon       = 50;
$DamageType::MagIonReactor      = 51;
$DamageType::Turbocharger       = 52;
$DamageType::BulletHE     		= 53;
$DamageType::BulletEM     		= 54;
$DamageType::Railgun     		= 55;
$DamageType::SubspaceMagnet     = 56;
$DamageType::NailMissile        = 57;
$DamageType::BulletIN           = 58;
$DamageType::StingerRail        = 59;
$DamageType::MitziRail          = 60;
$DamageType::Bolter             = 60;
$DamageType::PlasmaCannon       = 61;
$DamageType::MPDisruptor        = 62;
$DamageType::BlasterRifle       = 63;
$DamageType::PBCDevistator      = 64;
$DamageType::PBCQuantum         = 65;
$DamageType::VectorGlitch       = 66;
$DamageType::ShieldPulse        = 67;
$DamageType::SonicPulser        = 68;
$DamageType::RotaryCannon       = 69;
$DamageType::WalkerReactor      = 70;

// DMM -- added so MPBs that blow up under water get a message
$DamageType::Water                      = 97;

//Tinman - used in Hunters for cheap bastards  ;)
$DamageType::NexusCamping		= 98;

// MES -- added so CTRL-K can get a distinctive message
$DamageType::Suicide			= 99;
$DamageType::LifeSupportSuicide	= 100;

$DamageGroupMask::Misc             = 1 << 0;
$DamageGroupMask::Energy           = 1 << 1;
$DamageGroupMask::Explosive        = 1 << 2;
$DamageGroupMask::Kinetic          = 1 << 3;
$DamageGroupMask::Plasma           = 1 << 4;
$DamageGroupMask::Mitzi            = 1 << 5;
$DamageGroupMask::Poison           = 1 << 6;
$DamageGroupMask::Psionic          = 1 << 7;
$DamageGroupMask::Sonic            = 1 << 8;
$DamageGroupMask::ExternalMeans    = 1 << 9;

function getDamageGroup(%dmgType)
{
     %type = $DamageGroup[%dmgType];

     if(%type & $DamageGroupMask::Energy)
          return $DamageGroupMask::Energy;
     else if(%type & $DamageGroupMask::Explosive)
          return $DamageGroupMask::Explosive;
     else if(%type & $DamageGroupMask::Kinetic)
          return $DamageGroupMask::Kinetic;
     else if(%type & $DamageGroupMask::Plasma)
          return $DamageGroupMask::Plasma;
     else if(%type & $DamageGroupMask::Mitzi)
          return $DamageGroupMask::Mitzi;
     else if(%type & $DamageGroupMask::Poison)
          return $DamageGroupMask::Poison;
     else if(%type & $DamageGroupMask::Psionic)
          return $DamageGroupMask::Psionic;
     else if(%type & $DamageGroupMask::Sonic)
          return $DamageGroupMask::Sonic;
     else
          return $DamageGroupMask::Misc;
}

// Damage Type Mask definitions
$DamageGroup[$DamageType::Default] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Blaster] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::Plasma] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::Bullet] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::Disc] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Grenade] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Laser] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::ELF] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::Mortar] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Missile] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::ShockLance] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::Mine] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Explosion] = $DamageGroupMask::Explosive | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Impact] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc;
$DamageGroup[$DamageType::Ground] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Turret] = $DamageGroupMask::Misc;
$DamageGroup[$DamageType::PlasmaTurret] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::AATurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::ElfTurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::MortarTurret] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::MissileTurret] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::IndoorDepTurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::OutdoorDepTurret] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::SentryTurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::OutOfBounds] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Lava] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::ShrikeBlaster] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::BellyTurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::BomberBombs] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::TankChaingun] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::TankMortar] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::SatchelCharge] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::MPBMissile] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Lightning] = $DamageGroupMask::Energy | $DamageGroupMask::Misc;
$DamageGroup[$DamageType::VehicleSpawn] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::ForceFieldPowerup] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Crash] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Water] = $DamageGroupMask::Misc;
$DamageGroup[$DamageType::NexusCamping] = $DamageGroupMask::Misc| $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Suicide] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::LifeSupportSuicide] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;

// MD3 Damage Types
$DamageGroup[$DamageType::BlueBlaster] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::GreenBlaster] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::MitziBlast] = $DamageGroupMask::Mitzi;
$DamageGroup[$DamageType::PBW] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::Sniper] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::Railgun] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::Annihalator] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::EMP] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::Burn] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::Poison] = $DamageGroupMask::Poison;
$DamageGroup[$DamageType::Flak] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::AutoCannon] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Flamethrower] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::StarHammer] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::MeteorCannon] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::MagIonReactor] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Turbocharger] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::BulletHE] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::BulletEM] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::OutdoorDepTurretF] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::SubspaceMagnet] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::NailMissile] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::BulletIN] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::StingerRail] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::MitziRail] = $DamageGroupMask::Mitzi;
$DamageGroup[$DamageType::Bolter] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::PlasmaCannon] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::MPDisruptor] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::BlasterRifle] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::PBCDevistator] = $DamageGroupMask::Mitzi;
$DamageGroup[$DamageType::PBCQuantum] = $DamageGroupMask::Sonic;
$DamageGroup[$DamageType::VectorGlitch] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::SonicPulser] = $DamageGroupMask::Sonic;
$DamageGroup[$DamageType::RotaryCannon] = $DamageGroupMask::Kinetic;

// Etc, etc.

$DamageTypeText[0] = 'default';
$DamageTypeText[1] = 'blaster';
$DamageTypeText[2] = 'plasma';
$DamageTypeText[3] = 'chaingun';
$DamageTypeText[4] = 'disc';
$DamageTypeText[5] = 'grenade';
$DamageTypeText[6] = 'laser';
$DamageTypeText[7] = 'ELF';
$DamageTypeText[8] = 'mortar';
$DamageTypeText[9] = 'missile';
$DamageTypeText[10] = 'shocklance';
$DamageTypeText[11] = 'mine';
$DamageTypeText[12] = 'explosion';
$DamageTypeText[13] = 'impact';
$DamageTypeText[14] = 'ground';
$DamageTypeText[15] = 'turret';
$DamageTypeText[16] = 'plasma turret';
$DamageTypeText[17] = 'AA turret';
$DamageTypeText[18] = 'ELF turret';
$DamageTypeText[19] = 'mortar turret';
$DamageTypeText[20] = 'missile turret';
$DamageTypeText[21] = 'clamp turret';
$DamageTypeText[22] = 'spike turret';
$DamageTypeText[23] = 'sentry turret';
$DamageTypeText[24] = 'out of bounds';
$DamageTypeText[25] = 'lava';
$DamageTypeText[26] = 'shrike blaster';
$DamageTypeText[27] = 'belly turret';
$DamageTypeText[28] = 'bomber bomb';
$DamageTypeText[29] = 'tank chaingun';
$DamageTypeText[30] = 'tank mortar';
$DamageTypeText[31] = 'satchel charge';
$DamageTypeText[32] = 'MPB missile';
$DamageTypeText[33] = 'lighting';
$DamageTypeText[35] = 'ForceField';
$DamageTypeText[36] = 'Crash';
$DamageTypeText[98] = 'nexus camping';
$DamageTypeText[99] = 'suicide';
$DamageTypeText[100] = 'life support failure';

// Default Damage Profiles
datablock SimDataBlock(UniversalDamageProfile)
{
   shieldDamageScale[$DamageType::Blaster] 			= 1.0;
   shieldDamageScale[$DamageType::Bullet] 			= 1.0;
   shieldDamageScale[$DamageType::ELF] 				= 1.0;
   shieldDamageScale[$DamageType::ShockLance] 		= 1.0;
   shieldDamageScale[$DamageType::Laser] 			= 1.0;
   shieldDamageScale[$DamageType::ShrikeBlaster] 	= 1.0;
   shieldDamageScale[$DamageType::BellyTurret] 		= 1.0;
   shieldDamageScale[$DamageType::AATurret] 		= 1.0;
   shieldDamageScale[$DamageType::IndoorDepTurret] 	= 1.0;
   shieldDamageScale[$DamageType::OutdoorDepTurret] = 1.0;
   shieldDamageScale[$DamageType::SentryTurret] 	= 1.0;
   shieldDamageScale[$DamageType::Disc] 			= 1.0;
   shieldDamageScale[$DamageType::Grenade] 			= 1.0;
   shieldDamageScale[$DamageType::Mine] 			= 1.0;
   shieldDamageScale[$DamageType::Missile] 			= 1.0;
   shieldDamageScale[$DamageType::Mortar] 			= 1.0;
   shieldDamageScale[$DamageType::Plasma] 			= 1.0;
   shieldDamageScale[$DamageType::BomberBombs] 		= 1.0;
   shieldDamageScale[$DamageType::TankChaingun] 	= 1.0;
   shieldDamageScale[$DamageType::TankMortar] 		= 1.0;
   shieldDamageScale[$DamageType::MissileTurret] 	= 1.0;
   shieldDamageScale[$DamageType::MortarTurret] 	= 1.0;
   shieldDamageScale[$DamageType::PlasmaTurret] 	= 1.0;
   shieldDamageScale[$DamageType::SatchelCharge] 	= 1.0;
   shieldDamageScale[$DamageType::Default] 			= 1.0;
   shieldDamageScale[$DamageType::Impact] 			= 1.0;
   shieldDamageScale[$DamageType::Ground] 			= 1.0;
   shieldDamageScale[$DamageType::Explosion] 		= 1.0;
   shieldDamageScale[$DamageType::Lightning] 		= 10.0;

   damageScale[$DamageType::Blaster] 				= 1.0;
   damageScale[$DamageType::Bullet] 				= 1.0;
   damageScale[$DamageType::ELF] 					= 1.0;
   damageScale[$DamageType::ShockLance] 			= 1.0;
   damageScale[$DamageType::Laser] 					= 1.0;
   damageScale[$DamageType::ShrikeBlaster] 			= 1.0;
   damageScale[$DamageType::BellyTurret] 			= 1.0;
   damageScale[$DamageType::AATurret] 				= 1.0;
   damageScale[$DamageType::IndoorDepTurret] 		= 1.0;
   damageScale[$DamageType::OutdoorDepTurret] 		= 1.0;
   damageScale[$DamageType::SentryTurret] 			= 1.0;
   damageScale[$DamageType::Disc] 					= 1.0;
   damageScale[$DamageType::Grenade] 				= 1.0;
   damageScale[$DamageType::Mine] 					= 1.0;
   damageScale[$DamageType::Missile] 				= 1.0;
   damageScale[$DamageType::Mortar] 				= 1.0;
   damageScale[$DamageType::Plasma] 				= 1.0;
   damageScale[$DamageType::BomberBombs] 			= 1.0;
   damageScale[$DamageType::TankChaingun] 			= 1.0;
   damageScale[$DamageType::TankMortar] 			= 1.0;
   damageScale[$DamageType::MissileTurret] 			= 1.0;
   damageScale[$DamageType::MortarTurret] 			= 1.0;
   damageScale[$DamageType::PlasmaTurret] 			= 1.0;
   damageScale[$DamageType::SatchelCharge] 			= 1.0;
   damageScale[$DamageType::Default] 				= 1.0;
   damageScale[$DamageType::Impact] 				= 1.0;
   damageScale[$DamageType::Ground] 				= 1.0;
   damageScale[$DamageType::Explosion] 				= 1.0;
   damageScale[$DamageType::Lightning] 				= 10.0;
   
   damageScale[$DamageType::BlueBlaster] 			= 0.0;
   shieldDamageScale[$DamageType::BlueBlaster] 		= 2.0;
   damageScale[$DamageType::GreenBlaster] 			= 2.0;
   shieldDamageScale[$DamageType::GreenBlaster]		= 0.0;
   damageScale[$DamageType::MitziBlast] 			= 1.0;
   shieldDamageScale[$DamageType::MitziBlast]		= 1.0;
   damageScale[$DamageType::PBW] 			        = 1.0;
   shieldDamageScale[$DamageType::PBW]		        = 1.0;
   damageScale[$DamageType::Sniper] 			    = 1.0;
   shieldDamageScale[$DamageType::Sniper]		    = 1.0;
   damageScale[$DamageType::Annihalator] 			= 1.0;
   shieldDamageScale[$DamageType::Annihalator]		= 1.0;
   damageScale[$DamageType::EMP] 			        = 1.0;
   shieldDamageScale[$DamageType::EMP]		        = 1.0;
   damageScale[$DamageType::Burn] 			        = 1.0;
   shieldDamageScale[$DamageType::Burn]		        = 1.0;
   damageScale[$DamageType::Poison] 			    = 1.0;
   shieldDamageScale[$DamageType::Poison]		    = 1.0;
   damageScale[$DamageType::Flak] 			        = 1.0;
   shieldDamageScale[$DamageType::Flak]		        = 1.0;
   damageScale[$DamageType::AutoCannon] 			= 1.0;
   shieldDamageScale[$DamageType::AutoCannon]		= 1.0;
   damageScale[$DamageType::Flamethrower] 			= 1.0;
   shieldDamageScale[$DamageType::Flamethrower]		= 1.0;
   damageScale[$DamageType::StarHammer] 			= 1.0;
   shieldDamageScale[$DamageType::StarHammer]		= 1.0;
   damageScale[$DamageType::MeteorCannon] 			= 1.0;
   shieldDamageScale[$DamageType::MeteorCannon]		= 1.0;
   damageScale[$DamageType::MagIonReactor] 			= 1.0;
   shieldDamageScale[$DamageType::MagIonReactor]	= 1.0;
   damageScale[$DamageType::Turbocharger] 			= 1.0;
   shieldDamageScale[$DamageType::Turbocharger]		= 1.0;
   damageScale[$DamageType::BulletHE] 			    = 1.0;
   shieldDamageScale[$DamageType::BulletHE]	    	= 1.0;
   damageScale[$DamageType::BulletEM] 	     		= 1.0;
   shieldDamageScale[$DamageType::BulletEM]	    	= 1.0;
   damageScale[$DamageType::OutdoorDepTurretF] 	   	= 1.0;
   shieldDamageScale[$DamageType::OutdoorDepTurretF]= 1.0;
   damageScale[$DamageType::Railgun] 	          	= 1.0;
   shieldDamageScale[$DamageType::Railgun]          = 1.0;
   damageScale[$DamageType::SubspaceMagnet] 	   	= 1.0;
   shieldDamageScale[$DamageType::SubspaceMagnet]   = 1.0;
   damageScale[$DamageType::BulletIN] 	     		= 1.0;
   shieldDamageScale[$DamageType::BulletIN]	    	= 1.0;
   damageScale[$DamageType::StingerRail]     		= 1.0;
   shieldDamageScale[$DamageType::StingerRail]    	= 1.5;
   damageScale[$DamageType::MitziRail]         		= 1.0;
   shieldDamageScale[$DamageType::MitziRail]    	= 1.0;
   damageScale[$DamageType::Bolter]         		= 1.0;
   shieldDamageScale[$DamageType::Bolter]    	    = 1.0;
   damageScale[$DamageType::PlasmaCannon]         	= 1.0;
   shieldDamageScale[$DamageType::PlasmaCannon]    	= 1.0;
   damageScale[$DamageType::MPDisruptor]           	= 1.0;
   shieldDamageScale[$DamageType::MPDisruptor]      = 1.0;
   damageScale[$DamageType::BlasterRifle]         	= 1.0;
   shieldDamageScale[$DamageType::BlasterRifle]    	= 1.0;
   damageScale[$DamageType::PBCDevistator]         	= 1.0;
   shieldDamageScale[$DamageType::PBCDevistator]   	= 1.0;
   damageScale[$DamageType::PBCQuantum]         	= 1.0;
   shieldDamageScale[$DamageType::PBCQuantum]    	= 1.0;
   damageScale[$DamageType::VectorGlitch]         	= 1.0;
   shieldDamageScale[$DamageType::VectorGlitch]    	= 1.0;
   damageScale[$DamageType::SonicPulser]         	= 1.0;
   shieldDamageScale[$DamageType::SonicPulser]    	= 0.0;
   damageScale[$DamageType::RotaryCannon]         	= 1.0;
   shieldDamageScale[$DamageType::RotaryCannon]    	= 1.0;
};

datablock SimDataBlock(ArmorDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::AATurret] 				= 0.5;
   damageScale[$DamageType::Impact] 				= 0.75;
   damageScale[$DamageType::Ground] 				= 0.75;
};

datablock SimDataBlock(VehicleDamageProfile) : UniversalDamageProfile
{
   shieldDamageScale[$DamageType::AATurret] 		= 1.0;

   damageScale[$DamageType::AATurret] 				= 1.0;
   damageScale[$DamageType::Impact] 				= 1.25;
   damageScale[$DamageType::Ground] 				= 1.25;
};

datablock SimDataBlock(VTurretDamageProfile) : UniversalDamageProfile
{
   // Turret ammo here
   max[ChaingunAmmo] = 500;
   max[DiscAmmo] = 75;
   max[SpikeAmmo] = 150;
   max[MissileLauncherAmmo] = 80;
   max[BolterAmmo] = 300;
   max[PlasmaAmmo] = 125;
   max[MortarAmmo] = 60;
   max[GrenadeLauncherAmmo] = 200;
   max[AssaultMortarAmmo] = 180; // hailstorm ammo
   max[BombAmmo] = 60; // bomb ammo
};

//----------------------------------------------------------------------------
// VEHICLE DAMAGE PROFILES
//----------------------------------------------------------------------------

//**** SHRIKE SCOUT FIGHTER ****
datablock SimDataBlock(ShrikeDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** THUNDERSWORD BOMBER ****
datablock SimDataBlock(BomberDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** HAVOC TRANSPORT ****
datablock SimDataBlock(HavocDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning] 				= 10.0;
};

//**** WILDCAT GRAV CYCLE ****
datablock SimDataBlock(WildcatDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//**** BEOWULF TANK ****
datablock SimDataBlock(TankDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 10.0;
};

//**** JERICHO MPB ****
datablock SimDataBlock(MPBDamageProfile) : VehicleDamageProfile
{
   damageScale[$DamageType::Lightning]	= 10.0;
};

//----------------------------------------------------------------------------
// TURRET DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(TurretDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//----------------------------------------------------------------------------
// STATIC SHAPE DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(StaticShapeDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	= 5.0;
};

//----------------------------------------------------------------------------
// PLAYER DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(LightPlayerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Lightning]	=		10.0;
};

datablock SimDataBlock(HeavyPlayerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Lightning]	=		1.4;
};
