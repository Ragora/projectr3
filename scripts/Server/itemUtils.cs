// Item Utils: Registration factory
// Consolidate changes here

// Found and overriden in item.cs - load after that for permanent change
$WeaponSlot = 0;
$ShoulderSlot = 1;
$BackpackSlot = 2;
$FlagSlot = 3;
$ArmorWeightSlot = 3;
$WeaponSubImage1 = 4;
$WeaponSubImage2 = 5;
$WeaponSubImage3 = 6;
$WeightImageSlot = 7;

// Projectile flags
$Projectile::IgnoreShields              = 1 << 0;   // ex: blaster, ignores shield and damages armor
$Projectile::IgnoreReflections          = 1 << 1;   // not affected by reflectors (ex. forcefields)
$Projectile::IgnoreReflectorField       = 1 << 2;   // not affected by repulsor fields
$Projectile::PlayerFragment             = 1 << 3;   // will gib the player if killed by this projectile
$Projectile::CanHeadshot                = 1 << 4;   // if hit in the head, will use headshotMultipler to increase damage
$Projectile::CountMAs                   = 1 << 5;
$Projectile::Piercing                   = 1 << 6;
$Projectile::PlaysHitSound              = 1 << 7;
$Projectile::InternalDamage             = 1 << 8;

//------------------------------------------------------------------------------
// Armors
$ArmorCount = 0;

// For Dreadnought EVAC mode
$NameToInv["Plugsuit"] = "Plugsuit";
$InvArmor[$ArmorCount] = "Plugsuit";
$ArmorsListID["Plugsuit"] = $ArmorCount;
$ArmorsList[$ArmorCount] = "Plugsuit";
$ArmorsListData[$ArmorCount, "size"] = "Plugsuit";
$ArmorsListData[$ArmorCount, "name"] = $InvArmor[$ArmorCount];
$ArmorsListData[$ArmorCount, "desc"] = "The suit you wear when connecting to your powered armor";
$ArmorsListData[$ArmorCount, "mask"] = 1 << $ArmorCount;
$ArmorsList::Plugsuit = $ArmorsListData[$ArmorCount, "mask"];
$ArmorCount++;

$InvArmor[$ArmorCount] = "RSX-15a 'Scout'";
$NameToInv["RSX-15a 'Scout'"] = "Light";
$ArmorsListID["Light"] = $ArmorCount;
$ArmorsList[$ArmorCount] = "Light";
$ArmorsListData[$ArmorCount, "size"] = "Light";
$ArmorsListData[$ArmorCount, "name"] = $InvArmor[$ArmorCount];
$ArmorsListData[$ArmorCount, "desc"] = "Lightly armored chassis, best used for highly mobile operations";
$ArmorsListData[$ArmorCount, "mask"] = 1 << $ArmorCount;
$ArmorsList::Light = $ArmorsListData[$ArmorCount, "mask"];
$ArmorCount++;

$InvArmor[$ArmorCount] = "LSAC-08 'Assault'";
$NameToInv["LSAC-08 'Assault'"] = "Medium";
$ArmorsListID["Medium"] = $ArmorCount;
$ArmorsList[$ArmorCount] = "Medium";
$ArmorsListData[$ArmorCount, "size"] = "Medium";
$ArmorsListData[$ArmorCount, "name"] = $InvArmor[$ArmorCount];
$ArmorsListData[$ArmorCount, "desc"] = "Standard infantry chassis, multi-role capable";
$ArmorsListData[$ArmorCount, "mask"] = 1 << $ArmorCount;
$ArmorsList::Medium = $ArmorsListData[$ArmorCount, "mask"];
$ArmorCount++;

$InvArmor[$ArmorCount] = "MWP-X1 'Dreadnought'";
$NameToInv["MWP-X1 'Dreadnought'"]  = "Heavy";
$ArmorsListID["Heavy"] = $ArmorCount;
$ArmorsList[$ArmorCount] = "Heavy";
$ArmorsListData[$ArmorCount, "size"] = "Heavy";
$ArmorsListData[$ArmorCount, "name"] = $InvArmor[$ArmorCount];
$ArmorsListData[$ArmorCount, "desc"] = "Powered by proprietary Mobile Weapons Platform tech - a walking tank";
$ArmorsListData[$ArmorCount, "mask"] = 1 << $ArmorCount;
$ArmorsList::Heavy = $ArmorsListData[$ArmorCount, "mask"];
$ArmorCount++;

$InvArmor[$ArmorCount] = "YG Tek 'Gearhead' Mk3";
$NameToInv["YG Tek 'Gearhead' Mk3"]  = "Gearhead";
$ArmorsListID["Gearhead"] = $ArmorCount;
$ArmorsList[$ArmorCount] = "Gearhead";
$ArmorsListData[$ArmorCount, "size"] = "Gearhead";
$ArmorsListData[$ArmorCount, "name"] = $InvArmor[$ArmorCount];
$ArmorsListData[$ArmorCount, "desc"] = "The ever-reliable Engineer chassis, built Gearhead tough";
$ArmorsListData[$ArmorCount, "mask"] = 1 << $ArmorCount;
$ArmorsList::Gearhead = $ArmorsListData[$ArmorCount, "mask"];
$ArmorCount++;

$NameToInv["'Battle Angel' APM"]  = "BattleAngel";
$NameToInv["MASS Mk. 2 'Brawler'"]  = "Brawler";

//------------------------------------------------------------------------------
// Weapons
$WeaponCount = 0;

$InvWeapon[$WeaponCount] = "Disruptor";
$NameToInv["Disruptor"] = "Blaster";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "Blaster";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Blaster"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Blaster";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Directed energy pistol, dangerous and precise";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Blaster = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Thermal Projector";
$NameToInv["Thermal Projector"] = "Plasma";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Plasma";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaAmmo] = 10;
$WeaponsListID["Plasma"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Plasma";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Fires magnetically contained plasma fireballs";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Plasma = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Vulcan";
$NameToInv["Vulcan"] = "Chaingun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Chaingun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ChaingunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ChaingunAmmo] = 25;
$WeaponsListID["Chaingun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Chaingun";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Motor powered gattling weapon, high RoF";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Chaingun = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Disc Launcher";
$NameToInv["Disc Launcher"] = "Disc";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_disc";
$WeaponsHudData[$WeaponCount, itemDataName] = "Disc";
$WeaponsHudData[$WeaponCount, ammoDataName] = "DiscAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_disc";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[DiscAmmo] = 5;
$WeaponsListID["Disc"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Disc";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Gauss-powered gravitron disc launcher";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Disc = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Autocannon";
$NameToInv["Autocannon"] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_grenlaunch";
$WeaponsHudData[$WeaponCount, itemDataName] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "GrenadeLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_grenade";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[GrenadeLauncherAmmo] = 5;
$WeaponsListID["GrenadeLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "GrenadeLauncher";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "High velocity automatic shell launcher";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::GrenadeLauncher = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Assault Rifle";
$NameToInv["Laser Assault Rifle"] = "SniperRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "SniperRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["SniperRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "SniperRifle";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Rapid fire precision assault weapon, focused on burning holes in targets";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::SniperRifle = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Arc Welder";
$NameToInv["Arc Welder"] = "ELFGun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "ELFGun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_elf";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["ELFGun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "ELFGun";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Danger danger! High voltage! When we touch...";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::ELFGun = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Mortar";
$NameToInv["Mortar"] = "Mortar";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "Mortar";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MortarAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MortarAmmo] = 5;
$WeaponsListID["Mortar"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Mortar";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches high explosives over long distances";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Mortar = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Missile Launcher";
$NameToInv["Missile Launcher"] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MissileLauncherAmmo] = 2;
$WeaponsListID["MissileLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "MissileLauncher";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches heat seeking missiles";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::MissileLauncher = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;


$InvWeapon[$WeaponCount] = "Rocket Launcher";
$NameToInv["Rocket Launcher"] = "RocketLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "RocketLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["RocketLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "RocketLauncher";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches dumbfire rockets";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::RocketLauncher = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Missile Launcher";
$NameToInv["Laser Missile Launcher"] = "LaserMissileLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "LaserMissileLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["LaserMissileLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "LaserMissileLauncher";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches missiles that are laser seeking";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::LaserMissileLauncher = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

// 9
$InvWeapon[$WeaponCount] = "---------------";
//$NameToInv["Laser Pointer"] = "TargetingLaser";
// WARNING!!! If you change the weapon index of the targeting laser,
// you must change the HudWeaponInvBase::addWeapon function to test
// for the new value!
// 9
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_targetlaser";
$WeaponsHudData[$WeaponCount, itemDataName] = "TargetingLaser";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_targlaser";
$WeaponsHudData[$WeaponCount, visible] = "false";
//$WeaponsListID["TargetingLaser"] = $WeaponCount;
//$WeaponsListData[$WeaponCount, "item"] = "TargetingLaser";
//$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
//$WeaponsListData[$WeaponCount, "desc"] = "Targeting Laser Pointer";
//$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
//$WeaponsList::TargetingLaser = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

// 10
$InvWeapon[$WeaponCount] = "SLPlaceholder";
//$NameToInv["Shocklance (TBD)"] = "ShockLance";
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "ShockLance";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_shocklance";
$WeaponsHudData[$WeaponCount, visible] = "false";
//$WeaponsListID["ShockLance"] = $WeaponCount;
//$WeaponsListData[$WeaponCount, "item"] = "ShockLance";
//$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
//$WeaponsListData[$WeaponCount, "desc"] = "To be destroyed/refactored";
//$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
//$WeaponsList::ShockLance = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Mitzi Blast Cannon";
$NameToInv["Mitzi Blast Cannon"] = "MitziBlastCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "MitziBlastCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MitziCapacitor";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MitziCapacitor] = 1;
$WeaponsListID["MitziBlastCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "MitziBlastCannon";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "The jack of all trades";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Mitzi = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Compressed Spike Rifle";
$NameToInv["Compressed Spike Rifle"] = "SpikeRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "SpikeRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "SpikeAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_targlaser"; // hud_ret_sniper
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[SpikeAmmo] = 10;
$WeaponsListID["SpikeRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "SpikeRifle";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Self-contained Gauss battle rifle, marksman approved";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Spike = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Railgun";
$NameToInv["Railgun"] = "Railgun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "Railgun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "RailgunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[RailgunAmmo] = 1;
$WeaponsListID["Railgun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Railgun";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Reach out and touch someone with the finger of God";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Railgun = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Sagittarius";
$NameToInv["Sagittarius"] = "Sagittarius";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "Sagittarius";
$WeaponsHudData[$WeaponCount, ammoDataName] = "SagittariusAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[SagittariusAmmo] = 1;
$WeaponsListID["Sagittarius"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Sagittarius";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Micro-railgun sniper rifle, Scout only";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Sagittarius = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Subspace Magnet";
$NameToInv["Subspace Magnet REM"] = "SubspaceMagnet";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "SubspaceMagnet";
$WeaponsHudData[$WeaponCount, ammoDataName] = "SubspaceCapacitor";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[SubspaceCapacitor] = 0;
$WeaponsListID["SubspaceMagnet"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "SubspaceMagnet";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Collects energetic particles when triggered, fires fusion pulses";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::SubspaceMagnet = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Bolter";
$NameToInv["Bolter"] = "Bolter";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Bolter";
$WeaponsHudData[$WeaponCount, ammoDataName] = "BolterAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[BolterAmmo] = 10;
$WeaponsListID["SubspaceMagnet"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Bolter";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Heavy gattling weapon, hits significantly harder than the Vulcan";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Bolter = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Plasma Cannon";
$NameToInv["Plasma Cannon"] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaCannonAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaCannonAmmo] = 5;
$WeaponsListID["PlasmaCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "PlasmaCannon";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Superheated magnetic ball launcher";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::PlasmaCannon = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Heavy Disc Launcher";
$NameToInv["Heavy Disc Launcher"] = "Ripper";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_disc";
$WeaponsHudData[$WeaponCount, itemDataName] = "Ripper";
$WeaponsHudData[$WeaponCount, ammoDataName] = "RipperAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_disc";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[RipperAmmo] = 5;
$WeaponsListID["Ripper"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "Ripper";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Disc launcher for the Dreadnought";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::Ripper = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "MegaPulse Disruptor";
$NameToInv["MegaPulse Disruptor"] = "MPDisruptor";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "MPDisruptor";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["MPDisruptor"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "MPDisruptor";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Directed energy cannon, fires volatile blasts of energy";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::MPDisruptor = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Blaster Rifle";
$NameToInv["Blaster Rifle"] = "BlasterRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "BlasterRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["BlasterRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "BlasterRifle";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "High power directed energy assault rifle";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::BlasterRifle = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Particle Beam Cannon";
$NameToInv["Particle Beam Cannon"] = "ParticleBeamCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "ParticleBeamCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PBCAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PBCAmmo] = 2;
$WeaponsListID["ParticleBeamCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "ParticleBeamCannon";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Shoots charged particles near instantly at the target";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::ParticleBeamCannon = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$WeaponsHudCount = $WeaponCount;

$InvWeapon[$WeaponCount] = "Hand Disruptor";
$NameToInv["Hand Disruptor"] = "HandDisruptor";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "HandDisruptor";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["HandDisruptor"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "item"] = "HandDisruptor";
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Plugsuit's built-in disruptor, located on the palms of the suit";
$WeaponsListData[$WeaponCount, "mask"] = 1 << $WeaponCount;
$WeaponsList::HandDisruptor = $WeaponsListData[$WeaponCount, "mask"];
$WeaponCount++;

$WeaponsHudCount = $WeaponCount;

//------------------------------------------------------------------------------
// Shortcut Bitmasks
$Enhancement::AmmoWeapons           = $WeaponsList::Plasma | $WeaponsList::Chaingun | $WeaponsList::Disc | $WeaponsList::GrenadeLauncher | $WeaponsList::Mortar | $WeaponsList::MissileLauncher | $WeaponsList::Spike | $WeaponsList::Railgun | $WeaponsList::Bolter | $WeaponsList::PlasmaCannon | $WeaponsList::Ripper | $WeaponsList::ParticleBeamCannon;
$Enhancement::EnergyWeapons         = $WeaponsList::Blaster | $WeaponsList::SniperRifle | $WeaponsList::ELFGun | $WeaponsList::Railgun | $WeaponsList::MPDisruptor | $WeaponsList::BlasterRifle | $WeaponsList::ParticleBeamCannon;
$Enhancement::HybridWeapons         = $WeaponsList::Railgun | $WeaponsList::Sagittarius | $WeaponsList::ParticleBeamCannon;
$Enhancement::CapacitorWeapons      = $WeaponsList::Mitzi | $WeaponsList::SubspaceMagnet;

//------------------------------------------------------------------------------
// Packs
$PackCount = 0;

$InvPack[$PackCount] = "Power Core";
$NameToInv["Power Core"] = "EnergyPack";
$BackpackHudData[$PackCount, itemDataName] = "EnergyPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packenergy";
$PackListID["EnergyPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "EnergyPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Generates an additional 4.68 kw/s of energy.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Energy = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Nano Assembler";
$NameToInv["Nano Assembler"] = "RepairPack";
$BackpackHudData[$PackCount, itemDataName] = "RepairPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packrepair";
$PackListID["RepairPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "RepairPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Allows you to repair equipment and heal people.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Repair = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Shield Emitter";
$NameToInv["Shield Emitter"] = "ShieldPack";
$BackpackHudData[$PackCount, itemDataName] = "ShieldPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packshield";
$PackListID["ShieldPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "ShieldPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Emits a protective energy shield powered by your armor's reactor.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Shield = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Cloaking Device";
$NameToInv["Cloaking Device"] = "CloakingPack";
$BackpackHudData[$PackCount, itemDataName] = "CloakingPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packcloak";
$PackListID["CloakingPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "CloakingPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Bends light to turn the armor invisible.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Cloak = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Pulse Sensor Jammer";
$NameToInv["Pulse Sensor Jammer"] = "SensorJammerPack";
$BackpackHudData[$PackCount, itemDataName] = "SensorJammerPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsensjam";
$PackListID["SensorJammerPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "SensorJammerPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Jams Pulse sensors and Cloaking Devices.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Jammer = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Ammo Cache";
$NameToInv["Ammo Cache"] = "AmmoPack";
$BackpackHudData[$PackCount, itemDataName] = "AmmoPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packammo";
$PackListID["AmmoPack"] = $PackCount;
$PackListData[$PackCount, "item"] = "AmmoPack";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Increases stored ammunition.";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::Ammo = $PackListData[$PackCount, "mask"];
$PackCount++;

$InvPack[$PackCount] = "Satchel Charge";
$NameToInv["Satchel Charge"] = "SatchelCharge";
$BackpackHudData[$PackCount, itemDataName] = "SatchelCharge";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackCount++;

$InvPack[$PackCount] = "Motion Sensor Pack";
$NameToInv["Motion Sensor Pack"] = "MotionSensorDeployable";
$BackpackHudData[$PackCount, itemDataName] = "MotionSensorDeployable";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packmotionsens";
$PackCount++;

$InvPack[$PackCount] = "Pulse Sensor Pack";
$NameToInv["Pulse Sensor Pack"] = "PulseSensorDeployable";
$BackpackHudData[$PackCount, itemDataName] = "PulseSensorDeployable";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packradar";
$PackCount++;

$InvPack[$PackCount] = "Inventory Station";
$NameToInv["Inventory Station"] = "InventoryDeployable";
$BackpackHudData[$PackCount, itemDataName] = "InventoryDeployable";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packinventory";
$PackCount++;

$InvPack[$PackCount] = "Landspike Turret";
$NameToInv["Landspike Turret"] = "TurretOutdoorDeployable";
$BackpackHudData[$PackCount, itemDataName] = "TurretOutdoorDeployable";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packturretout";
$PackCount++;

$InvPack[$PackCount] = "Spider Clamp Turret";
$NameToInv["Spider Clamp Turret"] = "TurretIndoorDeployable";
$BackpackHudData[$PackCount, itemDataName] = "TurretIndoorDeployable";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packturretin";
$PackCount++;

$InvPack[$PackCount] = "Large Inv Station";
$NameToInv["Large Inv Station"] = "DeployableInvStationPack";
$BackpackHudData[$PackCount, itemDataName] = "DeployableInvStationPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packinventory";
$PackCount++;

$InvPack[$PackCount] = "Large Turret";
$NameToInv["Large Turret"] = "TurretBasePack";
$BackpackHudData[$PackCount, itemDataName] = "TurretBasePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packturretout";
$PackCount++;

$InvPack[$PackCount] = "Teleporter Pad";
$NameToInv["Teleporter Pad"] = "TelePack";
$BackpackHudData[$PackCount, itemDataName] = "TelePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packturretin";
$PackCount++;

$InvPack[$PackCount] = "Sensor Base";
$NameToInv["Sensor Base"] = "SensorBasePack";
$BackpackHudData[$PackCount, itemDataName] = "SensorBasePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packradar";
$PackCount++;

$InvPack[$PackCount] = "Flying Command Base";
$NameToInv["Flying Command Base"] = "CommandBasePack";
$BackpackHudData[$PackCount, itemDataName] = "CommandBasePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackCount++;

$InvPack[$PackCount] = "Orbital Bombardment Satellite";
$NameToInv["Orbital Bombardment Satellite"] = "SatelliteBasePack";
$BackpackHudData[$PackCount, itemDataName] = "SatelliteBasePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packammo";
$PackCount++;

$InvPack[$PackCount] = "Blast Wall";
$NameToInv["Blast Wall"] = "BlastWallPack";
$BackpackHudData[$PackCount, itemDataName] = "BlastWallPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packshield";
$PackCount++;

$InvPack[$PackCount] = "Forcefield Poles";
$NameToInv["Forcefield Poles"] = "ShieldGeneratorPack";
$BackpackHudData[$PackCount, itemDataName] = "ShieldGeneratorPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packshield";
$PackCount++;

$InvPack[$PackCount] = "Bussard Collector";
$NameToInv["Bussard Collector"] = "BussardCollectorPack";
$BackpackHudData[$PackCount, itemDataName] = "BussardCollectorPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackCount++;

$InvPack[$PackCount] = "Resource Driller";
$NameToInv["Resource Driller"] = "ResourceDrillerPack";
$BackpackHudData[$PackCount, itemDataName] = "ResourceDrillerPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackCount++;

$InvPack[$PackCount] = "Base Core Node";
$NameToInv["Base Core Node"] = "BaseCorePack";
$BackpackHudData[$PackCount, itemDataName] = "BaseCorePack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackCount++;

$InvPack[$PackCount] = "Spawn Refit Node";
$NameToInv["Spawn Refit Node"] = "NodeSpawnFavsPack";
$BackpackHudData[$PackCount, itemDataName] = "NodeSpawnFavsPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packammo";
$PackCount++;

$InvPack[$PackCount] = "Repulsor Node";
$NameToInv["Repulsor Node"] = "NodeRepulsorPack";
$BackpackHudData[$PackCount, itemDataName] = "NodeRepulsorPack";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packammo";
$PackCount++;

$InvPack[$PackCount] = "Synomic Regenerator";
$NameToInv["Synomic Regenerator"] = "SynomicRegenerator";
$BackpackHudData[$PackCount, itemDataName] = "SynomicRegenerator";
$BackpackHudData[$PackCount, bitmapName] = "gui/hud_new_packsatchel";
$PackListID["SynomicRegenerator"] = $PackCount;
$PackListData[$PackCount, "item"] = "SynomicRegenerator";
$PackListData[$PackCount, "name"] = $InvPack[$PackCount];
$PackListData[$PackCount, "desc"] = "Regenerates capacitor weapons and health";
$PackListData[$PackCount, "mask"] = 1 << $PackCount;
$PackList::SynomicRegenerator = $PackListData[$PackCount, "mask"];
$PackCount++;

$BackpackHudCount = $PackCount;

// non-team mission pack choices (DM, Hunters, Rabbit)

$NTInvPack[0] = "Energy Pack";
$NTInvPack[1] = "Repair Pack";
$NTInvPack[2] = "Shield Pack";
$NTInvPack[3] = "Cloak Pack";
$NTInvPack[4] = "Sensor Jammer Pack";
$NTInvPack[5] = "Ammunition Pack";
$NTInvPack[6] = "Satchel Charge";
$NTInvPack[7] = "Motion Sensor Pack";
$NTInvPack[8] = "Pulse Sensor Pack";
$NTInvPack[9] = "Inventory Station";

//------------------------------------------------------------------------------
// Inventory Defines
$InventoryHudCount = 0;

//------------------------------------------------------------------------------
// Grenades
$GrenadeCount = 0;

$InvGrenade[$GrenadeCount] = "HE Grenade";
$NameToInv["HE Grenade"] = "Grenade";
$AmmoIncrement[Grenade]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Napalm Grenade";
$NameToInv["Napalm Grenade"] = "NapalmGrenade";
$AmmoIncrement[NapalmGrenade]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = NapalmGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = NapalmGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "EM Pulse Grenade";
$NameToInv["EM Pulse Grenade"] = "EMPGrenade";
$AmmoIncrement[EMPGrenade]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = EMPGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = EMPGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Sonic Pulser";
$NameToInv["Sonic Pulser"] = "SonicPulser";
$AmmoIncrement[SonicPulser]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = SonicPulser;
$InventoryHudData[$InventoryHudCount, ammoDataName] = SonicPulser;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Booster";
$NameToInv["Booster"] = "ThrusterGrenade";
$AmmoIncrement[ThrusterGrenade] = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_whiteout_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = "ThrusterGrenade";
$InventoryHudData[$InventoryHudCount, ammoDataName] = "ThrusterGrenade";
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Whiteout Grenade";
$NameToInv["Whiteout Grenade"] = "FlashGrenade";
$AmmoIncrement[FlashGrenade]        = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_whiteout_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Concussion Grenade";
$NameToInv["Concussion Grenade"] = "ConcussionGrenade";
$AmmoIncrement[ConcussionGrenade]   = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_concuss_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Flare Grenade";
$NameToInv["Flare Grenade"] = "FlareGrenade";
$AmmoIncrement[FlareGrenade]        = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Deployable Camera";
$NameToInv["Deployable Camera"] = "CameraGrenade";
$AmmoIncrement[CameraGrenade]       = 2; // z0dd - ZOD, 4/17/02. Camera ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

//------------------------------------------------------------------------------
// Mines
$MineCount = 0;

$InvMine[$MineCount] = "HE Mine";
$NameToInv["HE Mine"] = "Mine";
$AmmoIncrement[Mine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = Mine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Mine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Cloaking Mine";
$NameToInv["Cloaking Mine"] = "CloakingMine";
$AmmoIncrement[CloakingMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = CloakingMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = CloakingMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Koi Mine";
$NameToInv["Koi Mine"] = "KoiMine";
$AmmoIncrement[KoiMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = KoiMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = KoiMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Concussion Mine";
$NameToInv["Concussion Mine"] = "ConcussionMine";
$AmmoIncrement[ConcussionMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = ConcussionMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = ConcussionMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Repair Patch";
$NameToInv["Repair Patch"] = "RepairPatchMine";
$AmmoIncrement[RepairPatchMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = RepairPatchMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = RepairPatchMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

//-----------------------------------------------------------------------------
// Misc
$AmmoIncrement[RepairKit]           = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_medpack";
$InventoryHudData[$InventoryHudCount, itemDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, ammoDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, slot]         = 3;
$InventoryHudCount++;

$AmmoIncrement[Beacon]              = 1; // z0dd - ZOD, 4/17/02. Beacon ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_beacon";
$InventoryHudData[$InventoryHudCount, itemDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, slot]         = 2;
$InventoryHudCount++;

//-----------------------------------------------------------------------------
// ShapeBase - Inventory Management

function ShapeBase::clearInventory(%this)
{
    for(%i = 0; %i < $WeaponCount; %i++)
    {
        %this.setInventory($WeaponsHudData[%i, itemDataName], 0);
        %this.setInventory($WeaponsHudData[%i, ammoDataName], 0);
    }
    
    for(%j = 0; %j < $InventoryHudCount; %j++)
        %this.setInventory($InventoryHudData[%j, itemDataName], 0);

    for(%k = 0; %k < Enhancement.moduleCount; %k++)
        %this.setInventory(Enhancement.moduleItem[%k], 0);
    
    // take away any pack the player has
    %curPack = %this.getMountedImage($BackpackSlot);

    if(%curPack > 0)
        %this.setInventory(%curPack.item, 0);
}

// deprecated - keeping around just in case I noobed something
function xShapeBase::clearInventory(%this)
{
   %this.setInventory(RepairKit, 0);
   %this.setInventory(Beacon, 0);
   
   %this.setInventory(Mine, 0);
   %this.setInventory(MinePackTrigger, 0);

   %this.setInventory(Grenade,0);
   %this.setInventory(FlashGrenade,0);
   %this.setInventory(ConcussionGrenade,0);
   %this.setInventory(FlareGrenade,0);
   %this.setInventory(CameraGrenade, 0);
   %this.setInventory(ThrusterGrenade, 0);

   %this.setInventory(Blaster,0);
   %this.setInventory(Plasma,0);
   %this.setInventory(Disc,0);
   %this.setInventory(Chaingun, 0);
   %this.setInventory(Mortar, 0);
   %this.setInventory(GrenadeLauncher, 0);
   %this.setInventory(MissileLauncher, 0);
   %this.setInventory(SniperRifle, 0);
   %this.setInventory(TargetingLaser, 0);
   %this.setInventory(ELFGun, 0);
   %this.setInventory(MitziBlastCannon, 0);
   %this.setInventory(SpikeRifle, 0);
   %this.setInventory(Railgun, 0);
   %this.setInventory(Sagittarius, 0);
   
   %this.setInventory(PlasmaAmmo,0);
   %this.setInventory(ChaingunAmmo, 0);
   %this.setInventory(DiscAmmo, 0);
   %this.setInventory(GrenadeLauncherAmmo, 0);
   %this.setInventory(MissileLauncherAmmo, 0);
   %this.setInventory(MortarAmmo, 0);
   %this.setInventory(MitziCapacitor, 0);
   %this.setInventory(StarHammerAmmo, 0);
   %this.setInventory(SpikeAmmo, 0);
   %this.setInventory(RailgunAmmo, 0);
   %this.setInventory(SagittariusAmmo, 0);
   %this.setInventory(SubspaceMagnet, 0);
   %this.setInventory(SubspaceCapacitor, 0);
   %this.setInventory(Bolter, 0);
   %this.setInventory(BolterAmmo, 0);
   %this.setInventory(PlasmaCannon, 0);
   %this.setInventory(PlasmaCannonAmmo, 0);
   %this.setInventory(Ripper, 0);
   %this.setInventory(RipperAmmmo, 0);
   %this.setInventory(MPDisruptor, 0);
   %this.setInventory(BlasterRifle, 0);
   %this.setInventory(ParticleBeamCannon, 0);
   %this.setInventory(PBCAmmo, 0);
   
   %this.setInventory(GravitronArmorPlating, 0);
   %this.setInventory(EnduriumArmorPlating, 0);
   %this.setInventory(DamageAmp, 0);
   %this.setInventory(Nosferatu, 0);
   %this.setInventory(SubspaceRegenerator, 0);
   %this.setInventory(MagneticClamp, 0);
   %this.setInventory(ExplosiveResistArmor, 0);
   %this.setInventory(ElectroMagResistArmor, 0);
   %this.setInventory(ThermalResistArmor, 0);
   %this.setInventory(KineticResistArmor, 0);
   %this.setInventory(SonicResistArmor, 0);
   %this.setInventory(ShieldHardener, 0);
   
   // take away any pack the player has
   %curPack = %this.getMountedImage($BackpackSlot);
   
   if(%curPack > 0)
      %this.setInventory(%curPack.item, 0);
}

// Weapons system transplant - must be loaded before any image
// Trigger Proxy Image
datablock ShapeBaseImageData(TriggerProxyImage)
{
   className = WeaponImage;

   shapeFile = "turret_muzzlepoint.dts";
   item = TargetingLaser;
   offset = "0 0 0";
   triggerProxySlot = $WeaponSubImage1;

   usesEnergy = true;
   minEnergy = 0;

   stateName[0]                     = "Activate";
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Trigger";

   stateName[3]                     = "Trigger";
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onTriggerDown";
   stateTransitionOnTriggerUp[3]    = "Release";
   stateTransitionOnNoAmmo[3]       = "Release";

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Release";
   stateScript[5]                   = "onTriggerUp";
   stateTransitionOnTimeout[5]      = "Ready";
};

function ShapeBaseImageData::onTriggerDown(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, true);
}

function ShapeBaseImageData::onTriggerUp(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, false);
}

//-----------------------------------------------------------------------------
// Misc Inventory Overrides

function ShapeBase::maxInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}

function ShapeBase::maxBaseInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}

//------------------------------------------------------------------------------
function InventoryScreen::updateHud( %this, %client, %tag )
{
   %armor = getArmorDatablock( %client, $NameToInv[%client.favorites[0]] );
   if ( %client.lastArmor !$= %armor )
   {
      %client.lastArmor = %armor;
      for ( %x = 0; %x < %client.lastNumFavs; %x++ )
         messageClient( %client, 'RemoveLineHud', "", 'inventoryScreen', %x );
      %setLastNum = true;
   }

   %cmt = $CurrentMissionType;
//Create - ARMOR - List
   %armorList = %client.favorites[0];
   for ( %y = 0; $InvArmor[%y] !$= ""; %y++ )
      if ( $InvArmor[%y] !$= %client.favorites[0] )
         %armorList = %armorList TAB $InvArmor[%y];

//Create - WEAPON - List
   for ( %y = 0; $InvWeapon[%y] !$= ""; %y++ )
   {
      %notFound = true;
      for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
      {
         %WInv = $NameToInv[$InvWeapon[%y]];
         if ( ( $InvWeapon[%y] $= %client.favorites[getField( %client.weaponIndex,%i )] ) || !%armor.max[%WInv] )
         {
            %notFound = false;
            break;
         }
      }

      if ( !($InvBanList[%cmt, %WInv]) )
      {
         if ( %notFound && %weaponList $= "" )
            %weaponList = $InvWeapon[%y];
         else if ( %notFound )
            %weaponList = %weaponList TAB $InvWeapon[%y];
      }
   }
//Create - PACK - List
   if ( getFieldCount( %client.packIndex ) )
      %packList = %client.favorites[getField( %client.packIndex, 0 )];
   else
   {
       %packList = "EMPTY";
       %client.numFavs++;
   }
   for ( %y = 0; $InvPack[%y] !$= ""; %y++ )
   {
      %PInv = $NameToInv[$InvPack[%y]];
      if ( ( $InvPack[%y] !$= %client.favorites[getField( %client.packIndex, 0 )]) &&
      %armor.max[%PInv] && !($InvBanList[%cmt, %PInv]))
      %packList = %packList TAB $Invpack[%y];
   }
//Create - MODULES - List
   if(getFieldCount(%client.armorModIndex))
      %armorModList = %client.favorites[getField(%client.armorModIndex, 0)];
   else
   {
      %armorModList = "EMPTY";
      %client.numFavs++;
   }
   for ( %am = 0; $InvArmorMod[%am] !$= ""; %am++ )
   {
      %aminv = $NameToInv[$InvArmorMod[%am]];
      if ( ( $InvArmorMod[%am] !$= %client.favorites[getField(%client.armorModIndex, 0)]) &&
      %armor.max[%aminv] && !($InvBanList[%cmt, %aminv]))
         %armorModList = %armorModList TAB $InvArmorMod[%am];
   }
//Create - GRENADE - List
   for ( %y = 0; $InvGrenade[%y] !$= ""; %y++ )
   {
      %notFound = true;
      for(%i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
      {
         %GInv = $NameToInv[$InvGrenade[%y]];
         if ( ( $InvGrenade[%y] $= %client.favorites[getField( %client.grenadeIndex, %i )] ) || !%armor.max[%GInv] )
         {
            %notFound = false;
            break;
         }
      }
      if ( !($InvBanList[%cmt, %GInv]) )
      {
         if ( %notFound && %grenadeList $= "" )
            %grenadeList = $InvGrenade[%y];
         else if ( %notFound )
            %grenadeList = %grenadeList TAB $InvGrenade[%y];
      }
   }

//Create - MINE - List
   for ( %y = 0; $InvMine[%y] !$= "" ; %y++ )
   {
      %notFound = true;
      // -----------------------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/24/02. This was broken, Fixed.
      for(%i = 0; %i < getFieldCount( %client.mineIndex ); %i++)
      {
         %MInv = $NameToInv[$InvMine[%y]];
         if ( ( $InvMine[%y] $= %client.favorites[getField( %client.mineIndex, %i )] ) || !%armor.max[%MInv] )
         {
            %notFound = false;
            break;
         }
      }
      // -----------------------------------------------------------------------------------------------------
      if ( !($InvBanList[%cmt, %MInv]) )
      {
         if ( %notFound && %mineList $= "" )
            %mineList = $InvMine[%y];
         else if ( %notFound )
            %mineList = %mineList TAB $InvMine[%y];
      }
   }
   %client.numFavsCount++;
   messageClient( %client, 'SetLineHud', "", %tag, 0, "Armor:", %armorList, armor, %client.numFavsCount );
   %lineCount = 1;

   for ( %x = 0; %x < %armor.maxWeapons; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.weaponIndex ) )
      {
         %list = %client.favorites[getField( %client.weaponIndex,%x )];
         if ( %list $= Invalid )
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.weaponIndex = %client.weaponIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.weaponIndex = %client.weaponIndex TAB %client.numFavs;
         %client.numFavs++;
      }
      if ( %list $= empty )
         %list = %list TAB %weaponList;
      else
         %list = %list TAB %weaponList TAB "EMPTY";
      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Weapon " @ %x + 1 @ ": ", %list , weapon, %client.numFavsCount );
   }
   %lineCount = %lineCount + %armor.maxWeapons;

   %client.numFavsCount++;
   if ( getField( %packList, 0 ) !$= empty)
      %packList = %packList TAB "EMPTY";
   %packText = %packList;
   %packOverFlow = "";
   if ( strlen( %packList ) > 255 )
   {
      %packText = getSubStr( %packList, 0, 255 );
      %packOverFlow = getSubStr( %packList, 255, 512 );
   }
   messageClient( %client, 'SetLineHud', "", %tag, %lineCount, "Pack:", %packText, pack, %client.numFavsCount, %packOverFlow );
   %lineCount++;

   // Armor Modules
   %client.numFavsCount++;
   if ( getField( %armorModList, 0 ) !$= empty)
       %armorModList = %armorModList TAB "EMPTY";

   %armorModText = %armorModList;
   %armorModOverFlow = "";
   if ( strlen( %armorModList ) > 255 )
   {
      %armorModText = getSubStr( %armorModList, 0, 255 );
      %armorModOverFlow = getSubStr( %armorModList, 255, 512 );
   }
   messageClient( %client, 'SetLineHud', "", %tag, %lineCount, "Module:", %armorModText, armormod, %client.numFavsCount, %armorModOverFlow );
   %lineCount++;

   for( %x = 0; %x < %armor.maxGrenades; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.grenadeIndex ) )
      {
         %list = %client.favorites[getField( %client.grenadeIndex, %x )];
         if (%list $= Invalid)
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.grenadeIndex = %client.grenadeIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.grenadeIndex = %client.grenadeIndex TAB %client.numFavs;
         %client.numFavs++;
      }

      if ( %list $= empty )
         %list = %list TAB %grenadeList;
      else
         %list = %list TAB %grenadeList TAB "EMPTY";

      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Grenade:", %list, grenade, %client.numFavsCount );
   }
   %lineCount = %lineCount + %armor.maxGrenades;

   for ( %x = 0; %x < %armor.maxMines; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.mineIndex ) )
      {
         %list = %client.favorites[getField( %client.mineIndex, %x )];
         if ( %list $= Invalid )
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.mineIndex = %client.mineIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.mineIndex = %client.mineIndex TAB %client.numFavs;
         %client.numFavs++;
      }

      if ( %list !$= Invalid )
      {
         if ( %list $= empty )
            %list = %list TAB %mineList;
         else if ( %mineList !$= "" )
            %list = %list TAB %mineList TAB "EMPTY";
         else
            %list = %list TAB "EMPTY";
      }

      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Mine:", %list, mine, %client.numFavsCount );
   }

   if ( %setLastNum )
      %client.lastNumFavs = %client.numFavs;
}

function serverCmdSetClientFav(%client, %text)
{
   if ( getWord( getField( %text, 0 ), 0 ) $= armor )
   {
      %client.curFavList = %text;
      %validList = checkInventory( %client, %text );
      %client.favorites[0] = getField( %text, 1 );
      %armor = getArmorDatablock( %client, $NameToInv[getField( %validList,1 )] );
      %weaponCount = 0;
      %packCount = 0;
      %armorModCount = 0;
      %grenadeCount = 0;
      %mineCount = 0;
      %count = 1;
      %client.weaponIndex = "";
      %client.packIndex = "";
      %client.grenadeIndex = "";
      %client.mineIndex = "";
      %client.armorModIndex = "";
      
      for(%i = 3; %i < getFieldCount(%validList); %i = %i + 2)
      {
         %setItem = false;
         switch$ (getField(%validList,%i-1))
         {
            case weapon:
               if(%weaponCount < %armor.maxWeapons)
               {
                  if(!%weaponCount)
                     %client.weaponIndex = %count;
                  else
                     %client.weaponIndex = %client.weaponIndex TAB %count;
                  %weaponCount++;
                  %setItem = true;
               }
            case pack:
               if(%packCount < 1)
               {
                  %client.packIndex = %count;
                  %packCount++;
                  %setItem = true;
               }
            case armormod:
               if(%armorModCount < 1)
               {
                  %client.armorModIndex = %count;
                  %armorModCount++;
                  %setItem = true;
               }
            case grenade:
               if(%grenadeCount < %armor.maxGrenades)
               {
                  if(!%grenadeCount)
                     %client.grenadeIndex = %count;
                  else
                     %client.grenadeIndex = %client.grenadeIndex TAB %count;
                  %grenadeCount++;
                  %setItem = true;
               }
            case mine:
               if(%mineCount < %armor.maxMines)
               {
                  if(!%mineCount)
                     %client.mineIndex = %count;
                  else
                     %client.mineIndex = %client.mineIndex TAB %count;
                  %mineCount++;
                  %setItem = true;
               }
         }
         if(%setItem)
         {
            %client.favorites[%count] = getField(%validList, %i);
            %count++;
         }
      }
      %client.numFavs = %count;
      %client.numFavsCount = 0;
      inventoryScreen::updateHud(1, %client, 'inventoryScreen');
   }
}

function createInvBanCount()
{
   $BanCount["Armor"] = 0;
   $BanCount["Weapon"] = 0;
   $BanCount["Pack"] = 0;
   $BanCount["Grenade"] = 0;
   $BanCount["Mine"] = 0;
   $BanCount["ArmorMod"] = 0;

   for(%i = 0; $InvArmor[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvArmor[%i]]])
         $BanCount["Armor"]++;
   $InvTotalCount["Armor"] = %i;

   for(%i = 0; $InvWeapon[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvWeapon[%i]]])
         $BanCount["Weapon"]++;
   $InvTotalCount["Weapon"] = %i;

   for(%i = 0; $InvPack[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvPack[%i]]])
         $BanCount["Pack"]++;
   $InvTotalCount["Pack"] = %i;

   for(%i = 0; $InvArmorMod[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvArmorMod[%i]]])
         $BanCount["ArmorMod"]++;
   $InvTotalCount["ArmorMod"] = %i;

   for(%i = 0; $InvGrenade[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvGrenade[%i]]])
         $BanCount["Grenade"]++;
   $InvTotalCount["Grenade"] = %i;

   for(%i = 0; $InvMine[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvMine[%i]]])
         $BanCount["Mine"]++;
   $InvTotalCount["Mine"] = %i;
}
