function CTFGame::playerTouchEnemyFlag(%game, %player, %flag)
{
   // ---------------------------------------------------------------
   // z0dd, ZOD - 9/27/02. Player must wait to grab after throwing it
   if(%player.flagTossWait == true || %player.noFlagTouch)
      return false;
   // ---------------------------------------------------------------

   cancel(%flag.searchSchedule); // z0dd - ZOD, 9/28/02. Hack for flag collision bug.  SquirrelOfDeath, 10/02/02: Moved from PlayerTouchFlag

   cancel(%game.updateFlagThread[%flag]); // z0dd - ZOD, 8/4/02. Cancel this flag's thread to KineticPoet's flag updater
   %game.flagHeldTime[%flag] = getSimTime(); // z0dd - ZOD, 8/15/02. Store time player grabbed flag.

   %client = %player.client;
   %player.holdingFlag = %flag;  //%player has this flag
   %flag.carrier = %player;  //this %flag is carried by %player

   %player.mountImage(FlagImage, $FlagSlot, true, %game.getTeamSkin(%flag.team));

   %game.playerGotFlagTarget(%player);
   //only cancel the return timer if the player is in bounds...
   if (!%client.outOfBounds)
   {
      cancel($FlagReturnTimer[%flag]);
      $FlagReturnTimer[%flag] = "";
   }

   //if this flag was "at home", see if both flags have now been taken
   if (%flag.isHome)
   {
      // tiebreaker score
      game.awardScoreFlagTouch( %client, %flag );

      %startStalemate = false;
      if ($TeamFlag[1] == %flag)
         %startStalemate = !$TeamFlag[2].isHome;
      else
         %startStalemate = !$TeamFlag[1].isHome;

      if (%startStalemate)
         %game.stalemateSchedule = %game.schedule(%game.stalemateTimeMS, beginStalemate);

   }
   %flag.hide(true);
   %flag.startFade(0, 0, false);
   %flag.isHome = false;
   if(%flag.stand)
      %flag.stand.getDataBlock().onFlagTaken(%flag.stand);//animate, if exterior stand

   $flagStatus[%flag.team] = %client.nameBase;
   %teamName = %game.getTeamName(%flag.team);
   messageTeamExcept(%client, 'MsgCTFFlagTaken', '\c2Teammate %1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   messageTeam(%flag.team, 'MsgCTFFlagTaken', '\c2Your flag has been taken by %1!~wfx/misc/flag_taken.wav',%client.name, 0, %flag.team, %client.nameBase);
   messageTeam(0, 'MsgCTFFlagTaken', '\c2%1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   messageClient(%client, 'MsgCTFFlagTaken', '\c2You took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") took team "@%flag.team@" flag");

   //call the AI function
   %game.AIplayerTouchEnemyFlag(%player, %flag);

   //if the player is out of bounds, then in 3 seconds, it should be thrown back towards the in bounds area...
   if (%client.outOfBounds)
      %game.schedule(3000, "boundaryLoseFlag", %player);
}

function PracticeCTFGame::playerTouchEnemyFlag(%game, %player, %flag)
{
   // ---------------------------------------------------------------
   // z0dd, ZOD - 9/27/02. Player must wait to grab after throwing it
   if(%player.flagTossWait == true || %player.noFlagTouch)
      return false;
   // ---------------------------------------------------------------

   cancel(%flag.searchSchedule); // z0dd - ZOD, 9/28/02. Hack for flag collision bug.  SquirrelOfDeath, 10/02/02: Moved from PlayerTouchFlag

   cancel(%game.updateFlagThread[%flag]); // z0dd - ZOD, 8/4/02. Cancel this flag's thread to KineticPoet's flag updater
   %game.flagHeldTime[%flag] = getSimTime(); // z0dd - ZOD, 8/15/02. Store time player grabbed flag.

   %client = %player.client;
   %player.holdingFlag = %flag;  //%player has this flag
   %flag.carrier = %player;  //this %flag is carried by %player

    %player.mountImage(FlagImage, $FlagSlot, true, %game.getTeamSkin(%flag.team));

   %game.playerGotFlagTarget(%player);
   //only cancel the return timer if the player is in bounds...
   if (!%client.outOfBounds)
   {
      cancel($FlagReturnTimer[%flag]);
      $FlagReturnTimer[%flag] = "";
   }

   //if this flag was "at home", see if both flags have now been taken
   if (%flag.isHome)
   {
      // tiebreaker score
      game.awardScoreFlagTouch( %client, %flag );

      %startStalemate = false;
      if ($TeamFlag[1] == %flag)
         %startStalemate = !$TeamFlag[2].isHome;
      else
         %startStalemate = !$TeamFlag[1].isHome;

      if (%startStalemate)
         %game.stalemateSchedule = %game.schedule(%game.stalemateTimeMS, beginStalemate);

   }

   %flag.hide(true);
   %flag.startFade(0, 0, false);
   %flag.isHome = false;
   if(%flag.stand)
      %flag.stand.getDataBlock().onFlagTaken(%flag.stand);//animate, if exterior stand

   $flagStatus[%flag.team] = %client.nameBase;
   %teamName = %game.getTeamName(%flag.team);
   messageTeamExcept(%client, 'MsgCTFFlagTaken', '\c2Teammate %1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   messageTeam(%flag.team, 'MsgCTFFlagTaken', '\c2Your flag has been taken by %1!~wfx/misc/flag_taken.wav',%client.name, 0, %flag.team, %client.nameBase);
   messageTeam(0, 'MsgCTFFlagTaken', '\c2%1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   messageClient(%client, 'MsgCTFFlagTaken', '\c2You took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") took team "@%flag.team@" flag");

   //call the AI function
   %game.AIplayerTouchEnemyFlag(%player, %flag);

   //if the player is out of bounds, then in 3 seconds, it should be thrown back towards the in bounds area...
   if (%client.outOfBounds)
      %game.schedule(3000, "boundaryLoseFlag", %player);
}

function HuntersGame::playerTouchFlag(%game, %player, %flag)
{
    if(%player.noFlagTouch)
        return;
        
   //make sure the player is still alive
   %client = %player.client;
   if (%player.getState() !$= "Dead")
   {
      //increase the count bye the flag value
      %flagValue = %flag.value;
      %client.flagCount += %flagValue;

      //delete the flag
      %flag.delete();

      //if the client has 5 or more flags, mount an image
      if (%client.flagCount >= 5)
         %player.mountImage(HuntersFlagImage, $FlagSlot);

      //schedule an update message
      cancel(%client.flagMsgPending);
      %client.flagMsgPending = %game.schedule(%game.flagMsgDelayMS, "sendFlagCountMessage", %client);
      messageClient(%client, 'MsgHuntYouHaveFlags', "", %client.flagCount - 1);

      //update the log...
      logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") has "@%client.flagCount-1@" flags");

      //play the sound pickup in 3D
      %player.playAudio(0, HuntersFlagPickupSound);

      //see if the client could set the record
      if (!%game.teamMode && !%client.couldSetRecord)
      {
         %numFlags = %client.flagCount - 1;
         if (%numFlags > 10 && %numFlags > $Host::HuntersRecords::Count[$currentMission])
         {
            //see if we have at least 4 non-AI players
            %humanCount = 0;
            %count = ClientGroup.getCount();
            for (%i = 0; %i < %count; %i++)
            {
               %cl = ClientGroup.getObject(%i);
               if (!%cl.isAIControlled())
                  %humanCount++;
               if (%humanCount >= %game.numHumansForRecord)
                  break;
            }

            if (%humanCount >= %game.numHumansForRecord)
            {
               %client.couldSetRecord = true;

               //send a message right away...
               if (isEventPending(%client.flagMsgPending))
               {
                  cancel(%client.flagMsgPending);
                  %game.sendFlagCountMessage(%client);
               }

               //send a message to everyone
               messageAllExcept(%client, -1, 'MsgHuntPlayerCouldSetRecord', '\c2%1 has enough flags to set the record for this mission!~wfx/misc/flag_return.wav');
               messageClient(%client, 'MsgHuntYouCouldSetRecord', '\c2You have enough flags to set the record for this mission!~wfx/misc/flag_return.wav');
            }
         }
      }

      //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
      %game.updateFlagHoarder(%client);
   }
}

function RabbitGame::playerTouchFlag(%game, %player, %flag)
{
    if(%player.noFlagTouch)
        return;
        
   if ((%flag.carrier $= "") && (%player.getState() !$= "Dead"))
   {
      %player.client.startTime = getSimTime();
      %player.holdingFlag = %flag;
      %flag.carrier = %player;
      %player.mountImage(FlagImage, $FlagSlot, true); //, $teamSkin[$RabbitTeam]);
      cancel(%flag.returnThread);
      %flag.hide(true);
      %flag.isHome = false;
      $flagStatus = %client.name;
      messageAll('MsgRabbitFlagTaken', '\c2%1 has taken the flag!~wfx/misc/flag_snatch.wav', %player.client.name);
      logEcho(%player.client.nameBase@" (pl "@%player@"/cl "@%player.client@") took flag");
      %player.client.team = $RabbitTeam;
      %player.client.setSensorGroup($RabbitTeam);
      setTargetSensorGroup(%player.getTarget(), $RabbitTeam);

      //increase the score
      %player.client.flagGrabs++;
      %game.recalcScore(%player.client);
      %game.schedule(5000, "RabbitFlagCheck", %player);

      //show the rabbit waypoint
      %game.rabbitDamageTime = 0;
      cancel(%game.waypointSchedule);
      %game.showRabbitWaypoint(%player.client);
   }
}
