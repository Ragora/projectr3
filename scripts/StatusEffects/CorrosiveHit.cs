// Corrosive Debuff

function CorrosiveHitEffect::setup(%this)
{
    %this.duration = 15000;
    %this.stackable = true;
    %this.stackableDuration = 1000;
    %this.ticking = false;
    %this.effectInvulnPeriod = 5000;
}

function CorrosiveHitEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted())
        return false;

    return true;
}

function CorrosiveHitEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    messageClient(%obj.client , 'MsgStartCorr' , "\c2Warning! Corrosive substance detected on armor, structure compromised.");
    %obj.damageReduceFactor += 0.05;
}

function CorrosiveHitEffect::onStackEffect(%this, %obj, %instigator)
{
    Parent::onStackEffect(%this, %obj, %instigator);

    %obj.damageReduceFactor += 0.05;
}

function CorrosiveHitEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
    {
        %obj.damageReduceFactor -= 0.05 * %obj.effectStacks[%this.getName()];
        messageClient(%obj.client , 'MsgEndCorr' , '\c2Corrosive solution has been nullified.');
    }
}

StatusEffect.registerEffect("CorrosiveHitEffect");
