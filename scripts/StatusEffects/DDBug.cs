// Defender Designator

function DDBugEffect::setup(%this)
{
    %this.duration = 30000;
    %this.stackable = false;
    %this.stackableDuration = 0;
    %this.ticking = false;
    %this.effectInvulnPeriod = 10000;
}

function DDBugEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted())
        return false;

    return true;
}

function DDBugEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    messageClient(%obj.client , 'MsgStartDDBug' , "\c2Warning! Tracking beacon detected, incoming damage increased by 50%!");
    %obj.damageReduceFactor += 0.5;
}

function DDBugEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
    {
        %obj.damageReduceFactor -= 0.5;
        messageClient(%obj.client , 'MsgEndDDBug' , '\c2Tracking beacon shorted out, armor integrity restored.');
    }
}

StatusEffect.registerEffect("DDBugEffect");
