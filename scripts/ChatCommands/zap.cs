//-----------------------------------------------------------------------------
// Xi Command System
// Strike with lightning (Zap)

function Xi::zap(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
                 messageClient(%cl, 'MsgXICantTorture', '\c5Torture: Cannot torture %1, they are higher level than you.', %client.nameBase);
            else
            {
               if(isObject(%client.player))
               {
                    %client.player.setInvincible(false);
                    %client.player.f_hold = true;
                    holdObject(%client.player, %client.player.position, 32);
                    schedule(4000, 0, disableHoldObject, %client.player);
                    %lightning = createLightning(%client.player.position, 1, 240, 100, true);
                    %lightning2 = createLightning(%client.player.getWorldBoxCenter(), 1, 240, 100, false);
                    %lightning.schedule(5000, delete);
                    %lightning2.schedule(1000, delete);
                    messageClient(%cl, 'MsgXITorture', '\c5Torture: Applied retribution to %1.', %client.nameBase);                    
               }
               else
                    messageClient(%cl, 'MsgXITortureNoPlayer', '\c5Torture: %1 has not spawned.', %client.nameBase);               
            }
         }
      }
      else
         messageClient(%cl, 'MsgXITortureNoName', '\c5Torture: No name specified.');
}

Xi.addCommand($XI::SuperAdmin, "zap", "Strikes target with lightning (partial names accepted) - ex. /zap noob");

