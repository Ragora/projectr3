//-----------------------------------------------------------------------------
// Xi Command System
// Reset F2 Menu

function Xi::resetmenu(%this, %cl, %val)
{
    %cl.scoreHudMenuState = $MenuState::Default;
    %cl.scoreHudMenu = %client.defaultMenu;
    
    messageClient(%cl, 'MsgXIMenuReset', '\c5The Score menu has been reset.');
}

Xi.addCommand($XI::Player, "resetmenu", "Reset the F2 menu if it glitches - ex. /resetmenu");

