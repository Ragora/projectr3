//-----------------------------------------------------------------------------
// Xi Command System
// Kick Player

function Xi::kick(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
            {
                 messageClient(%cl, 'MsgXIKickTooHigh', '\c5Kick: Cannot kick %1, they are higher level than you.', %client.nameBase);
                 return 1;
            }
            else
               kick(%client, %cl, %client.guid);
         }
      }
      else
         messageClient(%cl, 'MsgXIKickNoName', '\c5Kick: No name specified.');
}

Xi.addCommand($XI::Admin, "kick", "Kicks the player for a short amount of time (partial names accepted) - ex. /kick tard");
