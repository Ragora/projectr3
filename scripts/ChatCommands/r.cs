//-----------------------------------------------------------------------------
// Xi Command System
// Whisper (Private Chat) system - respond

function Xi::r(%this, %cl, %val)
{
     if(isObject(%cl.lastResponder))
     {
          %cl.lockedClient = %cl.lastResponder;
          %cl.lastResponder.lastResponder = %cl;
          messageClient(%cl, 'MsgOutgoingWhisper', '\c5Reply %1: %2', %cl.lockedClient.name, %val);
          messageClient(%cl.lockedClient, 'MsgIncomingWhisper', '\c5%1 says: %2', %cl.name, %val);          
          EngineBase.logManager.logToFile("WhisperLog", timestamp() SPC "[Whisper]" SPC %cl.namebase SPC "->" SPC %cl.lockedClient.namebase@":" SPC %val);          
     }
     else if(%cl.lastResponder)
     {
          %cl.lastResponder = 0;
          messageClient(%cl, 'MsgWhisper501', '\c5Whisper: This player has disconnected, auto-unlocking name.');
     }
     else
          messageClient(%cl, 'MsgWhisper403', '\c5Reply: Who are you replying to?');     
}

Xi.addCommand($XI::Player, "r", "Respond to the last player who whispered you - ex. /r message");

