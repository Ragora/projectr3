//-----------------------------------------------------------------------------
// Xi Command System
// Whisper (Private Chat) system - Unlock

function Xi::unlock(%this, %cl, %val)
{
     %cl.lockedClient = 0;
     messageClient(%cl, 'MsgWhisperUndo', '\c5Whisper: Unlocked current whisper target.');
}

Xi.addCommand($XI::Player, "unlock", "Unlocks the current locked player - ex. /unlock");

