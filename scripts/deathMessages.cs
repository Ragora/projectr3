

/////////////////////////////////////////////////////////////////////////////////////////////////
// %1 = Victim's name                                                                          //
// %2 = Victim's gender (value will be either "him" or "her")                                  //
// %3 = Victim's possessive gender	(value will be either "his" or "her")                      //
// %4 = Killer's name                                                                          //
// %5 = Killer's gender (value will be either "him" or "her")                                  //
// %6 = Killer's possessive gender (value will be either "his" or "her")                       //
// %7 = implement that killed the victim (value is the object number of the bullet, disc, etc) //
/////////////////////////////////////////////////////////////////////////////////////////////////

$DeathMessageCampingCount = 1;
$DeathMessageCamping[0] = '\c0%1 was killed for camping near the Nexus.';
																						   
 //Out of Bounds deaths
$DeathMessageOOBCount = 1;
$DeathMessageOOB[0] = '\c0%1 was killed for loitering outside the mission area.';

$DeathMessageLavaCount = 6;
$DeathMessageLava[0] = '\c0%1\'s last thought before falling into the lava : \'Oops\'.';
$DeathMessageLava[1] = '\c0%1 makes the supreme sacrifice to the lava gods.';
$DeathMessageLava[2] = '\c0%1 looks surprised by the lava - but only briefly.';
$DeathMessageLava[3] = '\c0%1 wimps out by jumping into the lava and trying to make it look like an accident.'; 
$DeathMessageLava[4] = '\c0%1 wanted to experience death by lava first hand.';
$DeathMessageLava[5] = '\c0%1 dives face first into the molten kool-aid... oh wait.';

$DeathMessageLightningCount = 3;
$DeathMessageLightning[0] = '\c0%1 was killed by lightning!';
$DeathMessageLightning[1] = '\c0%1 caught a lightning bolt!';
$DeathMessageLightning[2] = '\c0%1 stuck %3 finger in Mother Nature\'s light socket.';

//these used when a player presses ctrl-k
$DeathMessageSuicideCount = 8;
$DeathMessageSuicide[0] = '\c0%1 blows %3 own head off!';  
$DeathMessageSuicide[1] = '\c0%1 ends it all. Cue violin music.';
$DeathMessageSuicide[2] = '\c0%1 kills %2self.';
$DeathMessageSuicide[3] = '\c0%1 goes for the quick and dirty respawn.';
$DeathMessageSuicide[4] = '\c0%1 self-destructs in a fit of ennui.';
$DeathMessageSuicide[5] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSuicide[6] = '\c0%1 wanted to make sure %3 gun was loaded.';
$DeathMessageSuicide[7] = '\c0%1 was last heard shouting "death before dishonor!!".';

$DeathMessageLSSuicideCount = 4;
$DeathMessageLSSuicide[0] = '\c0%1 couldn\'t get to a station in time!';
$DeathMessageLSSuicide[1] = '\c0%1 dies from internal organ failure.';
$DeathMessageLSSuicide[2] = '\c0%1\'s life support system lets %3 down.';
$DeathMessageLSSuicide[3] = '\c0%1 shuffles off %3 mortal coil.';

$DeathMessageVehPadCount = 1;
$DeathMessageVehPad[0] = '\c0%1 got caught in a vehicle\'s spawn field.'; 

$DeathMessageFFPowerupCount = 1;
$DeathMessageFFPowerup[0] = '\c0%1 got caught up in a forcefield during power up.'; 

$DeathMessageRogueMineCount = 1;
$DeathMessageRogueMine[$DamageType::Mine, 0] = '\c0%1 is all mine.';

//these used when a player kills himself (other than by using ctrl - k)
$DeathMessageSelfKillCount = 5;
$DeathMessageSelfKill[$DamageType::Blaster, 0] = '\c0%1 kills %2self with a disruptor.';
$DeathMessageSelfKill[$DamageType::Blaster, 1] = '\c0%1 makes a note to watch out for disruptor ricochets.';
$DeathMessageSelfKill[$DamageType::Blaster, 2] = '\c0%1\'s disruptor kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::Blaster, 3] = '\c0%1 deftly guns %2self down with %3 own disruptor.';
$DeathMessageSelfKill[$DamageType::Blaster, 4] = '\c0%1 has a fatal encounter with %3 own disruptor.';

$DeathMessageSelfKill[$DamageType::BlueBlaster, 0] = '\c0%1 kills %2self with a disruptor.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 1] = '\c0%1 makes a note to watch out for disruptor ricochets.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 2] = '\c0%1\'s disruptor kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 3] = '\c0%1 deftly guns %2self down with %3 own disruptor.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 4] = '\c0%1 has a fatal encounter with %3 own disruptor.';

$DeathMessageSelfKill[$DamageType::GreenBlaster, 0] = '\c0%1 kills %2self with a disruptor.';
$DeathMessageSelfKill[$DamageType::GreenBlaster, 1] = '\c0%1 makes a note to watch out for disruptor ricochets.';
$DeathMessageSelfKill[$DamageType::GreenBlaster, 2] = '\c0%1\'s disruptor kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::GreenBlaster, 3] = '\c0%1 deftly guns %2self down with %3 own disruptor.';
$DeathMessageSelfKill[$DamageType::GreenBlaster, 4] = '\c0%1 has a fatal encounter with %3 own disruptor.';

$DeathMessageSelfKill[$DamageType::Bullet, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Bullet, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bullet, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Bullet, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bullet, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessageSelfKill[$DamageType::BulletHE, 0] = '\c0%1 manages to kill %2self with explosive bullets.';
$DeathMessageSelfKill[$DamageType::BulletHE, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletHE, 2] = '\c0%1 manages to kill %2self with explosive bullets.';
$DeathMessageSelfKill[$DamageType::BulletHE, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletHE, 4] = '\c0%1 manages to kill %2self with explosive bullets.';

$DeathMessageSelfKill[$DamageType::BulletEM, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::BulletEM, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletEM, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::BulletEM, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletEM, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessageSelfKill[$DamageType::BulletIN, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::BulletIN, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletIN, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::BulletIN, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::BulletIN, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessageSelfKill[$DamageType::Plasma, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::Plasma, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Plasma, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::Flamethrower, 0] = '\c0%1 roasts %2self.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 1] = '\c0%1 turns %2self into cajun meat chunks.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 2] = '\c0%1 tries to act like a fire eater, but failed.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Flamethrower, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::Burn, 0] = '\c0%1 lights %2self on fire.';
$DeathMessageSelfKill[$DamageType::Burn, 1] = '\c0%1 attempts to turn %3self into the human torch... and fails.';
$DeathMessageSelfKill[$DamageType::Burn, 2] = '\c0%1 lights %2self on fire.';
$DeathMessageSelfKill[$DamageType::Burn, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Burn, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::Disc, 0] = '\c0%1 kills %2self with a disc.';
$DeathMessageSelfKill[$DamageType::Disc, 1] = '\c0%1 catches %3 own spinfusor disc.';
$DeathMessageSelfKill[$DamageType::Disc, 2] = '\c0%1 heroically falls on %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 3] = '\c0%1 helpfully jumps into %3 own disc\'s explosion.';
$DeathMessageSelfKill[$DamageType::Disc, 4] = '\c0%1 plays Russian roulette with %3 spinfusor.';

$DeathMessageSelfKill[$DamageType::Grenade, 0] = '\c0%1 destroys %2self with a grenade!';   //applies to hand grenades *and* grenade launcher grenades
$DeathMessageSelfKill[$DamageType::Grenade, 1] = '\c0%1 took a bad bounce from %3 own grenade!';
$DeathMessageSelfKill[$DamageType::Grenade, 2] = '\c0%1 pulled the pin a shade early.';
$DeathMessageSelfKill[$DamageType::Grenade, 3] = '\c0%1\'s own grenade turns on %2.';
$DeathMessageSelfKill[$DamageType::Grenade, 4] = '\c0%1 blows %2self up real good.';

$DeathMessageSelfKill[$DamageType::Mortar, 0] = '\c0%1 kills %2self with a mortar!';
$DeathMessageSelfKill[$DamageType::Mortar, 1] = '\c0%1 hugs %3 own big green boomie.';
$DeathMessageSelfKill[$DamageType::Mortar, 2] = '\c0%1 mortars %2self all over the map.';
$DeathMessageSelfKill[$DamageType::Mortar, 3] = '\c0%1 experiences %3 mortar\'s payload up close.';
$DeathMessageSelfKill[$DamageType::Mortar, 4] = '\c0%1 suffered the wrath of %3 own mortar.';

$DeathMessageSelfKill[$DamageType::Missile, 0] = '\c0%1 kills %2self with a missile!';
$DeathMessageSelfKill[$DamageType::Missile, 1] = '\c0%1 runs a missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::Missile, 2] = '\c0%1 tests the missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 3] = '\c0%1 achieved missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 4] = '\c0%1 gracefully smoked %2self with a missile!';

$DeathMessageSelfKill[$DamageType::NailMissile, 0] = '\c0%1 kills %2self with a missile!';
$DeathMessageSelfKill[$DamageType::NailMissile, 1] = '\c0%1 runs a missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::NailMissile, 2] = '\c0%1 tests the missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::NailMissile, 3] = '\c0%1 achieved missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::NailMissile, 4] = '\c0%1 gracefully smoked %2self with a missile!';

$DeathMessageSelfKill[$DamageType::StarHammer, 0] = '\c0%1 kills %2self with a Star Hammer!';
$DeathMessageSelfKill[$DamageType::StarHammer, 1] = '\c0%1 destroys %2self with a Star Hammer.';
$DeathMessageSelfKill[$DamageType::StarHammer, 2] = '\c0%1 starhammers %2self to pieces.';
$DeathMessageSelfKill[$DamageType::StarHammer, 3] = '\c0%1 experiences %3 Star Hammer\'s payload up close.';
$DeathMessageSelfKill[$DamageType::StarHammer, 4] = '\c0%1 gracefully smoked %2self with a Star Hammer!';

$DeathMessageSelfKill[$DamageType::Mine, 0] = '\c0%1 kills %2self with a mine!';
$DeathMessageSelfKill[$DamageType::Mine, 1] = '\c0%1\'s mine violently reminds %2 of its existence.';
$DeathMessageSelfKill[$DamageType::Mine, 2] = '\c0%1 plants a decisive foot on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 3] = '\c0%1 fatally trips on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 4] = '\c0%1 makes a note not to run over %3 own mines.';

$DeathMessageSelfKill[$DamageType::SatchelCharge, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::SatchelCharge, 1] = '\c0%1 fall down...go boom.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 3] = '\c0%1 experiences explosive decompression!';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 4] = '\c0%1 splashes all over the map.';

$DeathMessageSelfKill[$DamageType::Ground, 0] = '\c0%1 lands too hard.';
$DeathMessageSelfKill[$DamageType::Ground, 1] = '\c0%1 finds gravity unforgiving.';
$DeathMessageSelfKill[$DamageType::Ground, 2] = '\c0%1 craters on impact.';
$DeathMessageSelfKill[$DamageType::Ground, 3] = '\c0%1 pancakes upon landing.';
$DeathMessageSelfKill[$DamageType::Ground, 4] = '\c0%1 loses a game of chicken with the ground.';

$DeathMessageSelfKill[$DamageType::EMP, 0] = '\c0%1 gracefully electrocuted %2self with an EM Pulse!';
$DeathMessageSelfKill[$DamageType::EMP, 1] = '\c0%1 feels the wrath of %3 own EM Pulse.';
$DeathMessageSelfKill[$DamageType::EMP, 2] = '\c0%1 kills %2self with an EM Pulse!';
$DeathMessageSelfKill[$DamageType::EMP, 3] = '\c0%1 gracefully electrocuted %2self with an EM Pulse!';
$DeathMessageSelfKill[$DamageType::EMP, 4] = '\c0%1 feels the wrath of %3 own EM Pulse.';

$DeathMessageSelfKill[$DamageType::MitziBlast, 0] = '\c0%1 lets %3 Mitzi Blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 1] = '\c0%1 was bit by %3 own Mitzi blast.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 2] = '\c0%1 vaporizes %2self.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 3] = '\c0%1\'s own Mitzi ran %2 down.';
$DeathMessageSelfKill[$DamageType::MitziBlast, 4] = '\c0%1 could not dodge the Mitzi Blast in time.';

$DeathMessageSelfKill[$DamageType::Poison, 0] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::Poison, 1] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::Poison, 2] = '\c0%1 heard "USE THE REPAIR KIT!" from far off.';
$DeathMessageSelfKill[$DamageType::Poison, 3] = '\c0%1 was killed by %3 own virus.';
$DeathMessageSelfKill[$DamageType::Poison, 4] = '\c0%1 was poisoned.';

$DeathMessageSelfKill[$DamageType::AutoCannon, 0] = '\c0%1 blows %2self to pieces with %3 autocannon bolt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 1] = '\c0%1 blew %3 own brain by autocannon bolt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 2] = '\c0%1 learns that autocannon bolts can hurt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 3] = '\c0%1 got too close to %3 autocannon explosion.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 4] = '\c0%1 blew %3 own brain by autocannon bolts.';

$DeathMessageSelfKill[$DamageType::Annihalator, 0] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by a nearby Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 2] = '\c0%1 used the force on %2self.';
$DeathMessageSelfKill[$DamageType::Annihalator, 3] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 4] = '\c0%1 is broken down into %3 component atoms by %3 own Annihalator!';

$DeathMessageSelfKill[$DamageType::MeteorCannon, 0] = '\c0%1 plays catch with %3 Meteor Cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 1] = '\c0%1 plays catch with %3 Meteor Cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 2] = '\c0%1 plays catch with %3 Meteor Cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 3] = '\c0%1 plays catch with %3 Meteor Cannon.';
$DeathMessageSelfKill[$DamageType::MeteorCannon, 4] = '\c0%1 plays catch with %3 Meteor Cannon.';

$DeathMessageSelfKill[$DamageType::Turbocharger, 0] = '\c0%1 overloaded and exploded.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 1] = '\c0%1 blew a capacitor.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 2] = '\c0%1 went critical.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 3] = '\c0%1 suffered a core breach and shorted out.';
$DeathMessageSelfKill[$DamageType::Turbocharger, 4] = '\c0%1 overvolts the nearby area with an electrical overload!';

$DeathMessageSelfKill[$DamageType::SubspaceMagnet, 0] = '\c0%1 blows %3 own brains out with a fusion pulse blast!';
$DeathMessageSelfKill[$DamageType::SubspaceMagnet, 1] = '\c0%1 had %3 subspace magnet facing the wrong way.';
$DeathMessageSelfKill[$DamageType::SubspaceMagnet, 2] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSelfKill[$DamageType::SubspaceMagnet, 3] = '\c0%1 wanted to make sure %3 subspace magnet was charged.';
$DeathMessageSelfKill[$DamageType::SubspaceMagnet, 4] = '\c0%1 was last heard saying "I\'ve made a huge mistake".';

$DeathMessageSelfKill[$DamageType::Railgun, 0] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Railgun, 1] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Railgun, 2] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Railgun, 3] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Railgun, 4] = '\c0%1 manages to kill %2self with a reflected rail shot.';

$DeathMessageSelfKill[$DamageType::Sniper, 0] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Sniper, 1] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Sniper, 2] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Sniper, 3] = '\c0%1 manages to kill %2self with a reflected rail shot.';
$DeathMessageSelfKill[$DamageType::Sniper, 4] = '\c0%1 manages to kill %2self with a reflected rail shot.';

$DeathMessageSelfKill[$DamageType::MPDisruptor, 0] = '\c0%1 kills %2self with a disruptor.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 1] = '\c0%1 makes a note to watch out for disruptor ricochets.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 2] = '\c0%1\'s disruptor kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 3] = '\c0%1 deftly guns %2self down with %3 own disruptor.';
$DeathMessageSelfKill[$DamageType::MPDisruptor, 4] = '\c0%1 has a fatal encounter with %3 own disruptor.';

$DeathMessageSelfKill[$DamageType::Bolter, 0] = '\c0%1 manages to kill %2self with a reflected bolt.';
$DeathMessageSelfKill[$DamageType::Bolter, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bolter, 2] = '\c0%1 manages to kill %2self with a reflected bolt.';
$DeathMessageSelfKill[$DamageType::Bolter, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bolter, 4] = '\c0%1 manages to kill %2self with a reflected bolt.';

$DeathMessageSelfKill[$DamageType::PlasmaCannon, 0] = '\c0%1 kills %2self with a heavy plasma cannon.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 2] = '\c0%1 swallows a white-hot mouthful of %3 own heavy plasma.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::BlasterRifle, 0] = '\c0%1 kills %2self with a stray blaster bolt.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 1] = '\c0%1 makes a note to watch out for blaster ricochets.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 2] = '\c0%1\'s blaster rifle kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 3] = '\c0%1 deftly guns %2self down with %3 own blaster rifle.';
$DeathMessageSelfKill[$DamageType::BlasterRifle, 4] = '\c0%1 has a fatal encounter with %3 own blaster rifle.';

$DeathMessageSelfKill[$DamageType::VectorGlitch, 0] = '\c0%1 finds %2self encased in solid carbonite.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 1] = '\c0%1 finds out %3 can\'t VectorPort into solid objects.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 2] = '\c0%1\'s life signs were forcefully terminated by rematerializing in a solid object.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 3] = '\c0%1 gets entangled in a solid object from their VectorPort.';
$DeathMessageSelfKill[$DamageType::VectorGlitch, 4] = '\c0%1 phases back into existence inbetween a rock and a hard place.';


//used when a player is killed by a teammate
$DeathMessageTeamKillCount = 1;	
$DeathMessageTeamKill[$DamageType::Blaster, 0] = '\c0%4 TEAMKILLED %1 with a disruptor!';
$DeathMessageTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with a thermal projector!';
$DeathMessageTeamKill[$DamageType::Bullet, 0] = '\c0%4 TEAMKILLED %1 with a vulcan!';
$DeathMessageTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a disc launcher!';
$DeathMessageTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with an autocannon!';
$DeathMessageTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a laser assault rifle!';
$DeathMessageTeamKill[$DamageType::Elf, 0] = '\c0%4 TEAMKILLED %1 with an arc welder!';
$DeathMessageTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a mortar!';
$DeathMessageTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a missile!';
$DeathMessageTeamKill[$DamageType::Shocklance, 0] = '\c0%4 TEAMKILLED %1 with a lightning arc!';
$DeathMessageTeamKill[$DamageType::Mine, 0] = '\c0%4 TEAMKILLED %1 with a mine!';
$DeathMessageTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 blew up TEAMMATE %1!';
$DeathMessageTeamKill[$DamageType::Impact, 0] = '\c0%4 runs down TEAMMATE %1!';

// MD3 Death Messages
$DeathMessageTeamKill[$DamageType::RotaryCannon, 0] = '\c0%4 TEAMKILLED %1 with a rotary cannon!';

$DefaultDeathMessageCount = 3;
$DefaultDeathMessage[0] = '\c0%4 kills %1.';
$DefaultDeathMessage[1] = '\c0%4 murders %1.';
$DefaultDeathMessage[2] = '\c0%4 kills %1.';
//$DefaultDeathMessage[2] = '\c0%1 gets taken down by %4\'s %8.';

//these used when a player is killed by an enemy
$DeathMessageCount = 5;
$DeathMessage[$DamageType::Blaster, 0] = '\c0%4 kills %1 with a disruptor.';
$DeathMessage[$DamageType::Blaster, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::Blaster, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::Blaster, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld disruptor.';
$DeathMessage[$DamageType::Blaster, 4] = '\c0%4 unleashes a terminal disruptor barrage into %1.';

$DeathMessage[$DamageType::BlueBlaster, 0] = '\c0%4 kills %1 with a disruptor.';
$DeathMessage[$DamageType::BlueBlaster, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::BlueBlaster, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::BlueBlaster, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld disruptor.';
$DeathMessage[$DamageType::BlueBlaster, 4] = '\c0%4 unleashes a terminal disruptor barrage into %1.';

$DeathMessage[$DamageType::GreenBlaster, 0] = '\c0%4 kills %1 with a disruptor.';
$DeathMessage[$DamageType::GreenBlaster, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::GreenBlaster, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::GreenBlaster, 3] = '\c0%4 fatally embarrasses %1 with %6 handheld disruptor.';
$DeathMessage[$DamageType::GreenBlaster, 4] = '\c0%4 unleashes a terminal disruptor barrage into %1.';

$DeathMessage[$DamageType::Plasma, 0] = '\c0%4 roasts %1 with the thermal projector.';
$DeathMessage[$DamageType::Plasma, 1] = '\c0%4 gooses %1 with an extra-friendly burst of plasma.';
$DeathMessage[$DamageType::Plasma, 2] = '\c0%4 entices %1 to try a faceful of plasma.';
$DeathMessage[$DamageType::Plasma, 3] = '\c0%4 introduces %1 to the plasma immolation dance.';
$DeathMessage[$DamageType::Plasma, 4] = '\c0%4 asks %1: "Need a light?"';

$DeathMessage[$DamageType::Flamethrower, 0] = '\c0%4 roasts %1 with the RAXX.';
$DeathMessage[$DamageType::Flamethrower, 1] = '\c0%4 gooses %1 with %6 RAXX.';
$DeathMessage[$DamageType::Flamethrower, 2] = '\c0%4 entices %1 to check %6 pilot light.';
$DeathMessage[$DamageType::Flamethrower, 3] = '\c0%4 introduces %1 to 5000 degrees of fun.';
$DeathMessage[$DamageType::Flamethrower, 4] = '\c0%1 steps into %4\'s stream of flames.';

$DeathMessage[$DamageType::Burn, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessage[$DamageType::Burn, 1] = '\c0%1 forgot to put out %4\'s flames.';
$DeathMessage[$DamageType::Burn, 2] = '\c0%4 sets %1 on fire.';
$DeathMessage[$DamageType::Burn, 3] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';
$DeathMessage[$DamageType::Burn, 4] = '\c0%4 slaps The Hot Kiss of Death on %1.';

$DeathMessage[$DamageType::Bullet, 0] = '\c0%4 rips %1 up with the vulcan.';
$DeathMessage[$DamageType::Bullet, 1] = '\c0%4 happily chews %1 into pieces with %6 vulcan.';
$DeathMessage[$DamageType::Bullet, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::Bullet, 3] = '\c0%1 suffers a serious hosing from %4\'s vulcan.';
$DeathMessage[$DamageType::Bullet, 4] = '\c0%4 bestows the blessings of %6 vulcan on %1.';

$DeathMessage[$DamageType::BulletHE, 0] = '\c0%4 rips %1 up with the explosive vulcan.';
$DeathMessage[$DamageType::BulletHE, 1] = '\c0%4 happily chews %1 into pieces with %6 explosive vulcan.';
$DeathMessage[$DamageType::BulletHE, 2] = '\c0%4 administers a dose of explosive decompression to %1.';
$DeathMessage[$DamageType::BulletHE, 3] = '\c0%1 suffers a serious hosing from %4\'s explosive vulcan.';
$DeathMessage[$DamageType::BulletHE, 4] = '\c0%4 bestows the blessings of %6 explosive vulcan on %1.';

$DeathMessage[$DamageType::BulletEM, 0] = '\c0%4 rips %1 up with the pulse vulcan.';
$DeathMessage[$DamageType::BulletEM, 1] = '\c0%4 happily chews %1 into pieces with %6 pulse vulcan.';
$DeathMessage[$DamageType::BulletEM, 2] = '\c0%4 administers a dose of EMP to %1.';
$DeathMessage[$DamageType::BulletEM, 3] = '\c0%1 suffers a serious hosing from %4\'s pulse vulcan.';
$DeathMessage[$DamageType::BulletEM, 4] = '\c0%4 bestows the blessings of %6 pulse vulcan on %1.';

$DeathMessage[$DamageType::BulletIN, 0] = '\c0%4 rips %1 up with the corrosive vulcan.';
$DeathMessage[$DamageType::BulletIN, 1] = '\c0%4 happily chews %1 into pieces with %6 corrosive vulcan.';
$DeathMessage[$DamageType::BulletIN, 2] = '\c0%4 administers a dose of corrosion to %1.';
$DeathMessage[$DamageType::BulletIN, 3] = '\c0%1 suffers a serious hosing from %4\'s corrosive vulcan.';
$DeathMessage[$DamageType::BulletIN, 4] = '\c0%4 bestows the blessings of %6 corrosive vulcan on %1.';

$DeathMessage[$DamageType::Disc, 0] = '\c0%4 demolishes %1 with the disc launcher.';
$DeathMessage[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessage[$DamageType::Disc, 2] = '\c0%4 shares a little blue friend with %1.';
$DeathMessage[$DamageType::Disc, 3] = '\c0%4 puts a little spin into %1.';
$DeathMessage[$DamageType::Disc, 4] = '\c0%1 becomes one of %4\'s greatest hits.';

$DeathMessage[$DamageType::Grenade, 0] = '\c0%4 eliminates %1 with a grenade.';   //applies to hand grenades *and* grenade launcher grenades
$DeathMessage[$DamageType::Grenade, 1] = '\c0%4 blows up %1 real good!';
$DeathMessage[$DamageType::Grenade, 2] = '\c0%1 gets annihilated by %4\'s grenade.';
$DeathMessage[$DamageType::Grenade, 3] = '\c0%1 receives a kaboom lesson from %4.';
$DeathMessage[$DamageType::Grenade, 4] = '\c0%4 turns %1 into grenade salad.'; 

$DeathMessage[$DamageType::Laser, 0] = '\c0%1 becomes %4\'s latest pincushion.';
$DeathMessage[$DamageType::Laser, 1] = '\c0%4 picks off %1 with %6 laser rifle.';
$DeathMessage[$DamageType::Laser, 2] = '\c0%4 uses %1 as the targeting dummy in a sniping demonstration.';						
$DeathMessage[$DamageType::Laser, 3] = '\c0%4 pokes a shiny new hole in %1 with %6 laser rifle.';
$DeathMessage[$DamageType::Laser, 4] = '\c0%4 caresses %1 with a couple hundred megajoules of laser.';

$DeathMessage[$DamageType::Elf, 0] = '\c0%4 fries %1 with the arc welder.';
$DeathMessage[$DamageType::Elf, 1] = '\c0%4 bug zaps %1 with %6 arc welder.';
$DeathMessage[$DamageType::Elf, 2] = '\c0%1 learns the shocking truth about %4\'s arc welder skills.';
$DeathMessage[$DamageType::Elf, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::Elf, 4] = '\c0%4\'s arc welder leaves %1 a crispy critter.';

$DeathMessage[$DamageType::EMP, 0] = '\c0%4 fries %1 with an EM Pulse.';
$DeathMessage[$DamageType::EMP, 1] = '\c0%4 zaps %1 with %6 EM Pulse.';
$DeathMessage[$DamageType::EMP, 2] = '\c0%1 learns the shocking truth about %4\'s nearby EM Pulse.';
$DeathMessage[$DamageType::EMP, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::EMP, 4] = '\c0%4\'s EM Pulse leaves %1 a crispy critter.';

$DeathMessage[$DamageType::Mortar, 0] = '\c0%4 obliterates %1 with the mortar.';
$DeathMessage[$DamageType::Mortar, 1] = '\c0%4 drops a mortar round right in %1\'s lap.';
$DeathMessage[$DamageType::Mortar, 2] = '\c0%4 delivers a mortar payload straight to %1.';
$DeathMessage[$DamageType::Mortar, 3] = '\c0%4 offers a little "heavy love" to %1.';
$DeathMessage[$DamageType::Mortar, 4] = '\c0%1 stumbles into %4\'s mortar reticle.';

$DeathMessage[$DamageType::Missile, 0] = '\c0%4 intercepts %1 with a missile.';
$DeathMessage[$DamageType::Missile, 1] = '\c0%4 watches %6 missile touch %1 and go boom.';
$DeathMessage[$DamageType::Missile, 2] = '\c0%4 got sweet tone on %1.';
$DeathMessage[$DamageType::Missile, 3] = '\c0By now, %1 has realized %4\'s missile killed %2.';
$DeathMessage[$DamageType::Missile, 4] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessage[$DamageType::NailMissile, 0] = '\c0%4 intercepts %1 with a missile.';
$DeathMessage[$DamageType::NailMissile, 1] = '\c0%4 watches %6 missile touch %1 and go boom.';
$DeathMessage[$DamageType::NailMissile, 2] = '\c0%4 got sweet tone on %1.';
$DeathMessage[$DamageType::NailMissile, 3] = '\c0By now, %1 has realized %4\'s missile killed %2.';
$DeathMessage[$DamageType::NailMissile, 4] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessage[$DamageType::StarHammer, 0] = '\c0%4 obliterates %1 with the Star Hammer.';
$DeathMessage[$DamageType::StarHammer, 1] = '\c0%4 chalks up another Star Hammer kill, courtesy of %1.';
$DeathMessage[$DamageType::StarHammer, 2] = '\c0%4 delivers a Star Hammer blast straight to %1.';
$DeathMessage[$DamageType::StarHammer, 3] = '\c0%4\'s Star Hammer blast leaves %1 nothin\' but smokin\' boots.';
$DeathMessage[$DamageType::StarHammer, 4] = '\c0%4 hammers %1 gooooood.';

$DeathMessage[$DamageType::Shocklance, 0] = '\c0%4 reaps a harvest of %1 with the arc welder.';
$DeathMessage[$DamageType::Shocklance, 1] = '\c0%4 feeds %1 the business end of %6 arc welder.';
$DeathMessage[$DamageType::Shocklance, 2] = '\c0%4 stops %1 dead with the arc welder.';
$DeathMessage[$DamageType::Shocklance, 3] = '\c0%4 eliminates %1 in close combat.';
$DeathMessage[$DamageType::Shocklance, 4] = '\c0%4 ruins %1\'s day with one zap of a arc welder.';

$DeathMessage[$DamageType::Mine, 0] = '\c0%4 reminds %1 that a mine is a terrible thing to waste.';
$DeathMessage[$DamageType::Mine, 1] = '\c0%1 doesn\'t see %4\'s mine in time.';
$DeathMessage[$DamageType::Mine, 2] = '\c0%4 gives %1 a piece of %6 mine.';
$DeathMessage[$DamageType::Mine, 3] = '\c0%1 puts their foot on %4\'s mine.';
$DeathMessage[$DamageType::Mine, 4] = '\c0%1 stepped on %4\'s toe-popper.';

$DeathMessage[$DamageType::SatchelCharge, 0] = '\c0%4 buys %1 a ticket to the moon.';  //satchel charge only
$DeathMessage[$DamageType::SatchelCharge, 1] = '\c0%4 blows %1 into low orbit.';
$DeathMessage[$DamageType::SatchelCharge, 2] = '\c0%4 makes %1 a hugely explosive offer.';
$DeathMessage[$DamageType::SatchelCharge, 3] = '\c0%4 turns %1 into a cloud of satchel-vaporized armor.';
$DeathMessage[$DamageType::SatchelCharge, 4] = '\c0%4\'s satchel charge leaves %1 nothin\' but smokin\' boots.';

$DeathMessage[$DamageType::MitziBlast, 0] = '\c0%1 is the victim of %4\'s Mitzi Blast.';
$DeathMessage[$DamageType::MitziBlast, 1] = '\c0%4 jams some Mitzi in %1\'s face.';
$DeathMessage[$DamageType::MitziBlast, 2] = '\c0%4 stops %1 dead with the Mitzi Blast Cannon.';
$DeathMessage[$DamageType::MitziBlast, 3] = '\c0%4 says to %1: "Mitzi. Kills bugs dead!"';
$DeathMessage[$DamageType::MitziBlast, 4] = '\c0%4\'s Mitzi gives %1 a bad case of static cling.';

$DeathMessage[$DamageType::Sniper, 0] = '\c0%4\'s crack shot puts a hole in %1.';
$DeathMessage[$DamageType::Sniper, 1] = '\c0%4 gives %1 a shot right between the eyes.';
$DeathMessage[$DamageType::Sniper, 2] = '\c0%4 stops %1 dead with a sniper round.';
$DeathMessage[$DamageType::Sniper, 3] = '\c0%4 takes a pot shot at %1.';
$DeathMessage[$DamageType::Sniper, 4] = '\c0%4 picks off %1 from afar.';

$DeathMessage[$DamageType::Railgun, 0] = '\c0%4 wrecks %1 hard with a solid rail.';
$DeathMessage[$DamageType::Railgun, 1] = '\c0%4 pops %1 with a railgun.';
$DeathMessage[$DamageType::Railgun, 2] = '\c0%1 gets extra friendly with %4\'s rail.';
$DeathMessage[$DamageType::Railgun, 3] = '\c0%4\'s railgun gives %1 a hot rail injection.';
$DeathMessage[$DamageType::Railgun, 4] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessage[$DamageType::StingerRail, 0] = '\c0%4 wrecks %1 hard with a solid rail.';
$DeathMessage[$DamageType::StingerRail, 1] = '\c0%4 pops %1 with a railgun.';
$DeathMessage[$DamageType::StingerRail, 2] = '\c0%1 gets extra friendly with %4\'s rail.';
$DeathMessage[$DamageType::StingerRail, 3] = '\c0%4\'s railgun gives %1 a hot rail injection.';
$DeathMessage[$DamageType::StingerRail, 4] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessage[$DamageType::MitziRail, 0] = '\c0%4 wrecks %1 hard with a Mitzi-infused rail.';
$DeathMessage[$DamageType::MitziRail, 1] = '\c0%4 pops %1 with a railgun.';
$DeathMessage[$DamageType::MitziRail, 2] = '\c0%1 gets extra friendly with %4\'s Mitzi-infused rail.';
$DeathMessage[$DamageType::MitziRail, 3] = '\c0%4\'s railgun gives %1 a hot rail injection.';
$DeathMessage[$DamageType::MitziRail, 4] = '\c0%1 has a very brief and rough fling with %4\'s railgun.';

$DeathMessage[$DamageType::Poison, 0] = '\c0%1 dies with green flesh open from %4\'s poison.';
$DeathMessage[$DamageType::Poison, 1] = '\c0%4 gives %1 a deadly dose of poison.';
$DeathMessage[$DamageType::Poison, 2] = '\c0%4 gives %1 "the clap".';
$DeathMessage[$DamageType::Poison, 3] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessage[$DamageType::Poison, 4] = '\c0%4 poisons %1.';

$DeathMessage[$DamageType::AutoCannon, 0] = '\c0%4 shreds %1 up with an AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 2] = '\c0%4 commands %1 to dance the AutoCannon dance.';
$DeathMessage[$DamageType::AutoCannon, 3] = '\c0%1 suffers a serious hosing from %4\'s AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 4] = '\c0%4 blows chunks away from %1 with %6 autocannon.';

$DeathMessage[$DamageType::Annihalator, 0] = '\c0%4 nukes %1 with %6 Annihalator.';
$DeathMessage[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by %4\'s nearby Annihalator.';
$DeathMessage[$DamageType::Annihalator, 2] = '\c0%1 felt the sun\'s burning rays from %4\'s Annihalator barrage.';
$DeathMessage[$DamageType::Annihalator, 3] = '\c0%4 nukes %1 with %6 Annihalator.';
$DeathMessage[$DamageType::Annihalator, 4] = '\c0%4 blows away %1 with %6 Annihalator.';

$DeathMessage[$DamageType::OutdoorDepTurret, 0] = '\c0%4\'s spike neatly drills %1.';
$DeathMessage[$DamageType::OutdoorDepTurret, 1] = '\c0%1 dies under %4\'s spike love.';
$DeathMessage[$DamageType::OutdoorDepTurret, 2] = '\c0%1 is chewed up by %4\'s spike.';
$DeathMessage[$DamageType::OutdoorDepTurret, 3] = '\c0%1 feels the burn from %4\'s spike.';
$DeathMessage[$DamageType::OutdoorDepTurret, 4] = '\c0%1 is nailed by %4\'s spike.';

$DeathMessage[$DamageType::OutdoorDepTurretF, 0] = '\c0%4\'s flare spike neatly drills %1.';
$DeathMessage[$DamageType::OutdoorDepTurretF, 1] = '\c0%1 dies under %4\'s flare spike burn.';
$DeathMessage[$DamageType::OutdoorDepTurretF, 2] = '\c0%1 is torched by %4\'s flare spike.';
$DeathMessage[$DamageType::OutdoorDepTurretF, 3] = '\c0%1 feels the burn from %4\'s flare spike.';
$DeathMessage[$DamageType::OutdoorDepTurretF, 4] = '\c0%1 gets fireballed by %4\'s flare spike.';

$DeathMessage[$DamageType::MeteorCannon, 0] = '\c0%1 eats a big helping of %4\'s Meteor Cannon blast.';
$DeathMessage[$DamageType::MeteorCannon, 1] = '\c0%4 plants a meteor bolt in %1\'s chest.';
$DeathMessage[$DamageType::MeteorCannon, 2] = '\c0%1 fails to evade %4\'s deft Meteor Cannon barrage.';
$DeathMessage[$DamageType::MeteorCannon, 3] = '\c0%1 helpfully jumps into %4\'s Meteor Cannon.';
$DeathMessage[$DamageType::MeteorCannon, 4] = '\c0%1 gets knocked into next week from %4\'s Meteor Cannon.';

$DeathMessage[$DamageType::MagIonReactor, 0] = '\c0%1 is annihilated by %4\'s Dreadnought reactor overload.';
$DeathMessage[$DamageType::MagIonReactor, 1] = '\c0%1 is blown away by %4\'s reactor overload.';
$DeathMessage[$DamageType::MagIonReactor, 2] = '\c0%1 contacts %4\'s exploding Dreadnought and goes boom!';
$DeathMessage[$DamageType::MagIonReactor, 3] = '\c0Ouch! %1 + %4\'s overloaded Dreadnought reactor = Dead %1.';
$DeathMessage[$DamageType::MagIonReactor, 4] = '\c0%1 gets a fatal booster shot from %4\'s reactor explosion.';

$DeathMessage[$DamageType::Turbocharger, 0] = '\c0%1 is annihilated by %4\'s electrical overload.';
$DeathMessage[$DamageType::Turbocharger, 1] = '\c0%1 is blown away by %4\'s power core breach.';
$DeathMessage[$DamageType::Turbocharger, 2] = '\c0%1 gives %4 a shocking hug!';
$DeathMessage[$DamageType::Turbocharger, 3] = '\c0%1 didn\'t expect %4\'s sudden outburst.';
$DeathMessage[$DamageType::Turbocharger, 4] = '\c0%1 is instantly vaporized by %4\'s core overload.';

$DeathMessage[$DamageType::SubspaceMagnet, 0] = '\c0%1 eats a big helping of %4\'s fusion pulse bolt.';
$DeathMessage[$DamageType::SubspaceMagnet, 1] = '\c0%4 plants a fusion pulse bolt in %1\'s belly.';
$DeathMessage[$DamageType::SubspaceMagnet, 2] = '\c0%1 fails to evade %4\'s deft fusion pulse barrage.';
$DeathMessage[$DamageType::SubspaceMagnet, 3] = '\c0%1 eats a big helping of %4\'s fusion pulse bolt.';
$DeathMessage[$DamageType::SubspaceMagnet, 4] = '\c0%4 plants a fusion pulse in %1\'s belly.';

$DeathMessage[$DamageType::PlasmaCannon, 0] = '\c0%4 broils %1 with the plasma cannon.';
$DeathMessage[$DamageType::PlasmaCannon, 1] = '\c0%4 asks %1: "Need a light?"';
$DeathMessage[$DamageType::PlasmaCannon, 2] = '\c0%4 entices %1 to try a faceful of blue-hot plasma.';
$DeathMessage[$DamageType::PlasmaCannon, 3] = '\c0%4 introduces %1 to the electro-plasmic induction dance.';
$DeathMessage[$DamageType::PlasmaCannon, 4] = '\c0%4 burns and fries %1 with the Plasma Cannon.';

$DeathMessage[$DamageType::BlasterRifle, 0] = '\c0%1 lunches on a blaster rifle sandwich, courtesy of %4.';
$DeathMessage[$DamageType::BlasterRifle, 1] = '\c0%4\'s blaster rifle barrage blaster catches %1 with %3 pants down.';
$DeathMessage[$DamageType::BlasterRifle, 2] = '\c0%1 gets drilled big-time by %4\'s blaster rifle.';
$DeathMessage[$DamageType::BlasterRifle, 3] = '\c0%1 gets railed apart from %4\'s expert blaster rifle usage.';
$DeathMessage[$DamageType::BlasterRifle, 4] = '\c0%4\'s blaster rifle riddles holes in %1 with ease.';

$DeathMessage[$DamageType::Bolter, 0] = '\c0%4 rips %1 up with the bolter.';
$DeathMessage[$DamageType::Bolter, 1] = '\c0%4 happily chews %1 into pieces with %6 bolter.';
$DeathMessage[$DamageType::Bolter, 2] = '\c0%4 administers a dose of depleted uranium to %1.';
$DeathMessage[$DamageType::Bolter, 3] = '\c0%1 suffers a serious hosing from %4\'s bolter.';
$DeathMessage[$DamageType::Bolter, 4] = '\c0%4 bestows the blessings of %6 bolter on %1.';

$DeathMessage[$DamageType::RotaryCannon, 0] = '\c0%4 rips %1 up with the rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 1] = '\c0%4 happily chews %1 into pieces with %6 rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 2] = '\c0%4 administers a dose of reinforced lead to %1.';
$DeathMessage[$DamageType::RotaryCannon, 3] = '\c0%1 suffers a serious hosing from %4\'s rotary cannon.';
$DeathMessage[$DamageType::RotaryCannon, 4] = '\c0%4 bestows the blessings of %6 rotary cannon on %1.';

$DeathMessage[$DamageType::MPDisruptor, 0] = '\c0%4 destroys %1 with a MegaPulse Disruptor.';
$DeathMessage[$DamageType::MPDisruptor, 1] = '\c0%4 pulses %1 to death.';
$DeathMessage[$DamageType::MPDisruptor, 2] = '\c0%1 gets a pointer in disruptor use from %4.';
$DeathMessage[$DamageType::MPDisruptor, 3] = '\c0%4 fatally decimates %1 with %6 MegaPulse Disruptor.';
$DeathMessage[$DamageType::MPDisruptor, 4] = '\c0%4 unleashes a terminal disruptor barrage into %1.';

$DeathMessage[$DamageType::PBW, 0] = '\c0%4 gives %1 the smackdown with %3 Particle Beam Cannon.';
$DeathMessage[$DamageType::PBW, 1] = '\c0%4\'s Particle Beam Cannon punches that fatal hole through %1.';
$DeathMessage[$DamageType::PBW, 2] = '\c0%4\'s Particle Beam Cannon beams %1 well... apart really.';
$DeathMessage[$DamageType::PBW, 3] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessage[$DamageType::PBW, 4] = '\c0%4 makes %1\'s day with a zap of %6 Particle Beam Cannon.';

$DeathMessage[$DamageType::PBCDevistator, 0] = '\c0%4 gives %1 the smackdown with %3 Particle Beam Cannon.';
$DeathMessage[$DamageType::PBCDevistator, 1] = '\c0%4\'s Particle Beam Cannon punches that fatal hole through %1.';
$DeathMessage[$DamageType::PBCDevistator, 2] = '\c0%4\'s Particle Beam Cannon beams %1 well... apart really.';
$DeathMessage[$DamageType::PBCDevistator, 3] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessage[$DamageType::PBCDevistator, 4] = '\c0%4 makes %1\'s day with a zap of %6 Particle Beam Cannon.';

$DeathMessage[$DamageType::PBCQuantum, 0] = '\c0%4 gives %1 the smackdown with %3 Particle Beam Cannon.';
$DeathMessage[$DamageType::PBCQuantum, 1] = '\c0%4\'s Particle Beam Cannon punches that fatal hole through %1.';
$DeathMessage[$DamageType::PBCQuantum, 2] = '\c0%4\'s Particle Beam Cannon beams %1 well... apart really.';
$DeathMessage[$DamageType::PBCQuantum, 3] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessage[$DamageType::PBCQuantum, 4] = '\c0%4 makes %1\'s day with a zap of %6 Particle Beam Cannon.';

$DeathMessageHeadshotCount = 3;
$DeathMessageHeadshot[$DamageType::Laser, 0] = '\c0%4 drills right through %1\'s braincase with %6 laser.';
$DeathMessageHeadshot[$DamageType::Laser, 1] = '\c0%4 pops %1\'s head like a cheap balloon.';
$DeathMessageHeadshot[$DamageType::Laser, 2] = '\c0%1 loses %3 head over %4\'s laser skill.';

// z0dd - ZOD, 8/25/02. Added Lance rear shot messages
$DeathMessageRearshotCount = 3;
$DeathMessageRearshot[$DamageType::ShockLance, 0] = '\c0%4 delivers a backdoor Lance to %1.';
$DeathMessageRearshot[$DamageType::ShockLance, 1] = '\c0%4 sends high voltage up %1\'s bum.';
$DeathMessageRearshot[$DamageType::ShockLance, 2] = '\c0%1 receives %4\'s rear-entry Lance attack.';
 
//These used when a player is run over by a vehicle
$DeathMessageVehicleCount = 5;
$DeathMessageVehicle[0] = '\c0%4 says to %1: "Hey! You scratched my paint job!".';
$DeathMessageVehicle[1] = '\c0%1 acquires that run-down feeling from %4.';
$DeathMessageVehicle[2] = '\c0%4 shows %1 %6 new ride.';
$DeathMessageVehicle[3] = '\c0%1 makes a painfully close examination of %4\'s front bumper.';
$DeathMessageVehicle[4] = '\c0%1\'s messy death leaves a mark on %4\'s vehicle finish.';

$DeathMessageVehicleCrashCount = 5;
$DeathMessageVehicleCrash[ $DamageType::Crash, 0 ] = '\c0%1 fails to eject in time.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 1 ] = '\c0%1 becomes one with his vehicle dashboard.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 2 ] = '\c0%1 drives under the influence of death.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 3 ] = '\c0%1 makes a perfect three hundred point landing.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 4 ] = '\c0%1 heroically pilots his vehicle into something really, really hard.';

$DeathMessageVehicleFriendlyCount = 3;
$DeathMessageVehicleFriendly[0] = '\c0%1 gets in the way of a friendly vehicle.';
$DeathMessageVehicleFriendly[1] = '\c0Sadly, a friendly vehicle turns %1 into roadkill.';
$DeathMessageVehicleFriendly[2] = '\c0%1 becomes an unsightly ornament on a team vehicle\'s hood.';

$DeathMessageVehicleUnmannedCount = 3;
$DeathMessageVehicleUnmanned[0] = '\c0%1 gets in the way of a runaway vehicle.';
$DeathMessageVehicleUnmanned[1] = '\c0An unmanned vehicle kills the pathetic %1.';
$DeathMessageVehicleUnmanned[2] = '\c0%1 is struck down by an empty vehicle.';

//These used when a player is killed by a nearby equipment explosion
$DeathMessageExplosionCount = 3;
$DeathMessageExplosion[0] = '\c0%1 was killed by exploding equipment!';
$DeathMessageExplosion[1] = '\c0%1 stood a little too close to the action!';
$DeathMessageExplosion[2] = '\c0%1 learns how to be collateral damage.';

//These used when an automated turret kills an  enemy player
$DeathMessageTurretKillCount = 3;
$DeathMessageTurretKill[$DamageType::PlasmaTurret, 0] = '\c0%1 is killed by a plasma turret.';
$DeathMessageTurretKill[$DamageType::PlasmaTurret, 1] = '\c0%1\'s body now marks the location of a plasma turret.';
$DeathMessageTurretKill[$DamageType::PlasmaTurret, 2] = '\c0%1 is fried by a plasma turret.';

$DeathMessageTurretKill[$DamageType::AATurret, 0] = '\c0%1 is killed by an AA turret.';
$DeathMessageTurretKill[$DamageType::AATurret, 1] = '\c0%1 is shot down by an AA turret.';
$DeathMessageTurretKill[$DamageType::AATurret, 2] = '\c0%1 takes fatal flak from an AA turret.';

$DeathMessageTurretKill[$DamageType::ElfTurret, 0] = '\c0%1 is killed by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ElfTurret, 1] = '\c0%1 is zapped by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ElfTurret, 2] = '\c0%1 is short-circuited by an ELF turret.';

$DeathMessageTurretKill[$DamageType::MortarTurret, 0] = '\c0%1 is killed by a mortar turret.';
$DeathMessageTurretKill[$DamageType::MortarTurret, 1] = '\c0%1 enjoys a mortar turret\'s attention.';
$DeathMessageTurretKill[$DamageType::MortarTurret, 2] = '\c0%1 is blown to kibble by a mortar turret.';

$DeathMessageTurretKill[$DamageType::MissileTurret, 0] = '\c0%1 is killed by a missile turret.';
$DeathMessageTurretKill[$DamageType::MissileTurret, 1] = '\c0%1 is shot down by a missile turret.';
$DeathMessageTurretKill[$DamageType::MissileTurret, 2] = '\c0%1 is blown away by a missile turret.';

$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 is killed by a clamp turret.';
$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 1] = '\c0%1 gets burned by a clamp turret.';
$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 2] = '\c0A clamp turret eliminates %1.';

$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 0] = '\c0A spike turret neatly drills %1.';
$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 1] = '\c0%1 gets taken out by a spike turret.';
$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 2] = '\c0%1 dies under a spike turret\'s love.';

$DeathMessageTurretKill[$DamageType::SentryTurret, 0] = '\c0%1 didn\'t see that Sentry turret, but it saw %2...';
$DeathMessageTurretKill[$DamageType::SentryTurret, 1] = '\c0%1 needs to watch for Sentry turrets.';
$DeathMessageTurretKill[$DamageType::SentryTurret, 2] = '\c0%1 now understands how Sentry turrets work.';


//used when a player is killed by a teammate controlling a turret
$DeathMessageCTurretTeamKillCount = 1;	
$DeathMessageCTurretTeamKill[$DamageType::PlasmaTurret, 0] = '\c0%4 TEAMKILLED %1 with a plasma turret!';

$DeathMessageCTurretTeamKill[$DamageType::AATurret, 0] = '\c0%4 TEAMKILLED %1 with an AA turret!';

$DeathMessageCTurretTeamKill[$DamageType::ELFTurret, 0] = '\c0%4 TEAMKILLED %1 with an ELF turret!';

$DeathMessageCTurretTeamKill[$DamageType::MortarTurret, 0] = '\c0%4 TEAMKILLED %1 with a mortar turret!';

$DeathMessageCTurretTeamKill[$DamageType::MissileTurret, 0] = '\c0%4 TEAMKILLED %1 with a missile turret!';

$DeathMessageCTurretTeamKill[$DamageType::IndoorDepTurret, 0] = '\c0%4 TEAMKILLED %1 with a clamp turret!';

$DeathMessageCTurretTeamKill[$DamageType::OutdoorDepTurret, 0] = '\c0%4 TEAMKILLED %1 with a spike turret!';

$DeathMessageCTurretTeamKill[$DamageType::SentryTurret, 0] = '\c0%4 TEAMKILLED %1 with a sentry turret!';

$DeathMessageCTurretTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 in a bombastic explosion of raining death.';

$DeathMessageCTurretTeamKill[$DamageType::BellyTurret, 0] = '\c0%4 TEAMKILLED %1 by annihilating him from a belly turret.';

$DeathMessageCTurretTeamKill[$DamageType::TankChainGun, 0] = '\c0%4 TEAMKILLED %1 with his tank\'s chaingun.';

$DeathMessageCTurretTeamKill[$DamageType::TankMortar, 0] = '\c0%4 TEAMKILLED %1 by lobbing the BIG green death from a tank.'; 

$DeathMessageCTurretTeamKill[$DamageType::ShrikeBlaster, 0] = '\c0%4 TEAMKILLED %1 by strafing from a Shrike.';

$DeathMessageCTurretTeamKill[$DamageType::MPBMissile, 0] = '\c0%4 TEAMKILLED %1 when the MPB locked onto him.';

 

//used when a player is killed by an uncontrolled, friendly turret
$DeathMessageCTurretAccdtlKillCount = 1; 
$DeathMessageCTurretAccdtlKill[$DamageType::PlasmaTurret, 0] = '\c0%1 got in the way of a plasma turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::AATurret, 0] = '\c0%1 got in the way of an AA turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::ELFTurret, 0] = '\c0%1 got in the way of an ELF turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::MortarTurret, 0] = '\c0%1 got in the way of a mortar turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::MissileTurret, 0] = '\c0%1 got in the way of a missile turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 got in the way of a clamp turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::OutdoorDepTurret, 0] = '\c0%1 got in the way of a spike turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::SentryTurret, 0] = '\c0%1 got in the way of a Sentry turret!';


//these messages for owned or controlled turrets
$DeathMessageCTurretKillCount = 3;
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 0] = '\c0%4 torches %1 with a plasma turret!'; 
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 1] = '\c0%4 fries %1 with a plasma turret!';
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 2] = '\c0%4 lights up %1 with a plasma turret!';

$DeathMessageCTurretKill[$DamageType::AATurret, 0] = '\c0%4 shoots down %1 with an AA turret.';
$DeathMessageCTurretKill[$DamageType::AATurret, 1] = '\c0%1 gets shot down by %1\'s AA turret.';
$DeathMessageCTurretKill[$DamageType::AATurret, 2] = '\c0%4 takes out %1 with an AA turret.';

$DeathMessageCTurretKill[$DamageType::ElfTurret, 0] = '\c0%1 gets zapped by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ElfTurret, 1] = '\c0%1 gets barbecued by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ElfTurret, 2] = '\c0%1 gets shocked by ELF gunner %4.';

$DeathMessageCTurretKill[$DamageType::MortarTurret, 0] = '\c0%1 is annihilated by %4\'s mortar turret.';
$DeathMessageCTurretKill[$DamageType::MortarTurret, 1] = '\c0%1 is blown away by %4\'s mortar turret.';
$DeathMessageCTurretKill[$DamageType::MortarTurret, 2] = '\c0%1 is pureed by %4\'s mortar turret.';

$DeathMessageCTurretKill[$DamageType::MissileTurret, 0] = '\c0%4 shows %1 a new world of pain with a missile turret.';
$DeathMessageCTurretKill[$DamageType::MissileTurret, 1] = '\c0%4 pops %1 with a missile turret.';
$DeathMessageCTurretKill[$DamageType::MissileTurret, 2] = '\c0%4\'s missile turret lights up %1\'s, uh, ex-life.';

$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 is chewed up and spat out by %4\'s clamp turret.';
$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 1] = '\c0%1 is knocked out by %4\'s clamp turret.';
$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 2] = '\c0%4\'s clamp turret drills %1 nicely.';

$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 0] = '\c0%1 is chewed up by %4\'s spike turret.';
$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 1] = '\c0%1 feels the burn from %4\'s spike turret.';
$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 2] = '\c0%1 is nailed by %4\'s spike turret.';

$DeathMessageCTurretKill[$DamageType::SentryTurret, 0] = '\c0%4 caught %1 by surprise with a turret.';
$DeathMessageCTurretKill[$DamageType::SentryTurret, 1] = '\c0%4\'s turret took out %1.';
$DeathMessageCTurretKill[$DamageType::SentryTurret, 2] = '\c0%4 blasted %1 with a turret.';

$DeathMessageCTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.'; 
$DeathMessageCTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.'; 
$DeathMessageCTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DeathMessageCTurretKill[$DamageType::BellyTurret, 0] = '\c0%1 eats a big helping of %4\'s belly turret bolt.';
$DeathMessageCTurretKill[$DamageType::BellyTurret, 1] = '\c0%4 plants a belly turret bolt in %1\'s belly.';
$DeathMessageCTurretKill[$DamageType::BellyTurret, 2] = '\c0%1 fails to evade %4\'s deft bomber strafing.';

$DeathMessageCTurretKill[$DamageType::TankChainGun, 0] = '\c0%1 enjoys the rich, metallic taste of %4\'s tank slug.';
$DeathMessageCTurretKill[$DamageType::TankChainGun, 1] = '\c0%4\'s tank chaingun plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::TankChainGun, 2] = '\c0%1 receives a stellar exit wound from %4\'s tank slug.';

$DeathMessageCTurretKill[$DamageType::TankMortar, 0] = '\c0Whoops! %1 + %4\'s tank mortar = Dead %1.';
$DeathMessageCTurretKill[$DamageType::TankMortar, 1] = '\c0%1 learns the happy explosion dance from %4\'s tank mortar.';
$DeathMessageCTurretKill[$DamageType::TankMortar, 2] = '\c0%4\'s tank mortar has a blast with %1.';

$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 0] = '\c0%1 dines on a Shrike blaster sandwich, courtesy of %4.';
$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 1] = '\c0The blaster of %4\'s Shrike turns %1 into finely shredded meat.';
$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 2] = '\c0%1 gets drilled big-time by the blaster of %4\'s Shrike.';

$DeathMessageCTurretKill[$DamageType::MPBMissile, 0] = '\c0%1 intersects nicely with %4\'s MPB Missile.';
$DeathMessageCTurretKill[$DamageType::MPBMissile, 1] = '\c0%4\'s MPB Missile makes armored chowder out of %1.';
$DeathMessageCTurretKill[$DamageType::MPBMissile, 2] = '\c0%1 has a brief, explosive fling with %4\'s MPB Missile.';

$DeathMessageTurretSelfKillCount = 3;
$DeathMessageTurretSelfKill[0] = '\c0%1 somehow kills %2self with a turret.';
$DeathMessageTurretSelfKill[1] = '\c0%1 apparently didn\'t know the turret was loaded.';
$DeathMessageTurretSelfKill[2] = '\c0%1 helps his team by killing himself with a turret.';



