function WalkerDamageAmp::applyEffect(%this, %obj, %armorName)
{
    %obj.damageBuffFactor += 0.1;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerDamageAmp", "Damage Amplifier", "Increases damage done by 10%", $Enhancement::Stackable, $VehicleList::Walkers);
