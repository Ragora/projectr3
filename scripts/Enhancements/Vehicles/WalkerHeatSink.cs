function WalkerHeatSink::applyEffect(%this, %obj, %armorName)
{
    %obj.heatDrain += 2;
}

Enhancement.registerEnhancement($EnhancementType::Vehicle, "WalkerHeatSink", "Additional Heat Sink", "Dissipates an additional 4k/s heat", $Enhancement::Stackable, $VehicleList::Walkers);
