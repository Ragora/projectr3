Enhancement.registerEnhancement($EnhancementType::Weapon, "DefaultWeaponEnh", "Empty", "Empty slot", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
Enhancement.registerEnhancement($EnhancementType::Armor, "DefaultArmorEnh", "Empty", "Empty slot", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
Enhancement.registerEnhancement($EnhancementType::Pack, "DefaultPackEnh", "Empty", "Empty slot", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
Enhancement.registerEnhancement($EnhancementType::Vehicle, "DefaultVehicleEnh", "Empty", "Empty slot", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
