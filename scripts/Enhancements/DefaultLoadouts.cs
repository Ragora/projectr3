// Disruptor
$DefaultWeaponEnh["Blaster", 0] = "DisruptorBlue";
$DefaultWeaponEnh["Blaster", 1] = "DisruptorGreen";

// Thermal Projector
$DefaultWeaponEnh["Plasma", 0] = "TPFlurry";
$DefaultWeaponEnh["Plasma", 1] = "TPDense";

// Vulcan
$DefaultWeaponEnh["Chaingun", 0] = "VulcanExplosiveShot";
$DefaultWeaponEnh["Chaingun", 1] = "VulcanHollowPoint";

// Disc
$DefaultWeaponEnh["Disc", 0] = "DiscPower";
$DefaultWeaponEnh["Disc", 1] = "DiscMitzi";

// AutoCannon
$DefaultWeaponEnh["GrenadeLauncher", 0] = "ACFlak";
$DefaultWeaponEnh["GrenadeLauncher", 1] = "ACClusterShot";
$DefaultWeaponEnh["GrenadeLauncher", 2] = "ACPhosphorIN";

// Laser Assault Rifle
$DefaultWeaponEnh["SniperRifle", 0] = "LaserBeamLaser";
$DefaultWeaponEnh["SniperRifle", 1] = "LaserMitziDeathRay";

// Arc Welder
$DefaultWeaponEnh["ELFGun", 0] = "ArcLightningLance";
$DefaultWeaponEnh["ELFGun", 1] = "ArcGrappleBeam";

// Mortar
$DefaultWeaponEnh["Mortar", 0] = "MortarImpactShell";
$DefaultWeaponEnh["Mortar", 1] = "MortarSBMIRV";
$DefaultWeaponEnh["Mortar", 2] = "MortarEMPShell";

// Missile Launcher
$DefaultWeaponEnh["MissileLauncher", 0] = "RLStreakMissile";
$DefaultWeaponEnh["MissileLauncher", 1] = "RLBusterMissile";
$DefaultWeaponEnh["MissileLauncher", 2] = "RLEMPMissile";

$DefaultWeaponEnh["LaserMissileLauncher", 0] = "RLStreakMissile";
$DefaultWeaponEnh["LaserMissileLauncher", 1] = "RLBusterMissile";
$DefaultWeaponEnh["LaserMissileLauncher", 2] = "RLEMPMissile";

$DefaultWeaponEnh["RocketLauncher", 0] = "RLStreakMissile";
$DefaultWeaponEnh["RocketLauncher", 1] = "RLBusterMissile";
$DefaultWeaponEnh["RocketLauncher", 2] = "RLEMPMissile";

// Mitzi Blast Cannon
$DefaultWeaponEnh["MitziBlastCannon", 0] = "MitziLightBlast";
$DefaultWeaponEnh["MitziBlastCannon", 1] = "MitziDenseBlast";
$DefaultWeaponEnh["MitziBlastCannon", 2] = "MitziKineticThrust";
$DefaultWeaponEnh["MitziBlastCannon", 3] = "MitziAnnihilator";

// Compressed Spike Rifle
$DefaultWeaponEnh["SpikeRifle", 0] = "SpikeBurstFire";
$DefaultWeaponEnh["SpikeRifle", 1] = "SpikeAutoFire";

// Railgun
$DefaultWeaponEnh["Railgun", 0] = "RailgunStinger";
$DefaultWeaponEnh["Railgun", 1] = "RailgunAuger";

// Sagittarius
$DefaultWeaponEnh["Sagittarius", 0] = "SagiDD";
$DefaultWeaponEnh["Sagittarius", 1] = "SagiExplBug";

// Subspace Magnet
$DefaultWeaponEnh["SubspaceMagnet", 0] = "";
$DefaultWeaponEnh["SubspaceMagnet", 1] = "";

// Bolter
$DefaultWeaponEnh["Bolter", 0] = "BolterPhosphorShot";
$DefaultWeaponEnh["Bolter", 1] = "BolterMitziShot";
$DefaultWeaponEnh["Bolter", 2] = "BolterMissileShot";

// Plasma Cannon
$DefaultWeaponEnh["PlasmaCannon", 0] = "PCFlamer";
$DefaultWeaponEnh["PlasmaCannon", 1] = "PCIonicPlasma";

// Ripper
$DefaultWeaponEnh["Ripper", 0] = "RipperDiscStorm";
$DefaultWeaponEnh["Ripper", 1] = "RipperSmartDisc";

// MegaPulse Disruptor
$DefaultWeaponEnh["MPDisruptor", 0] = "MPDPulsar";
$DefaultWeaponEnh["MPDisruptor", 1] = "MPDStarstorm";

// Blaster Rifle
$DefaultWeaponEnh["BlasterRifle", 0] = "BlasterBurstFire";
$DefaultWeaponEnh["BlasterRifle", 1] = "BlasterPowerShot";

// Particle Beam Cannon
$DefaultWeaponEnh["ParticleBeamCannon", 0] = "PBCDevistator";
$DefaultWeaponEnh["ParticleBeamCannon", 1] = "PBCQuantum";
