function GyanCapacitor::applyEffect(%this, %obj, %armorName)
{
    %obj.gyanMax += 2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "GyanCapacitor", "Gyan Capacitor", "+2 Maximum Gyan Power", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
