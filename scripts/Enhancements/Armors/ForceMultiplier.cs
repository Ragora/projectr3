function ForceMultiplier::applyEffect(%this, %obj, %armorName)
{
    %obj.damageBuffFactor += 0.05;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "ForceMultiplier", "Force Multiplier", "+5% Damage", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
