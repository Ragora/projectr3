function EMResistPlating::applyEffect(%this, %obj, %armorName)
{
    %obj.armorDamageFactor[$DamageGroupMask::Energy] -= 0.2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "EMResistPlating", "Nullifier Plating", "20% ElectroMagnetic damage reduction", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
