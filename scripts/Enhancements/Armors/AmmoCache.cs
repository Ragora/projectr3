function ArmorAmmoCache::applyEffect(%this, %obj, %armorName)
{
    %obj.maxAmmoCapacityFactor += 0.1;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "ArmorAmmoCache", "Ammo Storage", "+10% All Ammo Storage", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
