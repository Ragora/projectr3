function ExplositeResistPlating::applyEffect(%this, %obj, %armorName)
{
    %obj.armorDamageFactor[$DamageGroupMask::Explosive] -= 0.2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "ExplositeResistPlating", "Reactive Armoring", "20% Explosive damage reduction", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
