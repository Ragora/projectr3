function PowerRelay::applyEffect(%this, %obj, %armorName)
{
    %obj.rechargeRateFactor += 0.05;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "PowerRelay", "Power Relay", "+5% Recharge Rate", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
