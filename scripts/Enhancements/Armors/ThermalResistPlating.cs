function PlasmaResistPlating::applyEffect(%this, %obj, %armorName)
{
    %obj.armorDamageFactor[$DamageGroupMask::Plasma] -= 0.2;
}

Enhancement.registerEnhancement($EnhancementType::Armor, "PlasmaResistPlating", "Intercooler", "20% Thermal damage reduction", $Enhancement::Stackable | $Enhancement::AllArmorCompat, $Enhancement::All);
