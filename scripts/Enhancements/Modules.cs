function Enhancement::registerModule(%this, %name, %item)
{
    if($NameToInv[%name] !$= "")
        return;
        
    if(%this.moduleCount $= "")
        %this.moduleCount = 0;
        
    $InvArmorMod[%this.moduleCount] = %name;
    $NameToInv[%name] = %item;
    
    %this.moduleItem[%this.moduleCount] = %item;
    %this.moduleCount++;
}

execDir("Enhancements/Modules");
