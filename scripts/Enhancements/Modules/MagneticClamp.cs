datablock ItemData(MagneticClamp)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function MagneticClamp::onApply(%data, %obj)
{
     %obj.magneticClamp = true;
}

function MagneticClamp::onUnApply(%data, %obj)
{
     %obj.magneticClamp = false;
}

Enhancement.registerModule("Magnetic Grips", "MagneticClamp");
