datablock ItemData(NonNewtonianPadding)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function NonNewtonianPadding::onApply(%data, %obj)
{
    %obj.bonusWeight += 10;
    %obj.nonNewtonian = true;
}

function NonNewtonianPadding::onUnApply(%data, %obj)
{
    %obj.nonNewtonian = false;
}

Enhancement.registerModule("Non-Newtonian Padding", "NonNewtonianPadding");
