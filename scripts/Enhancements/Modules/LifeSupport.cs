datablock ItemData(LifeSupport)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function triggerLifeSupportCD(%obj)
{
    %obj.lifeSupportTicking = true;
    %obj.lifeSupport = false;
    
    LifeSupport.schedule(1000, "onTick", %obj);
    bottomPrint(%obj.client, "Emergency Life Support initiated! 30 seconds to death.", 2, 1);
}

function LifeSupport::onTick(%this, %obj)
{
    if(!%obj.isDead && %obj.lifeSupportTicking)
    {
        %obj.applyInternalDamage(1, %obj, $DamageType::LifeSupportSuicide);
        zapEffect(%obj, "FXRedShift");
        
        %this.schedule(1000, "onTick", %obj);
    }
}

function LifeSupport::onApply(%data, %obj)
{
    %obj.lifeSupport = true;
}

function LifeSupport::onUnApply(%data, %obj)
{
    %obj.lifeSupport = false;
    %obj.lifeSupportTicking = false;
}

Enhancement.registerModule("Life Support System", "LifeSupport");
