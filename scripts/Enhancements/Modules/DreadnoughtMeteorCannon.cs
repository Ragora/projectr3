datablock ShapeBaseImageData(MeteorCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   offset = "-0.5 -0.1 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   mass = 10;
   
   usesEnergy = true;
   fireEnergy = 125;
   minEnergy = 5;

   projectile = MeteorCannonBlast;
   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateSequence[1]                 = "Deploy";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 2.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
//   stateDirection[3]                = true;
//   stateSound[3]                    = MeteorCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;
   stateScript[3]                   = "onFire";
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(MeteorCannonDImage) : MeteorCannonImage
{
   usesEnergy = true;
   fireEnergy = 125;
   minEnergy = 5;

   shapeFile = "turret_elf_large.dts";
   offset = "-0.5 0.3 0.3";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
};

function MeteorCannonImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(MortarDryFireSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        createRemoteProjectile("LinearFlareProjectile", "BluePowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

        %obj.applyKick(-500);
        %obj.play3D(AAFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (3000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (1500 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(MortarDryFireSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function MeteorCannonDImage::onFire(%data, %obj, %slot)
{
    %time = getSimTime();

    if(%obj.fireTimeout[%data, %data.item] > %time)
    {
        %obj.play3D(MortarDryFireSound);
        return;
    }

    %p = Parent::onFire(%data, %obj, %slot);

    if(%p)
    {
        createRemoteProjectile("LinearFlareProjectile", "BluePowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

        %obj.applyKick(-500);
        %obj.play3D(AAFireSound);
        %obj.fireTimeout[%data, %data.item] = %time + (3000 / (1 + %obj.rateOfFire[%data.item]));
    }
    else
    {
        %obj.fireTimeout[%data, %data.item] = %time + (1500 / (1 + %obj.rateOfFire[%data.item]));
        %obj.play3D(MortarDryFireSound);
    }

    %obj.setImageTrigger(%slot, false);
}

function updateMeteorAmmo(%obj, %data)
{
    commandToClient(%obj.client, 'setInventoryHudAmount', 0, -1);
}

datablock ItemData(DreadMeteorCannon)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function DreadMeteorCannon::onApply(%data, %obj)
{
    %obj.play3D("AssaultTurretActivateSound");

    if(%obj.client.race $= "Bioderm")
        %obj.mountImage(MeteorCannonDImage, $ShoulderSlot);
    else
        %obj.mountImage(MeteorCannonImage, $ShoulderSlot);

    schedule(32, %obj, "updateMeteorAmmo", %obj, MeteorCannonImage);

    %obj.shoulderWeapon = true;
}

function DreadMeteorCannon::onUnApply(%data, %obj)
{
    %obj.unmountImage($ShoulderSlot);
    %obj.shoulderWeapon = false;
}

Enhancement.registerModule("SM: Meteor Cannon", "DreadMeteorCannon");
