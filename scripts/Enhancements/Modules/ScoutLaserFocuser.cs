datablock ItemData(ScoutLaserFocuser)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

	pickUpName = "abstract";
};

function ScoutLaserFocuser::onApply(%data, %obj)
{
     %obj.laserFocus = true;
}

function ScoutLaserFocuser::onUnApply(%data, %obj)
{
     %obj.laserFocus = false;
}

Enhancement.registerModule("Scout Laser Focuser", "ScoutLaserFocuser");
