datablock ItemData(Nosferatu)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

	pickUpName = "abstract";
};

function Nosferatu::onApply(%data, %obj)
{
    %obj.nosferatu = true;
}

function Nosferatu::onUnApply(%data, %obj)
{
    %obj.nosferatu = false;
}

Enhancement.registerModule("Nosferatu", "Nosferatu");
