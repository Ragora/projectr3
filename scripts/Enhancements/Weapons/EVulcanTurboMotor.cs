function EVulcanTurboMotor::applyEffect(%this, %obj, %item)
{
    %obj.rateOfFire[%item] += 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EVulcanTurboMotor", "Turbo Motor", "Replaces the Vulcan spin motor, +50% RoF, uses 1 KJ/shot", $Enhancement::AllArmorCompat, $WeaponsList::Chaingun);
