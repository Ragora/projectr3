datablock GrenadeProjectileData(ACPhosphorINShell)
{
   projectileShapeName = "grenade_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.9;
   damageRadius        = 12.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::Plasma;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 90;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   bubbleEmitTime      = 1.0;

   sound               = GrenadeProjectileSound;
   explosion           = "HeavyPlasmaExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";
   velInheritFactor    = 0.85; // z0dd - ZOD, 3/30/02. Was 0.5
   splash              = GrenadeSplash;

   baseEmitter         = GrenadeSmokeEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   grenadeElasticity = 0.01; // z0dd - ZOD, 9/13/02. Was 0.35
   grenadeFriction   = 0.99;
   numBounces        = 0;
   armingDelayMS     = 0; // z0dd - ZOD, 9/13/02. Was 1000
   muzzleVelocity    = 300.00; // z0dd - ZOD, 3/30/02. GL projectile is faster. Was 47.00
   //drag = 0.1; // z0dd - ZOD, 3/30/02. No drag.
   gravityMod        = 0.6; // z0dd - ZOD, 5/18/02. Make GL projectile heavier, less floaty
};

function ACPhosphorINShell::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    if(%targetObject.isPlayer())
        StatusEffect.applyEffect("BurnEffect", %targetObject, %projectile.instigator);
}

function ACPhosphorIN::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
        
    return %energy SPC %ammoUse SPC %gyan;
}

function ACPhosphorIN::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "ACPhosphorINShell", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ACPhosphorIN", "Phosphor IN Shell", "Instantly sets fire to enemy armors - 2 ammo, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::GrenadeLauncher);
