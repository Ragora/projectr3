
datablock ParticleData(DiscExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.3 0.3 0.66 1.0";
   colors[1]     = "0.3 0.3 0.66 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(DiscExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "DiscExplosionParticle";
};

datablock ShockwaveData(LDiscShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 10;
   acceleration = 20.0;
   lifetimeMS = 900;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.0 0.50";
   colors[1] = "0.4 0.4 1.0 0.25";
   colors[2] = "0.4 0.4 1.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ExplosionData(LDiscSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 100;
   offset = 4.0;
   playSpeed = 1.0;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "1.0  1.0  1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 50;
   offset = 2.0;
   playSpeed = 0.75;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.75 0.75 0.75";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = -1.0;
   playSpeed = 0.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LrgDiscExplosion)
{
   soundProfile   = discExpSound;

   shockwave = LDiscShockwave;
   shockwaveOnTerrain = false;

   subExplosion[0] = LDiscSubExplosion1;
   subExplosion[1] = LDiscSubExplosion2;
   subExplosion[2] = LDiscSubExplosion3;

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 400;
   particleRadius = 3.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock LinearProjectileData(PowerDiscProjectile)
{
   projectileShapeName = "disc.dts";
   scale               = "1.5 1.5 1.5";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Disc;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 3500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 150;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   sound 				= discProjectileSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 95;
   wetVelocity       = 55;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 32;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function DiscPower::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;
    
    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function DiscPower::onInit(%this)
{
    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
}

function DiscPower::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.applyKick(-2000);
    return createProjectile("LinearProjectile", "PowerDiscProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DiscPower", "Power Disc", "Double the damage of the standard Disc, 2 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Disc);
