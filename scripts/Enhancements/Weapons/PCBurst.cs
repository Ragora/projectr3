function PCBurst::validateUse(%this, %obj, %data)
{
    %ammoUse = 60;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function PCBurst::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = 0;
    %item = %obj.getMountedImage($WeaponSlot).item;
    %obj.setHeat(1.0);
    
    for(%i = 0; %i < 6; %i++)
    {
        %p = createProjectile(%data.projectileType, "PlasmaBolt", vectorSpread(%vector, 21), %pos, %obj, %slot, %obj);
        %p.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%item];
    }

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PCBurst", "Plasma Burst", "High cost (60 ammo), high damage spreadfire shot", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::PlasmaCannon);
