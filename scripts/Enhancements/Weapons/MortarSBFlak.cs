datablock ShockwaveData(SabotImpactShockwave)
{
   width = 0.5;
   numSegments = 24;
   numVertSegments = 24;
   velocity = 5;
   acceleration = 5;
   lifetimeMS = 500;
   height = 0.1;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ParticleData(SabotDebrisParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  100;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = "0.8 0.8 0.8 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.225;
   sizes[1]      = 0.3;
   sizes[2]      = 0.325;
   times[0]      = 0.0;
   times[1]      = 0.25;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SabotDebrisEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;
   thetaMax         = 40.0;
   particles = "SabotDebrisParticle";
};

datablock DebrisData(SabotDebris)
{
   emitters[0] = SabotDebrisEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.75;
   lifetimeVariance = 0.25;

   numBounces = 1;
};

datablock ParticleData(SabotSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.4;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SabotSparkEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 4;
   ejectionVelocity = 8;
   velocityVariance = 4.0;
   thetaMax         = 80;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "SabotSparks";
};

datablock ExplosionData(SabotExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 2.0;
   faceViewer = true;

   emitter[0] = SabotSparkEmitter;
   soundProfile = DebrisFXExplosionSound;
   
   debris = SabotDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 82;
   debrisNum = 3;
   debrisVelocity = 6.0;
   debrisVelocityVariance = 2.0;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(ExpChaingunExplosion) : SabotExplosion
{
   shockwave = SabotImpactShockwave;
};

datablock TracerProjectileData(SabotFrag1)
{
   scale = "5.0 5.0 5.0";
   doDynamicClientHits = true;
   projectileShapeName = "grenade_projectile.dts";

   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 14;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 150;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SabotExplosion";
   underwaterExplosion = "SabotExplosion";
   splash              = ChaingunSplash;
   sound 			     = ChaingunProjectile;

   dryVelocity       = 25;
   wetVelocity       = 25;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 250;
   lifetimeMS        = 250;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 500;

   tracerLength    = 0.11;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/blasterBolt";
	tracerTex[1]  	 = "special/blasterBoltCross";
	tracerWidth     = 0.01;
   crossSize       = 0.01;
   crossViewAng    = 0.7;
   renderCross     = false;
};

datablock TracerProjectileData(SabotFrag2) : SabotFrag1
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   fizzleTimeMS      = 500;
   lifetimeMS        = 500;
   explodeOnDeath    = true;
};

datablock TracerProjectileData(SabotFrag3) : SabotFrag1
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   fizzleTimeMS      = 750;
   lifetimeMS        = 750;
   explodeOnDeath    = true;
};

datablock TracerProjectileData(SabotFrag4) : SabotFrag1
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 40;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = true;
};

datablock GrenadeProjectileData(StarburstSabotMortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mortar;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "MissileExplosion";
//   underwaterExplosion = "MissileExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
//   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.99;
   armingDelayMS     = 250;

   gravityMod        = 1.1;
   muzzleVelocity    = 95.5;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

datablock LinearFlareProjectileData(StarburstSabotMortarTriggered)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   explosion           = "MissileExplosion";
//   underwaterExplosion = "UnderwaterMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function MortarSBFlak::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MortarSBFlak::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "StarburstSabotMortarShot", %vector, %pos, %obj, %slot, %obj);
}

function MortarSBFlak::onTriggerFire(%this, %obj, %data)
{
    %rnd = getRandom(21, 32);

    for(%i = 0; %i < %rnd; %i++)
    {
        %suffix = getRandom(1, 4);
        createRemoteProjectile("TracerProjectile", "SabotFrag"@%suffix, vectorSpread("0, 0, 1", 1000), %obj.lastProjectile.position, %slot, %obj);
    }
        
    transformProjectile(%obj.lastProjectile, "LinearFlareProjectile", "StarburstSabotMortarTriggered", %obj.lastProjectile.position, %obj.lastProjectile.initialDirection);
}

function MortarSBFlak::onInit(%this)
{
    %this.hasTrigger = true;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MortarSBFlak", "Starburst Flak Shell", "Trigger charge, sprays explosive chunks 6-25m - 2 ammo, 30 power.", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mortar);
