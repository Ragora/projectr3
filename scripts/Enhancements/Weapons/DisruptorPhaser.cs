//----------------------------------------------------
//  Explosion
//----------------------------------------------------

datablock LinearFlareProjectileData(PhaserDisruptorBolt)
{
   directDamage        = 0.0;
   directDamageType    = $DamageType::Blaster;
   hasDamageRadius     = true;
   indirectDamage      = 0.6;
   damageRadius        = 3.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::Blaster;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Blaster;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 60;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::IgnoreShields | $Projectile::IgnoreReflections | $Projectile::IgnoreReflectorField | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "PhaserExplosion";
   underwaterExplosion = "UnderwaterPhaserExplosion";
   
   scale              = "3.5 3.5 3.5";

   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 100;

   numFlares         = 18;
   size              = 1.25;
   flareColor        = "0.8 0.0 0.6";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 1.0 0.25";
};

function DisruptorPhaser::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 36;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";
        
    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function DisruptorPhaser::onInit(%this)
{
    %this.modeSwitchSound = "ElfFireWetSound";
    %this.fireSound = "SentryTurretFireSound";
}

function DisruptorPhaser::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "PhaserDisruptorBolt", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "DisruptorPhaser", "Phaser Disruptor", "Ignores shields, 36 energy, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Blaster);
