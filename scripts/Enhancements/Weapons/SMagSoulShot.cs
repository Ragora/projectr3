function SMagSoulShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 0;
    %gyan = 1;

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    // dirty hack: have to check for gyan twice, so refund them their gyan early
    %obj.gyanLevel++;

    return %energy SPC %ammoUse SPC %gyan;
}

function SMagSoulShot::onInit(%this)
{
    %this.fireSound = "PBLFireSound";
}

function SMagSoulShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %chargeLevel = %obj.getInventory(SubspaceCapacitor);
    %projName = mFloor(%chargeLevel * 0.1);

    if(%projName < 1)
        %projName = 1;
    else if(%projName > 13)
        %projName = 13;

    %p = createProjectile("LinearFlareProjectile", "FusionPulseSize"@%projName, %obj.getMuzzleVector(0), %obj.getMuzzlePoint(0), %obj, 0, %obj);

    %p.presetDamage = true;
    %p.soulShot = true;
    %p.chargeLevel = %chargeLevel;
    %p.smRadius = %chargeLevel * 0.125;
    %p.smDamage = %chargeLevel * 0.025;
    %p.smKick = %chargeLevel * 20;

    return %p;
}

//Enhancement.registerEnhancement($EnhancementType::Weapon, "SMagSoulShot", "Soul Shot", "Goodness gracious, great balls of Plasma - uses 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::SubspaceMagnet);
