datablock TracerProjectileData(ACClusterShell)
{
   doDynamicClientHits = true;

   directDamage        = 0.22;
   directDamageType    = $DamageType::Bullet;
   explosion           = "ACChaingunExplosion";
   splash              = ChaingunSplash;

   kickBackStrength  = 0;
   sound 				= ChaingunProjectile;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bullet; // change to AC-specific one
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 22;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   dryVelocity       = 500.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 400;
   lifetimeMS        = 400;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 400;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.35;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function ACClusterShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 3;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function ACClusterShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = 0;

    for(%i = 0; %i < 8; %i++)
        %p = createProjectile("TracerProjectile", "ACClusterShell", vectorSpread(%vector, 24), %pos, %obj, %slot, %obj);
        
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ACClusterShot", "AP Cluster Shot", "Painful shrapnel blast of metal, uses 3 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::GrenadeLauncher);
