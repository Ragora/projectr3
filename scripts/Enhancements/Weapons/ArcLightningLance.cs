// projectile located in weapons/shockLance.cs

function ArcLightningLance::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 30;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function ArcLightningLance::onInit(%this)
{
//    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
    %this.projectile = "BasicShocker";
}

function ArcLightningLance::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.play3D(ShockLanceDryFireSound);
    
    %dmgMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
    %everythingElseMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType | $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType;
    %mask = %dmgMasks | %everythingElseMask;
    %cast = castRay(%pos, %vector, 10, %mask, %obj);
    
    if(%cast)
    {
        if(%cast.hitObj.getType() & %dmgMasks)
        {
            %p = createLance(%this.projectile, %vector, %pos, %obj, %slot, %obj, %cast.hitObj);
            %cast.hitObj.applyImpulse(%cast.hitPos, vectorScale(%vector, %this.projectile.impulse));
            %obj.playAudio(0, ShockLanceHitSound);

            %damageMultiplier = 1.0;
            
            if(%cast.hitObj.isPlayer())
            {
                // Now we see if we hit from behind...
                %forwardVec = %cast.hitObj.getForwardVector();
                %objDir2D   = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
                %objPos     = %cast.hitPos;
                %dif        = VectorSub(%objPos, %pos);
                %dif        = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
                %dif        = VectorNormalize(%dif);
                %dot        = VectorDot(%dif, %objDir2D);

                // 120 Deg angle test...
                // 1.05 == 60 degrees in radians
                if (%dot >= mCos(1.05))
                {
                   // Rear hit
                   %damageMultiplier = 3.0;
                }
            }

            %cast.hitObj.lastHitFlags = %this.projectile.flags;
            %cast.hitObj.damage(%obj, %cast.hitPos, %damageMultiplier * %this.projectile.mdDamageAmount[0] * %p.damageBuffFactor, %this.projectile.mdDeathMessageSet, %p, %this.projectile.mdDamageType[0]);
        }
    }
    else
    {
        createLance(%this.projectile, %vector, %pos, %obj, %slot, %obj, 0);
        %obj.playAudio(0, ShockLanceMissSound);
        %obj.useEnergy(-27);
    }

    %obj.fireTimeout[%data, %this] = getSimTime() + 2500;
    %obj.schedule(2000, "play3d", "ShockLanceReloadSound");
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ArcLightningLance", "Lightning Arc", "300% damage from behind, reach out and touch someone", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::ELFGun);
