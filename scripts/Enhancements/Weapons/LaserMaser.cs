// Laser Maser Phaser Blazer? lol

datablock SniperProjectileData(LaserMaserBeam)
{
   directDamage        = 1.5;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Microwave;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Microwave;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   maxRifleRange       = 300;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   hasFalloff = true;
   optimalRange = 150;
   falloffRange = 250;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(LaserMaserBeamExtended) : LaserMaserBeam
{
   maxRifleRange       = 600;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Microwave;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;
   
   hasFalloff = true;
   optimalRange = 300;
   falloffRange = 600;
   falloffDamagePct = 0.25;
};

datablock TargetProjectileData(LaserMaserBeamFX)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 300;
   beamColor           	= "1.0 0.749 0.0";

   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock TargetProjectileData(LaserMaserBeamFXExtended) : LaserMaserBeamFX
{
   maxRifleRange       	= 600;
};

function LaserMaser::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 75;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function LaserMaser::onInit(%this)
{
    %this.fireSound = "underwaterDiscExpSound";
}

function LaserMaser::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    if(%obj.laserFocus)
    {
        %p = createProjectile("SniperProjectile", "LaserMaserBeamExtended", %vector, %pos, %obj, %slot, %obj);
        %q = createProjectile("TargetProjectile", "LaserMaserBeamFXExtended", %vector, %pos, %obj, %slot, %obj);
    }
    else
    {
        %p = createProjectile("SniperProjectile", "LaserMaserBeam", %vector, %pos, %obj, %slot, %obj);
        %q = createProjectile("TargetProjectile", "LaserMaserBeamFX", %vector, %pos, %obj, %slot, %obj);
    }

    %p.setEnergyPercentage(0.25);
    %q.schedule(500, "delete");

    %obj.fireTimeout[%data, %this] = getSimTime() + (2000 / (1 + %obj.rateOfFire[%data.item]));

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "LaserMaser", "Maser", "Microwave beam, reach out and torch someone! 1 GP, 150m optimal range", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::SniperRifle);
