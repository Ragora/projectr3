datablock LinearProjectileData(SolidRailDD)
{
//   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.2;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::Sniper;
   kickBackStrength    = 900;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Sniper;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 20;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 2.0;

   explosion           = "ChaingunExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = SagittariusSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1500;
   wetVelocity       = 750;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function SolidRailDD::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    if(%targetObject.isPlayer() || %targetObject.isVehicle())
        StatusEffect.applyEffect("DDBugEffect", %targetObject, %projectile.instigator);
}

function SagiDD::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 20;
    %gyan = 1;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function SagiDD::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "SolidRailDD", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "SagiDD", "Defender Designator", "Tags an enemy, boosting damage done to it by 50%, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Sagittarius);
