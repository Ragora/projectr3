function ESMagInjector::applyEffect(%this, %obj, %item)
{
    %obj.smPrecharge = true;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ESMagInjector", "Charge Injector", "Converts 30 Power into 20 charge when triggered", $Enhancement::AllArmorCompat, $WeaponsList::SubspaceMagnet);
