datablock LinearProjectileData(AugerRail)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 3.0;
   hasDamageRadius     = false;
   directDamageType    = $DamageType::RailAuger;
   kickBackStrength    = 5000;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Railgun;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 200;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound | $Projectile::Piercing;
   ticking             = false;
   headshotMultiplier  = 2.0;

   explosion           = "MitziShotgunExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function AugerRail::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    %projectile.hitSomething = true;

    if(%data.flags & $Projectile::Piercing)
    {
        %pierced = castRay(%position, %projectile.initialDirection, 2, $TypeMasks::PlayerObjectType);

        if(%pierced)
            %data.onCollision(%data, %projectile, %pierced.hitObj, %modifier, %pierced.hitPos, %normal);
    }

    if(isObject(%targetObject))
    {
        %modifier = 1;

        if(%targetObject.isPlayer())
        {
            if(%projectile.flags & $Projectile::CanHeadshot)
            {
                if(firstWord(%targetObject.getDamageLocation(%position)) $= "head")
                {
                    %targetObject.getOwnerClient().headShot = 1;
                    %modifier = %data.headshotMultiplier;
                }
            }
            
            if(%targetObject.isShielded != true && !%targetObject.nonNewtonian)
                %targetObject.applyInternalDamage(mFloor(8 * %modifier * %projectile.damageBuffFactor), %projectile.instigator, $DamageType::RailAuger);

            // Set flag here, deal with it in player scripts (more points for MA kills too)
            if(%projectile.flags & $Projectile::CountMAs)
            {
                %ground = castRay(%position, "0 0 -1", $g_MACountHeight, $TypeMasks::TerrainObjectType | $TypeMasks::BuildingObjectType);

                if(!%ground)
                    %targetObject.MAVicim = true;
            }
        }

        %targetObject.lastHitFlags = %data.flags;

        %targetObject.damage(%projectile.instigator, %position, %data.directDamage * %modifier * %projectile.damageBuffFactor, %data.directDamageType);
    }
}

function RailgunAuger::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 150;
    %gyan = 2;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function RailgunAuger::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "AugerRail", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RailgunAuger", "Auger Rail", "Penetrates up to 1.5m through a solid surface - 2 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Railgun);
