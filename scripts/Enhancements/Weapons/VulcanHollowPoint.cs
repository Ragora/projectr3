datablock AudioProfile(HollowPointBulletHit)
{
   filename    = "fx/weapons/cg_soft1.wav"; // 2
   description = AudioClose3d;
   preload = true;
};

datablock ParticleData(EMChaingunSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.26 0.36 0.9 1.0";
   colors[1]     = "0.26 0.36 0.9 1.0";
   colors[2]     = "0.1 0.36 1.0 0.0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.2;
   sizes[2]      = 0.05;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(EMChaingunSparkEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 7;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "EMChaingunSparks";
};

datablock ExplosionData(EMChaingunExplosion)
{
   soundProfile = HollowPointBulletHit;

   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = EMChaingunSparkEmitter;

   faceViewer           = false;
};

datablock TracerProjectileData(HollowPointBullet)
{
   doDynamicClientHits = true;

   directDamage        = 0.1;
   directDamageType    = $DamageType::Bullet;
   explosion           = "EMChaingunExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bullet;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 18;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 500.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function HollowPointBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    if(%targetObject.isPlayer())
        Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

function VulcanHollowPoint::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function VulcanHollowPoint::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("TracerProjectile", "HollowPointBullet", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "VulcanHollowPoint", "Hollow Point Shell", "Does devistating damage to player armors only", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Chaingun);
