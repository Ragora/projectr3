function EBolterTurboMotor::applyEffect(%this, %obj, %item)
{
    %obj.rateOfFire[%item] += 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EBolterTurboMotor", "Turbo Motor", "Replaces the Bolter spin motor, +50% RoF, uses 2 KJ/shot", $Enhancement::AllArmorCompat, $WeaponsList::Bolter);
