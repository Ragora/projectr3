datablock ParticleData(FireboltFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -180.0;
   spinRandomMax = 180.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 4.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltFireEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 2;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "FireboltFireParticle";
};

datablock ParticleData(FireboltSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltSmokeEmitter)
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "FireboltSmokeParticle";
};

datablock GrenadeProjectileData(DenseFireballProjectile)
{
   projectileShapeName = "plasmabolt.dts";
   emitterDelay        = -1;
   scale               = "2.0 2.0 2.0";
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.75;
   damageRadius        = 12.0;
   radiusDamageType    = $DamageType::Plasma;
   kickBackStrength    = 1750;
   bubbleEmitTime      = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Plasma;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

//   sound               = GrenadeProjectileSound;
   explosion           = "HeavyPlasmaExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";
   velInheritFactor    = 0.85;
   splash              = GrenadeSplash;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   grenadeElasticity = 0.01;
   grenadeFriction   = 0.9;
   armingDelayMS     = 250;
   muzzleVelocity    = 75.00;
   gravityMod        = 1.9;
   
   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "1 0.75 0.25";
};

function DenseFireballProjectile::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    if(%targetObject.isPlayer() && getRandom() > 1 - (0.08 + (%targetObject.getHeat() * 0.375)))
        StatusEffect.applyEffect("BurnEffect", %targetObject, %projectile.instigator);
}

function TPDense::validateUse(%this, %obj, %data)
{
    %ammoUse = 20;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function TPDense::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "DenseFireballProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "TPDense", "Flametongue", "Heavier plasma fireball, arcs when fired, uses 20 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Plasma);
