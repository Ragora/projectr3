function ERifleCoilBarrel::applyEffect(%this, %obj, %item)
{
    %obj.spreadFactor[%item] -= 0.333;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ERifleCoilBarrel", "Gauss Coil Barrel", "Reduces spread by 33%", $Enhancement::AllArmorCompat, $WeaponsList::BlasterRifle | $WeaponsList::Spike);
