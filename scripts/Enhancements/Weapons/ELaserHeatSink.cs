function ELaserHeatSink::applyEffect(%this, %obj, %item)
{
    %obj.rateOfFire[%item] += 0.25;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ELaserHeatSink", "Laser Heat Sink", "Increases RoF by 25%", $Enhancement::AllArmorCompat, $WeaponsList::SniperRifle);
