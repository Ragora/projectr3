function EAmmoBuffer::applyEffect(%this, %obj, %item)
{
    %ammo = %item.image.ammo;
    
    if(%ammo $= "")
        return;
    
    %amount = mCeil(%obj.maxBaseInventory(%ammo) * 0.5);
    %obj.ammoCapacityBonus[%ammo] += %amount;
    %obj.incInventory(%ammo, %amount);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EAmmoBuffer", "Ammo Buffer", "Increases ammo capacity by 50%", $Enhancement::AllArmorCompat, $Enhancement::AmmoWeapons);
