datablock LinearFlareProjectileData(MitziBoosterProjectile)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.25;
   damageRadius        = 10.0;
   kickBackStrength    = 7500;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 35;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Sonic;
   mdDamageAmount[1]   = 35;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   explosion           = "ConcussionGrenadeExplosion";
   underwaterExplosion = "ConcussionGrenadeExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 200.0;
   wetVelocity       = 300.0;       // faster uw
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = -1;
   numFlares         = 0;
   flareColor        = "1 0.6 0.7";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0 0 0";
   
   ticking = true;
};

function MitziBoosterProjectile::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount % 6 == 0)
        createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %proj.initialDirection, %proj.position, 0, %proj.instigator);
}

function MitziKineticThrust::validateUse(%this, %obj, %data)
{
    %ammoUse = 25;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MitziKineticThrust::onInit(%this)
{
//    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
}

function MitziKineticThrust::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearFlareProjectile", "MitziBoosterProjectile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MitziKineticThrust", "Mitzi Kinetic Thrust", "Converts Mitzi energy into Kinetic force, 25 cap", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mitzi);
