datablock ParticleData(StreakMissileSmokeParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.02;
    windCoefficient = 1;
    inheritedVelFactor = 0.1;
    constantAcceleration = 0;
    lifetimeMS = 1800;
    lifetimeVarianceMS = 200;
    useInvAlpha = 0;
    spinRandomMin = -90;
    spinRandomMax = 90;
    textureName = "special/expFlare.png";
    times[0] = 0;
    times[1] = 0.1;
    times[2] = 0.28;
    times[3] = 1;
    colors[0] = "0.228346 0.450000 1.000000 1.000000";
    colors[1] = "0.228346 0.350000 0.750000 0.800000";
    colors[2] = "0.500000 0.500000 0.500000 1.000000";
    colors[3] = "0.300000 0.300000 0.300000 0.000000";
    sizes[0] = 1.0;
    sizes[1] = 1.5;
    sizes[2] = 2.0;
    sizes[3] = 2.2;
};

datablock ParticleEmitterData(StreakMissileSmokeEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 1.5;
    velocityVariance = 0.3;
    ejectionOffset =   0.1;
    thetaMin = 0;
    thetaMax = 25;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "StreakMissileSmokeParticle";
};

datablock SeekerProjectileData(StreakMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 0.72;
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 1000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 70;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = StreakMissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 100.0;
   maxVelocity         = 300.0;
   turningSpeed        = 150.0;
   acceleration        = 200.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 90;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 10;

   flareDistance = 50;
   flareAngle    = 15;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = true;
};

function RLStreakMissile::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function RLStreakMissile::onInit(%this)
{
    %this.modeFireSound = "MILFireSound";
}

function RLStreakMissile::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("SeekerProjectile", "StreakMissile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RLStreakMissile", "Streak Missile", "2x speed, trades damage for accuracy and speed", $WeaponsList::MissileLauncher | $WeaponsList::LaserMissileLauncher | $WeaponsList::RocketLauncher);
