datablock ExplosionData(PhosphorExplosion)
{
//   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 44;
   particleRadius = 0.5;
   faceViewer = true;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock TracerProjectileData(BolterPhosphorBolt) : BolterBolt
{
   explosion           = "PhosphorExplosion";

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Bolter;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 10;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 20;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   kickBackStrength  = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   tracerLength    = 12.0;
   tracerAlpha     = false;
   tracerMinPixels = 8;
   tracerColor     = 224.0/255.0 @ " " @ 227.0/255.0 @ " " @ 159.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.45;
   crossViewAng    = 0.990;
   renderCross     = true;
};

function BolterPhosphorShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 2;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";
        
    return %energy SPC %ammoUse SPC %gyan;
}

function BolterPhosphorShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("TracerProjectile", "BolterPhosphorBolt", vectorSpread(%obj.getMuzzleVector(%slot), 4 * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item])), %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "BolterPhosphorShot", "Phosphor Tracer Bolt", "Bolts ignite on collision - 2 ammo, 2 power per shot", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Bolter);
