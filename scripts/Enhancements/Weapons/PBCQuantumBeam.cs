datablock SniperProjectileData(QuantumBeam)
{
   directDamage        = 1.667;
   velInheritFactor    = 1.0;
   faceViewer          = true;
//   sound 			   = PBWProjectileSound;
   explosion           = "PBWExplosion";
   splash              = PlasmaSplash;
   directDamageType    = $DamageType::PBCQuantum;
   kickBackStrength    = 2334;
   
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PBW;
   mdDamageTypeCount   = 3;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 33;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 33;
   mdDamageRadius[1]   = false;
   mdDamageType[2]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[2]   = 100;
   mdDamageRadius[2]   = false;
   
   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   maxRifleRange       = 350;
   rifleHeadMultiplier = 1.25;
   beamColor           = "1.0 1.0 1.0";
   fadeTime            = 1.0;

   hasFalloff = true;
   optimalRange = 175;
   falloffRange = 350;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.7;
   endBeamWidth 	     = 0.075;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 45.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 32.0;
   pulseLength         = 0.1;

   lightRadius         = 10.0;
   lightColor          = "1.0 1.0 1.0";

   textureName[0]   = "special/flare";
   textureName[1]   = "special/redbump2";
   textureName[2]   = "special/redbump2";
   textureName[3]   = "special/redbump2";
   textureName[4]   = "special/redbump2";
   textureName[5]   = "special/redbump2";
   textureName[6]   = "special/redflare";
   textureName[7]   = "special/redbump2";
   textureName[8]   = "special/redbump2";
   textureName[9]   = "special/redbump2";
   textureName[10]   = "special/redflare";
   textureName[11]   = "special/flare";
};

function PBCQuantum::onInit(%this)
{
    %this.data = ParticleBeamCannonImage;
}

function PBCQuantum::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 300;
    %gyan = 4;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";
        
    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function PBCQuantum::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "PBCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %this.schedule(300, "fragmentBeam", %obj, %slot);
    %this.schedule(425, "fragmentBeam", %obj, %slot);
    %this.schedule(550, "fragmentBeam", %obj, %slot);
    %obj.fireTimeout[%data, %this] = getSimTime() + (4000 / (1 + %obj.rateOfFire[%data.item]));

    return %p;
}

function PBCQuantum::fragmentBeam(%this, %obj, %slot)
{
    %data = %this.data;
    %proj = createProjectile(%data.projectileType, "QuantumBeam", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %proj.setEnergyPercentage(0.8);
    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PBCQuantum", "Quantum Tunneler Beam", "Completely ignores shields, 4 GP, 2 ammo, 175m optimal range", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::ParticleBeamCannon);
