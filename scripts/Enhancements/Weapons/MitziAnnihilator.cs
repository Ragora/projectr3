datablock ParticleData(AnExplosionSmoke)
{
   dragCoeffiecient     = 1;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 7500;
   lifetimeVarianceMS   = 7499;

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.7 0.7 0.7 0.65";
   colors[2]     = "0.25 0.25 0.25 0.375";
   colors[3]     = "0.1 0.1 0.1 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 12.0;
   sizes[2]      = 15.0;
   sizes[3]      = 19.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(AnSmokeEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;

   ejectionVelocity = 16;
   velocityVariance = 4;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 500;

   particles = "AnExplosionSmoke";
};

datablock ParticleData(AnSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 15;
   lifetimeMS           = 3500; //1500
   lifetimeVarianceMS   = 500;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AnSparkEmitter)
{
   ejectionPeriodMS = 1; //1
   periodVarianceMS = 0;
   ejectionVelocity = 5; //16
   velocityVariance = 0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90; //180
   phiReferenceVel  = 0;
   phiVariance      = 360; //360
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 425; //750
   particles = "AnSparks";
   //ejectionPeriodMS = 1;  //meh..testing
   //periodVarianceMS = 0;
   //ejectionVelocity = 32;
   //velocityVariance = 12;
   //ejectionOffset   = 1.0;
   //thetaMin         = 0;
   //thetaMax         = 180;
   //phiReferenceVel  = 0;
   //phiVariance      = 360;
   //overrideAdvances = false;
   //orientParticles  = true;
   //lifetimeMS       = 1500;
   //particles = "AnSparks";
};

datablock ParticleData( AnDebrisSmokeParticle )
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.05;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.1 0.1 0.1 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 4;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( AnDebrisSmokeEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "AnDebrisSmokeParticle";
};

datablock DebrisData(AnFireballDebris)
{
   emitters[0] = AnDebrisSmokeEmitter;
   emitters[1] = DebrisFireEmitter;

   explosion = DebrisExplosion;
   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 100.0;
   lifetimeVariance = 50.0;

   numBounces = 1;
   bounceVariance = 1;
};

datablock ShockwaveData(AnShockwave)
{
   width = 15;
   numSegments = 32;
   numVertSegments = 8;
   velocity = 48;
   acceleration = 50.0;
   lifetimeMS = 1800;
   height = 5.0;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.8 0.6 1.0";
   colors[1] = "0.8 0.5 0.2 0.6";
   colors[2] = "1.0 0.3 0.3 0.0";
};

datablock ShockwaveData(AnUWShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 24.0;
   acceleration = 40.0;
   lifetimeMS = 1250;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "0.25 0.2 0.9 0.35";
   colors[2] = "0 0.4 0.75 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(ANSubExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 0;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
   subExplosion = VLGSpikeExplosion;
};

datablock ExplosionData(ANUWSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
   subExplosion = VLGSpikeExplosion;
};

datablock ExplosionData(AnExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.1;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;

   sizes[0] = "30 30 30";
   sizes[1] = "30 30 30";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = AnSmokeEmitter;
   emitter[2] = AnSparkEmitter;
   shockwave = AnShockwave;

   debris = AnFireballDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 80;
   debrisNum = 30;
   debrisNumVariance = 10;
   debrisVelocity = 40.0;
   debrisVelocityVariance = 15.0;

   subExplosion[0] = ANSubExplosion;

   shakeCamera = true;
   camShakeFreq = "10.0 10.0 10.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 40.0;
};

datablock ExplosionData(AnUWExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.1;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;

   sizes[0] = "30 30 30";
   sizes[1] = "30 30 30";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = ANUWSubExplosion;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 30.0;
};

datablock LinearFlareProjectileData(MitziAnniProjectile)
{
   scale               = "50.0 50.0 50.0";
   faceViewer          = true;
   directDamage        = 0;
   directDamageType    = $DamageType::Annihalator;
   hasDamageRadius     = true;
   indirectDamage      = 7.5;
   damageRadius        = 45;
   kickBackStrength    = 32500;
   radiusDamageType    = $DamageType::Annihalator;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Annihalator;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 750;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 250;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   explosion           = "AnExplosion";
   underwaterExplosion = "AnUWExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 30.0;
   wetVelocity       = 10.0;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   size[0]           = 1;
   size[1]           = 70;
   size[2]           = 25;
   numFlares         = 50;
   flareColor        = "0.5 0.5 0.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/blasterboltcross.png";

   sound      = HAPCFlyerThrustSound;

   hasLight    = false;
   lightRadius = 8.0;
   lightColor  = "0.7 0.7 0.2";

   ticking = true;
};

function MitziAnniProjectile::onTick(%this, %proj)
{
    if(%proj.hitSomething)
    {
        %proj.vortexAura.stop();
        %proj.vortexAura.destroy();
        return;
    }
    
    Parent::onTick(%this, %proj);
}

function MitziAnnihilator::validateUse(%this, %obj, %data)
{
    %ammoUse = 100;
    %energy = 0;
    %gyan = 3;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function MitziAnnihilator::onInit(%this)
{
//    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
}

function MitziAnnihilator::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "MitziAnniProjectile", %vector, %pos, %obj, %slot, %obj);
    %aura = Aura.create("VortexMortar");
    %aura.attachToObject(%p);
    %aura.schedule(1300, "start");
    %p.vortexAura = %aura;
    
    %obj.applyKick(-3000);
    
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MitziAnnihilator", "Mitzi Annihilator", "100 cap, 3 gyan - here comes the death singularity!", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mitzi);
