datablock EnergyProjectileData(SpikeBurstShot) : SpikeSingleShot
{
   directDamage        = 0.3;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::OutdoorDepTurret;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 45;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
};

function SpikeBurstFire::validateUse(%this, %obj, %data)
{
    %ammoUse = 3;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function SpikeBurstFire::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.fireTimeout[%data.item, %this] = getSimTime() + (1250 / (1 + %obj.rateOfFire[%data.item]));
    %obj.schedule(32, "setImageTrigger", 0, false);
    %obj.schedule(1050, "play3D", GrenadeDryFireSound);
    
    // Since this is only 3 shots, and it has to be done specially... will go a different route
    schedule(150, %obj, "SBurstFire", %obj, %slot);
    schedule(300, %obj, "SBurstFire", %obj, %slot);
    return SBurstFire(%obj, %slot);
}

function SBurstFire(%obj, %slot, %pos)
{
    %vector = vectorSpread(%obj.getMuzzleVector(%slot), 2 * (%obj.spreadFactorBase + %obj.spreadFactor[%data.item]));
    %pos = %obj.getMuzzlePoint(%slot);
    %obj.play3D("OBLFireSound");
    return createProjectile("EnergyProjectile", "SpikeBurstShot", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "SpikeBurstFire", "Spike Burst Fire", "Fires off a 3 round burst, uses 3 ammo", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Spike);
