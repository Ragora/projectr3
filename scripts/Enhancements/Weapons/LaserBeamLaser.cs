// DESTROY BUILD DESTROY!

datablock SniperProjectileData(BeamLaser)
{
   directDamage        = 0.5;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 150;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 2.0;

   hasFalloff = true;
   optimalRange = 100;
   falloffRange = 150;
   falloffDamagePct = 0.333;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(BeamLaserExtended) : BeamLaser
{
   maxRifleRange       = 700;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Laser;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 3.0;

   hasFalloff = true;
   optimalRange = 500;
   falloffRange = 700;
   falloffDamagePct = 0.25;
};

function LaserBeamLaser::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 50;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function LaserBeamLaser::onInit(%this)
{
    %this.fireSound = "SniperRifleFireSound";
}

function LaserBeamLaser::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    if(%obj.laserFocus)
        %p = createProjectile("SniperProjectile", "BeamLaserExtended", %vector, %pos, %obj, %slot, %obj);
    else
        %p = createProjectile("SniperProjectile", "BeamLaser", %vector, %pos, %obj, %slot, %obj);

    %p.setEnergyPercentage(1.0);

    %obj.fireTimeout[%data, %this] = getSimTime() + (1500 / (1 + %obj.rateOfFire[%data.item]));

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "LaserBeamLaser", "Beam Laser", "High damage beam laser rated at 50kj - 100m optimal range, 150m falloff range", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::SniperRifle);
