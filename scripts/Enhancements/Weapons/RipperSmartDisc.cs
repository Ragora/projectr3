datablock LinearProjectileData(SmartDisc) : DiscProjectile
{
   damageRadius        = 15.0;
   kickBackStrength    = 2500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 100;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 50;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.25;
};

function SmartDisc::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, 8, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(vectorLen(%int.getVelocity()) > 8)
            transformProjectile(%proj, "LinearFlareProjectile", "SmartDiscBurst", %proj.position, %proj.initialDirection);
    }
}

datablock LinearFlareProjectileData(SmartDiscBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 25.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 3500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 200;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;
   
   sound               = GrenadeProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function RipperSmartDisc::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 2;
       
    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }
    
    return %energy SPC %ammoUse SPC %gyan;
}

function RipperSmartDisc::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("LinearProjectile", "SmartDisc", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RipperSmartDisc", "Smart Disc", "Seeks towards nearby moving targets - 1 ammo, 2 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Ripper);
