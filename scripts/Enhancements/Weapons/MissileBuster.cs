datablock ParticleData( BGDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.5 1.0 1.0";
   colors[1]     = "0.3 0.3 1.0 0.5";
   colors[2]     = "0.2 0.2 0.2 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 1.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BGDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "BGDebrisSmokeParticle";
};

datablock DebrisData( BusterExplosionDebris )
{
   emitters[0] = BGDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.5;
   lifetimeVariance = 0.02;

   numBounces = 1;
};

datablock ParticleData(BusterExplosionSmokeParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1250;
    lifetimeVarianceMS = 0;
    useInvAlpha = 1;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.2;
    times[2] = 1;
    colors[0] = "0.244094 0.656000 1.000000 1.000000";
    colors[1] = "0.200000 0.300000 1.000000 1.000000";
    colors[2] = "0.000000 0.100000 1.000000 0.000000";
    sizes[0] = 1.35484;
    sizes[1] = 3;
    sizes[2] = 7;
};

datablock ParticleEmitterData(BusterExplosionSmokeEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 12;
    velocityVariance = 2.5;
    ejectionOffset = 0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 250;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "BusterExplosionSmokeParticle";
};

datablock ParticleData(BusterSparkParticle)
{
    dragCoefficient = 0.658537;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 500;
    lifetimeVarianceMS = 350;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/underwaterSpark.PNG";
    times[0] = 0;
    times[1] = 0.677419;
    times[2] = 1;
    colors[0] = "0.322835 0.704000 1.000000 1.000000";
    colors[1] = "0.173228 0.544000 0.864000 1.000000";
    colors[2] = "0.000000 0.712000 1.000000 0.000000";
    sizes[0] = 0.790323;
    sizes[1] = 0.790323;
    sizes[2] = 0.790323;
};

datablock ParticleEmitterData(BusterSparkEmitter)
{
    ejectionPeriodMS = 2;
    periodVarianceMS = 0;
    ejectionVelocity = 19;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 250;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "BusterSparkParticle";
};

datablock ParticleData(BusterCrescentParticle)
{
    dragCoefficient = 1.60976;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = -0;
    lifetimeMS = 600;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/lightFalloffMono.png";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.173228 0.296000 1.000000 1.000000";
    colors[1] = "0.251969 0.296000 1.000000 0.500000";
    colors[2] = "0.488189 0.520000 1.000000 0.000000";
    sizes[0] = 4;
    sizes[1] = 8;
    sizes[2] = 9;
};

datablock ParticleEmitterData(BusterCrescentEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 40;
    velocityVariance = 5;
    ejectionOffset =   0.967742;
    thetaMin = 5;
    thetaMax = 175;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "BusterCrescentParticle";
};

datablock ExplosionData(BusterMissileExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.5;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

//   sizes[0] = "2 2 2";
// sizes[1] = "2 2 2";
//   sizes[2] = "2 2 2";

   emitter[0] = BusterExplosionSmokeEmitter;
   emitter[1] = BusterSparkEmitter;

   debris = BusterExplosionDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 4;
   debrisVelocity = 32.0;
   debrisVelocityVariance = 8.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 0.5;
   camShakeRadius = 7.0;
};

datablock SeekerProjectileData(BusterMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 32.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 280;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "BusterMissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 25.0;
   maxVelocity         = 75.0;
   turningSpeed        = 50.0;
   acceleration        = 50.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 45;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 10;

   flareDistance = 50;
   flareAngle    = 15;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = true;
};

function RLBusterMissile::validateUse(%this, %obj, %data)
{
    %ammoUse = 1;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function RLBusterMissile::onInit(%this)
{
//    %this.modeFireSound = "DiscFireSound";
}

function RLBusterMissile::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("SeekerProjectile", "BusterMissile", %vector, %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "RLBusterMissile", "Buster Missile", "2x damage, slow moving but powerful", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::MissileLauncher | $WeaponsList::LaserMissileLauncher | $WeaponsList::RocketLauncher);
