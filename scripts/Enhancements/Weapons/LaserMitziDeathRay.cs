datablock SniperProjectileData(MitziDeathRay)
{
   directDamage        = 1.5;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Mitzi;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mitzi;
   mdDamageTypeCount   = 3;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 75;
   mdDamageRadius[1]   = false;
   mdDamageType[2]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[2]   = 150;
   mdDamageRadius[2]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 3.0;

   maxRifleRange       = 300;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   hasFalloff = true;
   optimalRange = 150;
   falloffRange = 300;
   falloffDamagePct = 0.25;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

datablock SniperProjectileData(MitziDeathRayExtended) : MitziDeathRay
{
   maxRifleRange       = 600;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Mitzi;
   mdDamageTypeCount   = 3;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Plasma;
   mdDamageAmount[1]   = 75;
   mdDamageRadius[1]   = false;
   mdDamageType[2]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[2]   = 150;
   mdDamageRadius[2]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 3.0;
   
   hasFalloff = true;
   optimalRange = 300;
   falloffRange = 600;
   falloffDamagePct = 0.25;
};

datablock TargetProjectileData(MitziDeathRayFX)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 300;
   beamColor           	= "1.0 1.0 1.0";

   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock TargetProjectileData(MitziDeathRayFXExtended) : MitziDeathRayFX
{
   maxRifleRange       	= 600;
};

function LaserMitziDeathRay::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 75;
    %gyan = 2;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function LaserMitziDeathRay::onInit(%this)
{
    %this.fireSound = "underwaterDiscExpSound";
}

function LaserMitziDeathRay::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    if(%obj.laserFocus)
    {
        %p = createProjectile("SniperProjectile", "MitziDeathRayExtended", %vector, %pos, %obj, %slot, %obj);
        %q = createProjectile("TargetProjectile", "MitziDeathRayFXExtended", %vector, %pos, %obj, %slot, %obj);
    }
    else
    {
        %p = createProjectile("SniperProjectile", "MitziDeathRay", %vector, %pos, %obj, %slot, %obj);
        %q = createProjectile("TargetProjectile", "MitziDeathRayFX", %vector, %pos, %obj, %slot, %obj);
    }

    %p.setEnergyPercentage(0.25);
    %q.schedule(500, "delete");

    %obj.fireTimeout[%data, %this] = getSimTime() + (2500 / (1 + %obj.rateOfFire[%data.item]));

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "LaserMitziDeathRay", "Mitzi Death Ray", "Death in a can. Shoop da whoop! 2 GP, 150m optimal range", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::SniperRifle);
