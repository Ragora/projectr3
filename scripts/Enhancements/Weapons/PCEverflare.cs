datablock LinearFlareProjectileData(PlasmaEverflareBolt) : PlasmaCannonBolt
{
   damageRadius        = 14.0;
   kickBackStrength    = 0;
   explosion           = PlasmaBarrelBoltExplosion;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::PlasmaCannon;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Plasma;
   mdDamageAmount[0]   = 180;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;

   dryVelocity       = 250.0;
   fizzleTimeMS      = 2700;
   lifetimeMS        = 3000;
};

function PCEverflare::validateUse(%this, %obj, %data)
{
    %ammoUse = 20;
    %energy = 0;
    %gyan = 1;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.gyanLevel < %gyan)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this mode", 5, 1);
        return "f";
    }

    return %energy SPC %ammoUse SPC %gyan;
}

function PCEverflare::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "PCDisplayCharge", %vector, %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %obj.play3D("PBCShockwaveSound");
    %this.schedule(250, "PCDelayFire", %obj, %slot);

    return %p;
}

function PCEverflare::PCDelayFire(%this, %obj, %slot)
{
    %data = %this.data;
    %proj = createProjectile("LinearFlareProjectile", "PlasmaEverflareBolt", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %proj.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
    FlareSet.add(%proj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "PCEverflare", "Everflare", "Plasma so hot, it distracts missles - 20 ammo, 1 GP", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::PlasmaCannon);
