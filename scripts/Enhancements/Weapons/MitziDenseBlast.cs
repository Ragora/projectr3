datablock ParticleData(MitziDenseExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "1.0 0.2 0.1 1.0";
   colors[1]     = "0.8 0.075 0.01 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MitziDenseExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MitziDenseExplosionParticle";
};

datablock ExplosionData(MitziDenseExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;

   sizes[0] = "3 3 3";
   sizes[1] = "4 4 4";
   sizes[2] = "4 4 4";

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MitziDenseExplosionEmitter;
   particleDensity = 225;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ParticleData(MitziDenseTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "1.0 0.2 0.1 1.0";
   colors[1]     = "0.5 0.05 0.01 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MitziDenseTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MitziDenseTrailParticle";
};

datablock LinearFlareProjectileData(MitziDenseProjectile)
{
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 1.2;
   directDamageType    = $DamageType::MitziBlast;
   hasDamageRadius     = true;
   indirectDamage      = 0.9;
   kickBackStrength    = 2000;
   damageRadius        = 16.0;
   radiusDamageType    = $DamageType::MitziBlast;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MitziBlast;
   mdDamageTypeCount   = 3;
   mdDamageType[0]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[0]   = 125;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Mitzi;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;
   mdDamageType[2]     = $DamageGroupMask::Sonic;
   mdDamageAmount[2]   = 15;
   mdDamageRadius[2]   = false;
   
   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;

   explosion           = "MitziDenseExplosion";
   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MitziDenseTrailEmitter;

   dryVelocity       = 50.0;
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.1 0.0.075";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 0.1 0.075";
};

function MitziDenseProjectile::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(getSimTime() > %proj.irradiateTime && (%proj.tickCount % 12 == 0))
        RadiusRadiationExplosion(%proj, %proj.position, 8, 2, 0, %proj.instigator, false);
}

function igniteDenseProj(%p)
{
    createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

function MitziDenseBlast::validateUse(%this, %obj, %data)
{
    %ammoUse = 50;
    %energy = 0;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MitziDenseBlast::onInit(%this)
{
//    %this.modeSwitchSound = "DiscDryFireSound";
//    %this.modeFireSound = "DiscFireSound";
}

function MitziDenseBlast::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = createProjectile("LinearFlareProjectile", "MitziDenseProjectile", %vector, %pos, %obj, %slot, %obj);
    
    schedule(500, %p, "igniteDenseProj", %p);
    %p.irradiateTime = getSimTime() + 500;
    
    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MitziDenseBlast", "Mitzi Dense Blast", "2x damage, 50 cap use - Direct hits are murderous", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::Mitzi);
