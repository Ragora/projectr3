datablock GrenadeProjectileData(ACFlakShell)
{
   projectileShapeName = "grenade_projectile.dts";
   emitterDelay        = -1;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 16.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Flak;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 80;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.25;

   bubbleEmitTime      = 1.0;

   sound               = GrenadeProjectileSound;
   explosion           = "GrenadeExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";
   velInheritFactor    = 0.85; // z0dd - ZOD, 3/30/02. Was 0.5
   splash              = GrenadeSplash;

   baseEmitter         = GrenadeSmokeEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   grenadeElasticity = 0.01; // z0dd - ZOD, 9/13/02. Was 0.35
   grenadeFriction   = 0.99;
   numBounces        = 0;
   armingDelayMS     = 0; // z0dd - ZOD, 9/13/02. Was 1000
   muzzleVelocity    = 300.00; // z0dd - ZOD, 3/30/02. GL projectile is faster. Was 47.00
   //drag = 0.1; // z0dd - ZOD, 3/30/02. No drag.
   gravityMod        = 0.6; // z0dd - ZOD, 5/18/02. Make GL projectile heavier, less floaty
};

function ACFlakShell::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, 10, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
            {
                transformProjectile(%proj, "LinearFlareProjectile", "ACFlakShellBurst", %proj.position, %proj.initialDirection);
                return;
            }
        }
        else
        {
            if(%int.getHeat() > 0.8)
            {
                transformProjectile(%proj, "LinearFlareProjectile", "ACFlakShellBurst", %proj.position, %proj.initialDirection);
                return;
            }
        }
    }
}

datablock LinearFlareProjectileData(ACFlakShellBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 16.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Flak;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 80;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound               = GrenadeProjectileSound;
   explosion           = "HandGrenadeExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function ACFlak::validateUse(%this, %obj, %data)
{
    %ammoUse = 2;
    %energy = 15;
    %gyan = 0;

    if(%obj.getInventory(%data.ammo) < %ammoUse)
        return "f";

    if(%obj.getEnergyLevel() < %energy)
        return "f";
        
    return %energy SPC %ammoUse SPC %gyan;
}

function ACFlak::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    return createProjectile("GrenadeProjectile", "ACFlakShell", VectorSpread(%vector, 3), %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ACFlak", "HE Flak Shell", "Explodes near hot targets, 2 ammo 15 power", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::GrenadeLauncher);
