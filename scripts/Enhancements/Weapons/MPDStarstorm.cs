datablock LinearFlareProjectileData(DisruptorStarstormBolt)
{
   directDamage        = 0.3;
   directDamageType    = $DamageType::Blaster;

   explosion           = "BlasterExplosion";

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MPDisruptor;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   numFlares         = 15;
   size              = 0.5;
   flareColor        = "1.0 0.4 0.6";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "0.694 0.051 0.824";
};

function MPDStarstorm::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 150;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function MPDStarstorm::onInit(%this)
{
    %this.modeSwitchSound = "ElfFireWetSound";
    %this.fireSound = "PBCShockwaveSound";
}

function MPDStarstorm::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %p = 0;

    for(%i = 0; %i < 8; %i++)
    {
        %p = createProjectile(%data.projectileType, "DisruptorStarstormBolt", vectorSpread(%vector, 21), %pos, %obj, %slot, %obj);
        %p.damageBuffFactor = %obj.damageBuffFactor + %obj.weaponBonusDamage[%data.item];
    }

    return %p;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "MPDStarstorm", "Starstorm", "Unleashes a barrage of Disruptor blasts, uses 100 Power", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::MPDisruptor);
