function ERifleAutotron::applyEffect(%this, %obj, %item)
{
    %obj.rateOfFire[%item] += 0.333;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "ERifleAutotron", "Autotron", "Increases rate of fire by 33%", $Enhancement::AllArmorCompat, $WeaponsList::BlasterRifle | $WeaponsList::Spike);
