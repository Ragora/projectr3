function EDamageAmp::applyEffect(%this, %obj, %item)
{
    %obj.weaponBonusDamage[%item] += 0.1;
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "EDamageAmp", "Damage Amp", "Increases damage output by 10%", $Enhancement::AllArmorCompat, $Enhancement::AllWeapons);
