datablock TracerProjectileData(BlasterPowerBolt) : BlasterRifleBolt
{
   directDamage        = 0.75;
   kickBackStrength    = 350.0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BlasterRifle;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 107;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "1.0 0.1 0.1 0.75";
	tracerTex[0]  	 = "special/blasterBolt";
	tracerTex[1]  	 = "small_circle";
   tracerWidth     = 0.45;
   crossSize       = 2.15;
   crossViewAng    = 0.1;
   renderCross     = true;
   emap = true;
};

function BlasterPowerShot::validateUse(%this, %obj, %data)
{
    %ammoUse = 0;
    %energy = 50;
    %gyan = 0;

    if(%obj.getEnergyLevel() < %energy)
        return "f";

    return %energy SPC %ammoUse SPC %gyan;
}

function BlasterPowerShot::spawnProjectile(%this, %data, %obj, %slot, %vector, %pos)
{
    %obj.fireTimeout[%data.item, %this] = getSimTime() + (1000 / (1 + %obj.rateOfFire[%data.item]));
    %obj.schedule(32, "setImageTrigger", 0, false);
    %obj.play3D("BomberTurretFireSound");
    
    return createProjectile("TracerProjectile", "BlasterPowerBolt", %obj.getMuzzleVector(%slot), %pos, %obj, %slot, %obj);
}

Enhancement.registerEnhancement($EnhancementType::Weapon, "BlasterPowerShot", "Power Shot", "Pinpoint accurate and extremely fast", $Enhancement::WeaponMode | $Enhancement::AllArmorCompat, $WeaponsList::BlasterRifle);
