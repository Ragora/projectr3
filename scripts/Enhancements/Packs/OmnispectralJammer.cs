function OmnispectralJammer::applyEffect(%this, %obj, %armorName)
{
    %obj.omniJammer = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "OmnispectralJammer", "Omnispectral Jammer", "Prevents weapons fire when active, even you.", 0, $PackList::Jammer);
