function ShieldThermalMod::applyEffect(%this, %obj, %armorName)
{
    %obj.shieldDamageFactor[$DamageGroupMask::Plasma] -= 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldThermalMod", "Thermal Resist Modulation", "Reduces Thermal damage by 50%", 0, $PackList::Shield);
