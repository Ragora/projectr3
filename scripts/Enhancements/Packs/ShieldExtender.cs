function ShieldExtender::applyEffect(%this, %obj, %armorName)
{
    %obj.shieldExtended = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldExtender", "Matrix Extender", "Expands shields out 25m, protecting nearby targets.", 0, $PackList::Shield);
