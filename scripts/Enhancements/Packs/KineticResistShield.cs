function ShieldKineticMod::applyEffect(%this, %obj, %armorName)
{
    %obj.shieldDamageFactor[$DamageGroupMask::Kinetic] -= 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldKineticMod", "Kinetic Resist Modulation", "Reduces Kinetic damage by 50%", 0, $PackList::Shield);
