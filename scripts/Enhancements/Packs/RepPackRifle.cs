datablock EnergyProjectileData(RRBolt)
{
   emitterDelay        = -1;
   directDamage        = 0.0;
   directDamageType    = $DamageType::Blaster;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 0.5;

   explosion           = "BBlasterExplosion";
   splash              = BlasterSplash;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 500;

   muzzleVelocity    = 300.0;

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 300.0;
   wetVelocity       = 300.0;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.175 0.175 0.5";

   scale = "0.25 20.0 1.0";
   crossViewAng = 0.99;
   crossSize = 0.55;

   lifetimeMS     = 5000;
   blurLifetime   = 0.2;
   blurWidth      = 0.25;
   blurColor = "0.0 0.0 0.4 1.0";

   texture[0] = "special/shrikeBolt";
   texture[1] = "special/shrikeBoltCross";
};

function RRBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
    
    %targetObject.incHP(100);
}

datablock ShapeBaseImageData(RepairRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_sniper.dts";
   item = RepairPack;
   projectile = RRBolt;
   projectileType = EnergyProjectile;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   enhancementSlots = 0;

   weaponDescName = "Repair Rifle";
   defaultModeName = "Healing Bolt";
   defaultModeDescription = "Heals target for 100 HP, uses 1 GP per shot";
   defaultModeSwitchSound = "BlasterDryFireSound";
   defaultModeFireSound = "SentryTurretFireSound";
   defaultModeFailSound = "BlasterDryFireSound";

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 2.0;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTriggerUp[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.5;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function RepairRifleImage::validateFireMode(%data, %obj)
{
    %ammoUse = 0;
    %energyUse = 0;
    %gyanUse = 1;

    if(%obj.gyanLevel < %gyanUse)
    {
        bottomPrint(%obj.client, "Not enough Gyan power ("@%gyan@") to fire this gun", 5, 1);
        return "f";
    }

    return %energyUse SPC %ammoUse SPC %gyanUse;
}

function RepairPackRifle::applyEffect(%this, %obj, %armorName)
{
    %obj.repRifle = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "RepairPackRifle", "Repair Rifle", "Uses 1 GP, fires a healing bolt that heals for 100 HP (magic!)", 0, $PackList::Repair);
