function PhasedCloakingMod::applyEffect(%this, %obj, %armorName)
{
    %obj.phasedCloak = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "PhasedCloakingMod", "Phase Shifter", "Ignores damage from indirect sources while cloaked, +25% energy usage.", 0, $PackList::Cloak);
