function ShieldElectroMagMod::applyEffect(%this, %obj, %armorName)
{
    %obj.shieldDamageFactor[$DamageGroupMask::Energy] -= 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldElectroMagMod", "ElectroMag Resist Modulation", "Reduces ElectroMag damage by 50%", 0, $PackList::Shield);
