function ShieldExplosiveMod::applyEffect(%this, %obj, %armorName)
{
    %obj.shieldDamageFactor[$DamageGroupMask::Explosive] -= 0.5;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldExplosiveMod", "Explosive Resist Modulation", "Reduces Explosive damage by 50%", 0, $PackList::Shield);
