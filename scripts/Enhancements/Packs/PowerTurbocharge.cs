function PowerCoreTurbocharger::applyEffect(%this, %obj, %armorName)
{
    %obj.turbocharger = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "PowerCoreTurbocharger", "Turbocharger", "Trades energy generation for +20% damage.", 0, $PackList::Energy);
