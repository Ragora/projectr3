function RepairPackSubspace::applyEffect(%this, %obj, %armorName)
{
    %obj.repSubspace = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "RepairPackSubspace", "Subspace Nano-Regenerator", "Slowly regenerates armor, does not require max energy.", 0, $PackList::Repair);
