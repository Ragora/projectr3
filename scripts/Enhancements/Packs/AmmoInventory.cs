function AmmoInventoryMod::applyEffect(%this, %obj, %armorName)
{
    %obj.ammoInventory = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "AmmoInventoryMod", "Inventory Circuit", "Trades storage for an inventory circuit, 75 Power", 0, $PackList::Ammo);
