function RepairPackAegis::applyEffect(%this, %obj, %armorName)
{
    %obj.repAegis = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "RepairPackAegis", "Nano-Repair Aegis", "Protects you from a fatal hit but reduces HP to 1", 0, $PackList::Repair);
