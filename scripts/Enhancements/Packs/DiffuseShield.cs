function ShieldDiffuseType::applyEffect(%this, %obj, %armorName)
{
    %obj.diffuseShield = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "ShieldDiffuseType", "Diffuse Shield", "Absorbs 50% damage and uses half energy on shield hits.", 0, $PackList::Shield);
