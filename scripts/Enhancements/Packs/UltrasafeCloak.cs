function UltrasafeCloakingMod::applyEffect(%this, %obj, %armorName)
{
    %obj.ultrasafeCloak = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "UltrasafeCloakingMod", "Ultrasafe Modulation", "No longer decloaked by jammer fields, +50% energy usage.", 0, $PackList::Cloak);
