function PowerCoreOverdrive::applyEffect(%this, %obj, %armorName)
{
    %obj.energyPackOverdrive = true;
}

Enhancement.registerEnhancement($EnhancementType::Pack, "PowerCoreOverdrive", "Overdrive", "Doubles power output but armor runs hot.", 0, $PackList::Energy);
