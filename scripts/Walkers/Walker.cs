// Walker system
exec("scripts/Walkers/WalkerBlocks.cs");

$WalkerHardpointPos["Fury", 2] = "0 0 0";
$WalkerHardpointPos["Fury", 3] = "-1.25 0 0";
$WalkerHardpointPos["Fury", 4] = "0.2 -0.9 1.4";
$WalkerHardpointPos["Fury", 5] = "-1.35 -0.9 1.4";

$WalkerHardpointPos["Stormguard", 2] = "0 0 0";
$WalkerHardpointPos["Stormguard", 3] = "-3.5 0 0";
$WalkerHardpointPos["Stormguard", 4] = "0 -1 2.5";
$WalkerHardpointPos["Stormguard", 5] = "-3.5 -1 2.5";
$WalkerHardpointPos["Stormguard", 6] = "-1.75 0 0.75";

exec("scripts/Walkers/WalkerWeaponBlocks.cs");

// Walker Virtual Client - places a stub client on the walkers
function VirtualClient::__construct(%this)
{
    VirtualClient.Version = 1.0;
}

function VirtualClient::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(VirtualClient))
   System.addClass(VirtualClient);
   
function VirtualClient::isAIControlled(%this)
{
    return false;
}

function VirtualClient::clientDetected(%this)
{
    // reduce console spam
}

function spawnWalker(%client, %walkerName, %trans)
{
    %block = %walkerName@"WalkerBase";

    %walker = new Player()
    {
        dataBlock = %block;
        isWalker = true;
        client = VirtualClient;
    };

    %walker.nameOverride = %client.pendingVehicleName[$VehicleListID[%block]];

    if(%walker.nameOverride !$= "")
        %walkerName = %walker.nameOverride;

    %typeTag = %walkerName $= "Fury" ? "Scout Walker" : "Assault Walker";
//    %loadoutName = %client.loadoutName[0]; // todo
//    %nameTag = addTaggedString(%loadoutName SPC %typeTag);
    %nameTag = addTaggedString(%walkerName SPC %typeTag);
    %skinTag = addTaggedString("basebbot");
    %voiceTag = addTaggedString("Bot1");
    %walkerTarget = allocTarget(%nameTag, %skinTag, %voiceTag, '_ClientConnection', %client.team, %block, 1.0);

    %walker.setCloaked(true);
    %walker.schedule(3700, "playAudio", 0, VehicleAppearSound);
    %walker.schedule(1500, "playAudio", 1, WalkerPowerOn);
    %walker.schedule(4800, "setCloaked", false);
    %walker.setTransform(%trans);
    MissionCleanup.add(%walker);
    $VehicleTotalCount[%client.team, %walkerName]++;

    %walker.team = %client.team;
    %walker.outOfBounds = false;
    %walker.pilotObject = "";
    %walker.shortName = %walkerName;
    %walker.vehicleID = $VehicleWalkerID[%walkerName];
    %walker.vid = $VehicleWalkerID[%walkerName]; // compatibility with existing framework
    %client.player.lastVehicle = %walker;
    %walker.lastPilot = %client.player;
    %walker.mountedGuns = 0;
    %walker.targeter = false;
    
    // sets walker target for viewing
    %walker.target = %walkerTarget;
    %walker.setTarget(%walkerTarget);
    setTargetSensorData(%walkerTarget, WalkerBaseSensor);
    
    // used for creating walker
    %walker.spawningClient = %client;
    
    %block.initWalker(%walker);
    %walker.installEnhancements($EnhancementType::Vehicle);
    %walker.isWalker = true; // Have to set this twice since it gets unset
    %block.installParts(%walker, %client.player);
    %walker.mountImage(WalkerDefaultParam, 0);
    %block.schedule($g_TickTime, "onTick", %walker);

    setTargetSensorGroup(%walker.target, %client.team);
    %walker.spawningClient = "";
    if(%client.isVehicleTeleportEnabled())
       %block.schedule(6000, "mountDriver", %walker, %client.player);
}

// this code may get run multiple times on startup for some reason, so only init
// things that may be set by modules once
function Armor::initWalker(%data, %walker)
{
    %walker.heat = 0;
    %walker.maxHeat = $VehicleWalkerData[%walker.vid, "maxHeat"];
    %walker.coolant = 3;
    %walker.selectedFireGroup = 0;
    %walker.powerState = true;
    %walker.shutdownTimeout = getSimTime() + 10000;
    %walker.powerTransition = false;
    %walker.fireTrigger = false;
    %walker.jetState = false;

    if(%walker.heatDrain $= "")
        %walker.heatDrain = 2;
}

function Armor::walkerTick(%this, %obj)
{
    if((%obj.tickCount % 16 == 0))
    {
        if(!%obj.jetState && %obj.heat > 0)
        {
            %obj.heat -= %obj.powerState ? %obj.heatDrain : %obj.heatDrain * 3;

            if(%obj.heat < 0)
                %obj.heat = 0;
        }
        else if(%obj.jetState)
            %obj.heat += 6;
            
        if(%obj.heat > %obj.maxHeat)
        {
            %obj.damage(%obj.pilotObject, %obj.position, 10000, $DamageType::WalkerReactor);
            return;
        }
        
        %obj.setHeat(%obj.heat * 0.01);
    }

    %pilot = %obj.pilotObject;
    
    if(isObject(%pilot) && (%obj.tickCount % 8 == 0))
        %this.updatePilotStatus(%obj, %pilot);

    %pct = %obj.getDamagePct();

    if(%pct >= 0.7 && (%obj.tickCount % 48 == 0))
        %obj.playAudio(2, EngineAlertSound);

    if(%pct >= 0.8 && (%obj.tickCount % 12 == 0))
    {
        if(getRandom() > 0.9)
        {
            %dmgFXPos = vectorProject(%obj.getWorldBoxCenter(), VectorRand(), 2);
            createHitDebris(%dmgFXPos);
        }
    }
}

function Player::walkerCycleWeapon(%obj, %dir)
{
     if(%dir $= "prev")
     {
          %obj.selectedFireGroup--;

          if(%obj.selectedFireGroup < 0)
               %obj.selectedFireGroup = $VehicleMaxFireGroups - 1;
     }
     else
     {
          %obj.selectedFireGroup++;

          if(%obj.selectedFireGroup >= $VehicleMaxFireGroups)
               %obj.selectedFireGroup = 0;
     }

     %obj.fireTrigger = false;
     %word = $VehicleFiregroupName[%obj.selectedFireGroup];
     
     commandToClient(%obj.ownerClient, 'setAmmoHudCount', %word);
}

function Player::walkerSelectKey(%obj, %num)
{
//    echo("Walker" SPC %obj SPC "selected key:" SPC %num);
}

function Armor::updatePilotStatus(%data, %obj, %pilot)
{
    if(%obj.powerState)
    {
        %hGraph = createColorGraph("|", 40, (%obj.heat / %obj.maxHeat), "<color:00FF00>", "<color:FFFF00>", "<color:FF0000>", "<color:777777>", 0.45, 0.7);
        %tcword = %obj.targeter ? "Online" : "Offline";

        if(%obj.maxShieldStrength !$= "")
        {
            %amt = mCeil((%obj.shieldStrength / %obj.maxShieldStrength) * 100);
            %shcol = "<color:FF0000>";

            if(%amt > 65)
                %shcol = "<color:00FF00>";
            else if(%amt > 32)
                %shcol = "<color:FFFF00>";
                
            %shieldText = %shcol@%amt@"%<color:42DBEA>";
        }
        else
            %shieldText = "None";

        commandToClient(%pilot.client, 'BottomPrint', "Heat: "@%hGraph SPC mCeil(%obj.heat)@"K - Armor: "@%obj.hitPoints@" HP\nShield: "@%shieldText@" - Speed:" SPC mFloor(vectorLen(%obj.getVelocity()) * 3.6) SPC "KPH - Coolant: "@%obj.coolant@" - Targeting Computer:" SPC %tcword, 1, 2);

        %ammoStr = "Ammo:";

        for(%i = 0; %i < %obj.mountedGuns; %i++)
        {
            %ammo = %obj.ammo[$VehicleHardpoints[%obj.vid, %i, "mountPoint"]];

            if(%ammo $= "")
                %ammo = 0;
                    
            %ammoStr = %ammoStr SPC (%i + 1) @ ":" SPC %ammo;
        }

        messageClient(%pilot.client, 'MsgSPCurrentObjective2', "", %ammoStr);
    }
}

// todo: fix
function Player::walkerInLava(%walker, %damageAmount, %state)
{
    %walker.addHeat(mFloor(%damageAmount * 100));
}

function Player::walkerInWater(%walker, %damageAmount, %state)
{
    %walker.addHeat(-10);
}

function Armor::onWalkerCollision(%this, %obj, %col)
{
    // Handle walker collisions here
}

function Armor::mountDriver(%data, %obj, %player)
{
    if(isObject(%obj) && !%obj.isDead)
    {
        %player.startFade(1000, 0, true);
        walkerMountPlayer(%obj, %player.client);
        %player.schedule(1500, "startFade", 1000, 0, false);
    }
}

function walkerMountPlayer(%walker, %client)
{
    %player = %client.player;
    %data = %player.getDatablock();
    
    if(%data.armorClass != $ArmorClassMask::Light || (%data.armorFlags & $ArmorFlags::CanPilot) == 0)
    {
        messageClient(%player.client, 'MsgArmorCantMountVehicle', '\c2This vehicle can only be used by light frame armors.~wfx/misc/misc.error.wav');
        return;
    }
    
    // Prevent people from jacking your ride while you're in it
    if(isObject(%walker.pilotObject))
        return;
    
    %player.ejectFlag();
    %player.lastWeapon = (%player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).getName().item;
    %player.unmountImage($WeaponSlot);

    if(%player == %player.lastVehicle.lastPilot && %player.lastVehicle != %walker)
    {
        schedule(15000, %player.lastVehicle, "vehicleAbandonTimeOut", %player.lastVehicle);
          %player.lastVehicle.lastPilot = "";
    }

    if(%player.lastPilot !$= "" && %walker == %player.lastPilot.lastVehicle)
        %walker.lastPilot.lastVehicle = "";

    %walker.abandon = false;
    %walker.lastPilot = %player;
    %player.lastVehicle = %walker;
    %walker.team = %client.team;
    %walker.pilotObject = %player;
    %walker.ownerClient = %client;
    %client.walker = %walker;
//    %skin = %client.team & 1 ? addTaggedString("basebot") : addTaggedString("basebbot");

    setTargetRenderMask(%walkerTarget, (1 << $TargetInfo::HudRenderStart));
    setTargetSensorGroup(%walker.target, %client.team);

    if(!%walker.powerState)
    {
        %client.camera.setTransform(%walker.getTransform());
        %client.camera.getDataBlock().setMode(%client.camera, "mechOrbit", %walker);
        %client.camera.setOrbitMode(%walker, %walker.getTransform(), 0.75, 9.5, 9.5);
        %client.setControlObject(%client.camera);
    }
    else
        %client.setControlObject(%walker);

    %walker.pilotObject.transient = true;
    %walker.pilotObject.initialBuriedPos = vectorAdd(getTerrainHeight(%walker.position), "0 0 -500");
    %walker.pilotObject.setTransform(%obj.initialBuriedPos SPC "0 0 1 0");
    %walker.playAudio(3, CameraGrenadeActivateSound);
    %walker.setTargeterStatus();

    commandToClient(%client, 'setAmmoHudCount', $VehicleFiregroupName[%walker.selectedFireGroup]);
}

function Armor::damageWalker(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC, %srcProj, %element)
{
   if(%damageType == $DamageType::Impact)
      if(%sourceObject.getDamageState() $= "Destroyed")
         return;

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/8/02. Check to see if this turret has a valid owner, if not clear the variable.
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
      {
         %sourceObject.owner = "";
      }
   }
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) && %sourceObject.isVehicle())
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

    // Convert damage calculation here:
   if(%targetObject.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
   {
      if(%targetObject.shieldSource > 0)
          %amount = %data.onShieldDamage(%targetObject.shieldSource, %position, %amount, %damageType, %element);
      else
          %amount = %data.onWalkerShieldDamage(%targetObject, %position, %amount, %damageType, %element);
   }
   
   %amount *= %targetObject.damageReduceFactor;
   %amount *= %targetObject.armorDamageFactor[%element];

   //%damageScale = $InheritDamageProfile[%damageType] !$= "" ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
   %damageScale = %data.damageScale[%damageType];

   if(%damageScale !$= "")
      %amount *= %damageScale;

   if(%amount == 0)
      return;

   %targetObject.applyHPDamage(%amount);
   Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

   %targetClient.lastDamagedBy = %damagingClient;
   %targetClient.lastDamaged = getSimTime();

   // Mech health exceeded? BLOW THEM UP!
   if(%targetObject.getState() $= "Dead")
   {
      $VehicleTotalCount[%targetObject.team, %targetObject.shortName]--; // potentially causes negative gains in vehicles
      
      %flingee = %targetObject.pilotObject;
      %targetObject.fireTrigger = false;
      
      if(isObject(%flingee))
      {
          %flingee.setVelocity(%targetObject.getVelocity());
          %flingee.setPosition(%targetObject.getWorldBoxCenter());
          %flingee.client.setControlObject(%flingee);
          %flingee.getDataBlock().doDismount(%flingee, true);

          if(%flingee.inv[%flingee.lastWeapon])
              %flingee.use(%flingee.lastWeapon);

          if(%flingee.getMountedImage($WeaponSlot) == 0)
              %fingee.selectWeaponSlot(0);

          %flingee.transient = false;
          %flingee.client.walker = "";
          %xVel = 250.0 - (getRandom() * 500.0);
          %yVel = 250.0 - (getRandom() * 500.0);
          %zVel = (getRandom() * 100.0) + 50.0;
          %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
//          %flingee.damage(0, %obj.getPosition(), 1.25, $DamageType::Crash);
          %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
        }
         
      %targetObject.setMomentumVector(%momVec);
      createRemoteProjectile("LinearFlareProjectile", "MagIonOLDeathCharge", vectorRand(), %targetObject.getWorldBoxCenter(), 0, %targetObject);
      %targetObject.gib();
   }
}

function Armor::onWalkerShieldDamage(%data, %obj, %position, %amount, %damageType, %element)
{
    if(%element == $DamageGroupMask::Sonic)
        return %amount;

    %energy = %obj.shieldStrength;

    // Passthrough if no shields
    if(%energy < 1)
        return %amount;

    // shield ignore stuff
    if(%obj.lastHitFlags & $Projectile::IgnoreShields)
        return %amount;

    %shieldScale = %data.shieldDamageScale[%damageType];

    if(%shieldScale $= "")
        %shieldScale = 1;

    %amount *= %shieldScale;
    %amount *= %obj.shieldDamageFactor[%element];
    %damageAmount = %amount;
    %remainder = 0;

    if(%energy > %damageAmount)
    {
        %obj.shieldStrength = %energy - %damageAmount;
        %obj.playShieldEffect("0.0 0.0 1.0"); // find a new shield effect

        %data.walkerRestartRecharge(%obj, false);
    }
    else
    {
        %obj.shieldStrength = 0;
        %remainder = %damageAmount - %energy;

        if(%remainder < 1)
            %remainder = 0;
        else
            %remainder /= 100;

        %data.walkerRestartRecharge(%obj, true);
    }

    return %remainder;
}

function Armor::walkerStartShield(%data, %obj)
{
    if(%obj.initShield == true)
    {
        %obj.initShield = false;
        %obj.shieldStrength = 0;
    }

    if(%obj.maxShieldStrength > 0)
        %data.walkerStartRecharge(%obj);
    else
        %obj.isShielded = false;
}

function Armor::walkerStartRecharge(%data, %obj)
{
    if(!isObject(%obj))
        return;

    %obj.isShielded = true;
    %obj.playAudio(2, ShieldsUpSound);

    if(%obj.shieldStrength == 0)
        %obj.shieldStrength = %obj.maxShieldStrength * 0.25;

    %data.walkerTickRecharge(%obj);
}

function Armor::walkerRestartRecharge(%data, %obj, %bDepleted)
{
    %obj.isShielded = (%obj.shieldStrength > 0);

    if(%obj.shieldRechargeThread > 0)
        cancel(%obj.shieldRechargeThread);

    if(%bDepleted)
        %obj.playAudio(2, ShieldsDownSound);

    %obj.shieldRechargeThread = %data.schedule(%obj.shieldRechargeDelay, "walkerStartRecharge", %obj);
}

function Armor::walkerTickRecharge(%data, %obj)
{
    if(%obj.shieldStrength < %obj.maxShieldStrength)
    {
        %obj.shieldStrength += %obj.shieldRechargeRate;

        if(%obj.shieldStrength > %obj.maxShieldStrength)
            %obj.shieldStrength = %obj.maxShieldStrength;

        %obj.shieldRechargeThread = %data.schedule($g_TickTime, "walkerTickRecharge", %obj);
    }
    else
        %obj.shieldRechargeThread = 0;
}

function Armor::walkerTrigger(%data, %walker, %triggerNum, %val)
{
    switch(%triggerNum)
    {
        case 0:
            %data.walkerFire(%walker, %val);

        case 2:
            %data.walkerJump(%walker, %val);

        case 3:
            %data.walkerJet(%walker, %val);

        case 4:
            %data.walkerGrenade(%walker, %val);

        case 5:
            %data.walkerMine(%walker, %val);

        default:
            return;
    }
}

function Player::walkerUse(%walker, %data)
{
    %block = %walker.getDatablock();
    
    switch$(%data)
    {
        case "BackPack":
            %block.togglePack(%walker);

        case "Beacon":
            %block.toggleBeacon(%walker);

        case "RepairKit":
            %block.toggleRepairKit(%walker);

        default:
//            echo("Walker" SPC %walker SPC "attempted to used item:" SPC %data);
            return false;
    }
     
    return true;
}

function Player::walkerMountWeapon(%walker, %slot, %shape, %weapon)
{
    %walker.mountImage(%walker.shortName@%shape@%slot, %slot);
    %walker.mountedGun[%slot] = %weapon;
    %walker.gun[%walker.mountedGuns] = %slot;
    %walker.slotToIndex[%slot] = %walker.mountedGuns;
    %walker.mountedGuns++;
}

function Armor::walkerFire(%data, %walker, %state)
{
    %walker.fireTrigger = %state;
    
    if(!%state)
        return;

    %client = %walker.pilotObject.client;
    %time = getSimTime();
    
    for(%i = 0; %i < %walker.mountedGuns; %i++)
    {
        %slot = %walker.gun[%i];
        
        if(%walker.slotFireGroup[%i] & (1 << %walker.selectedFireGroup))
        {
            %gun = %walker.mountedGun[%slot];
            %gun.walkerFireSlot(%walker, %slot, %time);
        }
    }
}

function Armor::walkerJump(%data, %walker, %state)
{
    if(!%state)
        return;

    %walker.fireTrigger = false;
    %pilot = %walker.pilotObject;
    %pilot.setVelocity(%walker.getVelocity());
    %pilot.setPosition(vectorProject(%walker.getWorldBoxCenter(), %walker.getForwardVector(), 5));
    %pilot.client.setControlObject(%pilot);
    %pilot.getDataBlock().doDismount(%pilot, true);
    
    if(%pilot.inv[%pilot.lastWeapon])
        %pilot.use(%pilot.lastWeapon);

    if(%pilot.getMountedImage($WeaponSlot) == 0)
        %pilot.selectWeaponSlot(0);
    
    %pilot.transient = false;
    %walker.playAudio(3, CameraGrenadeAttachSound);
    
    %walker.ownerClient = "";
    %walker.pilotObject.client.walker = "";
    %walker.pilotObject = "";
}

function Armor::walkerJet(%data, %walker, %state)
{
    if(%data.hasJumpJets)
        %walker.jetState = %state;
}

function Armor::walkerGrenade(%data, %walker, %state)
{
    if(!%state)
        return;
        
    %time = getSimTime();

    if(%time > %walker.nextRestockTime)
    {
        InitContainerRadiusSearch(%walker.position, 10, $TypeMasks::StaticShapeObjectType);

        while((%hit = containerSearchNext()) != 0)
        {
            if(%hit.team == %walker.ownerClient.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
            {
                // Reload vehicle
                %walker.play3d(MissileReloadSound);
                messageClient(%walker.ownerClient, 'MsgWalkerReload', '\c2Walker parked on pad, ammo restocked.');

                for(%i = 0; %i < 5; %i++)
                {
                    %part = %walker.installedVehiclePart[(%i + 3)];
                    
                    if(isObject(%part))
                        %part.reArm(%data, %walker, %walker.pilotObject, %i);
                }

                %walker.nextRestockTime = %time + 120000;
                return;
            }
        }
    }
    else
        %walker.toggleShutdown(%data);
}

function Player::toggleShutdown(%walker, %data)
{
    %time = getSimTime();
    %client = %walker.pilotObject.client;
    
    if(%time < %walker.shutdownTimeout)
    {
        messageClient(%client, 'msgWalkerPowerCycle', '\c2EAX Walker power system cycling, please wait.');
        return false;
    }

    %walker.fireTrigger = false;
    %walker.shutdownTimeout = %time + 10000;
    %walker.powerState = !%walker.powerState;
    %sound = %walker.powerState ? "WalkerPowerOn" : "WalkerPowerOff";
    %walker.setPassiveJammed(true);
    %walker.powerTransition(5000);
    %walker.playAudio(1, %sound);
    
    if(%walker.powerState)
    {
        messageClient(%client, 'MsgWalkerStartup', '\c2%1 initiating startup sequence.', %walker.shortName);
        %client.schedule(5000, "setControlObject", %walker);
    }
    else
    {
        messageClient(%client, 'MsgWalkerShutdown', '\c2%1 initiating shutdown sequence.', %walker.shortName);

        %client.camera.setTransform(%walker.getTransform());
        %client.camera.getDataBlock().setMode(%client.camera, "mechOrbit", %walker);
        %client.camera.setOrbitMode(%walker, %walker.getTransform(), 0.75, 9.5, 9.5);
        %client.setControlObject(%client.camera);
    }

    return true;
}

function Player::powerTransition(%walker, %time)
{
    %walker.powerTransition = !%walker.powerTransition;
    
    if(%walker.powerTransition)
        %walker.schedule(%time, "powerTransition", 0);
}

function Armor::walkerMine(%data, %walker, %state)
{
    if(!%state)
        return;

    %walker.targeter = !%walker.targeter;

    %walker.setTargeterStatus();
}

function Player::setTargeterStatus(%walker)
{
    %image = %walker.targeter ? WalkerMissileParam : WalkerDefaultParam;
    %sound = %walker.targeter ? "TurretPackActivateSound" : "BlasterDryFireSound";
    %ret = %walker.targeter ? "MissileLauncher" : "Blaster";
    
    %walker.unmountImage(0);
    %walker.mountImage(%image, 0);
    %walker.playAudio(1, %sound);
    %walker.pilotObject.client.setWeaponsHudActive(%ret);
}

function Armor::togglePack(%data, %obj)
{
 //   %module = %obj.installedVehiclePart[$VehiclePartType::Module];

    if(%obj.installedModule == true)
    {
        %time = getSimTime();
        %module = %obj.installedVehiclePart[$VehiclePartType::Module];

        if(%obj.moduleTimeout[%module] > %time)
            return;

        if(%module.triggerType == $VMTrigger::Push)
            %module.triggerPush(%data, %obj, %obj.pilotObject);
        else if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = !%obj.moduleTriggerState;

            %module.triggerToggle(%data, %obj, %obj.pilotObject, %obj.moduleTriggerState);
        }

        %obj.moduleTimeout[%module] = %time + %module.cooldownTime;
    }
}

function Armor::triggerModuleOff(%data, %obj)
{
    if(%obj.installedModule == true)
    {
        %module = %obj.installedVehiclePart[$VehiclePartType::Module];
        
        if(%module.triggerType == $VMTrigger::Toggle)
        {
            %obj.moduleTriggerState = false;

            %module.triggerToggle(%data, %obj, %obj.pilotObject, %obj.moduleTriggerState);
        }
    }
}

function Armor::toggleBeacon(%data, %walker)
{
    if(%walker.coolant > 0)
    {
        %walker.coolant--;
        %walker.addHeat(%walker.maxHeat * -0.75);
        %walker.play3D(VehicleExitWaterSoftSound);
    }
    else
        %walker.play3d(BomberTurretDryFireSound);
}

function Player::addHeat(%walker, %delta)
{
    %walker.heat += %delta;
    
    if(%walker.heat < 0)
        %walker.heat = 0;
        
    if(%walker.heat > %walker.maxHeat)
        %walker.heat = %walker.maxHeat;
}

function Armor::toggleRepairKit(%data, %walker)
{
    // Placeholder
}
