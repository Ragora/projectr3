datablock ParticleData(GreenReflectParticle)
{
    dragCoefficient = 0.707317;
    gravityCoefficient = 0;
    windCoefficient = 0;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 500;
    lifetimeVarianceMS = 350;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.314961 1.000000 0.576000 1.000000";
    colors[1] = "0.560000 0.360000 0.260000 1.000000";
    colors[2] = "1.000000 0.360000 0.260000 0.000000";
    sizes[0] = 2.31452;
    sizes[1] = 0.25;
    sizes[2] = 0.25;
};

datablock ParticleEmitterData(GreenReflectEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 0;
    ejectionVelocity = 18;
    velocityVariance = 6.75;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
   lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "GreenReflectParticle";
};

function RepulsorBeaconField::init(%this)
{
    %this.detectMask = $TypeMasks::ProjectileObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;
    %this.radius = 30;
}

function RepulsorBeaconField::forEachInAura(%this, %obj)
{
    if(!%obj.getDatablock().canReflect(%obj))
        return;

    if(%obj.lastReflectedFrom == %this.source)
        return;

//    if(isObject(%obj.instigator) && %obj.instigator.team == %this.source.team)
//        return;

    // Code below is a modified version of the one used in MD2
    // this code created by SouthTown (ST) with mathemagical guidance from
    // mostlikely (construction)
    %point = %obj.getPosition();
    %pVec = VectorNormalize(%obj.initialDirection);
    %ctrPos = VectorAdd(%point, "0 0 3");
    %vecFromCtr = VectorNormalize(VectorSub(%point, %ctrPos)); // --- Acts as surface normal (ST)
    // --- Thanks to Mostlikely
    %flip = VectorDot(%vecFromCtr, %pVec);
    %flip = VectorScale(%vecFromCtr, %flip);
    %newVec = VectorAdd(%pVec, VectorScale(%flip, 2));
    // --- ...again LOL (ST)
    %newPos = VectorAdd(%point, %newVec);
    // --- End reflection angle (ST)

    if(%obj.getClassName() $= "BombProjectile")
        %newVec = vectorScale(%newVec, 350);

    %p = cloneProjectile(%obj, %newPos, %newVec);
    %p.lastReflectedFrom = %this.source;
    %data = %p.getDatablock();
    
    createLifeEmitter(%newPos, "GreenReflectEmitter", 500);
    
    if(isObject(%this.source))
    {
        if(%data.hasDamageRadius && %data.indirectDamage > (2 * %data.directDamage))
            %this.source.applyDamage(%data.indirectDamage);
        else
            %this.source.applyDamage(%data.directDamage);
    }
}

Aura.registerAura("RepulsorBeaconField", $AuraType::Instance);
