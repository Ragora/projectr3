function GearheadDamageAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.noPersistentTick = true;
    %this.radius = 25;
}

function GearheadDamageAura::validateAuraTick(%this)
{
    if(%this.source.isDead || %this.source.getDatablock().armorType != $ArmorType::Gearhead)
    {
        %this.destroy();
        return false;
    }
    
    return true;
}

function ShieldExtenderAura::onEnter(%this, %obj)
{
    if(%obj.isDead || %obj.client.team != %this.source.client.team)
    {
        %this.removeFromAura(%obj);
        return;
    }

    %obj.damageBuffFactor += 0.1;
}

function ShieldExtenderAura::onLeave(%this, %obj)
{
    %obj.damageBuffFactor -= 0.1;
}

Aura.registerAura("GearheadDamageAura", $AuraType::Persistent);
