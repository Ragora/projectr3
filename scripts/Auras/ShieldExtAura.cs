function ShieldExtenderAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.noPersistentTick = true;
    %this.radius = 25;
}

function ShieldExtenderAura::validateAuraTick(%this)
{
    if(%this.source.isDead || %this.source.wearingShieldPack != true)
    {
        %this.destroy();
        return false;
    }

    return true;
}

function ShieldExtenderAura::onEnter(%this, %obj)
{
    if(%obj.wearingShieldPack == true || %obj.isDead || %obj.client.team != %this.source.client.team)
    {
        %this.removeFromAura(%obj);
        return;
    }
    
//    messageClient(%obj.client, 'MsgShieldExtApplied', '\c2You are enveloped by a nearby shield projector.');
    %obj.isShielded = true;
    %obj.shieldSource = %this.source;
    %obj.playShieldEffect("0 0 1");
}

function ShieldExtenderAura::onLeave(%this, %obj)
{
//    messageClient(%obj.client, 'MsgShieldExtApplied', '\c2The shield projection has faded.');
    %obj.isShielded = false;
    %obj.shieldSource = 0;
    %obj.playShieldEffect("0 0 1");
}

Aura.registerAura("ShieldExtenderAura", $AuraType::Persistent);
