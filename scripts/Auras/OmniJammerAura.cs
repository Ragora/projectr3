function OmniJammerAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.noPersistentTick = true;
    %this.radius = 15;
}

function OmniJammerAura::validateAuraTick(%this)
{
    if(%this.source.isDead)
    {
        %this.destroy();
        return false;
    }

    return true;
}

function OmniJammerAura::onEnter(%this, %obj)
{
    if(%obj.isDead)
    {
        %this.removeFromAura(%obj);
        return;
    }

    %obj.weaponJammed = true;
}

function OmniJammerAura::onLeave(%this, %obj)
{
    %obj.weaponJammed = false;
}

Aura.registerAura("OmniJammerAura", $AuraType::Persistent);
