//SOUNDS ----- HEAVY ARMOR--------
datablock AudioProfile(LFootHeavySoftSound)
{
   filename    = "fx/armor/heavy_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavySoftSound)
{
   filename    = "fx/armor/heavy_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyHardSound)
{
   filename    = "fx/armor/heavy_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyHardSound)
{
   filename    = "fx/armor/heavy_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyMetalSound)
{
   filename    = "fx/armor/heavy_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyMetalSound)
{
   filename    = "fx/armor/heavy_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootHeavySnowSound)
{
   filename    = "fx/armor/heavy_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavySnowSound)
{
   filename    = "fx/armor/heavy_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyShallowSplashSound)
{
   filename    = "fx/armor/heavy_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyShallowSplashSound)
{
   filename    = "fx/armor/heavy_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyWadingSound)
{
   filename    = "fx/armor/heavy_LF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyWadingSound)
{
   filename    = "fx/armor/heavy_RF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyUnderwaterSound)
{
   filename    = "fx/armor/heavy_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyUnderwaterSound)
{
   filename    = "fx/armor/heavy_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(ImpactHeavySoftSound)
{
   filename    = "fx/armor/heavy_land_soft.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactSoftEffect;
};

datablock AudioProfile(ImpactHeavyHardSound)
{
   filename    = "fx/armor/heavy_land_hard.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactHardEffect;
};

datablock AudioProfile(ImpactHeavyMetalSound)
{
   filename    = "fx/armor/heavy_land_metal.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactMetalEffect;
};

datablock AudioProfile(ImpactHeavySnowSound)
{
   filename    = "fx/armor/heavy_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ImpactSnowEffect;
};

datablock AudioProfile(ImpactHeavyWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterHeavySound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------------------------------
datablock DecalData(HeavyMaleFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.5;
   textureName = "special/footprints/H_male";
};

datablock ParticleData(MagIonJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.7";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 1;
   sizes[1]      = 3;
};

datablock ParticleEmitterData(MagIonJetEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MagIonJetParticle";
};

// Structural Integrity Field - Activate an instant shield that absorbs 50 HP per GP (max 10) for 5 seconds, 60 sec cooldown
function triggerSpecialMagneticIon(%data, %obj, %imageData)
{
    %time = getSimTime();

    if(%obj.nextGyanShield > %time)
        return;

    if(%obj.gyanLevel < 1)
        return;
        
    if(%obj.gyanShieldSeconds > 0)
        return;
       
    %obj.isShielded = true;
    %obj.gyanShieldPool = %obj.gyanLevel * 0.5;
    %obj.gyanShieldSeconds = 5 + %obj.gyanLevel;
    %obj.setGyan(0);

    triggerSpecialMagneticIonT(%data, %obj, %imageData);
}

function triggerSpecialMagneticIonT(%data, %obj, %imageData)
{
    if(%obj.gyanShieldSeconds > 0 && %data.armorType == $ArmorType::MagneticIon)
    {
        commandToClient(%obj.client, 'BottomPrint', "Dreadnought Structural Integrity Field active, "@ %obj.gyanShieldSeconds @"s remaining.", 2, 1);

        %obj.gyanShieldSeconds--;
        %obj.gyanShieldThread = schedule(1000, %obj, "triggerSpecialMagneticIonT", %data, %obj, %imageData);
    }
    else
        triggerSpecialMagneticIonE(%data, %obj, %imageData);
}

function triggerSpecialMagneticIonE(%data, %obj, %imageData)
{
    cancel(%obj.gyanShieldThread);
    
    %obj.gyanShieldPool = 0;
    %obj.gyanShieldSeconds = 0;
    %obj.isShielded = false;
    %obj.nextGyanShield = getSimTime() + 60000;
    
    commandToClient(%obj.client, 'BottomPrint', "Dreadnought Structural Integrity Field depleted - cooling off, 60s", 5, 1);
    schedule(60000, %obj, "triggerSpecialMagneticIonCD", %obj);
}

function triggerSpecialMagneticIonCD(%obj)
{
    if(!%obj.isDead && %obj.getDatablock().armorType == $ArmorType::MagneticIon)
        commandToClient(%obj.client, 'BottomPrint', "Dreadnought Structural Integrity Field ready!", 5, 1);
}

datablock PlayerData(HeavyMaleHumanArmor) : ArmorDamageProfile
{
   emap = true;

   armorType = $ArmorType::MagneticIon;
   armorMask = $ArmorMask::MagneticIon;
   armorClass = $ArmorClassMask::Heavy;
   armorFlags = $ArmorFlags::ExplodeOnDeath | $ArmorFlags::NoDeathFiring;
   enhancementMultiplier = 3.0;
   enhancementSlots = 5;
   
   armorSpecialCount = 1;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   
   className = Armor;
   shapeFile = "heavy_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_new_packammo";
   hudImageNameEnemy[0] = "gui/hud_new_packammo";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = MagIonDebris;

   aiAvoidThis = true;

   minLookAngle = -1.5;
   maxLookAngle = 1.5;
   maxFreelookAngle = 3.0;

   mass = 180;
   drag = 0.23;
   maxdrag = 0.5;
   density = 27;
   maxDamage = 3.5;
   maxEnergy = 300;
   repairRate = 0.0033;
   energyPerDamagePoint = 70.0; // shield energy required to block one point of damage

   rechargeRate = 0.3125;
   jetForce = 29.58 * 180;
   underwaterJetForce = 25.25 * 180 * 1.5;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain =  1.75;
   underwaterJetEnergyDrain =  0.875;
   minJetEnergy = 3;
   maxJetHorizontalPercentage = 0.3;

   runForce = 40.25 * 180;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 7.5;
   maxBackwardSpeed = 5;
   maxSideSpeed = 5;

   maxUnderwaterForwardSpeed = 5;
   maxUnderwaterBackwardSpeed = 3;
   maxUnderwaterSideSpeed = 3;

   recoverDelay = 8;
   recoverRunForceScale = 0.2;

   jumpForce = 8.35 * 180;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 4.0; // takes 4 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 75;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   noFrictionOnSki = true;
   horizMaxSpeed = 500;
   horizResistSpeed = 34.81;
   horizResistFactor = 0;
   maxJetForwardSpeed = 20;

   upMaxSpeed = 52;
   upResistSpeed = 20.89;
   upResistFactor = 0.18;

   minImpactSpeed = 45;
   speedDamageScale = 0.0030;

   jetSound = MagIonJetSound;
   wetJetSound = ArmorJetSound;
   jetEmitter = MagIonJetEmitter;

   boundingBox = "1.63 1.63 2.6";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 11.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 5;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have

   // Inventory restrictions
   max[RepairKit]          = 3;
   max[Mine]               = 3;
   max[Grenade]            = 8;
   max[Blaster]            = 0;
   max[Plasma]             = 0;
   max[PlasmaAmmo]         = 0;
   max[Disc]               = 0;
   max[DiscAmmo]           = 0;
   max[SniperRifle]        = 1;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 200;
   max[Mortar]             = 1;
   max[MortarAmmo]         = 15;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 8;
   max[LaserMissileLauncher] = 1;
   max[RocketLauncher]     = 1;
   max[Chaingun]           = 0;
   max[ChaingunAmmo]       = 0;
   max[SubspaceMagnet]     = 1;
   max[SubspaceCapacitor]  = 30;
   max[RepairGun]          = 1;
   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[RepairPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[FlashGrenade]       = 8;
   max[ConcussionGrenade]  = 8;
   max[FlareGrenade]       = 8;
   max[TargetingLaser]     = 1;
   max[ELFGun]             = 1;
   max[Beacon]             = 3;

   max[SynomicRegenerator] = 1;
   
   max[MitziBlastCannon]   = 1;
   max[MitziCapacitor]     = 100;
   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[Railgun]            = 1;
   max[RailgunAmmo]        = 8;
   
   max[Bolter]             = 1;
   max[BolterAmmo]         = 100;
   max[PlasmaCannon]       = 1;
   max[PlasmaCannonAmmo]   = 400;
   max[Ripper]             = 1;
   max[RipperAmmo]         = 40;
   max[ParticleBeamCannon] = 1;
   max[PBCAmmo]            = 10;
   max[MPDisruptor]        = 1;
   max[BlasterRifle]       = 1;
   
   max[StarHammerAmmo]     = 20;
   max[ThrusterGrenade]    = 0;
   
   max[SonicPulser] = 8;
   max[NapalmGrenade] = 8;
   max[EMPGrenade] = 8;
   max[CloakingMine] = 0;
   max[KoiMine] = 0;
   max[ConcussionMine] = 0;
   max[RepairPatchMine] = 5;

   max[Nosferatu] = 1;
   max[SubspaceRegenerator] = 1;
   max[MagneticClamp] = 1;
   max[DreadnoughtEVAC] = 1;
   max[DreadStarHammer] = 1;
   max[DreadMeteorCannon] = 1;
   max[LifeSupport] = 1;
   max[NonNewtonianPadding] = 1;
   max[ShieldHardener] = 1;
   max[ResourceHarvester] = 1;
   
   observeParameters = "0.5 4.5 4.5";
   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------
datablock PlayerData(HeavyFemaleHumanArmor) : HeavyMaleHumanArmor
{
   shapeFile = "heavy_male.dts";
   waterBreathSound = WaterBreathFemaleSound;
};

//----------------------------------------------------------------------------
datablock DecalData(HeavyBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.5;
   textureName = "special/footprints/H_bioderm";
};

datablock PlayerData(HeavyMaleBiodermArmor) : HeavyMaleHumanArmor
{
   emap = false;

   shapeFile = "bioderm_heavy.dts";
   jetEmitter = BiodermArmorJetEmitter;

   debrisShapeName = "bio_player_debris.dts";

   //Foot Prints
   decalData    = HeavyBiodermFootprint;
   decalOffset  = 0.4;

   waterBreathSound = WaterBreathBiodermSound;
};

