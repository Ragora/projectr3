//SOUNDS ----- Medium ARMOR--------
datablock AudioProfile(LFootMediumSoftSound)
{
   filename    = "fx/armor/med_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumSoftSound)
{
   filename    = "fx/armor/med_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumHardSound)
{
   filename    = "fx/armor/med_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumHardSound)
{
   filename    = "fx/armor/med_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumMetalSound)
{
   filename    = "fx/armor/med_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumMetalSound)
{
   filename    = "fx/armor/med_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootMediumSnowSound)
{
   filename    = "fx/armor/med_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumSnowSound)
{
   filename    = "fx/armor/med_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumShallowSplashSound)
{
   filename    = "fx/armor/med_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumShallowSplashSound)
{
   filename    = "fx/armor/med_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumWadingSound)
{
   filename    = "fx/armor/med_LF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumWadingSound)
{
   filename    = "fx/armor/med_RF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumUnderwaterSound)
{
   filename    = "fx/armor/med_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumUnderwaterSound)
{
   filename    = "fx/armor/med_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(ImpactMediumSoftSound)
{
   filename    = "fx/armor/med_land_soft.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ImpactSoftEffect;
};

datablock AudioProfile(ImpactMediumHardSound)
{
   filename    = "fx/armor/med_land_hard.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactHardEffect;
};

datablock AudioProfile(ImpactMediumMetalSound)
{
   filename    = "fx/armor/med_land_metal.wav";
   description = AudioClose3d;
   preload = true;
   effect = ImpactMetalEffect;
};

datablock AudioProfile(ImpactMediumSnowSound)
{
   filename    = "fx/armor/med_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ImpactSnowEffect;
};

datablock AudioProfile(ImpactMediumWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterMediumSound)
{
   filename    = "fx/armor/general_water_exit.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------------------------------

datablock DecalData(MediumMaleFootprint)
{
   sizeX       = 0.125;
   sizeY       = 0.25;
   textureName = "special/footprints/M_male";
};

// Juggernaut - Reduces damage taken by 10% and increases recharge rate by 20% per GP (max 5), lasts for 30 seconds, 3 minute cooldown
function triggerSpecialAssault(%data, %obj, %imageData)
{
    %time = getSimTime();

    if(%obj.nextJuggernaut > %time)
        return;

    if(%obj.gyanLevel < 1)
        return;

    %gpuse = %obj.gyanLevel > 5 ? 5 : %obj.gyanLevel;

    %obj.juggMode = true;
    %obj.juggRechargeBoost = %data.rechargeRate * (0.2 * %gpuse);
    %obj.juggDamageReduction = %gpUse * 0.1;

    %obj.damageReduceFactor -= %obj.juggDamageReduction;
    %obj.setRechargeRate(%obj.getRechargeRate() + %obj.juggRechargeBoost);
    %obj.decGyan(%gpuse);
    %obj.play3D("BomberBombFireSound");

    commandToClient(%obj.client, 'BottomPrint', "Juggernaut mode engaged! ", 5, 1);

    %obj.nextJuggernaut = %time + 210000;
    schedule(30000, %obj, "triggerSpecialAssaultE", %data, %obj, %imageData);
}

function triggerSpecialAssaultE(%data, %obj, %imageData)
{
    if(!%obj.isDead && %obj.getDatablock().armorType == $ArmorType::Assault)
    {
        %obj.juggMode = false;
        %obj.damageReduceFactor += %obj.juggDamageReduction;
        %obj.setRechargeRate(%obj.getRechargeRate() - %obj.juggRechargeBoost);

        commandToClient(%obj.client, 'BottomPrint', "Juggernaut mode deactivated - cooling off, 3m", 5, 1);
        schedule(180000, %obj, "triggerSpecialAssaultCD", %obj);
    }
}

function triggerSpecialAssaultCD(%obj)
{
    if(!%obj.isDead && %obj.getDatablock().armorType == $ArmorType::Assault)
        commandToClient(%obj.client, 'BottomPrint', "Juggernaut mode ready!", 5, 1);
}


datablock PlayerData(MediumMaleHumanArmor) : ArmorDamageProfile
{
   emap = true;

   armorType = $ArmorType::Assault;
   armorMask = $ArmorMask::Assault;
   armorClass = $ArmorClassMask::Medium;
   armorFlags = $ArmorFlags::CanPilot;
   enhancementMultiplier = 2.0;
   enhancementSlots = 4;
   
   armorSpecialCount = 1;
   armorSpecial[0] = "TargetingLaserFakeImage";
   armorSpecial[1] = "ArmorSpecialImage";
   
   className = Armor;
   shapeFile = "medium_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_elfgun";
   hudImageNameEnemy[0] = "gui/hud_elfgun";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.5;
   maxLookAngle = 1.5;
   maxFreelookAngle = 3.0;

   mass = 130;
   drag = 0.2;
   maxdrag = 0.4;
   density = 20;
   maxDamage = 2.5;
   maxEnergy = 150;
   repairRate = 0.0033;
   energyPerDamagePoint = 75.0; // shield energy required to block one point of damage

   rechargeRate = 0.256;
   jetForce = 33.79 * 130;
   underwaterJetForce = 27.00 * 130 * 1.5;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain =  1.3;
   underwaterJetEnergyDrain =  0.725;
   minJetEnergy = 3;
   maxJetHorizontalPercentage = 0.5;

   runForce = 46 * 130;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 12;
   maxBackwardSpeed = 10;
   maxSideSpeed = 10;

   maxUnderwaterForwardSpeed = 10;
   maxUnderwaterBackwardSpeed = 8;
   maxUnderwaterSideSpeed = 8;

   recoverDelay = 8;
   recoverRunForceScale = 0.8;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 4.0; // takes 4 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   jumpForce = 8.5 * 130;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 15;
   maxJumpSpeed = 25;

   noFrictionOnSki = true;
   horizMaxSpeed = 500;
   horizResistSpeed = 41.78;
   horizResistFactor = 0;
   maxJetForwardSpeed = 25;

   upMaxSpeed = 52;
   upResistSpeed = 20.89;
   upResistFactor = 0.23;

   minImpactSpeed = 45;
   speedDamageScale = 0.0023;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;

   jetEmitter = HumanArmorJetEmitter;
   jetEffect = HumanMediumArmorJetEffect;

   boundingBox = "1.45 1.45 2.4";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.35;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootMediumSoftSound;
   RFootSoftSound       = RFootMediumSoftSound;
   LFootHardSound       = LFootMediumHardSound;
   RFootHardSound       = RFootMediumHardSound;
   LFootMetalSound      = LFootMediumMetalSound;
   RFootMetalSound      = RFootMediumMetalSound;
   LFootSnowSound       = LFootMediumSnowSound;
   RFootSnowSound       = RFootMediumSnowSound;
   LFootShallowSound    = LFootMediumShallowSplashSound;
   RFootShallowSound    = RFootMediumShallowSplashSound;
   LFootWadingSound     = LFootMediumWadingSound;
   RFootWadingSound     = RFootMediumWadingSound;
   LFootUnderwaterSound = LFootMediumUnderwaterSound;
   RFootUnderwaterSound = RFootMediumUnderwaterSound;
   LFootBubblesSound    = LFootMediumBubblesSound;
   RFootBubblesSound    = RFootMediumBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactMediumSoftSound;
   impactHardSound      = ImpactMediumHardSound;
   impactMetalSound     = ImpactMediumMetalSound;
   impactSnowSound      = ImpactMediumSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactMediumWaterEasySound;
   impactWaterMedium    = ImpactMediumWaterMediumSound;
   impactWaterHard      = ImpactMediumWaterHardSound;

   groundImpactMinSpeed    = 13.0;
   groundImpactShakeFreq   = "3.5 3.5 3.5";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.7;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterMediumSound;

   maxWeapons = 4;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have

   // Inventory restrictions
   max[RepairKit]          = 2;
   max[Mine]               = 3;
   max[Grenade]            = 6;
   max[Blaster]            = 1;
   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 500;
   max[Disc]               = 1;
   max[DiscAmmo]           = 25;
   max[SniperRifle]        = 1;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 100; // z0dd - ZOD, 9/28/02. Was 12
   max[Mortar]             = 1;
   max[MortarAmmo]         = 5;
   max[MissileLauncher]    = 1;
   max[LaserMissileLauncher] = 1;
   max[RocketLauncher]     = 1;
   max[MissileLauncherAmmo]= 4;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 300;
   max[SubspaceMagnet]     = 1;
   max[SubspaceCapacitor]  = 20;
   max[RepairGun]          = 1;
   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[RepairPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[SatchelCharge]      = 1;
   max[FlashGrenade]       = 6;
   max[ConcussionGrenade]  = 6;
   max[FlareGrenade]       = 6;
   max[TargetingLaser]         = 1;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[Beacon]             = 3;

   max[SynomicRegenerator] = 1;
   
   max[MitziBlastCannon]   = 1;
   max[MitziCapacitor]     = 50;
   max[SpikeRifle]         = 1;
   max[SpikeAmmo]          = 100;
   max[Bolter]             = 0;
   max[BolterAmmo]         = 0;
   max[PlasmaCannon]       = 0;
   max[PlasmaCannonAmmo]   = 0;
   max[Ripper]             = 0;
   max[RipperAmmo]         = 0;
   max[ParticleBeamCannon] = 1;
   max[PBCAmmo]            = 5;
   max[MPDisruptor]        = 0;
   max[BlasterRifle]       = 1;
   
   max[ThrusterGrenade]    = 1;
   max[CameraGrenade]      = 3;

   max[SonicPulser] = 5;
   max[NapalmGrenade] = 5;
   max[EMPGrenade] = 5;
   max[CloakingMine] = 0;
   max[KoiMine] = 3;
   max[ConcussionMine] = 3;
   max[RepairPatchMine] = 3;
   
   max[Nosferatu] = 1;
   max[SubspaceRegenerator] = 1;
   max[MagneticClamp] = 1;
   max[AssaultRPGLauncher] = 1;
   max[LifeSupport] = 1;
   max[NonNewtonianPadding] = 1;
   max[ShieldHardener] = 1;
   max[ResourceHarvester] = 1;
   
   observeParameters = "0.5 4.5 4.5";
   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------
datablock PlayerData(MediumFemaleHumanArmor) : MediumMaleHumanArmor
{
   shapeFile = "medium_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock DecalData(MediumBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.25;
   textureName = "special/footprints/M_bioderm";
};

datablock PlayerData(MediumMaleBiodermArmor) : MediumMaleHumanArmor
{
   shapeFile = "bioderm_medium.dts";
   jetEmitter = BiodermArmorJetEmitter;
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = BiodermDebris;
   
   //Foot Prints
   decalData   = MediumBiodermFootprint;
   decalOffset = 0.35;

   waterBreathSound = WaterBreathBiodermSound;
};


