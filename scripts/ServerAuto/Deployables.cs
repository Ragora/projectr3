// Deployables Core Library

// ScriptObject RBaseCoreController
// 3 simsets: structure (pieces), shields, internals
// special internal: RemoteBaseGenerator - upon destruction will destroy the entire base
// All internals and shields are self powered, any damage done to them will be redirected
// to the RBaseCoreController's HP pool, any healing done will be reflected by every structure object

function DefaultGame::clearDeployableMaxes(%game)
{
   for(%i = 0; %i <= %game.numTeams; %i++)
   {
      $TeamDeployedCount[%i, TurretIndoorDeployable] = 0;
      $TeamDeployedCount[%i, TurretOutdoorDeployable] = 0;
      $TeamDeployedCount[%i, PulseSensorDeployable] = 0;
      $TeamDeployedCount[%i, MotionSensorDeployable] = 0;
      $TeamDeployedCount[%i, InventoryDeployable] = 0;
      $TeamDeployedCount[%i, DeployedCamera] = 0;
      $TeamDeployedCount[%i, MineDeployed] = 0;
      $TeamDeployedCount[%i, TargetBeacon] = 0;
      $TeamDeployedCount[%i, MarkerBeacon] = 0;

      // MD3 stuff
      $TeamDeployedCount[%i, DeployableInvStationPack] = 0;
      $TeamDeployedCount[%i, TelePack] = 0;
      $TeamDeployedCount[%i, SensorBasePack] = 0;
      $TeamDeployedCount[%i, CommandBasePack] = 0;
      $TeamDeployedCount[%i, SatelliteBasePack] = 0;
      $TeamDeployedCount[%i, BlastWallPack] = 0;
      $TeamDeployedCount[%i, TurretBasePack] = 0;
      $TeamDeployedCount[%i, ShieldGeneratorPack] = 0;
      $TeamDeployedCount[%i, BussardCollectorPack] = 0;
      $TeamDeployedCount[%i, ResourceDrillerPack] = 0;
      $TeamDeployedCount[%i, BaseCorePack] = 0;
      $TeamDeployedCount[%i, NodeSpawnFavsPack] = 0;
      $TeamDeployedCount[%i, NodeRegeneratorPack] = 0;
      $TeamDeployedCount[%i, NodeRepulsorPack] = 0;
      $TeamDeployedCount[%i, NodeShieldPack] = 0;
      $TeamDeployedCount[%i, NodeCloakPack] = 0;
   }
}

// Construction Building Datablock Compatibility Library

$NotDeployableReason::EnemyFlagTooClose         =  20;
$NotDeployableReason::FriendlyFlagTooClose      =  21;
$NotDeployableReason::RequiresBaseNode          =  22;
$NotDeployableReason::OutOfBounds               =  23;
$NotDeployableReason::EnemySpawnTooClose        =  24;
$NotDeployableReason::FriendlySpawnTooFar       =  25;
$NotDeployableReason::BaseNodeTooFar            =  26;
$NotDeployableReason::ExtendedDeployFail        =  27;

// Deployable system overrides
function ShapeBaseImageData::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      $MaxDeployDistance,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSlopeTooGreat(%surface, %surfaceNrm))
   {
      %disqualified = $NotDeployableReason::SlopeTooGreat;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if (%item.testObjectTooClose(%surfacePt))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose;
   }
   else if (%item.testTurretTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::TurretTooClose;
   }
   else if (%item.testInventoryTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::InventoryTooClose;
   }
   else if (%item.testTurretSaturation())
   {
      %disqualified = $NotDeployableReason::TurretSaturation;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::ExtendedDeployFail;
   }
   else if(%item.testEnemyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemyFlagTooClose;
   }
   else if(%item.testFriendlyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlyFlagTooClose;
   }
   else if(%item.testRequiresBaseNode(%plyr))
   {
      %disqualified = $NotDeployableReason::RequiresBaseNode;
   }
   else if(%item.testEnemySpawnTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemySpawnTooClose;
   }
   else if(%item.testFriendlySpawnTooFar(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlySpawnTooFar;
   }
   else if(%item.testOutOfBounds(%plyr))
   {
      %disqualified = $NotDeployableReason::OutOfBounds;
   }
   else if(%item.testBaseNodeTooFar(%plyr))
   {
      %disqualified = $NotDeployableReason::BaseNodeTooFar;
   }
   //---------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//   else if (%item.testOrganicTooClose(%plyr))
//   {
//      %disqualified = $NotDeployableReason::OrganicTooClose;
//   }
   //---------------------------------------------------------------------------------------
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      if(%item.deployed.className $= "DeployedTurret")
      {
         %xform = %item.deployed.getDeployTransform(%item.surfacePt, %item.surfaceNrm);
      }
      else
      {
         %xform = %surfacePt SPC %rot;
      }

      if (!%item.deployed.checkDeployPos(%xform))
      {
         %disqualified = $NotDeployableReason::ObjectTooClose;
      }
      else if (!%item.testHavePurchase(%xform))
      {
         %disqualified = $NotDeployableReason::SurfaceTooNarrow;
      }
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(25, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function Deployables::displayErrorMsg(%item, %plyr, %slot, %error)
{
   deactivateDeploySensor(%plyr);

   %errorSnd = '~wfx/misc/misc.error.wav';
   switch (%error)
   {
      case $NotDeployableReason::None:
         %item.onDeploy(%plyr, %slot);
         messageClient(%plyr.client, 'MsgTeamDeploySuccess', "");
         return;

      case $NotDeployableReason::NoSurfaceFound:
         %msg = '\c2Item must be placed within reach.%1';

      case $NotDeployableReason::MaxDeployed:
         %msg = '\c2Your team\'s control network has reached its capacity for this item.%1';

      case $NotDeployableReason::SlopeTooGreat:
         %msg = '\c2Surface is too steep to place this item on.%1';

      case $NotDeployableReason::SelfTooClose:
         %msg = '\c2You are too close to the surface you are trying to place the item on.%1';

      case $NotDeployableReason::ObjectTooClose:
         %msg = '\c2You cannot place this item so close to another object.%1';

      case $NotDeployableReason::NoTerrainFound:
         %msg = '\c2You must place this on outdoor terrain.%1';

      case $NotDeployableReason::NoInteriorFound:
         %msg = '\c2You must place this on a solid surface.%1';

      case $NotDeployableReason::TurretTooClose:
         %msg = '\c2Interference from a nearby turret prevents placement here.%1';

      case $NotDeployableReason::TurretSaturation:
         %msg = '\c2There are too many turrets nearby.%1';

      case $NotDeployableReason::SurfaceTooNarrow:
         %msg = '\c2There is not adequate surface to clamp to here.%1';

      case $NotDeployableReason::InventoryTooClose:
         %msg = '\c2Interference from a nearby inventory prevents placement here.%1';

      case $NotDeployableReason::EnemyFlagTooClose:
         %msg = '\c2Cannot place here, enemy flag is too close.%1';

      case $NotDeployableReason::FriendlyFlagTooClose:
         %msg = '\c2Cannot place here, friendly flag is too close.%1';
         
      case $NotDeployableReason::RequiresBaseNode:
         %msg = '\c2Cannot deploy, a Base Core Node must be placed first.%1';
         
      case $NotDeployableReason::EnemySpawnTooClose:
         %msg = '\c2Cannot place here, enemy defense in this area is too strong.%1';
         
      case $NotDeployableReason::FriendlySpawnTooFar:
         %msg = '\c2Cannot place here, you are too far from your base\'s network.%1';
         
      case $NotDeployableReason::OutOfBounds:
         %msg = '\c2No, you can\'t deploy outside the mission area.%1';
         
      case $NotDeployableReason::BaseNodeTooFar:
         %msg = '\c2Cannot place here, it is too far from your team\'s Base Core Node.%1';
         
      case $NotDeployableReason::ExtendedDeployFail:
         %msg = "";
         
      // --------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//      case $NotDeployableReason::OrganicTooClose:
//         %msg = '\c2You cannot place this item so close to an organic object.%1';
      // --------------------------------------------------------------------------------------

      default:
         %msg = '\c2Deploy failed.';
   }
   
   if(%error == $NotDeployableReason::ExtendedDeployFail)
        messageClient(%plyr.client, 'MsgDeployFailed', '\c2%2%1', %errorSnd, $NotDeployableReason::ExtendedDeployFailReason);
   else
        messageClient(%plyr.client, 'MsgDeployFailed', %msg, %errorSnd);
}

function getTeamSpawnSphereCount(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
      return %group.getCount();
   else
      return 0;
}

function getTeamSpawnSphere(%team, %index)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%index > %count)
          return 0;

      %sphere = %group.getObject(%index);

      if(isObject(%sphere))
           return %sphere;

      return 0; // no spawnsphere found, nuts
   }
}

function getTeamSpawnSpheres(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();
      %spheres = %count;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
              %spheres = %spheres SPC %sphere;
      }

      return %spheres; // no spawnsphere found, nuts
   }
}

function getTeamNearestSpawnSphere(%team, %pos)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops"@%team;
   %sphereIndex = 0;
   %group = nameToID(%teamDropsGroup);
   %lowestDist = 0;
   %lowestDistID = 0;

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%count < 1)
           return isObject(%group.getObject(0)) ? %group.getObject(0) : 0;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
          {
              %sphereArray[%sphereIndex] = %sphere;
              %dist = VectorDist(%pos, %sphere.position);

              if(%dist < %lowestDist)
              {
                   %lowestDist = %dist;
                   %lowestDistID = %i;
              }

              %sphereIndex++;
          }
      }

      return %sphereArray[%lowestDistID];
   }

   return 0; // No sphere
}

function getEnemyNearestSpawnSphere(%team, %pos)
{
     %team = %team == 2 ? 1 : 2;
     return getTeamNearestSpawnSphere(%team, %pos);
}

function ShapeBase::isNearbyFriendlyBase(%obj, %rad)
{
   %team = %obj.client.team;
   %pos = %obj.getWorldBoxCenter();

   %friendlyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%friendlyBase))
   {
      %dist = VectorDist(%friendlyBase.position, %pos);

      if(%dist <= %rad)
         return true;
   }

   return false;
}

function ShapeBase::isNearbyEnemyBase(%obj, %rad)
{
   %team = %obj.client.team == 2 ? 1 : 2;
   %pos = %obj.getWorldBoxCenter();

   %enemyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%enemyBase))
   {
      %dist = VectorDist(%enemyBase.position, %pos);

      if(%dist <= %rad)
         return true;
   }

   return false;
}

function ShapeBaseImageData::testOutOfBounds(%item, %plyr)
{
    return %plyr.client.outOfBounds;
}

function ShapeBaseImageData::extendedDeployChecks(%item, %plyr)
{
    $NotDeployableReason::ExtendedDeployFailReason = "";

    return false;
}

function ShapeBaseImageData::testEnemyFlagTooClose(%item, %plyr)
{
    if(%item.maxEnemyFlagDist $= "" || %item.maxEnemyFlagDist < 1)
        return false;

    %team = %plyr.client.team == 1 ? 2 : 1;
    %enemyFlag = $TeamFlag[%team];

    if(isObject(%enemyFlag))
    {
        %dist = VectorDist(%enemyFlag.originalPosition, %plyr.position);
        
        if(%dist <= %item.maxEnemyFlagDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testFriendlyFlagTooClose(%item, %plyr)
{
    if(%item.minFriendlyFlagDist $= "" || %item.minFriendlyFlagDist < 1)
        return false;

    %team = %plyr.client.team;
    %friendlyFlag = $TeamFlag[%team];

    if(isObject(%friendlyFlag))
    {
        %dist = VectorDist(%friendlyFlag.originalPosition, %plyr.position);

        if(%dist <= %item.minFriendlyFlagDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testRequiresBaseNode(%item, %plyr)
{
    if(%item.requiresBaseNode $= "" || %item.requiresBaseNode == false)
        return false;

    if($BaseCore[%plyr.client.team])
        return false;
    else
        return true;
}

function ShapeBaseImageData::testBaseNodeTooFar(%item, %plyr)
{
    if(%item.maxBaseNodeDist $= "" || %item.maxBaseNodeDist == false)
        return false;

    if($BaseCore[%plyr.client.team])
    {
        if(vectorDist($BaseCore[%plyr.client.team].position, %plyr.position) >= %item.maxBaseNodeDist)
            return true;
    }
    else
        return true;

    return false;
}

function ShapeBaseImageData::testEnemySpawnTooClose(%item, %plyr)
{
    if(%item.maxEnemySpawnDist $= "" || %item.maxEnemySpawnDist < 1)
        return false;

    %team = %plyr.client.team == 1 ? 2 : 1;
    %pos = %plyr.getWorldBoxCenter();

    %enemySpawn = getTeamNearestSpawnSphere(%team, %pos);

    if(isObject(%enemySpawn))
    {
        %dist = VectorDist(%enemySpawn.position, %pos);

        if(%dist <= %item.maxEnemySpawnDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testFriendlySpawnTooFar(%item, %plyr)
{
    if(%item.maxFriendlySpawnDist $= "" || %item.maxFriendlySpawnDist < 1)
        return false;

    %team = %plyr.client.team;
    %pos = %plyr.getWorldBoxCenter();

    %friendlySpawn = getTeamNearestSpawnSphere(%team, %pos);

    if(isObject(%friendlySpawn))
    {
        %dist = VectorDist(%friendlySpawn.position, %pos);

        if(%dist >= %item.maxFriendlySpawnDist)
            return true;
    }

    return false;
}

// Linked Component System - Flags
$LC::Node                       = 1 << 0;
$LC::Invulnerable               = 1 << 1;
$LC::DeleteOnly                 = 1 << 2;
$LC::DestroysParent             = 1 << 3;
$LC::RandomChildDeath           = 1 << 4;
$LC::PrimaryNode                = 1 << 5;
$LC::RootNode                   = 1 << 6;
$LC::IndependantDamage          = 1 << 7;
$LC::DeleteOnDestroy            = 1 << 8;
$LC::Forcefield                 = 1 << 9;

datablock StaticShapeData(GenericRootNode)
{
   shapeFile = "turret_muzzlepoint.dts";
   explosion      = HandGrenadeExplosion; //ShapeExplosion;

   maxDamage      = 1.0;
   destroyedLevel = 1.0;
   disabledLevel  = 1.0;

   expDmgRadius = 5.0;
   expDamage = 0.5;
   expImpulse = 750.0;

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   // Tag objects based on whatever they would be called
//   targetTypeTag = 'Bombardment Base';
};

function GenericRootNode::onDestroyed(%data, %obj, %prevState)
{
    Parent::onDestroyed(%data, %obj, %prevState);

    if(%obj.deployedItem !$= "")
        $TeamDeployedCount[%obj.team, %obj.deployedItem]--;
}

function createRootNode(%plyr, %trans, %type)
{
    if(%type $= "")
        %type = "GenericRootNode";

    %root = createTeamObject(%plyr, "StaticShape", %type, %trans, false, "1 1 1");
    %root.createNode();
    %root.setRootNode();

    return %root;
}

function ShapeBase::setRootNode(%obj)
{
    if(%obj.flags & $LC::RootNode)
        return;

    %obj.flags |= $LC::DeleteOnly | $LC::RootNode;
    %obj.isLinkedNode = true;
    %obj.parentNode = %obj;
    %obj.childNodes = new SimSet();
}

function ShapeBase::getRootNode(%obj)
{
    if(%obj.isLinkedNode)
    {
        if(!isObject(%obj.parentNode))
            return 0;

        if(%obj.flags & $LC::RootNode)
            return %obj;
        else
            return %obj.parentNode;
    }
    else
        return 0;
}

function ShapeBase::isRootNode(%obj)
{
    return %obj.flags & $LC::RootNode;
}

function ShapeBase::propagateDamageLevel(%obj, %damage)
{
    %obj.setDamageLevel(%damage);
//    echo("propagatedamagelevel" SPC %obj SPC %obj.getDatablock().getName() SPC %damage);

    for(%idx = 0; %idx < %obj.childNodes.getCount(); %idx++)
    {
        %child = %obj.childNodes.getObject(%idx);

        if(!isObject(%child))
            continue;

        if(%child.flags & ($LC::IndependantDamage | $LC::Invulnerable))
            continue;

        %obj.setDamageLevel(%damage);
    }
}

function GameBase::createNode(%obj, %flags)
{
    if(%obj.parentNode !$= "")
        return;
        
    %obj.isLinkedNode = false;
    %obj.parentNode = 0;
    %obj.flags = %flags;
}

function GameBase::linkNode(%obj, %parent)
{
    if(%obj.isLinkedNode == true)
        %obj.unlinkNode();

    %obj.isLinkedNode = true;
    %obj.parentNode = %parent;
    %parent.childNodes.add(%obj);
}

function GameBase::addNode(%obj, %node, %flags)
{
    if(%obj.parentNode $= "")
        %node.createNode(%flags);
    else
        %node.unlinkNode();

    %node.flags = %flags;
    %node.isLinkedNode = true;
    %node.parentNode = %obj;
    
    %obj.childNodes.add(%node);
}

function GameBase::unlinkNode(%obj)
{
    if(%obj.parentNode != %obj)
    {
        if(isObject(%obj.parentNode))
            %obj.parentNode.childNodes.remove(%obj);
            
        %obj.parentNode = 0;
        %obj.isLinkedNode = false;
    }
}

function ShapeBase::destroyNode(%obj)
{
//    echo("DestroyNode called!" SPC %obj SPC %obj.getDatablock().getName());
    
    for(%idx = 0; %idx < %obj.childNodes.getCount(); %idx++)
    {
        %child = %obj.childNodes.getObject(%idx);
        
        if(!isObject(%child))
            continue;
        
        if(%child.flags & $LC::DeleteOnly)
            %child.deleteNode();
        else if(%child.flags & $LC::Forcefield)
        {
            %pos = "-10000 -10000 -10000";
            %child.setPosition(%pos);

            if(isObject(%child.pz))
            {
                %child.pz.setPosition(%pos);
                %child.getDatablock().losePower(%child);
                %child.pz.schedule(500, "delete");
            }
            
            %child.delete();
        }
        else
        {
            if(%child.getDamageState() !$= "Destroyed")
            {
                if(%child.flags & $LC::RandomChildDeath)
                {
                    %t = getRandom(500, 3000);
                    %child.schedule(%t, "setDamageState", "Destroyed");

                    if(%child.flags & $LC::DeleteOnDestroy)
                    {
                        if(isObject(%child.trigger))
                            %child.trigger.delete();
                            
                        %child.schedule(%t, "deleteNode");
                    }
                }
                else
                {
                    %child.setDamageState("Destroyed");
                    
                    if(%child.flags & $LC::DeleteOnDestroy)
                    {
                        if(isObject(%child.trigger))
                            %child.trigger.delete();
                            
                        %child.deleteNode();
                    }
                }
            }
            else
                %child.deleteNode();
        }
    }
    
    %obj.deleteNode(4000);
}

function GameBase::deleteNode(%obj, %time)
{
    if(%time $= "")
        %time = 500;
        
    %obj.schedule(400, "setPosition", ((getRandom() * 10000) SPC (getRandom() * 10000) SPC "-10000"));
    %obj.schedule(%time, "delete");
}

function GameBase::setOffsetPosition(%obj, %newPos)
{
    %obj.setPosition(vectorAdd(%obj.position, %newPos));
}

function ShapeBase::initializeBasePiece(%obj, %plyr, %newPos, %parent, %flags)
{
    %obj.initializeTeamObject(%obj, %plyr);
    %obj.setOffsetPosition(%newPos);
    %parent.addNode(%obj, %flags);
}

function ShapeBase::initializeTeamObject(%obj, %plyr)
{
    MissionCleanup.add(%obj);
    addToDeployGroup(%obj);
    AIDeployObject(%plyr.client, %obj);

    %obj.team = %plyr.client.team;
    %obj.owner = %plyr.client;

    if(%obj.getTarget() != -1)
        setTargetSensorGroup(%obj.getTarget(), %plyr.client.team);
}

function createTeamObject(%plyr, %type, %data, %trans, %selfPower, %scale)
{
    if(%scale $= "")
        %scale = "1 1 1";

    if(%selfPower $= "")
        %selfPower = false;
        
    %obj = new (%type)()
    {
        dataBlock = %data;
//        team = %plyr.client.team;
        scale = %scale;
    };

    %obj.initializeTeamObject(%plyr);
    %obj.setTransform(%trans);

    if(%selfPower)
    {
        %obj.setSelfPowered();
        %obj.setRechargeRate(%data.rechargeRate);
    }
    
    return %obj;
}

function createTeamObjectV(%plyr, %type, %data, %pos, %vec, %selfPower, %scale)
{
    if(%scale $= "")
        %scale = "1 1 1";

    if(%selfPower $= "")
        %selfPower = false;

    %obj = new (%type)()
    {
        dataBlock = %data;
//        team = %plyr.client.team;
        scale = %scale;
    };

    %obj.initializeTeamObject(%plyr);
    %obj.setPosition(%pos);
    %obj.setDeployRotation(%pos, %vec);

    if(%selfPower)
    {
        %obj.setSelfPowered();
        %obj.setRechargeRate(%data.rechargeRate);
    }

    return %obj;
}

function addStation(%plyr, %trans, %bSelfPowered, %parent)
{
    %obj = createTeamObject(%plyr, "StaticShape", "StationInventory", %trans, %bSelfPowered, "1 1 1");
    %obj.createNode($LC::PrimaryNode | $LC::IndependantDamage);

    if(isObject(%parent))
        %obj.linkNode(%parent);
    
    %trigger = new Trigger()
    {
        dataBlock = stationTrigger;
        polyhedron = "-0.75 0.75 0.1 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.3";
    };
    MissionCleanup.add(%obj);
    
    %trigger.setTransform(%obj.getTransform());
    %trigger.station = %obj;
    %trigger.mainObj = %obj;
    %trigger.disableObj = %obj;

    %obj.trigger = %trigger;

    return %obj;
}

function addTurret(%plyr, %trans, %bSelfPowered, %parent, %barrel)
{
    if(%barrel $= "")
        %barrel = "PlasmaBarrelLarge";

    %turret = new Turret()
    {
        dataBlock = "FreeBaseTurret";
        initialBarrel = %barrel;
    };

    %turret.initializeTeamObject(%plyr);
    %turret.setTransform(%trans);
    %turret.createNode($LC::PrimaryNode | $LC::IndependantDamage);

    if(%bSelfPowered)
    {
        %turret.setSelfPowered();
        %turret.setRechargeRate(%turret.getDatablock().rechargeRate);
    }

    if(isObject(%parent))
        %turret.linkNode(%parent);

    return %turret;
}

function addForcefield(%plyr, %data, %pos, %rot, %scale, %parent)
{
   %ff = new ForceFieldBare()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);

   if(isObject(%parent))
      %parent.addNode(%ff, $LC::Invulnerable | $LC::DeleteOnly | $LC::Forcefield);

   return %ff;
}

function addForcefieldV(%plyr, %data, %pos, %vec, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
//      rotation = "";
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };
   %ff.setDeployRotation(%pos, %vec);

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);

   if(isObject(%parent))
      %parent.addNode(%ff, $LC::Invulnerable | $LC::DeleteOnly | $LC::Forcefield);

   return %ff;
}

// legacy MD2 function - refactored for new node system
// keen: this seems to be returning true not finding anything? wat - see commented out code
function glueToRemoteBase(%deplObj, %pos)
{
    %found = false;
    
    InitContainerRadiusSearch(%pos, 2, $TypeMasks::StaticShapeObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.isLinkedNode == true && %int.getClassName() !$= "TerrainBlock" && %int.getClassName() !$= "InteriorInstance") // && %int.getClassName() !$= "StaticTSObject" && !%int.isPlayer() && !%int.isVehicle())
        {
            %deplObj.linkNode(%int.parentNode);
//            %deplObj.setSelfPowered();
//            %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);
            break; // keen: return here seemed to continue executing code? gave me a syntax error
        }
    }
    
    if(%found)
        return true;
    else
        return false;
}

// Functions
function addTrigger(%plyr, %data, %pos, %rot, %vec, %scale, %parent)
{
   %Tg = new Trigger()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };
   %Tg.setDeployRotation(%pos, %vec);

   %ff.target = createTarget(%TG, "Trigger", "", "", "", 0, 0);
   MissionCleanup.add(%Tg);
   //%Tg.parent = %parent;

   //if(!%parent.attachedFF)
   //   %parent.attachedFF = 0;

   //%parent.shield[%parent.attachedFF] = %Tg;
   //%parent.attachedFF++;

   //return %Tg;
}

function ForceFieldBareData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   %pz = new PhysicalZone() {
      position = %obj.position;
      rotation = %obj.rotation;
      scale    = %obj.scale;
      polyhedron = "0.000000 1.0000000 0.0000000 1.0000000 0.0000000 0.0000000 0.0000000 -1.0000000 0.0000000 0.0000000 0.0000000 1.0000000";
      velocityMod  = 1.0;
      gravityMod   = 1.0;
      appliedForce = "0 0 0";
		ffield = %obj;
   };
	%pzGroup = nameToID("MissionCleanup/PZones");
	if(%pzGroup <= 0) {
		%pzGroup = new SimGroup("PZones");
		MissionCleanup.add(%pzGroup);
	}
	%pzGroup.add(%pz);
   %obj.pz = %pz;
   //MissionCleanupGroup.add(%pz);
}

function ShapeBase::findNearestGen(%obj)
{
    InitContainerRadiusSearch(%obj.position, 10000, $TypeMasks::StaticShapeObjectType);

    while((%gen = ContainerSearchNext()) != 0)
    {
        if(%gen.getDatablock().className $= "Generator")
        {
            if(%gen.team == %obj.team)
            {
                %pg = %gen.getGroup();
                %pg.add(%obj);
                %pg.updatePowerState();

                return;
            }
        }
    }
   
    %obj.setSelfPowered();
    %obj.setRechargeRate(%obj.getDatablock().rechargeRate);
}

// Pieces for each individual base
datablock StaticShapeData(FCONode) : GenericRootNode
{
   maxDamage      = 400;
   destroyedLevel = 400;
   disabledLevel  = 400;

   targetTypeTag = 'Flying Command Outpost';
};

function FCONode::onDestroyed(%data, %obj, %prevState)
{
    Parent::onDestroyed(%data, %obj, %prevState);

    if(%obj.deployedItem !$= "")
        $TeamDeployedCount[%obj.team, %obj.deployedItem]--;
}

datablock StaticShapeData(FCOPiece) : FCONode
{
   shapeFile      = "pmiscf.dts";
};

datablock StaticShapeData(FCOPad) : FCONode
{
   shapeFile      = "nexusbase.dts";
};

datablock StaticShapeData(SBNode) : GenericRootNode
{
   maxDamage      = 150;
   destroyedLevel = 150;
   disabledLevel  = 150;

   targetTypeTag = 'Sensor Base';
};

function SBNode::onDestroyed(%data, %obj, %prevState)
{
    Parent::onDestroyed(%data, %obj, %prevState);

    if(%obj.deployedItem !$= "")
        $TeamDeployedCount[%obj.team, %obj.deployedItem]--;
}

datablock StaticShapeData(SBPiece) : SBNode
{
   shapeFile      = "dmiscf.dts";
};

datablock StaticShapeData(SBFloor) : SBNode
{
   shapeFile      = "nexusbase.dts";
};

datablock StaticShapeData(SBCrate) : SBNode
{
   shapeFile      = "stackable1l.dts";
};

datablock StaticShapeData(BSNode) : GenericRootNode
{
   maxDamage      = 25;
   destroyedLevel = 25;
   disabledLevel  = 25;

   targetTypeTag = 'Orbital Bombardment Satellite';
};

function BSNode::onDestroyed(%data, %obj, %prevState)
{
    Parent::onDestroyed(%data, %obj, %prevState);

    if(%obj.deployedItem !$= "")
    {
        $TeamDeployedCount[%obj.team, %obj.deployedItem]--;
        $Satellite[%obj.team] = 0;
    }
}

datablock StaticShapeData(BSPiece) : BSNode
{
   shapeFile      = "pmiscf.dts";
};

datablock StaticShapeData(BSFloor) : BSNode
{
   shapeFile      = "smiscf.dts";
};

datablock StaticShapeData(BSCrate) : BSNode
{
   shapeFile      = "stackable2m.dts";
};

datablock StaticShapeData(BSApeture) : BSNode
{
   shapeFile      = "nexusbase.dts";
};

datablock StaticShapeData(BSLocator) : BSNode
{
   shapeFile      = "turret_muzzlepoint.dts";
   
   cmdCategory = "Tactical";
   cmdIcon = CMDCameraIcon;
   cmdMiniIconName = "commander/MiniIcons/com_camera_grey";
   targetTypeTag = 'Orbital Bombardment Satellite';
};

// Forcefields
datablock ForceFieldBareData(BlueTransparentField)
{
   fadeMS           = 1000;
   baseTranslucency = 0.2;
   powerOffTranslucency = 0.0;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.28 0.99";
   powerOffColor    = "0.28 0.28 0.99";

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
};

datablock ForceFieldBareData(YellowTransparentField) : BlueTransparentField
{
   color            = "0.99 0.87 0.28";
   powerOffColor    = "0.99 0.87 0.28";
   //they are secure..but not THAT secure
   teamPermiable    = true;
   otherPermiable   = true;
};

datablock ForceFieldBareData(VehicleField)
{
   fadeMS           = 3000;
   baseTranslucency = 0.8;
   powerOffTranslucency = 0.0;
   teamPermiable    = false;
   otherPermiable   = false;
   color            = "0.6 0.6 0.4";
   powerOffColor    = "0.6 0.6 0.1";
   targetNameTag    = 'Force Field';
   targetTypeTag    = 'ForceField';

   texture[0] = "LiquidTiles/LavaPool01";
   texture[1] = "LiquidTiles/LavaPool02";
   texture[2] = "LiquidTiles/LavaPool03";

   framesPerSec = 8;
   numFrames = 3;
   scrollSpeed = 0.0;
   umapping = 0.15;
   vmapping = 0.15;
};

datablock TriggerData(VpadTrigger)
{
  tickPeriodMS = 80;
};

// Deployables

//====================================== Deployable Teleport Pads (MD2 Transplant)
$TeamDeployableMax[TelePack] = 2;

datablock StaticShapeData(TelePadBase) : StaticShapeDamageProfile
{
   shapeFile = "nexuscap.dts";   // teamlogo_projector
   maxDamage = 3.5;
   destroyedLevel = 3.5;
   disabledLevel = 3.5;
   explosion = DeployablesExplosion;
   dynamicType = $TypeMasks::StaticShapeObjectType;

   deployedObject = true;

   targetNameTag = 'Teleporter';
   targetTypeTag = 'Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock StaticShapeData(TelePadCap) : StaticShapeDamageProfile
{
   shapeFile = "nexuscap.dts";   // teamlogo_projector
   maxDamage = 3.5;
   destroyedLevel = 3.5;
   disabledLevel = 3.5;
   explosion = DeployablesExplosion;
   dynamicType = $TypeMasks::StaticShapeObjectType;

   deployedObject = true;

   targetNameTag = 'Teleporter';
   targetTypeTag = 'Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock StaticShapeData(TelePadBeam) : StaticShapeDamageProfile
{
   catagory = "Objectives";
   shapefile = "nexus_effect.dts";
   mass = 10;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   heatSignature = 0;
   invulnerable = true;
   
   cmdCategory = "Tactical";
   cmdIcon = CMDVehicleStationIcon;
   cmdMiniIconName = "commander/MiniIcons/com_vehicle_pad_inventory";
   targetNameTag = 'Teleporter';
   targetTypeTag = 'Pad';
};

datablock ShapeBaseImageData(TelePackImage)
{
   mass = 10;
   emap = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = TelePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = TelePadBase;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 20;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(TelePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "TelePackImage";
   pickUpName = "a deployable teleport pack";
   heatSignature = 0;

   emap = true;
};

// todo: create simsets of each teleporter per team instead of radiussearch
function TelePackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);
   %rot = %item.getInitialRotation(%plyr);

   %trans = %item.surfacePt SPC "0 1 0 0";
   %teleBase = createTeamObjectV(%plyr, "StaticShape", "TelePadBase", vectorAdd(%item.surfacePt, "0 0 .5"), "0 0 -1", false, "2 2 2");
   %teleCap = createTeamObject(%plyr, "StaticShape", "TelePadCap", vectorAdd(%item.surfacePt, "0 0 5.5") SPC "0 1 0 0", false, "2 2 2");
   %teleBeam = createTeamObject(%plyr, "StaticShape", "TelePadBeam", %item.surfacePt SPC "0 1 0 0", false, "2 2 0.75");
   %teleBase.createNode();
   %teleBase.setRootNode();
   %teleBase.deployedItem = "TelePack";
   %teleBase.addNode(%teleCap, $LC::DeleteOnDestroy);
   %teleBase.addNode(%teleBeam, $LC::Invulnerable | $LC::DeleteOnly);

   %teleBase.isTele = true;
   %teleBase.playThread(1, "ambient");
   %teleCap.playThread(1, "ambient");
   %teleBeam.playThread(1, "ambient");

   %teleBase.cap = %teleCap;
   %teleBase.beam = %teleBeam;

   %teleBase.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "TelePack"]++;     // just fine

   glueToRemoteBase(%teleBase, %item.surfacePt);
}

function teleCalSec(%obj)
{
    if(isObject(%obj))
    {
        if(%obj.teleCalTime > 0)
        {
            %obj.teleCalTime--;
            schedule(1000, 0, "teleCalSec", %obj);
        }
        else teleCalibrate(%obj, 0);
    }
}

function teleFadeFX(%obj, %time)
{
   %obj.startFade(250, 0, true);
   %obj.startFade(250, %time-2000, false);
//   %obj.startFade(1000, 0, true);
//   %obj.startFade(%time, 1000, false);
}

function teleCalibrate(%obj, %time)
{
    if(isObject(%obj))
    {
        if(!%obj.isDisabled())
        {
            %obj.setDamageState(Disabled);
            %obj.teleCalTime = mFloor(%time / 1000);
            teleCalSec(%obj);
        }
        else
        {
            %obj.setDamageState(Enabled);
            %obj.playThread(0, "ambient");
            %obj.cap.playThread(0, "ambient");
            %obj.beam.playThread(0, "ambient");
            %obj.teleCalibrating = false;
        }
    }
}

function checkForTele(%obj, %col, %range)
{
   InitContainerRadiusSearch(%obj.getPosition(), %range, $TypeMasks::StaticObjectType);

   while((%pad = ContainerSearchNext()) != 0)
   {
      if(%pad.isTele)
         if(%pad != %obj)
            if(%pad.team == %obj.team)
               return %pad;

      else continue;
   }
   return false;
}

function finishTele(%pad1, %pad2, %pl, %dist, %time)
{
      %pl.setMoveState(false);
      %trans = %pad2.getTransform();
      %pl.setTransform(%trans);
      %pl.teleportEndFX();
      %pad2.play3d(UnTeleportSound);
      %pad1.playThread(0, "transition");
      %pad2.playThread(0, "transition");
      %pad1.cap.playThread(0, "transition");
      %pad2.cap.playThread(0, "transition");
      %pad1.beam.playThread(0, "transition");
      %pad2.beam.playThread(0, "transition");
      %time = (%dist / 35) * 0.5 * 1000;
      teleCalibrate(%pad2, %time);
      schedule(1100, %pl, "checkCloakOff", %pl);

//      if(%time > 4000)
//      {
//         moveObjectInSteps(%pad1.cap, vectorAdd(%pad1.getPosition(), "0 0 -3"), %pad1.cap.oldPos, 10, 100);
//         moveObjectInSteps(%pad2.cap, vectorAdd(%pad2.getPosition(), "0 0 -3"), %pad2.cap.oldPos, 10, 100);
//      }
}

function startTele(%pad1, %pad2, %pl, %dist)
{
      %pl.setMoveState(true);
      %pl.teleportStartFX();
      %pad1.play3d(TeleportSound);
      %pad1.playThread(0, "flash");
      %pad2.playThread(0, "flash");
      %pad1.cap.playThread(0, "flash");
      %pad2.cap.playThread(0, "flash");
      %pad1.beam.playThread(0, "transition");
      %pad2.beam.playThread(0, "transition");
      %time = (%dist / 35) * 0.5 * 1000;

      if(%time > 5000)
      {
         schedule(4000, 0, teleFadeFX, %pad1.beam, mFloor(%time));
         schedule(4000, 0, teleFadeFX, %pad2.beam, mFloor(%time));
      }

      if(%time > 4000)
      {
         %pad1.cap.oldPos = %pad1.cap.getPosition();
         %pad2.cap.oldPos = %pad2.cap.getPosition();
//         moveObjectInSteps(%pad1.cap, %pad1.cap.oldPos,  vectorAdd(%pad1.getPosition(), "0 0 -3"), 10, 100);
//         moveObjectInSteps(%pad2.cap, %pad2.cap.oldPos,  vectorAdd(%pad2.getPosition(), "0 0 -3"), 10, 100);
      }

      teleCalibrate(%pad1, %time);
      schedule(2000, 0, "finishTele", %pad1, %pad2, %pl, %dist, %time);
}

function TelePadTimeout(%obj)
{
   if(isObject(%obj))
      %obj.telePadCollTime = 0;
}

function TelePadBase::onCollision(%data, %obj, %col)
{
     if(%col.getDataBlock().className !$= "Armor" || %col.getState() $= "Dead")
          return;

     if(%col.telePadCollTime)
        return;

//     else if(%obj.team != %col.client.team)
//     {
//          messageClient(%col.client, 'MsgNoUseEnemyTeles', '\c2As much as you probably want to, you cant use enemy teleporters!');
//          return;
//     }
     else if(%obj.isDisabled())
     {
          messageClient(%col.client, 'MsgTeleRecharge', '\c2Teleporter system is re-calibrating, %1 seconds', %obj.teleCalTime);
          return;
     }
     else
     {
        %pad = checkForTele(%obj, %col, 10000);   // /me special script for teleporting :)
        if(%pad && !%obj.isDisabled()) // final check
        {
           %pos = %obj.getWorldBoxCenter();
           %col.setVelocity("0 0 0");
           %rot = getWords(%col.getTransform(),3, 6);
           %col.setTransform(getWord(%pos,0) @ " " @ getWord(%pos,1) @ " " @ getWord(%pos,2)+0.8 @ " " @ %rot);//center player on object
           %col.setVelocity("0 0 0");
           %dist = vectorDist(%obj.getPosition(), %pad.getPosition());

           startTele(%obj, %pad, %col, %dist);
           %col.telePadCollTime = 12000;
           schedule(%col.telePadCollTime, 0, TelePadTimeout, %col);
           return;
        }
        else if(%obj.isDisabled())
        {
           messageClient(%col.client, 'MsgTeleRecharge', '\c2Teleporter system is re-calibrating, %1 seconds', %obj.teleCalTime);
           return;
        }
        else
        {
           messageClient(%col.client, 'Msg2ndTeleNoExist', '\c2Second telepad has not been placed.');
           return;
        }
    }

   %col.telePadCollTime = 3000;
      schedule(%col.telePadCollTime, 0, "TelePadTimeout", %col);
}

function TelePadBase::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);

   $TeamDeployedCount[%obj.team, TelePack]--;
}

function TelePadBase::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//====================================== Deployable Inventory Station
$TeamDeployableMax[DeployableInvStationPack] = 3;

datablock ShapeBaseImageData(DeployableInvStationImage)
{
   mass = 10;

   shapeFile = "pack_deploy_inventory.dts";
   item = DeployableInvStationPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = StationInventory;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 200;
	minFriendlyFlagDist = 50;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 200;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(DeployableInvStationPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "DeployableInvStationImage";
   pickUpName = "a deployable inventory station";
   heatSignature = 0;

   emap = true;
};

function DeployableInvStationPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DeployableInvStationImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %trans = %item.surfacePt SPC %rot;
   %deplObj = addStation(%plyr, %trans, false);
   %deplObj.createNode();
   %deplObj.setRootNode();
   %deplObj.deployedItem = "DeployableInvStationPack";

   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "DeployableInvStationPack"]++;

   if(!glueToRemoteBase(%deplObj, %item.surfacePt))
       %deplObj.findNearestGen();
}

function DeployableInvStationImage::testSlopeTooGreat(%item)
{
   if (%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 45;
}

function StationInventory::checkDeployPos(%deplObj, %norm)
{
   return getTerrainAngle(%norm) < 30;
}

//====================================== Deployable Sensor Base
$TeamDeployableMax[SensorBasePack] = 3;

datablock ShapeBaseImageData(SensorBaseImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = SensorBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = SBNode;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(SensorBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "SensorBaseImage";
   pickUpName = "a remote sensor base";
   heatSignature = 0;

   emap = true;
};

function SensorBaseImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function SensorBaseImage::testObjectTooClose(%item)
{
   %mask = $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
   InitContainerRadiusSearch( %item.surfacePt, 50,  %mask); // 0.5

   return containerSearchNext();
}

function xSensorBaseImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25, 150))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isEnemyGenInArea(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function SensorBaseImage::onDeploy(%item, %plyr, %slot)
{
    %plyr.unmountImage(%slot);
    %plyr.decInventory(%item.item, 1);
    %rot = %item.getInitialRotation(%plyr);
    %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 2"));

    %endPos = vectorAdd(%item.surfacePt, "0 0 2.75");
    %endTrans = %endPos SPC "0 0 1 0";

    %coreNode = createRootNode(%plyr, %item.surfacePt, "SBNode");
    %coreNode.deployedItem = "SensorBasePack";
    %coreNode.play3d(%item.deploySound);

    // Stations
    %station = addStation(%plyr, "1.1344 4.666 -2.18905 0 0 1" SPC mDegToRad(4.59731), true);
    %station.setOffsetPosition(%endPos);
    %station.trigger.setTransform(%station.getTransform());
    %coreNode.addNode(%station, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);
    
    %station = addStation(%plyr, "-4.9117 -0.548996 -2.16005 0 0 -1" SPC mDegToRad(99.1194), true);
    %station.setOffsetPosition(%endPos);
    %station.trigger.setTransform(%station.getTransform());
    %coreNode.addNode(%station, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    // Other pieces
    %gen = createTeamObject(%plyr, "StaticShape", "BaseGenerator", "0.772699 -3.25301 -2.18905 0 0 -1" SPC mDegToRad(38.7274), true, "1 0.801278 1");
    %gen.setOffsetPosition(%endPos);
    %coreNode.addNode(%gen, $LC::DestroysParent | $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);
    
    %sensor = createTeamObject(%plyr, "StaticShape", "SensorLargePulse", "0.288399 0.322998 5.01095 0 0 1" SPC mDegToRad(2.54237), true, "1 1 1");
    %sensor.setOffsetPosition(%endPos);
    %coreNode.addNode(%sensor, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);
    
    // Forcefields
    %ff = addForcefield(%plyr, "BlueTransparentField", vectorAdd(%endPos, "9.0528 -5.34 -2.98985"), "0.373069 0.612497 0.696898 160.783", "10.4143 0.519998 4.6262", %coreNode);
    %ff = addForcefield(%plyr, "BlueTransparentField", vectorAdd(%endPos, "-5.4902 -9.144 -2.98545"), "0.738931 0.179509 0.649429 223.165", "10.5716 0.519998 4.78201", %coreNode);
    
    // Main building
    %building = new (StaticShape) () {datablock = "SBFloor";position = "-0.135302 -0.132996 -2.18905";rotation = "1 0 0 180";scale = "5.03556 5.11583 15.5081";impulse = "500";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath); %building.playThread(1, "ambient");
    %building = new (StaticShape) () {datablock = "SBFloor";position = "0.145699 -0.199997 4.96795";rotation = "1 0 0 180";scale = "1.68691 1.82258 6.13359";impulse = "500";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath); %building.playThread(1, "ambient");
    %building = new (StaticShape) () {datablock = "SBCrate";position = "2.9333 -6.12399 -2.38905";rotation = "0 0 1 53.6956";scale = "0.489578 1.06126 1.19155";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "5.4054 -7.311 -1.18905";rotation = "0.478795 0.520769 0.706792 176.601";scale = "4.22231 0.166666 7.61825";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "8.9977 -0.994003 -1.27605";rotation = "0.254495 0.695615 0.671827 145.354";scale = "4.16487 0.166666 7.43496";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "2.2581 -8.97501 -1.31205";rotation = "0.574343 0.41966 0.702863 192.486";scale = "4.30511 0.19541 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "8.7982 2.311 -1.27305";rotation = "0.119645 0.768787 0.628213 130.688";scale = "4.19727 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "7.1801 5.49 -1.22405";rotation = "-0.0347392 0.82742 0.560508 117.263";scale = "4.12724 0.159076 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "4.5288 7.913 -1.26705";rotation = "-0.209421 0.862072 0.461493 105.7";scale = "4.1405 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "2.7735 -9.032 -1.45205";rotation = "0.316668 0.865504 -0.388103 100.215";scale = "4.21712 0.166666 7.57058";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-7.4507 -5.63 -1.29805";rotation = "0.804614 0.0337879 0.592836 237.186";scale = "4.2424 0.166666 7.51007";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-9.0528 1.218 -1.09405";rotation = "0.306964 -0.661162 0.684571 208.122";scale = "0.125 5.61241 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "1.1286 9.144 -1.32905";rotation = "0.316657 0.86551 -0.388099 100.214";scale = "0.125 5.53057 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-8.998 -2.37601 -1.25905";rotation = "-0.851039 0.13244 -0.508126 110.368";scale = "4.19203 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-7.9382 4.562 -1.14305";rotation = "-0.8312 0.506283 -0.229749 93.1945";scale = "4.18588 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-2.449 8.861 -1.26505";rotation = "-0.583109 0.79805 0.151987 91.3547";scale = "4.14375 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-5.989 7.553 -1.67505";rotation = "-0.735503 0.676224 -0.0419162 90.1005";scale = "3.82378 0.166666 8";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "8.817 -5.282 -5.01095";rotation = "0.628217 0.459017 0.628209 130.689";scale = "1.45467 0.166065 9.46418";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "SBPiece";position = "-5.428 -9.00999 -4.86465";rotation = "-0.695427 -0.508128 -0.508122 110.368";scale = "0.1 1.86081 9.44093";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    
    $TeamDeployedCount[%plyr.team, %coreNode.deployedItem]++;
}

//====================================== Deployable Command Base
$TeamDeployableMax[CommandBasePack] = 1;

datablock ShapeBaseImageData(CommandBaseImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = CommandBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = FCONode;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 100;
	minFriendlyFlagDist = 50;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(CommandBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "CommandBaseImage";
   pickUpName = "a flying command base";
   heatSignature = 0;

   emap = true;
};

datablock StaticShapeData(DeployableVehiclePad)
{
//   className = StaticShape;
//   dynamicType = $TypeMasks::DamageableItemObjectType;
   shapeFile = "vehicle_pad.dts";
   maxDamage = 10000;
   destroyedLevel = 10000;
   disabledLevel = 10000;
   explosion      = ShapeExplosion;
	expDmgRadius = 10.0;
	expDamage = 0.4;
	expImpulse = 1500.0;
   rechargeRate = 0.05;
   targetTypeTag = 'Deployable Vehicle Pad';
};

datablock StaticShapeData(DeployableStationVehicle) : StaticShapeDamageProfile
{
   className = Station;
   catagory = "Stations";
   shapeFile = "vehicle_pad_station.dts";

   maxDamage = 2.5;
   destroyedLevel = 2.5;
   disabledLevel = 2.0;
   explosion      = ShapeExplosion;
	expDmgRadius = 10.0;
	expDamage = 0.4;
	expImpulse = 1500.0;

   dynamicType = $TypeMasks::StationObjectType;
	isShielded = true;
	energyPerDamagePoint = 33;
	maxEnergy = 250;
	rechargeRate = 0.31;
   humSound = StationVehicleHumSound;
	// don't let these be damaged in Siege missions

   cmdCategory = "DSupport";
   cmdIcon = CMDVehicleStationIcon;
   cmdMiniIconName = "commander/MiniIcons/com_vehicle_pad_inventory";
//   targetNameTag = 'Deployable';
   targetTypeTag = 'Vehicle Station';

   debrisShapeName = "debris_generic.dts";
   debris = StationDebris;
};

function DeployableStationVehicle::createTrigger(%this, %obj)
{
   %trigger = new Trigger()
   {
      dataBlock = stationTrigger;
      polyhedron = "-0.75 0.75 0.0 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.0";
   };
   MissionCleanup.add(%trigger);
   %trigger.setTransform(%obj.getTransform());
   %trigger.station = %obj;
   %obj.trigger = %trigger;
}

function DeployableStationVehicle::stationReady(%data, %obj)
{
   // Make sure none of the other popup huds are active:
   messageClient( %obj.triggeredBy.client, 'CloseHud', "", 'scoreScreen' );
   messageClient( %obj.triggeredBy.client, 'CloseHud', "", 'inventoryScreen' );

   //Display the Vehicle Station GUI
   commandToClient(%obj.triggeredBy.client, 'StationVehicleShowHud');
}

function DeployableStationVehicle::stationFinished(%data, %obj)
{
   //Hide the Vehicle Station GUI
   if(!%obj.triggeredBy.isMounted())
      commandToClient(%obj.triggeredBy.client, 'StationVehicleHideHud');
   else
      commandToClient(%obj.triggeredBy.client, 'StationVehicleHideJustHud');
}

function DeployableStationVehicle::getSound(%data, %forward)
{
   if(%forward)
      return "StationVehicleAcitvateSound";
   else
      return "StationVehicleDeactivateSound";
}

function DeployableStationVehicle::setPlayersPosition(%data, %obj, %trigger, %colObj)
{
   %vel = getWords(%colObj.getVelocity(), 0, 1) @ " 0";
   if((VectorLen(%vel) < 22) && (%obj.triggeredBy != %colObj))
   {
      %posXY = getWords(%trigger.getTransform(),0 ,1);
      %posZ = getWord(%trigger.getTransform(), 2);
      %rotZ =  getWord(%obj.getTransform(), 5);
      %angle =  getWord(%obj.getTransform(), 6);
	   %angle += 3.141592654;
      if(%angle > 6.283185308)
         %angle = %angle - 6.283185308;
      %colObj.setvelocity("0 0 0");
      %colObj.setTransform(%posXY @ " " @ %posZ + 0.2 @ " " @ "0 0 "  @ %rotZ @ " " @ %angle );//center player on object
      return true;
   }
   return false;
}

function DeployableVehiclePad::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);

    %obj.ready = true;
    %obj.setRechargeRate(%obj.getDatablock().rechargeRate);

    if($CurrentMissionType $= %obj.missionTypesList || %obj.missionTypesList $="")
        %this.schedule(0, "createStationVehicle", %obj);
}

function DeployableVehiclePad::onEndSequence(%data, %obj, %thread)
{
   if(%thread == $ActivateThread)
   {
      %obj.ready = true;
      %obj.stopThread($ActivateThread);
   }
   Parent::onEndSequence(%data, %obj, %thread);
}

function DeployableVehiclePad::createStationVehicle(%data, %obj)
{
   // This code used to be called from StationVehiclePad::onAdd
   // This was changed so we can add the station to the mission group
   // so it gets powered properly and auto cleaned up at mission end.

   // Get the v-pads mission group so we can place the station in it.
   %group = MissionCleanup;

   // Set the default transform based on the vehicle pads slot
   %xform = %obj.getSlotTransform(0);
   %position = vectorAdd(getWords(%xform, 0, 2), "-1 0 -1.5");
   %rotation = getWords(%xform, 3, 5);
   %angle = (getWord(%xform, 6) * 180) / 3.14159;

   // Place these parameter's in the v-pad datablock located in mis file.
   // If the mapper doesn't move the station, use the default location.
   if(%obj.stationPos $= "" || %obj.stationRot $= "")
   {
      %pos = %position;
      %rot = %rotation @ " " @ %angle;
   }
   else
   {
      %pos = %obj.stationPos;
      %rot = %obj.stationRot;
   }

   %sv = new StaticShape() {
	scale = "1 1 1";
      dataBlock = "DeployableStationVehicle";
	lockCount = "0";
	homingCount = "0";
	team = %obj.team;
      position = %pos;
      rotation = %rot;
   };

   // Add the station to the v-pads mission group for cleanup and power.
   %group.add(%sv);
   %sv.setPersistent(false); // set the station to not save.

   // Create the trigger
   %sv.getDataBlock().createTrigger(%sv);
   %sv.pad = %obj;
   %obj.station = %sv;
   %sv.trigger.mainObj = %obj;
   %sv.trigger.disableObj = %sv;

   // Set the sensor group.
   if(%sv.getTarget() != -1)
      setTargetSensorGroup(%sv.getTarget(), %obj.team);

   %sv.setSelfPowered();
   %sv.pad.setSelfPowered();
   %sv.setRechargeRate(%data.rechargeRate);
}

function DeployableStationVehicle::onDestroyed(%data, %obj, %prevState) // stationObject: special case
{
   Parent::onDestroyed(%data, %obj, %prevState);
   %obj.pad.setDamageState(Destroyed);
   %obj.pad.parent.setDamageState(Destroyed);
   %obj.trigger.schedule(200, "delete");
   %obj.schedule(200, "delete");
}

function DeployableVehiclePad::onEndSequence(%data, %obj, %thread)
{
   if(%thread == $ActivateThread)
   {
      %obj.ready = true;
      %obj.stopThread($ActivateThread);
   }
   Parent::onEndSequence(%data, %obj, %thread);
}

function DeployableVehiclePad::gainPower(%data, %obj)
{
   %obj.station.setSelfPowered();
   Parent::gainPower(%data, %obj);
}

function DeployableVehiclePad::losePower(%data, %obj)
{
   %obj.station.clearSelfPowered();
   Parent::losePower(%data, %obj);
}

function DeployableVehiclePad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.station.setDamageState(Destroyed);
   %obj.schedule(250, "delete");
}

function CommandBaseImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function CommandBaseImage::testObjectTooClose(%item)
{
   %mask = $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
   InitContainerRadiusSearch( %item.surfacePt, 50,  %mask); // 0.5

   return containerSearchNext();
}

function xCommandBaseImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25, 150))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isEnemyGenInArea(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function CommandBaseImage::onDeploy(%item, %plyr, %slot)
{
    %plyr.unmountImage(%slot);
    %plyr.decInventory(%item.item, 1);
    %rot = %item.getInitialRotation(%plyr);
    %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 41"));

    %endPos = vectorAdd(%item.surfacePt, "0 0 40");
    %endTrans = %endPos SPC "0 0 1 0";

    %coreNode = createRootNode(%plyr, %item.surfacePt, "FCONode");
    %coreNode.deployedItem = "CommandBasePack";
    %coreNode.play3d(%item.deploySound);

    // Stations
    %station = addStation(%plyr, "-8.79961 4.56734 -5.61499 0 0 1" SPC mDegToRad(213.387), true);
    %station.setOffsetPosition(%endPos);
    %station.trigger.setTransform(%station.getTransform());
    %coreNode.addNode(%station, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %station = addStation(%plyr, "-11.2766 18.8168 -5.61499 0 0 -1" SPC mDegToRad(62.952), true);
    %station.setOffsetPosition(%endPos);
    %station.trigger.setTransform(%station.getTransform());
    %coreNode.addNode(%station, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %station = addStation(%plyr, "1.0938 20.9671 -5.61499 0 0 1" SPC mDegToRad(38.4847), true);
    %station.setOffsetPosition(%endPos);
    %station.trigger.setTransform(%station.getTransform());
    %coreNode.addNode(%station, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    // Turrets
    %turret = addTurret(%plyr, "-4.0055 12.7653 -10.886 0.65419 0.75633 9.58717e-07" SPC mDegToRad(180), true, "", ""); // barrel
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);
    
    %turret = addTurret(%plyr, "2.27219 -13.3287 10.886 0 0 -1" SPC mDegToRad(99.807), true, "", "MissileBarrelLarge");
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);
    
    // Other pieces
    %gen = createTeamObject(%plyr, "StaticShape", "BaseGenerator", "-5.86961 22.4562 -5.11499 0 0 -1" SPC mDegToRad(180), true, "3 1 1.5");
    %gen.setOffsetPosition(%endPos);
    %coreNode.addNode(%gen, $LC::DestroysParent | $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %sensor = createTeamObject(%plyr, "StaticShape", "SensorLargePulse", "-3.86851 13.5897 -0.343994 0 0 1" SPC mDegToRad(79.8852), true, "1 1 1");
    %sensor.setOffsetPosition(%endPos);
    %coreNode.addNode(%sensor, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    // Forcefields
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-7.3233 -0.944656 -5.12499"), "-0.992641 -0.0856301 -0.0856268 90.4231", "11.5355 4.29099 0.48", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-1.81561 16.9677 -5.62499"), "0.765265 -0.643716 -1.78893e-06 180", "7.30777 5.99888 0.48", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-13.6078 24.7838 -1.41199"), "-0.957445 0.106032 -0.268432 94.4538", "1.51971 0.519998 26.4367", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "1.4711 27.4045 -0.931992"), "0.197642 -0.681162 0.704951 188.928", "1.51981 0.519998 26.4154", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-14.5675 24.6173 -2.41999"), "-0.865891 0.274173 -0.418404 102.252", "1.52029 0.519998 26.4427", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-15.0745 24.5294 -3.72399"), "-0.742975 0.408789 -0.529981 112.991", "1.52017 0.519998 26.3889", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "-15.0509 24.5335 -5.12499"), "0.607729 -0.511209 0.607726 234.153", "1.52 0.519998 26.376", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "2.5971 27.6007 -2.05299"), "0.333425 -0.642412 0.690025 204.65", "1.52018 0.519998 26.355", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "3.2151 27.708 -3.52499"), "0.470074 -0.587166 0.658989 219.859", "1.52018 0.519998 26.3546", %coreNode);
    %ff = addForcefield(%plyr, "YellowTransparentField", vectorAdd(%endPos, "3.2306 27.7106 -5.12499"), "0.607729 -0.511209 0.607726 234.153", "1.52 0.519998 26.3114", %coreNode);

    // Vehicle Pad
    %pad = new (StaticShape) () {datablock = "DeployableVehiclePad";position = "2.0496 -14.9357 -5.62";rotation = "0 0 1 80.2141";scale = "0.875 0.75 0.5";}; %pad.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    
    // Main building
    %building = new (StaticShape) () {datablock = "FCOPad";position = "0.892593 -13.9977 -5.51499";rotation = "-2.77198e-11 1 -7.31565e-06 180";scale = "7.78267 6.99403 8.97612";impulse = "500";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath); %building.playThread(1, "ambient");
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "14.4473 2.62035 -5.36499";rotation = "-0.705793 -0.0608809 0.705797 173.032";scale = "0.125 0.166666 64.1104";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-10.788 -0.051651 -5.36499";rotation = "0.64347 -0.541271 0.541269 114.48";scale = "0.721245 0.166666 36.1114";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-11.0777 1.61535 -5.36499";rotation = "0.511208 0.60773 0.607726 234.153";scale = "0.125 0.166666 36.1116";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-7.7929 2.20535 -5.86499";rotation = "0.643469 -0.541272 0.54127 114.48";scale = "0.125 0.166666 29.4262";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-2.18501 3.17034 -10.386";rotation = "0 0 -1 9.86013";scale = "2.87873 0.166666 8.542";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-7.85741 2.18434 -10.636";rotation = "0.643464 -0.541273 0.541274 114.48";scale = "0.125 0.166666 23.0298";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "5.6615 7.82835 -5.86499";rotation = "-0.643473 0.541269 0.541268 114.48";scale = "1.49331 0.166666 34.112";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-9.4373 1.92635 -5.61499";rotation = "0.765261 -0.64372 -8.15974e-07 180";scale = "0.121518 1.11294 4";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-8.8558 2.01234 -9.16299";rotation = "0.702987 -0.591344 0.395122 129.679";scale = "0.12095 0.815575 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "4.352 4.30534 -8.83499";rotation = "0.702989 -0.59134 -0.395125 230.321";scale = "0.108748 1.05237 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-9.6312 1.87734 -7.10999";rotation = "0.759064 -0.638507 0.127006 162.828";scale = "0.102451 1.04705 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-9.4606 1.91035 -8.18099";rotation = "0.739425 -0.621991 0.257637 145.942";scale = "0.112931 0.939731 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "4.9603 4.41634 -8.05";rotation = "0.739427 -0.621988 -0.25764 214.058";scale = "0.116417 1.06322 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "6.70589 4.70634 -6.86499";rotation = "0.705796 0.0608848 -0.705794 186.968";scale = "0.375 0.166666 6.52984";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "5.4339 4.48235 -7.18099";rotation = "0.759064 -0.638507 -0.127008 197.172";scale = "0.115002 0.92218 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "4.07529 0.764343 4.77";rotation = "-0.643471 0.541269 0.54127 114.48";scale = "0.125 3.74267 23.0234";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-11.955 -1.99265 -5.11499";rotation = "0 0 1 170.139";scale = "2.37643 0.166666 30.998";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "9.016 1.65234 -5.11499";rotation = "0 0 1 170.139";scale = "2.50855 0.166666 30.998";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-2.22701 3.41235 -10.636";rotation = "0.992641 0.0856274 -0.0856208 90.423";scale = "2.87867 0.166666 37.9458";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "2.22089 14.3666 -0.593994";rotation = "0.511206 0.60773 -0.607728 125.847";scale = "6.73231 0.166666 25.0304";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-10.3086 15.4824 -5.86499";rotation = "-0.0608842 0.705796 -0.705794 186.969";scale = "1.31641 0.166666 14.5757";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "16.888 -11.4237 -5.36499";rotation = "-0.643464 0.541275 0.541272 114.479";scale = "6.99805 0.166666 64.1108";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.03899 17.4537 -5.86499";rotation = "-0.0608842 0.705796 -0.705794 186.969";scale = "1.45291 0.166666 14.5748";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-10.7862 -1.52065 -5.36499";rotation = "0.992641 0.0856265 -0.0856259 90.4236";scale = "0.125 0.166666 45.7807";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "7.4954 1.65534 -5.36499";rotation = "-0.607728 0.511205 0.60773 234.153";scale = "0.125 0.166666 45.7803";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-14.1278 20.6211 -6.61499";rotation = "0.60773 -0.511208 0.607726 234.153";scale = "0.500003 0.166666 37.964";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-16.888 -2.82565 -5.11499";rotation = "0 0 1 170.14";scale = "0.125 0.166666 16";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-16.6417 -2.78265 10.635";rotation = "0.705797 0.0608812 0.705794 173.032";scale = "0.125 0.166666 62.1102";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.171 23.6122 -6.61499";rotation = "0.607731 -0.511204 0.607728 234.152";scale = "0.5 0.166666 37.9646";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "14.2012 2.57735 -5.11499";rotation = "0 0 1 170.14";scale = "0.125 0.166666 16";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-16.888 -2.82565 2.88501";rotation = "0 0 1 170.14";scale = "0.125 0.166666 16";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.4135 20.7603 -5.86499";rotation = "-0.643473 0.541269 0.541268 114.48";scale = "1.42553 0.166666 34.1118";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "14.2012 2.57735 2.88501";rotation = "0 0 1 170.14";scale = "0.125 0.166666 16";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.4087 -27.4647 10.636";rotation = "0.541275 0.643467 0.541269 114.48";scale = "0.125 10.6851 55.0024";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.3643 23.8995 -5.86499";rotation = "-0.643473 0.541269 0.541268 114.48";scale = "0.125 0.166666 36.0624";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-14.417 20.8274 -5.36499";rotation = "0.511208 0.60773 0.607726 234.153";scale = "0.125 0.166666 36.1114";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-5.5195 22.3553 -10.386";rotation = "0 0 1 170.139";scale = "2.87875 0.166666 8.542";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "0.152893 23.3413 -10.636";rotation = "-0.643469 0.541267 0.541275 114.48";scale = "0.125 0.166666 23.0296";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-14.4105 20.8193 -6.86499";rotation = "0.705796 0.0608877 0.705794 173.032";scale = "0.375 0.166666 6.53077";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-10.8597 21.4486 -9.813";rotation = "-0.551113 0.463581 0.693806 100.968";scale = "0.114429 0.760211 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-12.4074 21.1643 -8.49799";rotation = "-0.225926 0.190042 0.955427 82.7225";scale = "0.121946 1.04244 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-11.7709 21.2788 -9.26199";rotation = "-0.414098 0.348327 0.840947 90.0157";scale = "0.120066 0.92059 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.3548 23.8977 -6.86499";rotation = "-0.0856316 0.99264 -0.0856309 90.4236";scale = "0.375 0.166666 6.49996";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-0.143707 23.2971 -9.91199";rotation = "0.551113 -0.463589 0.693801 100.968";scale = "0.121377 0.68879 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "0.749695 23.4372 -9.28099";rotation = "0.414098 -0.348333 0.840944 90.0157";scale = "0.113713 0.903309 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.39359 23.5585 -8.509";rotation = "0.225925 -0.190051 0.955426 82.722";scale = "0.115678 1.02308 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.4797 26.2215 -5.36499";rotation = "-0.511214 -0.607722 0.607729 234.152";scale = "0.999865 0.166667 38.1118";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-6.2075 26.3138 -0.844994";rotation = "-0.0859451 0.9963 1.2629e-06 180";scale = "3.12882 0.166666 8.542";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-13.4198 25.0471 -1.94199";rotation = "-0.423381 -0.503321 -0.753271 115.281";scale = "0.11829 0.725037 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-13.9979 24.954 -3.02499";rotation = "-0.306421 -0.364275 -0.879437 107.015";scale = "0.121928 0.916834 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-13.9835 24.9628 -5.11499";rotation = "0 0 -1 99.8602";scale = "0.125 1.09006 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "-14.1737 24.9193 -4.10799";rotation = "-0.16194 -0.192515 -0.96784 101.7";scale = "0.119919 1.02571 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.0199 27.5699 -1.90999";rotation = "0.423381 0.503321 -0.753271 115.281";scale = "0.125 0.702402 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.5733 27.666 -3.03499";rotation = "0.306421 0.364275 -0.879437 107.015";scale = "0.125 0.926974 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.5685 27.6653 -5.11499";rotation = "0 0 -1 99.8602";scale = "0.125 1.09008 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "1.75349 27.6881 -4.11199";rotation = "0.16194 0.192515 -0.96784 101.7";scale = "0.120418 1.03199 3";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "FCOPiece";position = "3.4515 -27.7106 -5.11499";rotation = "0 0 1 170.14";scale = "8.01373 0.166666 32";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);

    $TeamDeployedCount[%plyr.team, %coreNode.deployedItem]++;
}

//====================================== Deployable Orbital Bombardment Satellite
$TeamDeployableMax[SatelliteBasePack] = 1;

datablock SeekerProjectileData(MantaSatMissile)
{
   scale = "15.0 15.0 15.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 2000;

   explosion           = "MissileExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 0.1;
   maxVelocity         = 1000.0;
   turningSpeed        = 90.0;
   acceleration        = 500.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 3000;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock StaticShapeData(MantaLaunchPad) : StaticShapeDamageProfile
{
   shapeFile = "nexusbase.dts";
   maxDamage = 100;
   destroyedLevel = 100;
   disabledLevel = 100;
   explosion      = TurretExplosion;
	expDmgRadius = 10.0;
	expDamage = 0.1;
	expImpulse = 3000.0;

   deployedObject = true;

   targetNameTag = 'Satellite';
   targetTypeTag = 'Launch Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock ShapeBaseImageData(SatelliteBaseImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = SatelliteBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BSNode;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 100;
	minFriendlyFlagDist = 50;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(SatelliteBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "SatelliteBaseImage";
   pickUpName = "an orbital satellite base";
   heatSignature = 0;

   emap = true;
};

function SatelliteBaseImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function SatelliteBaseImage::testObjectTooClose(%item)
{
   %mask = $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType;
   InitContainerRadiusSearch( %item.surfacePt, 5,  %mask); // 0.5

   return containerSearchNext();
}

function xSatelliteBaseImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25, 150))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isEnemyGenInArea(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function SatelliteBaseImage::onDeploy(%item, %plyr, %slot)
{
    %plyr.unmountImage(%slot);
    %plyr.decInventory(%item.item, 1);
    %rot = %item.getInitialRotation(%plyr);

    %endPos = vectorAdd(%item.surfacePt, "0 0 1000");
    %endTrans = %endPos SPC "0 0 1 0";

    %coreNode = createRootNode(%plyr, %endPos, "BSNode");
    %coreNode.deployedItem = "SatelliteBasePack";
    %coreNode.play3d(%item.deploySound);
    
    // For later when orbital bombardments are implemented
    $Satellite[%plyr.team] = %coreNode;

    // "Launch"
    %launchpad = createTeamObject(%plyr, "StaticShape", "MantaLaunchPad", %item.surfacePt SPC "0 0 1 0", false, "1 1 1");
    %launchpad.playThread(0, "ambient");
    %launchpad.schedule(2800, "setDamageState", "Destroyed");
    %launchpad.schedule(3000, "delete");

    %p = new SeekerProjectile()
    {
        dataBlock        = MantaSatMissile;
        initialDirection = "0 0 1";
        initialPosition  = vectorAdd(%item.surfacePt, "0 0 0.75");
        sourceObject     = %launchpad;
        sourceSlot       = 0;
        vehicleObject    = 0;
        instigator       = %plyr;
    };
    MissionCleanup.add(%p);

    commandToClient(%plyr.client, 'BottomPrint', "Satellite launch in 3 seconds.", 3, 1);

    // Turrets
    %turret = addTurret(%plyr, "-0.682899 -17.4438 9.09351 0 0 1" SPC mDegToRad(180.32), true, "", "");
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %turret = addTurret(%plyr, "17.5031 -0.4505 9.09351 0 0 1" SPC mDegToRad(91.5168), true, "", "");
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %turret = addTurret(%plyr, "-17.5031 0.542698 9.09351 0 0 -1" SPC mDegToRad(89.1316), true, "", "");
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    %turret = addTurret(%plyr, "0.0431061 17.4438 9.09351 0 0 1" SPC mDegToRad(0.71436), true, "", "");
    %turret.setOffsetPosition(%endPos);
    %coreNode.addNode(%turret, $LC::RandomChildDeath | $LC::IndependantDamage | $LC::DeleteOnDestroy);

    // Main building
    %building = new (StaticShape) () {datablock = "BSApeture";position = "-0.0251999 -0.105801 9.09351";rotation = "1 0 0 180";scale = "10 10 30.7212";impulse = "500";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath); %building.playThread(1, "ambient");
    %building = new (StaticShape) () {datablock = "BSApeture";position = "-0.0606995 -0.0858002 -9.7205";rotation = "0.707107 0.707106 9.61033e-07 180";scale = "1.89066 2.2148 5.16791";impulse = "500";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath); %building.playThread(1, "ambient");
    %building = new (StaticShape) () {datablock = "BSCrate";position = "3.6412 -6.721 10.3795";rotation = "0 0 -1 87.7032";scale = "3.14793 3 3.1425";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSCrate";position = "4.1912 5.4937 10.4165";rotation = "0 0 -1 87.7032";scale = "3.14793 3 3.14";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSCrate";position = "-4.93439 -6.4615 10.4685";rotation = "0 0 -1 87.7032";scale = "3.14793 3 3.1425";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSCrate";position = "-4.5373 5.7903 10.5175";rotation = "0 0 -1 87.7032";scale = "3.14793 3 3.14";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSCrate";position = "-0.222397 0.0366974 -9.8065";rotation = "0 0 1 80.2559";scale = "1 1 7.45944";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSPiece";position = "0.354103 -2.7752 -9.8975";rotation = "-0.991576 -0.0495666 -0.119664 45.3437";scale = "0.630296 0.166666 24.7905";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSPiece";position = "2.3315 -0.2057 -10.2685";rotation = "0.367516 -0.355902 0.859224 96.8373";scale = "0.530384 0.166666 25.5197";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSPiece";position = "-0.458801 3.3805 -9.5685";rotation = "0.992018 0.0482536 -0.116496 45.3255";scale = "0.401914 0.166666 22.319";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSPiece";position = "-2.51559 0.00309753 -10.5175";rotation = "0.335764 0.360467 -0.870245 101.943";scale = "0.413023 0.166666 25.3842";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);
    %building = new (StaticShape) () {datablock = "BSFloor";position = "-0.449196 -0.456303 7.9935";rotation = "-0 -0 1 91.5884";scale = "4 4 5";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);

    // Locator Beacon (for CC)
    %building = new (StaticShape) () {datablock = "BSLocator";position = "0 0 -3";rotation = "0 0 1 0";scale = "1 1 1";}; %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath | $LC::IndependantDamage); %building.initializeBasePiece(%plyr, %endPos, %coreNode, $LC::DeleteOnDestroy | $LC::RandomChildDeath);

    $TeamDeployedCount[%plyr.team, %coreNode.deployedItem]++;
}

//====================================== Blast Wall
$TeamDeployableMax[BlastWallPack] = 7;

datablock StaticShapeData(BlastWall) : StaticShapeDamageProfile
{
   shapeFile      = "bmiscf.dts";
   explosion      = ShapeExplosion;

   maxDamage      = 100;
   destroyedLevel = 100;
   disabledLevel  = 100;

   expDmgRadius = 5.0;
   expDamage = 0.1;
   expImpulse = 1500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Blast Wall';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
};

datablock ShapeBaseImageData(BlastWallPackImage)
{
   mass = 10;

   shapeFile = "pack_upgrade_shield.dts";
   item = BlastWallPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BlastWall;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 250;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 200;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(BlastWallPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BlastWallPackImage";
   pickUpName = "a blast wall";
   heatSignature = 0;

   emap = true;
};

function BlastWallPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function xBlastWallPackImage::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      5,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::StaticShapeObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
//            echo("why");
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      %xform = %surfacePt SPC %rot;
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(25, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function BlastWallPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

// Tried to get walls that stretch to fit, then I realized my math was terrible, so meh
//   %rotx = 1.5708;
//   %zoffset = 1.5;
//   %hscale = 0.25;
//   %wscale = 0.2;
//   %up = 2;
//   %side = 2.5;
//   %depth = 0.75;
   
//   %ray1 = castRay(%item.surfacePt, "0 0 1", 10, $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType);
   
//   if(%ray1)
//       %up = %ray1.hitDist * %hscale;

//   %ray2 = castRay(vectorAdd(%item.surfacePt, "0 0" SPC %up), "1 0 0", 15, $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType);

//   if(%ray2)
//       %side = %ray2.hitDist * %wscale;
   
//   %nscale = %side SPC %up SPC %depth;
//   echo("blastwall" SPC %deplObj SPC %up SPC %side);
   
   %deplObj.setScale("2.75 0.25 20");
   
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BlastWallPack"]++;
   MissionCleanup.add(%deplObj);
   glueToRemoteBase(%deplObj, %item.surfacePt);
   
   %deplObj.deployedItem = "BlastWallPack";
}

function BlastWallPackImage::testSlopeTooGreat(%item)
{
//   if(%item.surface)
//      return getTerrainAngle(%item.surfaceNrm) > 90;
   return false; // deplpyable at all angles
}

function BlastWall::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "BlastWallPack"]--;
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Deployable Turret Base
function calculateTurretMax()
{
     %count = 0;

     for(%i = 0; %i < ClientGroup.getCount(); %i++)
     {
          if(!ClientGroup.getObject(%i).isAIControlled())
               %count++;
     }

     $TeamDeployableMax[TurretBasePack] = 2 + mFloor(%count / 8);

     schedule(30000, 0, calculateTurretMax);
}

//if($TeamDeployableMax[TurretBasePack] $= "")
//     calculateTurretMax();

$TeamDeployableMax[TurretBasePack] = 2;

datablock TurretData(FreeBaseTurret) : TurretDamageProfile
{
   className      = TurretBase;
   catagory       = "Turrets";
   shapeFile      = "turret_base_large.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxDamage      = 1.4;
   destroyedLevel = 1.4;
   disabledLevel  = 1.0;
   explosion      = TurretExplosion;
	expDmgRadius = 25.0;
	expDamage = 2.7;
	expImpulse = 2000.0;
   repairRate     = 0;
   emap = true;

   thetaMin = 15;
   thetaMax = 140;
   deployedObject = true;

   isShielded           = true;
   energyPerDamagePoint = 50;
   maxEnergy = 125;
   rechargeRate = 0.35;
   humSound = SensorHumSound;
   pausePowerThread = true;

   canControl = true;
   cmdCategory = "Tactical";
   cmdIcon = CMDTurretIcon;
   cmdMiniIconName = "commander/MiniIcons/com_turretbase_grey";
   targetNameTag = 'Deployed Base';
   targetTypeTag = 'Turret';
//   sensorData = TurretBaseSensorObj;
//   sensorRadius = TurretBaseSensorObj.detectRadius;
//   sensorColor = "0 212 45";

   firstPersonOnly = true;

   debrisShapeName = "debris_generic.dts";
   debris = TurretDebris;

   undeployPack = "TurretBasePack";
};

datablock ShapeBaseImageData(TurretBaseImage)
{
   mass = 20;

   shapeFile = "pack_deploy_turreti.dts";
   item = TurretBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = FreeBaseTurret;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 360;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 150;
	minFriendlyFlagDist = 15;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 200;
	maxFriendlySpawnDist = 200;
};

datablock ItemData(TurretBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_turreti.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "TurretBaseImage";
   pickUpName = "a deployable turret base";
   heatSignature = 0;

   emap = true;
};

function TurretBasePack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function xTurretBaseImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(4, 200))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isEnemyGenInArea(200))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

//   if(getSimTime() < ($TurretDeployedTime[%plyr.client.team] + 60000))
//   {
//      $EDC::Reason = "Supply ship is reloading a Deployable Turret Base, stand by...";
//      return true;
//   }

   InitContainerRadiusSearch(%item.surfacePt, 1, $TypeMasks::StaticShapeObjectType);
   while((%found = containerSearchNext()) != 0)
   {
         if((%found.team == %plyr.team) && %found.notRepairable)
         {
            $EDC::Reason = "Cannot deploy turret on a Blast Wall.";
            return true;
         }
   }

   $EDC::Reason = "";
   return false;
}

function TurretBaseImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %trans = %item.surfacePt SPC %rot;
   %deplObj = addTurret(%plyr, %trans, false);
   %deplObj.createNode();
   %deplObj.setRootNode();
   %deplObj.deployedItem = "TurretBasePack";

   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "TurretBasePack"]++;

   if(!glueToRemoteBase(%deplObj, %item.surfacePt))
       %deplObj.findNearestGen();
}

function xTurretBaseImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);

   initSpecialDeploySequenceItem("Base Turret", 500, 3, %item, %plyr, %slot, %rot, %true);
}

function xFreeBaseTurret::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   
   if(!%obj.parent)
      $TeamDeployedCount[%obj.team, TurretBasePack]--;

   %obj.schedule(500, "delete");
}

function TurretBaseImage::testTurretTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, 32, $TypeMasks::TurretObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
//      %foundName = %found.getDataBlock().getName();
//      if(true) //(%foundname $= TurretDeployedFloorIndoor) || (%foundname $= FreeBaseTurret) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) || (%foundname $= BlastWall) || (%foundname $= BlastDoor))
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }

   return %turretInRange;
}

//=============================================================================
// Deployable Base Generator

datablock StaticShapeData(BaseGenerator) : StaticShapeDamageProfile
{
   shapeFile      = "station_generator_large.dts";
   explosion      = MortarExplosion;
   maxDamage      = 3.5;
   destroyedLevel = 3.5;
   disabledLevel  = 3.5;
   expDmgRadius = 25.0;
   expDamage = 10;
   expImpulse = 2000.0;

   maxEnergy = 50;
   rechargeRate = 0.05;

   targetNameTag = 'Remote Base';
   targetTypeTag = 'Generator';

   cmdCategory = "Support";
   cmdIcon = "CMDGeneratorIcon";
   cmdMiniIconName = "commander/MiniIcons/com_generator";

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
};

function BaseGenerator::onAdd(%this, %obj, %prevState)
{
    Parent::onAdd(%this, %obj, %prevState);
}

function BaseGenerator::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Deployable Forcefield
$TeamDeployableMax[ShieldGeneratorPack]   = 8;

$FF::MaxConnections = 2;
$FF::CheckHeight = 50;

datablock AudioProfile(TelePadBeamSound)
{
   filename    = "fx/powered/nexus_deny.WAV";
   description = AudioExplosion3d;
   preload = true;
};

function ShieldBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function ShieldGeneratorDeployableImage::testNoTerrainFound(%item)
{
     return false;   // HAK!!
}

function ShieldGeneratorDeployedBase::checkDeployPos(%item) // --- (ST)
{
   return true;
}

function ShieldGeneratorDeployableImage::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function ShieldGeneratorPack::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function ShieldGeneratorDeployedBase::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function deleteFF(%obj, %i)
{
   %shield = %obj.shield[%i];

  if(isObject(%shield))
  {
      %shield.setPosition("-10000 -10000 -10000");

      if(isObject(%shield.pz))
         %shield.pz.setPosition(%shield.getPosition());

      %shield.getDatablock().losePower(%shield);
      %shield.delete();
  }
}

function ShieldGeneratorDeployedBase::onEndSequence(%data, %obj, %thread)
{
//   Parent::onEndSequence(%data, %obj, %thread);
}

function ShieldGeneratorDeployedBase::onDamage(%this, %obj)
{
     Parent::onDamage(%this, %obj);

     if(isObject(%obj.cap))
          %obj.cap.setDamageLevel(%obj.getDamageLevel());
//     else
//         %obj.setDamageState(Destroyed);

     if(isObject(%obj.base))
          %obj.base.setDamageLevel(%obj.getDamageLevel());
//     else
//          %obj.setDamageState(Destroyed);
}

function ShieldGeneratorDeployedBase::onDestroyed(%this, %obj, %prevState)
{
Parent::onDestroyed(%this, %obj, %prevState);

for(%i = 0; %i < $FF::maxConnections; %i++)
      deleteFF(%obj, %i);

   %shield = %obj.beam;

  if(isObject(%shield))
  {
      %shield.setPosition("-10000 -10000 -10000");

      if(isObject(%shield.pz))
         %shield.pz.setPosition(%shield.getPosition());

      %shield.getDatablock().losePower(%shield);
      %shield.delete();
  }
  if(%obj.cap !$= "") // --- if obj is a base to prevent double decrimenting (ST)
  {
      $TeamDeployedCount[%obj.team, ShieldGeneratorPack]--;
      %obj.cap.setDamageState(Destroyed);
  }

   %obj.schedule(250, "delete");
}

function ShieldGeneratorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function ShieldGenUpdate(%obj)
{
   if(!isObject(%obj))
      return;

   if(!%obj.isPowered())
   {
        for(%i = 0; %i < %obj.connections; %i++)
        {
           if(%obj.connected[%i] && !isObject(%obj.connected[%i]))
           {
              deleteFF(%obj, %i);
              %obj.connected[%i] = "";
              %obj.connections--;
              %obj.playAudio(1, TelePadBeamSound);
           }
        }

        schedule(1000, %obj, "ShieldGenUpdate", %obj);
        return;
   }

   for(%i = 0; %i < %obj.connections; %i++)
   {
      if(%obj.connected[%i] && !isObject(%obj.connected[%i]))
      {
         deleteFF(%obj, %i);
         %obj.connected[%i] = "";
         %obj.connections--;
         %obj.playAudio(1, TelePadBeamSound);
      }
   }

   %surfacePt = %obj.getPosition();
   %found = getClosestFF(%obj);

   if(%found)
   {
      %distance_to = %obj.distances[%found];
      if(%obj.connections > $FF::MaxConnections)
      {
         %farthest = getFarthestFF(%obj);
         if(%distance_to < %obj.distances[%farthest]) // there is a non connected one closer then our farthest connection..
         {
            for(%i = 0; %i <= %obj.connections; %i++) // destroy the one that is far away..
            {
               if(%obj.connected[%i] == %farthest)
               {
                  deleteFF(%obj, %i);
                  %obj.connected[%i] = "";
                  %obj.connections--;
                  %obj.playAudio(1, TelePadBeamSound);
               }
            }
         }
      }
      if(%obj.connections < $FF::maxConnections)
      {
         %fnd_pos = %found.getPosition();
         %target = setWord(%fnd_pos, 2, 0);
         %src = setWord(%surfacePt, 2, 0);
         %vec = VectorNormalize(VectorSub(%target, %src));
         %mask = $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType |
                 $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType |
                 $TypeMasks::TurretObjectType |//$TypeMasks::PlayerObjectType | --- Hope you're on my team LOL (ST)
                 $TypeMasks::StaticObjectType | $TypeMasks::TerrainObjectType;

         %res = containerRayCast(VectorAdd(%surfacePt, "0 0 1.5"), VectorAdd(%fnd_pos, "0 0 1.5"), %mask, 0);
         if(!%res)
         {
            %obj.connections++;
            %radian = mAcos(getWord(%vec, 1));
            %angle = (%radian / 0.0175);
            %newrot = " 0 0 1 " @ (270 - %angle);
//            %height = 6; --- (ST)

            if(isObject(%obj.shield[0]))
               %newshieldidx = 1;
            else
               %newshieldidx = 0;

            %src_z = getWord(%surfacePt, 2);
            %tar_z = getWord(%fnd_pos, 2);
//            if(%src_z > %tar_z)
//            %height -= (%src_z - %tar_z); --- (ST)

// --- Begin auto-height field-size/pos (ST)
            if(%src_z >= %tar_z) // --- Who's higher =) (ST)
            {
               %diff = %src_z - %tar_z;
               %up = 0.4;
               if((%src_z + %obj.height) > (%tar_z + %found.height))
               {
                  %shortest = %found.height;
                  %fieldHeight = (%tar_z + %found.height) - (%src_z + 0.5);
               }
               else
               {
                  %shortest = %obj.height;
                  %fieldHeight = (%src_z + %obj.height) - (%src_z + 0.5);
               }
            }
            else
            {
               %diff = %tar_z - %src_z;
               %up = %diff + 0.4;
               if((%src_z + %obj.height) > (%tar_z + %found.height))
               {
                  %shortest = %found.height;
                  %fieldHeight = (%tar_z + %found.height) - (%tar_z + 0.5);
               }
               else
               {
                  %shortest = %obj.height;
                  %fieldHeight = (%src_z + %obj.height) - (%tar_z + 0.5);
               }
            }

            if(%fieldHeight > %shortest / 2) // --- Min. height of potential forcefield (ST)
            {
// --- End auto-height field-size/pos (ST)

               %obj.playAudio(1, TelePadBeamSound);
               %obj.shield[%newshieldidx] = new forceFieldBare()
               {
                  position = vectorAdd(%surfacePt, "0 0 "@ %up);//0.3"); --- (ST)
                  rotation = %newrot;
                  scale = %distance_to @ " .05" SPC %fieldHeight;//%height; --- (ST)
                  dataBlock = "DeployedTeamSlowFieldBareGreen";
                  team = %obj.team;
               };

               %obj.shield[%newshieldidx].deployBase = %obj;
               %obj.shield[%newshieldidx].fieldSource = %found; // for FF damage code
               %obj.shield[%newshieldidx].isFF = true;

//               %obj.shield[%newshieldidx].getDatablock().gainPower(%obj.shield[%newshieldidx]);
               %obj.shield[%newshieldidx].target = createTarget(%obj.shield[%newshieldidx], "Force Field", "", "", "", 0, 0);
               setTargetSensorGroup(%obj.shield[%newshieldidx].getTarget(), %obj.team);

               MissionCleanup.add(%obj.shield[%newshieldidx]);
               %obj.connected[%newshieldidx] = %found;

               // Add to parent's power group
               %found.getGroup().add(%obj.shield[%newshieldidx]);
            }// (ST)
         }
      }
   }
   schedule(500, %obj, "ShieldGenUpdate", %obj);
}

function getClosestFF(%obj)
{
   if(!isObject(%obj))
      return 0;

   %surfacePt = posFromTransform(%obj.getTransform());
   InitContainerRadiusSearch(VectorAdd(%surfacePt, "0 0 0.3"), 24, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
%obj.closest = 0;

  %nearestDist = 2000;
while(%found)
   {
// --- Moved (ST)
//   %nearestDist = 2000;

      %foundName = %found.getDataBlock().getName();
   if(%foundName $= "ShieldGeneratorDeployedBase" &&
   %found != %obj &&
   %found.team == %obj.team &&
   %found.FieldBasePoint &&
   %found.connections < 2 &&
   %obj.connected[0] != %found &&
   %obj.connected[1] != %found &&
   %found.connected[0] != %obj &&
   %found.connected[1] != %obj)
   {

%obj_pos = %obj.getPosition();
%fnd_pos = %found.getPosition();

// ============================= Ignore all non-visible beacons
// --- Don't connect if all of these raycasts are interrupted (ST)
//  o___o
//   \ /
//    X     =)
//   /_\
//  o   0
         %coverage = calcExplosionCoverage(%obj_pos, %found, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 0.35"), %found.cap, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 "@ %obj.height), %found, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 "@ %obj.height), %found.cap, %mask);
         if(%coverage == 1)
         {
   %obj_x = getWord(%obj_pos, 0);
   %obj_y = getWord(%obj_pos, 1);
   %obj_z = getWord(%obj_pos, 2);
   %fnd_x = getWord(%fnd_pos, 0);
   %fnd_y = getWord(%fnd_pos, 1);
   %fnd_z = getWord(%fnd_pos, 2);

   %target = setWord(%fnd_pos, 2, 0);
    %src = setWord(%obj_pos, 2, 0);

   if((%src > %target) && ((%fnd_z - %obj_z) < 0.3))
   {
   %distance_to = VectorDist(%src, %target);

   %obj.distances[%found] = %distance_to;
   if(%distance_to < %nearestDist)
   {
                  %nearestDist = %distance_to;
   %obj.closest = %found;
   }
            }
} // ...and, of course (ST)
}
  %found = containerSearchNext();
}
return %obj.closest;
}

function getFarthestFF(%obj)
{
   if(!isObject(%obj))
      return -1;

%farthest = -1;

for(%i = 1; %i <= %obj.connections; %i ++)
{
if(%obj.distances[%obj.connected[%i]] > %farthest)
{
%farthest = %obj.distances[%obj.connected[%i]];
%ff = %obj.connected[%i];
}
}
return %ff;
}

function ShieldGeneratorDeployableImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.deploySensors--;
   %plyr.client.updateSensorPackText(%plyr.deploySensors);

   if(%plyr.deploySensors <= 0)
   {
      // take the deployable off the player's back and out of inventory
      %plyr.unmountImage(%slot);
      %plyr.decInventory(%item.item, 1);
   }

   // create the actual deployable
   %rot = %item.getInitialRotation(%plyr);
// --- Not needed, will always be beacon (ST)
//   if(%item.deployed.className $= "DeployedTurret")
//      %className = "Turret";
//   else
//      %className = "StaticShape";

   %deplObj = new ("StaticShape") () { //(%className)() {
      dataBlock = %item.deployed;
   };


   // set orientation
// --- Not needed, will always be beacon (ST)
//   if(%className $= "Turret")
//      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
//   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   // set the recharge rate right away
//   if(%deplObj.getDatablock().rechargeRate)
//      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   // set team, owner, and handle
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   // set the sensor group if it needs one
   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   // place the deployable in the MissionCleanup/Deployables group (AI reasons)
//   addToDeployGroup(%deplObj);
   MissionCleanup.add(%deplObj);

   //let the AI know as well...
   AIDeployObject(%plyr.client, %deplObj);

   // play the deploy sound
   serverPlay3D(%item.deploySound, %deplObj.getTransform());

   // increment the team count for this deployed object
   $TeamDeployedCount[%plyr.team, %item.item]++;
//   %deplObj.deploy();
   %deplObj.playThread($DeployThread, "deploy");

// --- Begin auto-height adjusting (ST)
   %pos = %deplObj.getPosition();
   %height = $FF::CheckHeight;
   %maxHeight = VectorAdd(%pos, "0 0 "@ %height);
   %obstructMask = $TypeMasks::InteriorObjectType | //$TypeMasks::TerrainObjectType | --- =( der... (ST)
                   $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType;

   %obstruction = ContainerRayCast(%pos, %maxHeight, %obstructMask, %deplObj);
   if(%obstruction)
      %height = getWord(%obstruction, 3) - getWord(%pos, 2) - 0.4;
   else // --- outdoors (ST)
      %height = 7;

   %capPos = VectorAdd(%pos, "0 0 "@ %height);
// --- End auto-height adjusting (ST)
   %capObj = new ("StaticShape")() {
      dataBlock = ShieldGeneratorDeployedBase;
   };
   %deplObj.FieldBasePoint = true;
   %deplObj.cap = %capObj;
   %deplObj.height = %height; // --- (ST)
   %capObj.base = %deplObj;
   %capObj.team = %deplObj.team; // --- (ST)
   //addAttachment(%deplObj, %capObj);
//   addAttachment(%capObj, %deplObj);

   %deplObj.connections = 0;
   %deplObj.connected[0] = 0;
   %deplObj.connected[1] = 0;

//   %rot = vectorAdd(%rot, "0 0 1");
   %cappoint = vectorAdd(%item.surfacePt, "0 0 "@ %height);//6.2"); (ST)
   %capObj.setTransform(%cappoint SPC %rot);
   setTargetSensorGroup(%capObj.getTarget(), %deplObj.team);
   %capObj.playThread($AmbientThread, "ambient");
   %deplObj.playThread($AmbientThread, "ambient");

   %deplObj.beam = new forceFieldBare()
   {
      position = VectorAdd(%item.surfacePt, "-0.0375 -0.0375 0");// --- centering (ST)
      rotation = %rot;//"0 0 0 0"; (ST)
      scale = "0.075 0.075 "@ %height;//6.4"; (ST)
      dataBlock = "DeployedShieldBeam";
      team = %deplObj.team;
   };
   %deplObj.beam.setSelfPowered();

   %deplObj.beam.isFF = true;
   %deplObj.beam.deployBase = %capObj;

   MissionCleanup.add(%deplObj.beam);
   %deplObj.findNearestGen();
   schedule(1000, %deplObj, "ShieldGenUpdate", %deplObj);
}

function DeployedTeamSlowFieldBareGreen::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   %base = %targetObject.deployBase;
   %base.getDataBlock().damageObject(%base, %sourceObject, %position, %amount, %damageType);

   %otherBase = %base.connected[0];
   %otherBase = %base.connected[1];

   if (%otherBase == %targetObject)
      %other.getDataBlock().damageObject(%other, %sourceObject, %position, %amount, %damageType);
   else
   {
      %other = %base.connected[1];
      %other.getDataBlock().damageObject(%other, %sourceObject, %position, %amount, %damageType);
   }
}

datablock ShapeBaseImageData(ShieldGeneratorDeployableImage)
{
   mass = 15;
   emap = true;

   shapeFile = "pack_deploy_sensor_motion.dts";
   item = ShieldGeneratorPack;
   mountPoint = 1;
   offset = "0 0 0";

   deployed = ShieldGeneratorDeployedBase;
   heatSignature = 0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;
   
    maxEnemyFlagDist = 150;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 150;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(ShieldGeneratorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_sensor_motion.dts";
   mass = 4.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = false;
   image = "ShieldGeneratorDeployableImage";
   pickUpName = "a deployable array beacon";
   heatSignature = 0;

   emap = true;
};

datablock SensorData(ShieldGeneratorBaseSensorObj)
{
detects = false;
detectsUsingLOS = true;
detectsPassiveJammed = false;
detectsActiveJammed = false;
detectsCloaked = false;
detectionPings = true;
detectRadius = 10;
};

datablock StaticShapeData(ShieldGeneratorDeployedBase)
{
   className = "Station";
   catagory = "Deployables";
   //shapefile = "solarpanel.dts";
   shapefile = "deploy_sensor_motion.dts";
   rechargeRate = 0.31;

   needsNoPower = true;
   mass = 2.0;
   maxDamage = 62.5;
   destroyedLevel = 62.5;
   disabledLevel = 62.5;
   repairRate = 0;
explosion= SmallTurretExplosion;
expDmgRadius = 2.0;
expDamage = 0.8;
expImpulse = 2000.0;

deployedObject = true;

   energyPerDamagePoint = 50;
   maxEnergy = 50;

  humSound = SensorHumSound;
   heatSignature = 0;
pausePowerThread = true;

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;

   cmdIcon = CMDSwitchIcon;
   cmdCategory = "DSupport";
   cmdMiniIconName = "commander/MiniIcons/com_switch_grey";
   targetNameTag = 'Deployable';
   targetTypeTag = 'Forcefield Beacon';

   sensorData = ShieldGeneratorBaseSensorObj;
   sensorRadius = ShieldGeneratorBaseSensorObj.detectRadius;
   sensorColor = "0 212 45";

   firstPersonOnly = true;

   //lightOnlyStatic = true;
   lightType = "PulsingLight";
   lightColor = "0 1 0 1";
   lightTime = 1200;
   lightRadius = 6;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;

   undeployPack = "ShieldGeneratorPack";
};

datablock ForceFieldBareData(DeployedTeamSlowFieldBareGreen): StaticShapeDamageProfile
{
   fadeMS           = 1000;
   baseTranslucency = 0.2;
   powerOffTranslucency = 0.0;
   dynamicType = $TypeMasks::DamagableItemObjectType;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.89 0.31";
   powerOffColor    = "0.0 0.0 0.0";

   cmdCategory = "DSupport";

   targetNameTag = 'Force Field';

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
   isFF = true;
};

datablock ForceFieldBareData(DeployedShieldBeam)
{
   fadeMS           = 1000;
   baseTranslucency = 0.3;
   powerOffTranslucency = 0.3;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.28 0.99";
   powerOffColor    = "0 0 0";
   targetTypeTag    = 'Force Field';

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
   isFF = true;
};

//====================================== Bussard Resource Collector
$TeamDeployableMax[BussardCollectorPack] = 1;

datablock StaticShapeData(BussardCollector) : StaticShapeDamageProfile
{
   shapeFile      = "stackable4m.dts";
   explosion      = MortarExplosion;

   maxDamage      = 10;
   destroyedLevel = 10;
   disabledLevel  = 10;

   expDmgRadius = 30.0;
   expDamage = 1.15;
   expImpulse = 3500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Bussard Resource Collector';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSolarGeneratorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_solargen_grey";
   humSound = SensorJammerActivateSound;
};

datablock ShapeBaseImageData(BussardCollectorPackImage)
{
   mass = 10;

   shapeFile = "stackable5m.dts";
   item = BussardCollectorPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BussardCollector;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 0;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(BussardCollectorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "stackable5m.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BussardCollectorPackImage";
   pickUpName = "a bussard collector";
   heatSignature = 0;

   emap = true;
};

function BussardCollectorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function BussardCollectorPackImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function BussardCollectorPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("12 12 15");
   %deplObj.setSelfPowered();
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BussardCollectorPack"]++;
   MissionCleanup.add(%deplObj);

   startBussardCollector(%deplObj);

   %deplObj.deployedItem = "BussardCollectorPack";
}

function BussardCollectorPackImage::testSlopeTooGreat(%item)
{
   if(%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 65;

   return false;
}

function startBussardCollector(%obj)
{
    messageAll('MsgBussardDeployed', '\c2%1 Bussard Resource Collector activated, capacity in 300 seconds.', $TeamName[%obj.team]);
    schedule(300 * 1000, %obj, "endBussardCollector", %obj);
}

function endBussardCollector(%obj)
{
    messageAll('MsgBussardComplete', '\c2%1 Bussard Resource Collector full!', $TeamName[%obj.team]);

    $ResourceCount[%obj.team] += 10000;

    if($ResourceCount[%obj.team] > $ResourceMax[%obj.team])
        $ResourceCount[%obj.team] = $ResourceMax[%obj.team];

    %obj.setDamageState("Destroyed");
}

function BussardCollector::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "BussardCollectorPack"]--;
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Bussard Resource Collector
$TeamDeployableMax[ResourceDrillerPack] = 2;

datablock StaticShapeData(ResourceDriller) : StaticShapeDamageProfile
{
   shapeFile      = "stackable5l.dts";
   explosion      = ShapeExplosion;

   maxDamage      = 2.25;
   destroyedLevel = 2.25;
   disabledLevel  = 2.25;

   expDmgRadius = 12.5;
   expDamage = 0.65;
   expImpulse = 1850.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Resource Driller';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
   
   cmdCategory = "DSupport";
   cmdIcon = CMDSolarGeneratorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_solargen_grey";
   humSound = GeneratorHumSound;
};

datablock ShapeBaseImageData(ResourceDrillerPackImage)
{
   mass = 10;

   shapeFile = "stackable5m.dts";
   item = ResourceDrillerPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = ResourceDriller;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
   surfaceScanMask = $TypeMasks::TerrainObjectType;
    maxEnemyFlagDist = 0;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(ResourceDrillerPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "stackable5m.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "ResourceDrillerPackImage";
   pickUpName = "a resource collector";
   heatSignature = 0;

   emap = true;
};

function ResourceDrillerPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function ResourceDrillerPackImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function ResourceDrillerPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("2 2 2");
   %deplObj.setSelfPowered();
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "ResourceDrillerPack"]++;
   MissionCleanup.add(%deplObj);

   startResourceDrill(%deplObj);

   %deplObj.deployedItem = "ResourceDrillerPack";
}

function ResourceDrillerPackImage::testSlopeTooGreat(%item)
{
   if(%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 30;

   return false;
}

function startResourceDrill(%obj)
{
    schedule(10000, %obj, "resourceDrillTick", %obj);
}

function resourceDrillTick(%obj)
{
    $ResourceCount[%obj.team] += mCeil(20 * %obj.getDamageLeftPct());

    if($ResourceCount[%obj.team] > $ResourceMax[%obj.team])
        $ResourceCount[%obj.team] = $ResourceMax[%obj.team];
    
    schedule(10000, %obj, "resourceDrillTick", %obj);
}

function ResourceDriller::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "ResourceDrillerPack"]--;
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Base Core Node
$TeamDeployableMax[BaseCorePack] = 1;

datablock AudioProfile(BaseCoreHumSound)
{
    filename = "fx/powered/turret_heavy_idle.wav";
    description = CloseLooping3d;
    preload = true;
};

datablock StaticShapeData(BaseCore) : StaticShapeDamageProfile
{
   shapeFile      = "stackable3l.dts";
   explosion      = MortarExplosion;

   maxDamage      = 3.0;
   destroyedLevel = 3.0;
   disabledLevel  = 3.0;

   expDmgRadius = 22.5;
   expDamage = 4.0;
   expImpulse = 4000.0;

   isShielded = true;
   energyPerDamagePoint = 100;
   maxEnergy = 300;
   rechargeRate = 15 / 32;

   targetTypeTag = 'Base Core Node';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
   
   cmdCategory = "Support";
   cmdIcon = CMDSwitchIcon;
   cmdMiniIconName = "commander/MiniIcons/com_switch_grey";
   humSound = BaseCoreHumSound;
   
    maxEnemyFlagDist = 250;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 200;
	maxFriendlySpawnDist = 200;
};

datablock ShapeBaseImageData(BaseCorePackImage)
{
   mass = 10;

   shapeFile = "stackable5m.dts";
   item = BaseCorePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BaseCore;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(BaseCorePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "stackable5m.dts";
   mass = 5.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BaseCorePackImage";
   pickUpName = "a base core node";
   heatSignature = 0;

   emap = true;
};

function BaseCorePack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function BaseCorePackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("1.5 1.5 1.5");

   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
   %deplObj.setSelfPowered();
   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BaseCorePack"]++;
   MissionCleanup.add(%deplObj);

   initBaseCore(%deplObj);

   %deplObj.deployedItem = "BaseCorePack";
}

function BaseCorePackImage::testSlopeTooGreat(%item)
{
   if(%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 30;

   return false;
}

function initBaseCore(%obj)
{
    $ResourceMax[%obj.team] = 150000;
    $BaseCore[%obj.team] = %obj;

    %obj.nodes = createRandomSimSet();
    %obj.isBaseNode = 1;
    
    schedule(5000, %obj, "baseCoreTick", %obj);
}

function baseCoreTick(%obj)
{
    schedule(5000, %obj, "baseCoreTick", %obj);
}

function baseCoreCheck(%player, %dist)
{
    InitContainerRadiusSearch(%player.position, %dist, $TypeMasks::StaticShapeObjectType);

    while((%core = ContainerSearchNext()) != 0)
    {
        if(%core.isBaseNode == 1)
            return true;
    }
    
    messageClient(%player.client, 'MsgTooFarFromCore', '\c1You must be within %1m of the Base Core Node to deploy this item.', %dist);
    return false;
}

function BaseCore::onDestroyed(%this, %obj, %prevState)
{
    $ResourceMax[%obj.team] = 15000;
    $BaseCore[%obj.team] = 0;

    if($ResourceCount[%obj.team] > $ResourceMax[%obj.team])
        $ResourceCount[%obj.team] = $ResourceMax[%obj.team];

    $TeamDeployedCount[%obj.team, "BaseCorePack"]--;
   
    for(%i = 0; %i < %obj.nodes.getCount(); %i++)
    {
        %node = %obj.nodes.getObject(%i);
        
        if(isObject(%node))
            %node.setDamageState("Destroyed");
    }
   
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Spawn Refit Node
$TeamDeployableMax[NodeSpawnFavsPack] = 4;

datablock StaticShapeData(NodeSpawnFavs) : StaticShapeDamageProfile
{
   shapeFile      = "ammo_mine.dts";
   explosion      = MortarExplosion;

   maxDamage      = 1.0;
   destroyedLevel = 1.0;
   disabledLevel  = 1.0;

   expDmgRadius = 22.5;
   expDamage = 4.0;
   expImpulse = 4000.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0;

   targetTypeTag = 'Spawn Refit Node';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_deploymotionsensor";
};

datablock ShapeBaseImageData(NodeSpawnFavsPackImage)
{
   mass = 10;

   shapeFile = "ammo_mine.dts";
   item = NodeSpawnFavsPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = NodeSpawnFavs;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
   requiresBaseNode = true;
   maxBaseNodeDist = 300;
};

datablock ItemData(NodeSpawnFavsPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "ammo_mine.dts";
   mass = 2.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodeSpawnFavsPackImage";
   pickUpName = "a spawn refit node";
   heatSignature = 0;

   emap = true;
};

function NodeSpawnFavsPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function NodeSpawnFavsPackImage::onDeploy(%item, %plyr, %slot)
{
//   if(!baseCoreCheck(%plyr, 300))
//       return;

   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   // Add to Base Core
   $BaseCore[%deplObj.team].nodes.add(%deplObj);
   
   %deplObj.setScale("3 3 3");
   %deplObj.spawnRefit = 1;
   %deplObj.setSelfPowered();
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "NodeSpawnFavsPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "NodeSpawnFavsPack";
}

function NodeSpawnFavs::onDestroyed(%this, %obj, %prevState)
{
    $TeamDeployedCount[%obj.team, "NodeSpawnFavsPack"]--;
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Projectile Repulsor Node
$TeamDeployableMax[NodeRepulsorPack] = 3;

datablock StaticShapeData(RepulsorBubble) : StaticShapeDamageProfile
{
//   className = "StaticShape";
//   dynamicType = $TypeMasks::StaticShapeObjectType;
   shapefile = "grenade_flare.dts";
   rechargeRate = 0.31;

   needsNoPower = true;
   mass = 2.0;
   maxDamage = 45;
   destroyedLevel = 45;
   disabledLevel = 45;
   repairRate = 0;
	explosion	= VehicleBombExplosion;
	expDmgRadius = 25.0;
	expDamage = 3;
	expImpulse = 2000.0;

	deployedObject = true;

   energyPerDamagePoint = 50;
   maxEnergy = 50;

  	humSound = SensorJammerActivateSound;
   heatSignature = 0;

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDCameraIcon;
   cmdMiniIconName = "commander/MiniIcons/com_camera_grey";

   targetNameTag = '';
   targetTypeTag = 'Projectile Repulsor Node';

   firstPersonOnly = true;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;
};

datablock ShapeBaseImageData(NodeRepulsorImage)
{
   mass = 10;

   shapeFile = "pack_deploy_inventory.dts";
   item = NodeRepulsorPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = RepulsorBubble;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 45;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    requiresBaseNode = true;
    maxBaseNodeDist = 250;
};

datablock ItemData(NodeRepulsorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodeRepulsorImage";
   pickUpName = "a projectile repulsor node";
   heatSignature = 0;

   emap = true;
};

function NodeRepulsorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function NodeRepulsorImage::onDeploy(%item, %plyr, %slot)
{
//   if(!baseCoreCheck(%plyr, 200))
//       return;

   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %trans = %item.surfacePt SPC %rot;
   %deplObj = createTeamObject(%plyr, "StaticShape", "RepulsorBubble", %trans, false, "15 15 15"); //addStation(%plyr, %trans, false);
   %deplObj.createNode();
   %deplObj.setRootNode();
   %deplObj.deployedItem = "NodeRepulsor";
   %deplObj.playthread(0, "Deploy");

   // Add to Base Core
   $BaseCore[%deplObj.team].nodes.add(%deplObj);
   
   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "NodeRepulsorPack"]++;

   glueToRemoteBase(%deplObj, %item.surfacePt);
   
   // Repulsor beacon magic
   %aura = Aura.create("RepulsorBeaconField");
   %aura.attachToObject(%deplObj);
   %deplObj.repulsorField = %aura;
   %aura.start();
}

function NodeRepulsorImage::testSlopeTooGreat(%item)
{
    if(%item.surface)
        return getTerrainAngle(%item.surfaceNrm) > 45;
}

function RepulsorBubble::onDestroyed(%this, %obj, %prevState)
{
    %obj.repulsorField.stop();
    %obj.repulsorField.destroy();
    $TeamDeployedCount[%obj.team, "NodeRepulsorPack"]--;
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}
