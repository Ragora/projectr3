//----------------------------------------------------------------------------
// Score HUD Override

function ScoreScreen::__construct(%this)
{
    ScoreScreen.Version = 1.0;
}

function ScoreScreen::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(ScoreScreen))
   System.addClass(ScoreScreen);

// String table for menus
$Menu::Main = 0;
$MDMenuName[0] = "Main Menu";

$Menu::Score = 1;
$MDMenuName[1] = "Score Menu";

$Menu::Vehicle = 2;
$MDMenuName[2] = "Vehicle Config";

$Menu::Mech = 3;
$MDMenuName[3] = "Mech Lab";

$Menu::Enhancements = 4;
$MDMenuName[4] = "Enhancements";

$Menu::Stats = 5;
$MDMenuName[5] = "Stats";

$Menu::Account = 6;
$MDMenuName[6] = "Account";

$MenuState::Default = 0;
$MenuState::Static = 1;

$MHNotStackableBreak = false;

// Gametype overloads
function DefaultGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function HuntersGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function TeamHuntersGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function RabbitGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function BountyGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function CnHGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function DnDGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function CTFGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function DMGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function SiegeGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

function ConstructionGame::updateScoreHud(%game, %client, %tag)
{
     ScoreScreen.updateScoreHud(%game, %client, %tag);
}

// processGameLink
function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function HuntersGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function TeamHuntersGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function RabbitGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function CTFGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DMGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function CnHGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DnDGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function SiegeGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function ConstructionGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     ScoreScreen.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

// Code
function ScoreScreen::updateScoreHud(%this, %game, %client, %tag)
{
     if(%client.scoreHudMenuState $= "")
          %client.scoreHudMenuState = $MenuState::Default;

     if(%client.scoreHudMenuState == $MenuState::Static)
          return;

     if(%client.scoreHudMenu $= "")
          if(%client.defaultMenu $= "")
               %client.scoreHudMenu = $Menu::Main;
          else
               %client.scoreHudMenu = %client.defaultMenu;

     %client.menuHudTag = %tag;

     messageClient(%client, 'SetScoreHudHeader', "", "");
     messageClient(%client, 'SetScoreHudHeader', "", '<just:center><a:gamelink\tGL\t1>Score</a> | <a:gamelink\tGL\t2>Vehicle Config</a> | <a:gamelink\tGL\t4>Enhancements</a> | <a:gamelink\tGL\t6>Account and Stats</a> | <a:gamelink\tClose>Close</a>');
     renderMainMenu(%game, %client, %tag);
}

function ScoreScreen::processGameLink(%this, %game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     %tag = %client.menuHudTag;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     %index = 0;

     if(%arg1 $= "GL")
     {
          %client.scoreHudMenu = %arg2;
          %game.processGameLink(%client, "Main");
          return;
     }

     switch(%client.scoreHudMenu)
     {
          case $Menu::Score:
               scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Vehicle:
               vehicleProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Stats:
               statsProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Enhancements:
               enhancementProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Account:
               accountProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          default:
//               echo(%arg1 SPC %arg2 SPC %arg3);
//               %client.scoreHudMenu = %arg2;
//               %game.processGameLink(%client, %arg1);
               closeModuleHud(%client);
     }
}

function renderMainMenu(%game, %client, %tag)
{
     switch(%client.scoreHudMenu)
     {
          case $Menu::Score:
//               %client.scoreHudMenuState = $MenuState::Default;
               scoreMainMenuRender(%game, %client, %tag);

          case $Menu::Vehicle:
               vehicleMainMenuRender(%game, %client, %tag);

          case $Menu::Mech:
               mechMainMenuRender(%game, %client, %tag);

          case $Menu::Stats:
               statsMainMenuRender(%game, %client, %tag);

          case $Menu::Enhancements:
               enhancementMainMenuRender(%game, %client, %tag);

          case $Menu::Account:
               accountMainMenuRender(%game, %client, %tag);

          default:
               %index = 0;

               messageClient(%client, 'ClearHud', "", %tag, 0);
               messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Meltdown v%1: Sol Conflict -- Menu Selector', System.Version);
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Click on an option above to browse that specific sub-menu.');
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'This mod is a Work In Progress (WIP) - beta approaching soon!');
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>You may also drop by the forums at <a:wwwlink\tforum.radiantage.com>http://forum.radiantage.com/</a>');
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>for Meltdown 3 news and updates.');
               %index++;

               messageClient(%client, 'ClearHud', "", %tag, %index);
     }
}

function closeModuleHud(%client)
{
     %tag = %client.menuHudTag;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     serverCmdHideHud(%client, 'scoreScreen');
     commandToClient(%client, 'setHudMode', 'Standard', "", 0);
     %client.scoreHudMenuState = $MenuState::Default;
     %client.scoreHudMenu = %client.defaultMenu;

     %obj = %client.player;	// +[soph]
     if( !isObject( %obj ) )	// this lot had -better- fix it!
          return;
     if( %obj.isMounted() )
     {
          %mount = %obj.getObjectMount();
          %block = %mount.getDataBlock();
          for (%i = 0; %i < %block.numMountPoints; %i++)
          {
               if ( %mount.getMountNodeObject( %i ) == %obj )
               {
                    %node = %i;
                    break;
               }
          }
          %block.playerMounted( %mount , %obj , %node );
     }				// +[/soph]
}

//------------------------------------------------------------------------------
function scoreMainMenuRender(%game, %client, %tag)
{
   if (Game.numTeams > 1)
   {
      // Send header:
//      messageClient( %client, 'SetScoreHudHeader', "", '<tab:15,315>\t%1<rmargin:260><just:right>%2<rmargin:560><just:left>\t%3<just:right>%4',
//            %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      // Send subheader:
      messageClient( %client, 'SetScoreHudSubheader', "", '<tab:15,315>\t%3 (%1)<rmargin:260><just:right>SCORE: %4<rmargin:560><just:left>\t%5 (%2)<just:right>SCORE: %6',
            $TeamRank[1, count], $TeamRank[2, count], %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      %index = 0;
      while ( true )
      {
         if ( %index >= $TeamRank[1, count]+2 && %index >= $TeamRank[2, count]+2 )
            break;

         //get the team1 client info
         %team1Client = "";
         %team1ClientScore = "";
         %col1Style = "";
         if ( %index < $TeamRank[1, count] )
         {
            %team1Client = $TeamRank[1, %index];
            %team1ClientScore = %team1Client.score $= "" ? 0 : %team1Client.score;
            %col1Style = %team1Client == %client ? "<color:dcdcdc>" : "";
            %team1playersTotalScore += %team1Client.score;
         }
         else if( %index == $teamRank[1, count] && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = "--------------";
         }
         else if( %index == $teamRank[1, count]+1 && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = %team1playersTotalScore != 0 ? %team1playersTotalScore : 0;
         }
         //get the team2 client info
         %team2Client = "";
         %team2ClientScore = "";
         %col2Style = "";
         if ( %index < $TeamRank[2, count] )
         {
            %team2Client = $TeamRank[2, %index];
            %team2ClientScore = %team2Client.score $= "" ? 0 : %team2Client.score;
            %col2Style = %team2Client == %client ? "<color:dcdcdc>" : "";
            %team2playersTotalScore += %team2Client.score;
         }
         else if( %index == $teamRank[2, count] && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = "--------------";
         }
         else if( %index == $teamRank[2, count]+1 && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = %team2playersTotalScore != 0 ? %team2playersTotalScore : 0;
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200>%3</clip><just:right>%4',
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200><a:gamelink\t%8>%3</a></clip><just:right>%4',
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style, %team1Client, %team2Client );
         }

         %index++;
      }
   }
   else
   {
      //tricky stuff here...  use two columns if we have more than 15 clients...
      %numClients = $TeamRank[0, count];
      if ( %numClients > $ScoreHudMaxVisible )
         %numColumns = 2;

      // Clear header:
//      messageClient( %client, 'SetScoreHudHeader', "", "" );

      // Send header:
      if (%numColumns == 2)
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15,315>\tPLAYER<rmargin:270><just:right>SCORE<rmargin:570><just:left>\tPLAYER<just:right>SCORE');
      else
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15>\tPLAYER<rmargin:270><just:right>SCORE');

      %countMax = %numClients;
      if ( %countMax > ( 2 * $ScoreHudMaxVisible ) )
      {
         if ( %countMax & 1 )
            %countMax++;
         %countMax = %countMax / 2;
      }
      else if ( %countMax > $ScoreHudMaxVisible )
         %countMax = $ScoreHudMaxVisible;

      for ( %index = 0; %index < %countMax; %index++ )
      {
         //get the client info
         %col1Client = $TeamRank[0, %index];
         %col1ClientScore = %col1Client.score $= "" ? 0 : %col1Client.score;
         %col1Style = %col1Client == %client ? "<color:dcdcdc>" : "";

         //see if we have two columns
         if ( %numColumns == 2 )
         {
            %col2Client = "";
            %col2ClientScore = "";
            %col2Style = "";

            //get the column 2 client info
            %col2Index = %index + %countMax;
            if ( %col2Index < %numClients )
            {
               %col2Client = $TeamRank[0, %col2Index];
               %col2ClientScore = %col2Client.score $= "" ? 0 : %col2Client.score;
               %col2Style = %col2Client == %client ? "<color:dcdcdc>" : "";
            }
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195>%3</clip><just:right>%4',
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195>%1</clip><rmargin:260><just:right>%2',
                     %col1Client.name, %col1ClientScore, %col1Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195><a:gamelink\t%8>%3</a></clip><just:right>%4',
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style, %col1Client, %col2Client );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195><a:gamelink\t%4>%1</a></clip><rmargin:260><just:right>%2',
                     %col1Client.name, %col1ClientScore, %col1Style, %col1Client );
         }
      }

   }

   // Tack on the list of observers:
   %observerCount = 0;
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.team == 0)
         %observerCount++;
   }

   if (%observerCount > 0)
   {
	   messageClient( %client, 'SetLineHud', "", %tag, %index, "");
      %index++;
		messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:10, 310><spush><font:Univers Condensed:22>\tOBSERVERS (%1)<rmargin:260><just:right>TIME<spop>', %observerCount);
      %index++;
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         //if this is an observer
         if (%cl.team == 0)
         {
            %obsTime = getSimTime() - %cl.observerStartTime;
            %obsTimeStr = %game.formatTime(%obsTime, false);
		      messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20, 310>\t<clip:150>%1</clip><rmargin:260><just:right>%2',
		                     %cl.name, %obsTimeStr );
            %index++;
         }
      }
   }

   //clear the rest of Hud so we don't get old lines hanging around...
   messageClient( %client, 'ClearHud', "", %tag, %index );
}

function scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
   //the default behavior when clicking on a game link is to start observing that client
   %targetClient = %arg1;

   if(%arg1 $= "Close")
//   {
        closeModuleHud(%client);
//        return;		// -soph
//   }
   else if( %arg1 $= "Main" )	// +[soph]
        renderMainMenu(%game, %client, %tag);
   else				// +[/soph]
   if ((%client.team == 0) && isObject(%targetClient) && (%targetClient.team != 0))
   {
      %prevObsClient = %client.observeClient;

      // update the observer list for this client
      observerFollowUpdate( %client, %targetClient, %prevObsClient !$= "" );

      serverCmdObserveClient(%client, %targetClient);
      displayObserverHud(%client, %targetClient);

      if (%targetClient != %prevObsClient)
      {
         messageClient(%targetClient, 'Observer', '\c1%1 is now observing you.', %client.name);
         messageClient(%prevObsClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);
      }
   }
}

//------------------------------------------------------------------------------
function enhancementMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tConfigWeapons>[Configure Weapons]</a> | <a:gamelink\tConfigPacks>[Configure Packs]</a> | <a:gamelink\tConfigArmors>[Configure Armors]</a>');

//     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><a:gamelink\tSelectArmor>[Select Armor]</a> Current Armor: %1', %client.pendingWeaponEnhArmor);
//     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Click above to configure the available options for Enhancements.');
     %index++;

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function enhancementProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
//     %tag = %client.moduleHudTag;
//     messageClient(%client, 'ClearHud', "", %tag, 0);
//     %index = 0;
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        // ---------------------------------------------------------------------
        // Weapon section
        case "ConfigWeapons":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Weapon');

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Select a modifiable weapon below:');
             %index++;

             for(%i = 0; %i < $WeaponCount; %i++)
             {
                if($WeaponsListData[%i, "name"] $= "")
                   continue;

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSelectSlot\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $WeaponsListData[%i, "name"], $WeaponsListData[%i, "desc"], %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SelectSlot":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>%1 -> Select Slot', $WeaponsListData[%arg2, "name"]);

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Select a slot to configure:');
             %index++;

             %weapon = $WeaponsListData[%arg2, "item"];
             %count = %weapon.image.enhancementSlots;

             if(%count $= "")
                %count = 0;

             if(%client.pendingEnhancement[$EnhancementType::Weapon, %weapon, 0] $= "")
                %client.setDefaultEnhancements($EnhancementType::Weapon, %weapon);

             for(%i = 0; %i < %count; %i++)
             {
                 %enh = %client.pendingEnhancement[$EnhancementType::Weapon, %weapon, %i];

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tModWeapon\t%4\t%5>Slot %1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320>%3</clip>', %i+1, %enh.name, %enh.description, %arg2, %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, 'Loadouts:');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;

             %ecount = $SCEnhTotal[%client.guid, %weapon];

             if(%ecount $= "")
                %ecount = 0;

             for(%i = 0; %i < %ecount; %i++)
             {
                 %enhid = $SCEnhLookup[%client.guid, %weapon, %i];
                 %ename = $SCEnhName[%enhid] !$= "" ? $SCEnhName[%enhid] : "New Loadout";

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSetLoadoutW\t%3\t%4>%1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320><a:gamelink\tUpdateLoadoutW\t%3\t%4>[overwrite]</a> <a:gamelink\tSetNameW\t%3\t%4>[set name]</a></clip>', %i+1, %ename, %arg2, %enhid);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tNewLoadoutW\t%1\t%2>Create current loadout as new loadout</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigWeapons\t1>Back</a>');
             %index++;
             return;

        case "NewLoadoutW":
             %weapon = $WeaponsListData[%arg2, "item"];

             if($SCEnhTotal[%client.guid, %weapon] < 0 || $SCEnhTotal[%client.guid, %weapon] $= "")
                $SCEnhTotal[%client.guid, %weapon] = 0;

             if($SCEnhCount[%client.guid] < 0)
                $SCEnhCount[%client.guid] = 0;

             if($SCEnhTotal[%client.guid, %weapon] > 9)
                return %game.processGameLink(%client, "SelectSlot", %arg2);

             // Dirty flag set, purge this data if player reconnects since data can't be erased from sparse array
             $SCEnhDirty[%client.guid, %weapon] = 1;

             %trueid = %client.guid @ $SCEnhCount[%client.guid];
             $SCEnhLookup[%client.guid, %weapon, $SCEnhTotal[%client.guid, %weapon]] = %trueid;
             $SCEnhData[%trueid, "type"] = $EnhancementType::Weapon;
             $SCEnhData[%trueid, "item"] = %weapon;

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%trueid, %i] = %client.pendingEnhancement[$EnhancementType::Weapon, %weapon, %i];

             $SCEnhTotal[%client.guid, %weapon]++;
             $SCEnhCount[%client.guid]++;

             %game.processGameLink(%client, "SelectSlot", %arg2);
             return;

        case "UpdateLoadoutW":
             %weapon = $WeaponsListData[%arg2, "item"];

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%arg3, %i] = %client.pendingEnhancement[$EnhancementType::Weapon, %weapon, %i];

             %game.processGameLink(%client, "SelectSlot", %arg2);
             return;

        case "SetNameW":
             %client.enhSetName = %arg3;
             %client.screenNextArg = %arg2;
             %client.screenReference = %game;
             %client.screenNextWindow = "SelectSlot";
             bottomprint(%client, "Name will be set to the next chat typed in the chat box.", 5, 1);

             %game.processGameLink(%client, "SelectSlot", %arg2);
             return;

        case "SetLoadoutW":
             %weapon = $WeaponsListData[%arg2, "item"];

             for(%i = 0; %i < 6; %i++)
                %client.pendingEnhancement[$EnhancementType::Weapon, %weapon, %i] = $SCEnhData[%arg3, %i];

             %game.processGameLink(%client, "SelectSlot", %arg2);
             return;

        case "ModWeapon":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Weapon: %1 (Slot %2)', $WeaponsListData[%arg2, "name"], %arg3+1);
             %weapon = $WeaponsListData[%arg2, "item"];

//             %selected = %client.vModPriWeapon[%vData] $= "" ? $VehicleDefaultPrimaryWep[%arg2] : %client.vModPriWeapon[%vData];

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Available Enhancements:');
             %index++;

//             %count = 0;

             %enhCount = Enhancement.enhancementCount[$EnhancementType::Weapon];
             %wEnhCount = %weapon.image.enhancementSlots;

             for(%i = 0; %i < %enhCount; %i++)
             {
                %enh = Enhancement.enhancements[$EnhancementType::Weapon, %i].getName();
                %wearable = %enh.wearableMask & $WeaponsListData[%arg2, "mask"];

                if(%wearable)
                {
                    %stackable = %enh.prefFlags & $Enhancement::Stackable;

                    // If not stackable, prevent.. well, stacking
                    if(!%stackable)
                    {
                        for(%j = 0; %j < %wEnhCount; %j++)
                        {
                            if(%client.pendingEnhancement[$EnhancementType::Weapon, %weapon, %j] $= %enh)
                                $MHNotStackableBreak = true;
                        }

                        if($MHNotStackableBreak)
                        {
                            $MHNotStackableBreak = false;
                            continue;
                        }
                    }

                    messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tApplyMode\t%3\t%4\t%5\t%6>%1: %2</a></clip>', %enh.name, %enh.description, %enh, %weapon, %arg3, %arg2);
                    %index++;
                }
//                %count++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tSelectSlot\t%1><color:FFFF00>Back</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigWeapons\t1><color:FF0000>Done modifying %1</a>', $WeaponsListData[%arg2, "name"]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "ApplyMode":
             %client.pendingEnhancement[$EnhancementType::Weapon, %arg3, %arg4] = %arg2;
             %game.processGameLink(%client, "SelectSlot", %arg5);
             return;

        // ---------------------------------------------------------------------
        // Armor section
        case "ConfigArmors":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Armor');

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Select a modifiable armor below:');
             %index++;

             for(%i = 0; %i < $ArmorCount; %i++)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSelectArmorSlot\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $ArmorsListData[%i, "name"], $ArmorsListData[%i, "desc"], %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SelectArmorSlot":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>%1 -> Select Slot', $ArmorsListData[%arg2, "name"]);

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Select a slot to configure:' ) ;
             %index++;

             %armor = $ArmorsListData[%arg2, "size"];
             %count = armorSizeToBlock(%armor).enhancementSlots;

             if(%count $= "")
                %count = 0;

             if(%client.pendingEnhancement[$EnhancementType::Armor, %armor, 0] $= "")
                %client.setDefaultEnhancements($EnhancementType::Armor, %armor);

             for(%i = 0; %i < %count; %i++)
             {
                 %enh = %client.pendingEnhancement[$EnhancementType::Armor, %armor, %i];

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tModArmor\t%4\t%5>Slot %1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320>%3</clip>', %i+1, %enh.name, %enh.description, %arg2, %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, 'Loadouts:');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;

             %ecount = $SCEnhTotal[%client.guid, %armor];

             if(%ecount $= "")
                %ecount = -1;

             for(%i = 0; %i <= %ecount; %i++)
             {
                 %enhid = $SCEnhLookup[%client.guid, %armor, %i];
                 %ename = $SCEnhName[%enhid] !$= "" ? $SCEnhName[%enhid] : "New Loadout";

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSetLoadoutA\t%3\t%4>%1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320><a:gamelink\tUpdateLoadoutA\t%3\t%4>[overwrite]</a> <a:gamelink\tSetNameA\t%3\t%4>[set name]</a></clip>', %i+1, %ename, %arg2, %enhid);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tNewLoadoutA\t%1\t%2>Create current loadout as new loadout</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigArmors\t1>Back</a>');
             %index++;
             return;

        case "NewLoadoutA":
             %armor = $ArmorsListData[%arg2, "size"];

             if($SCEnhTotal[%client.guid, %armor] $= "")
                $SCEnhTotal[%client.guid, %armor] = -1;

             if($SCEnhCount[%client.guid] $= "")
                $SCEnhCount[%client.guid] = -1;

             if($SCEnhTotal[%client.guid, %armor] >= 9)
                return %game.processGameLink(%client, "SelectArmorSlot", %arg2);

             $SCEnhTotal[%client.guid, %armor]++;
             $SCEnhCount[%client.guid]++;
             %trueid = %client.guid @ $SCEnhCount[%client.guid];
             $SCEnhLookup[%client.guid, %armor, $SCEnhTotal[%client.guid, %armor]] = %trueid;

             $SCEnhData[%trueid, "type"] = $EnhancementType::Armor;
             $SCEnhData[%trueid, "item"] = %armor;

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%trueid, %i] = %client.pendingEnhancement[$EnhancementType::Armor, %armor, %i];

             %game.processGameLink(%client, "SelectArmorSlot", %arg2);
             return;

        case "UpdateLoadoutA":
             %armor = $ArmorsListData[%arg2, "size"];

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%arg3, %i] = %client.pendingEnhancement[$EnhancementType::Armor, %armor, %i];

             %game.processGameLink(%client, "SelectArmorSlot", %arg2);
             return;

        case "SetNameA":
             %client.enhSetName = %arg3;
             %client.screenNextArg = %arg2;
             %client.screenReference = %game;
             %client.screenNextWindow = "SelectArmorSlot";
             bottomprint(%client, "Name will be set to the next chat typed in the chat box.", 5, 1);

             %game.processGameLink(%client, "SelectArmorSlot", %arg2);
             return;

        case "SetLoadoutA":
             %armor = $ArmorsListData[%arg2, "size"];

             for(%i = 0; %i < 6; %i++)
                %client.pendingEnhancement[$EnhancementType::Armor, %armor, %i] = $SCEnhData[%arg3, %i];

             %game.processGameLink(%client, "SelectArmorSlot", %arg2);
             return;

        case "ModArmor":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Armor: %1 (Slot %2)', $ArmorsListData[%arg2, "name"], %arg3+1);
             %armor = $ArmorsListData[%arg2, "size"];

//             %selected = %client.vModPriWeapon[%vData] $= "" ? $VehicleDefaultPrimaryWep[%arg2] : %client.vModPriWeapon[%vData];

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Available Enhancements:');
             %index++;

//             %count = 0;

             %enhCount = Enhancement.enhancementCount[$EnhancementType::Armor];
             %aEnhCount = armorSizeToBlock(%armor).enhancementSlots;

             for(%i = 0; %i < %enhCount; %i++)
             {
                %enh = Enhancement.enhancements[$EnhancementType::Armor, %i].getName();
                %wearable = %enh.wearableMask & $ArmorsListData[%arg2, "mask"];

                if(%wearable)
                {
                    %stackable = %enh.prefFlags & $Enhancement::Stackable;

                    // If not stackable, prevent.. well, stacking
                    if(!%stackable)
                    {
                        for(%j = 0; %j < %aEnhCount; %j++)
                        {
                            if(%client.pendingEnhancement[$EnhancementType::Armor, %armor, %j] $= %enh)
                                $MHNotStackableBreak = true;
                        }

                        if($MHNotStackableBreak)
                        {
                            $MHNotStackableBreak = false;
                            continue;
                        }
                    }

                    messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tApplyArmorMode\t%3\t%4\t%5\t%6>%1: %2</a></clip>', %enh.name, %enh.description, %enh, %armor, %arg3, %arg2);
                    %index++;
                }
//                %count++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tSelectArmorSlot\t%1><color:FFFF00>Back</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigArmors\t1><color:FF0000>Done modifying %1</a>', $ArmorsListData[%arg2, "name"]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "ApplyArmorMode":
             %client.pendingEnhancement[$EnhancementType::Armor, %arg3, %arg4] = %arg2;
             %game.processGameLink(%client, "SelectArmorSlot", %arg5);
             return;

        // ---------------------------------------------------------------------
        // Pack section
        case "ConfigPacks":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Pack');

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Select a modifiable pack below:');
             %index++;

             for(%i = 0; %i < $PackCount; %i++)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSelectPackSlot\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $PackListData[%i, "name"], $PackListData[%i, "desc"], %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SelectPackSlot":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>%1 -> Select Slot', $PackListData[%arg2, "name"]);

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Select a slot to configure:' ) ;
             %index++;

             %pack = $PackListData[%arg2, "item"];
             %count = %pack.image.enhancementSlots;

             if(%count $= "")
                %count = 0;

             if(%client.pendingEnhancement[$EnhancementType::Pack, %pack, 0] $= "")
                %client.setDefaultEnhancements($EnhancementType::Pack, %pack);

             for(%i = 0; %i < %count; %i++)
             {
                 %enh = %client.pendingEnhancement[$EnhancementType::Pack, %pack, %i];

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tModPack\t%4\t%5>Slot %1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320>%3</clip>', %i+1, %enh.name, %enh.description, %arg2, %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, 'Loadouts:');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;

             %ecount = $SCEnhTotal[%client.guid, %pack];

             if(%ecount $= "")
                %ecount = -1;

             for(%i = 0; %i <= %ecount; %i++)
             {
                 %enhid = $SCEnhLookup[%client.guid, %pack, %i];
                 %ename = $SCEnhName[%enhid] !$= "" ? $SCEnhName[%enhid] : "New Loadout";

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSetLoadoutP\t%3\t%4>%1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320><a:gamelink\tUpdateLoadoutP\t%3\t%4>[overwrite]</a> <a:gamelink\tSetNameP\t%3\t%4>[set name]</a></clip>', %i+1, %ename, %arg2, %enhid);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tNewLoadoutP\t%1\t%2>Create current loadout as new loadout</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigPacks\t1>Back</a>');
             %index++;
             return;

        case "NewLoadoutP":
             %pack = $PackListData[%arg2, "item"];

             if($SCEnhTotal[%client.guid, %pack] $= "")
                $SCEnhTotal[%client.guid, %pack] = -1;

             if($SCEnhCount[%client.guid] $= "")
                $SCEnhCount[%client.guid] = -1;

             if($SCEnhTotal[%client.guid, %pack] >= 9)
                return %game.processGameLink(%client, "SelectPackSlot", %arg2);

             $SCEnhTotal[%client.guid, %pack]++;
             $SCEnhCount[%client.guid]++;
             %trueid = %client.guid @ $SCEnhCount[%client.guid];
             $SCEnhLookup[%client.guid, %pack, $SCEnhTotal[%client.guid, %pack]] = %trueid;

             //error("NewLoadoutW" SPC %client.guid SPC $SCEnhCount[%client.guid]);

             $SCEnhData[%trueid, "type"] = $EnhancementType::Pack;
             $SCEnhData[%trueid, "item"] = %pack;

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%trueid, %i] = %client.pendingEnhancement[$EnhancementType::Pack, %pack, %i];

             %game.processGameLink(%client, "SelectPackSlot", %arg2);
             return;

        case "UpdateLoadoutP":
             %pack = $PackListData[%arg2, "item"];

             for(%i = 0; %i < 6; %i++)
                $SCEnhData[%arg3, %i] = %client.pendingEnhancement[$EnhancementType::Pack, %pack, %i];

             %game.processGameLink(%client, "SelectPackSlot", %arg2);
             return;

        case "SetNameP":
             %client.enhSetName = %arg3;
             %client.screenNextArg = %arg2;
             %client.screenReference = %game;
             %client.screenNextWindow = "SelectPackSlot";
             bottomprint(%client, "Name will be set to the next chat typed in the chat box.", 5, 1);

             %game.processGameLink(%client, "SelectPackSlot", %arg2);
             return;

        case "SetLoadoutP":
             %pack = $PackListData[%arg2, "item"];

             for(%i = 0; %i < 6; %i++)
                %client.pendingEnhancement[$EnhancementType::Pack, %pack, %i] = $SCEnhData[%arg3, %i];

             %game.processGameLink(%client, "SelectPackSlot", %arg2);
             return;

        case "ModPack":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Pack: %1 (Slot %2)', $PackListData[%arg2, "name"], %arg3+1);
             %pack = $PackListData[%arg2, "item"];

//             %selected = %client.vModPriWeapon[%vData] $= "" ? $VehicleDefaultPrimaryWep[%arg2] : %client.vModPriWeapon[%vData];

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Available Enhancements:');
             %index++;

//             %count = 0;

             %enhCount = Enhancement.enhancementCount[$EnhancementType::Pack];
             %pEnhCount = %pack.image.enhancementSlots;

             for(%i = 0; %i < %enhCount; %i++)
             {
                %enh = Enhancement.enhancements[$EnhancementType::Pack, %i].getName();
                %wearable = %enh.wearableMask & $PackListData[%arg2, "mask"];

                if(%wearable)
                {
                    %stackable = %enh.prefFlags & $Enhancement::Stackable;

                    // If not stackable, prevent.. well, stacking
                    if(!%stackable)
                    {
                        for(%j = 0; %j < %pEnhCount; %j++)
                        {
                            if(%client.pendingEnhancement[$EnhancementType::Pack, %pack, %j] $= %enh)
                                $MHNotStackableBreak = true;
                        }

                        if($MHNotStackableBreak)
                        {
                            $MHNotStackableBreak = false;
                            continue;
                        }
                    }

                    messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tApplyPackMode\t%3\t%4\t%5\t%6>%1: %2</a></clip>', %enh.name, %enh.description, %enh, %pack, %arg3, %arg2);
                    %index++;
                }
//                %count++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tSelectPackSlot\t%1><color:FFFF00>Back</a>', %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigPacks\t1><color:FF0000>Done modifying %1</a>', $PackListData[%arg2, "name"]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "ApplyPackMode":
             %client.pendingEnhancement[$EnhancementType::Pack, %arg3, %arg4] = %arg2;
             %game.processGameLink(%client, "SelectPackSlot", %arg5);
             return;

        // ---------------------------------------------------------------------
        // Default
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function vehicleMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tVSelect>[Configure Vehicle]</a>');

     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Welcome to the Vehicle Configuration Screen! This is the screen that will allow');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>you to configure your vehicles as you see fit. Click on List Vehicles to get started.');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, "<just:center>Walkers have been added to the Vehicles list.");
     %index++;

     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;

     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Keys for Vehicles<color:42dbea>');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Pack: Use Module');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Next/Previous Weapon: Switch between weapons');
     %index++;

//     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//     %index++;

//     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Saved Presets:');
//     %index++;

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function vehicleProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "VSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Vehicle');

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Below is a list of customizable vehicles.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Click a vehicle name to customize its weaponry and equipment.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Your custom vehicle can then be created at your team\'s vehicle station.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
             %index++;

             for(%i = 0; %i < $VehicleCount; %i++)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tModVehicle\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $VehicleListData[%i, "name"], $VehicleListData[%i, "desc"], %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "ModVehicle":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             %vData = $VehicleListData[%arg2, "block"];
             %dPwr = %vData.rechargeRate * 32;
             %dMass = %vData.mass;
             %dHP = %vData.maxDamage * 100;

             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Vehicle: %1 - PWR: %2 KW MASS: %3 KG HP: %4', $VehicleListData[%arg2, "name"], %dPwr, %dMass, %dHP);
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Weapon Hardpoints:');
             %index++;

             %hpc = $VehicleListData[%arg2, "hardpoints"] + 3;

             for(%i = 3; %i < %hpc; %i++)
             {
                 %o = %i - 3;
                 %vpart = %client.pendingVehicleMod[%i, %arg2];

                 if(%vpart $= "")
                 {
                    %vpart = $DefaultVehicleWep[%arg2, %o];
                    %client.pendingVehicleMod[%i, %arg2] = %vpart;
                 }

                 if($VehicleListData[%arg2, "class"] == $VehicleClass::Walker)
                 {
                    %client.vWepFireGroup[%arg2, %i] = %client.vWepFireGroup[%arg2, %i] $= "" ? %vpart.defaultFireGroup : %client.vWepFireGroup[%arg2, %i];
                    %firegroup[%o] = %client.vWepFireGroup[%arg2, %i];

                    for(%f = 0; %f < $VehicleMaxFireGroups; %f++)
                    {
                        %fgName = "<a:gamelink\tToggleFireGroup\t"@%arg2@"\t"@%i@"\t"@$VehicleFiregroupMask[%f]@">"@$VehicleFiregroupName[%f]@"</a>";
                        %inFiregroup = %firegroup[%o] & $VehicleFiregroupMask[%f] ? "<color:FFFFFF>"@%fgName@"<color:42DBEA>" : %fgName;
                        %firegroupText[%o] = %firegroupText[%o] $= "" ? %inFiregroup : %firegroupText[%o] SPC %inFiregroup;
                    }

                    messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tModHardpoint\t%4\t%5>%1: %2</a> - Firegroups: %6</clip>', $VehicleHardpoints[%arg2, %o, "name"], %vpart.name, %vpart.description, %arg2, %o, %firegroupText[%o]);
                    %index++;
                 }
                 else
                 {
                     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tModHardpoint\t%4\t%5>%1: %2</a></clip>', $VehicleHardpoints[%arg2, %o, "name"], %vpart.name, %vpart.description, %arg2, %o);
                     %index++;
                 }
             }

             if($VehicleListData[%arg2, "class"] == $VehicleClass::Walker)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '');
                 %index++;
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Enhancements:');
                 %index++;

                 %vehicle = $VehicleListData[%arg2, "block"];
                 %count = %vehicle.enhancementSlots;

                 if(%count $= "")
                    %count = 0;

//                 if(%client.pendingEnhancement[$EnhancementType::Vehicle, %vehicle, 0] $= "")
//                    %client.setDefaultEnhancements($EnhancementType::Vehicle, %vehicle);

                 for(%i = 0; %i < %count; %i++)
                 {
                     %enh = %client.pendingEnhancement[$EnhancementType::Vehicle, %vehicle, %i];

                     if(%enh $= "")
                        %enh = $DefaultVehicleEnh[%arg2, %i];

                     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tModEnhancement\t%4\t%5>Slot %1: %2 - %3</a></clip>', %i+1, %enh.name, %enh.description, %arg2, %i);
                     %index++;
                 }
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Module:');
             %index++;

             %selected = %client.pendingVehicleMod[$VehiclePartType::Module, %arg2];

             if(%selected $= "")
             {
                %selected = VehiclePart.partID[$VehiclePartType::Module, $DefaultVehiclePart[%arg2, $VehiclePartType::Module]];
                %client.pendingVehicleMod[$VehiclePartType::Module, %arg2] = %selected;
             }

             %count = VehiclePart.vehiclePartCount[$VehiclePartType::Module];

             for(%i = 0; %i < %count; %i++)
             {
                 %vpart = VehiclePart.vehicleParts[$VehiclePartType::Module, %i];

                 if($VehicleListData[%arg2, "mask"] & %vpart.wearableMask)
                 {
                    if(%i == %selected)
                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleMod\t%3\t%4>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %i);
                    else
                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleMod\t%3\t%4>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %i);

                    %index++;
                }
             }

             if($VehicleListData[%arg2, "class"] != $VehicleClass::Walker)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, "");
                 %index++;
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Shield:');
                 %index++;

                 %selected = %client.pendingVehicleMod[$VehiclePartType::Shield, %arg2];

                 if(%selected $= "")
                 {
                    %selected = VehiclePart.partID[$VehiclePartType::Shield, $DefaultVehiclePart[%arg2, $VehiclePartType::Shield]];
                    %client.pendingVehicleMod[$VehiclePartType::Shield, %arg2] = %selected;
                 }

                %count = VehiclePart.vehiclePartCount[$VehiclePartType::Shield];

                 for(%i = 0; %i < %count; %i++)
                 {
                     %vpart = VehiclePart.vehicleParts[$VehiclePartType::Shield, %i];

                     if($VehicleListData[%arg2, "mask"] & %vpart.wearableMask)
                     {
                        if(%i == %selected)
                            messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %i);
                        else
                            messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %i);

                        %index++;
                    }
                 }
              }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, 'Loadouts:');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;

             %ecount = $SCVehTotal[%client.guid, %vData];

             if(%ecount $= "")
                %ecount = 0;

             for(%i = 0; %i < %ecount; %i++)
             {
                 %Vehid = $SCVehLookup[%client.guid, %vData, %i];
                 %vname = $SCVehName[%Vehid] !$= "" ? $SCVehName[%Vehid] : "New Loadout";

                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tSetLoadoutV\t%3\t%4>%1: %2</a></clip><spop><rmargin:720><just:left>\t<clip:320><a:gamelink\tUpdateLoadoutV\t%3\t%4>[overwrite]</a> <a:gamelink\tSetNameV\t%3\t%4>[set name]</a></clip>', %i+1, %vname, %arg2, %Vehid);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, '');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tNewLoadoutV\t%1\t%2>Create current loadout as new loadout</a>', %arg2);
             %index++;

//             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tVehicleResetDefaults\t%2\t%3><color:FFFF00>Reset vehicle defaults for %1</a>', $VehicleListData[%arg2, "name"], %i, %arg2);
//             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tVSelect\t1><color:FF0000>Done modifying %1</a>', $VehicleListData[%arg2, "name"]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "NewLoadoutV":
             %vData = $VehicleListData[%arg2, "block"];
             
             if($SCVehTotal[%client.guid, %vData] < 0 || $SCVehTotal[%client.guid, %vData] $= "")
                $SCVehTotal[%client.guid, %vData] = 0;

             if($SCVehCount[%client.guid] < 0)
                $SCVehCount[%client.guid] = 0;

             if($SCVehTotal[%client.guid, %vData] > 9)
                return %game.processGameLink(%client, "ModVehicle", %arg2);

             // Dirty flag set, purge this data if player reconnects since data can't be erased from sparse array
             $SCVehDirty[%client.guid, %vData] = 1;

             %trueid = %client.guid @ $SCVehCount[%client.guid];
             $SCVehLookup[%client.guid, %vData, $SCVehTotal[%client.guid, %vData]] = %trueid;
             $SCVehData[%trueid, "item"] = %vData;
             $SCVehData[%trueid, "firemask"] = "";

             for(%i = 0; %i < 8; %i++)
             {
                if(%i < 3)
                    $SCVehData[%trueid, %i] = VehiclePart.vehicleParts[%i, %client.pendingVehicleMod[%i, $VehicleListID[%vData]]];
                else
                    $SCVehData[%trueid, %i] = %client.pendingVehicleMod[%i, $VehicleListID[%vData]];
                
                if($SCVehData[%trueid, %i] $= "")
                    $SCVehData[%trueid, %i] = "XEmptyHardpoint";
                
                $SCVehEnhData[%trueid, %i] = %client.pendingEnhancement[$EnhancementType::Vehicle, %vData, %i];
                
                %fm = %client.vWepFireGroup[$VehicleListID[%vData], %i];

                if(%fm $= "")
                    %fm = 0;

                $SCVehData[%trueid, "firemask"] = %i ? $SCVehData[%trueid, "firemask"] @ " " @ %fm : $SCVehData[%trueid, "firemask"] @ %fm;
             }

             $SCVehTotal[%client.guid, %vData]++;
             $SCVehCount[%client.guid]++;

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "UpdateLoadoutV":
             %vData = $VehicleListData[%arg2, "block"];

             $SCVehData[%arg3, "firemask"] = "";

             for(%i = 0; %i < 8; %i++)
             {
                if(%i < 3)
                    $SCVehData[%arg3, %i] = VehiclePart.vehicleParts[%i, %client.pendingVehicleMod[%i, $VehicleListID[%vData]]];
                else
                    $SCVehData[%arg3, %i] = %client.pendingVehicleMod[%i, $VehicleListID[%vData]];

                if($SCVehData[%arg3, %i] $= "")
                    $SCVehData[%arg3, %i] = "XEmptyHardpoint";

                $SCVehEnhData[%arg3, %i] = %client.pendingEnhancement[$EnhancementType::Vehicle, %vData, %i];

                %fm = %client.vWepFireGroup[$VehicleListID[%vData], %i];

                if(%fm $= "")
                    %fm = 0;

                $SCVehData[%arg3, "firemask"] = %i ? $SCVehData[%arg3, "firemask"] @ " " @ %fm : $SCVehData[%arg3, "firemask"] @ %fm;
             }

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "SetNameV":
             %client.vehSetName = %arg3;
             %client.screenNextArg = %arg2;
             %client.screenReference = %game;
             %client.screenNextWindow = "ModVehicle";
             bottomprint(%client, "Name will be set to the next chat typed in the chat box.", 5, 1);

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "SetLoadoutV":
             %vehicle = $VehicleListData[%arg2, "block"];

             %client.pendingVehicleName[$VehicleListID[%vehicle]] = $SCVehName[%arg3]; // todo: onSpawn, set vehicle target

             for(%i = 0; %i < 8; %i++)
             {
                if(%i < 3)
                    %client.pendingVehicleMod[%i, $VehicleListID[%vehicle]] = VehiclePart.partID[%i, $SCVehData[%arg3, %i]];
                else
                    %client.pendingVehicleMod[%i, $VehicleListID[%vehicle]] = $SCVehData[%arg3, %i];
                    
                %client.pendingEnhancement[$EnhancementType::Vehicle, %vehicle, %i] = $SCVehEnhData[%arg3, %i];
                %client.vWepFireGroup[$VehicleListID[%vehicle], %i] = getWord($SCVehData[%arg3, "firemask"], %i);
             }

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "ModHardpoint":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Hardpoint: %1', $VehicleHardpoints[%arg2, %arg3, "name"]);

             %vData = $VehicleListData[%arg2, "block"];
             %hpc = $VehicleListData[%arg2, "hardpoints"] + 3;
             %idx = %arg3 + 3;

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Select Weapon');
             %index++;

             %count = VehiclePart.vehiclePartCount[$VehiclePartType::Weapon];
//             %selected = %client.pendingVehicleMod[%idx, %arg2];

             for(%i = 0; %i < %count; %i++)
             {
                 %vpart = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %i];

                 if($VehicleListData[%arg2, "mask"] & %vpart.wearableMask && $VehicleHardpoints[%arg2, %arg3, "size"] == %vpart.mountSize && $VehicleHardpoints[%arg2, %arg3, "type"] & %vpart.mountType)
                 {
//                    if(%i == %selected)
//                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tWeaponMod\t%3\t%4\t%5>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %arg3, %i);
//                    else
                        messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tWeaponMod\t%3\t%4\t%5>%1: %2</a></clip>', %vpart.name, %vpart.description, %arg2, %arg3, %i);

                    %index++;
                }
             }
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tModVehicle\t%1>Back</a>', %arg2);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "ModEnhancement":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>%1 -> Slot %2', $VehicleListData[%arg2, "name"], %arg3+1);
             %vehicle = $VehicleListData[%arg2, "block"];

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Available Enhancements:');
             %index++;

             %enhCount = Enhancement.enhancementCount[$EnhancementType::Vehicle];
             %aEnhCount = %vehicle.enhancementSlots;

             for(%i = 0; %i < %enhCount; %i++)
             {
                %enh = Enhancement.enhancements[$EnhancementType::Vehicle, %i].getName();
                %wearable = %enh.wearableMask & $VehicleListData[%arg2, "mask"];

                if(%wearable)
                {
                    %stackable = %enh.prefFlags & $Enhancement::Stackable;

                    // If not stackable, prevent.. well, stacking
                    if(!%stackable)
                    {
                        for(%j = 0; %j < %aEnhCount; %j++)
                        {
                            if(%client.pendingEnhancement[$EnhancementType::Vehicle, %vehicle, %j] $= %enh)
                                $MHNotStackableBreak = true;
                        }

                        if($MHNotStackableBreak)
                        {
                            $MHNotStackableBreak = false;
                            continue;
                        }
                    }

                    messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tApplyWalkerMode\t%3\t%4\t%5\t%6>%1: %2</a></clip>', %enh.name, %enh.description, %enh, %vehicle, %arg3, %arg2);
                    %index++;
                }
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             return;

        case "ApplyWalkerMode":
             %client.pendingEnhancement[$EnhancementType::Vehicle, %arg3, %arg4] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg5);
             return;

        case "ToggleFireGroup":
             %client.vWepFireGroup[%arg2, %arg3] ^= %arg4;

             if(isObject(%client.walker) && %client.walker.vid == %arg2)
                 %client.walker.slotFireGroup[(%arg3 - 3)] = %client.vWepFireGroup[%arg2, %arg3];

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "WeaponMod":
             %part = VehiclePart.vehicleParts[$VehiclePartType::Weapon, %arg4];
             %client.pendingVehicleMod[(%arg3 + 3), %arg2] = %part;
             %client.vWepFireGroup[%arg2, (%arg3 + 3)] = %part.defaultFireGroup;

             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "VehicleMod":
             %client.pendingVehicleMod[$VehiclePartType::Module, %arg2] = %arg3;
             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "VehicleShield":
             %client.pendingVehicleMod[$VehiclePartType::Shield, %arg2] = %arg3;
             %game.processGameLink(%client, "ModVehicle", %arg2);
             return;

        case "VehicleResetDefaults":
             %vData = $VehicleData[%arg2];
             %client.vModPriWeapon[%vData] = "";
             %client.vModSecWeapon[%vData] = "";
             %client.vModule[%vData] = "";
             %client.vModShield[%vData] = "";
             %client.vModArmor[%vData] = "";
             %game.processGameLink(%client, "ModVehicle", %arg2);

        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function accountMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     messageClient(%client, 'SetScoreHudSubheader', "", ' <just:left>My Account: %1<just:right> ID #%2 ', %client.name, %client.guid);

     %accsync = %client.accountLoaded ? "Yes" : "No";
     %rankstatus = Net.isStatTracking ? "Yes" : "No";
     %rankstatusdesc = Net.isStatTracking ? "Points gained will be saved." : "Points gained will not be saved.";
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Current info:<color:42dbea>');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Rank: %1 - Next rank: %2 (%3) - Total Points: %4', $SCRanks[%client.currentRank, "name"], $SCRanks[(%client.currentRank + 1), "name"], $SCRanks[(%client.currentRank + 1), "points"], $SCAccountData[%client.guid, "totalpoints"]);
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Account sync: </clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>[Request account sync]</clip><just:right>', %accsync);
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Ranked server?</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200></clip><just:right>%2', %rankstatus, %rankstatusdesc);
     %index++;

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function accountProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "SSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select player for Infocard');

             for(%i = 0; %i < ClientGroup.getCount(); %i++)
             {
                 %tgt = ClientGroup.getObject(%i);

                 if(!%tgt.isAIControlled())
                 {
                      %rank = getTrackedStat(%tgt.guid, "rank");

                      if(%rank == 0 || %rank $= "")
                           %rank = 0;

                      messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush><clip:260><a:gamelink\tSInfocard\t%1>%2</a></clip><spop><rmargin:720><just:left>\t<clip:320>%3</clip>', %tgt, %tgt.name, $StatRankID[%rank]);
                      %index++;
                 }
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SServerSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:left>Infocard for %1<just:right><a:gamelink\tSSelect>[View Player Infocards]</a> <a:gamelink\tMain\t1>[Back]</a>', $Host::GameName);

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Server-wide Statistics');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Flag captures:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Objective captures:</clip><just:right>%2', getTrackedStat(0, "flagcaps"), getTrackedStat(0, "objectivecaps"));
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Inventory Stations used:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Maps played:</clip><just:right>%2', getTrackedStat(0, "inventoryusage"), getTrackedStat(0, "mapsplayed"));
             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Server-wide Information');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Max Players:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Players online:</clip><just:right>%2', $Host::MaxPlayers, ClientGroup.getCount() - $HostGameBotCount);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Anti-baserape count:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Map time limit (minutes):</clip><just:right>%2', $Host::MD2::AntiBaseRapeCount, $Host::TimeLimit);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Repulsor Tick rate:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Wormhole Tick rate:</clip><just:right>%2', $Host::RepulsorTickRate, $Host::WormholeTickRate);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Respawn time (seconds):</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Suicide respawn (seconds):</clip><just:right>%2', $Host::MD2::RespawnTime, $Host::MD2::SuicideRespawnTime);
             %index++;

             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SInfocard":
           renderInfoCard(%arg2, %tag, %client);
           return;

        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}
