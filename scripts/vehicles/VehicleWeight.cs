// Vehicle weight system
// Define weight images here

datablock ShapeBaseImageData(MasterWeight)
{
   shapeFile = "turret_muzzlepoint.dts";
   item = WeightItem;
   mountPoint = 0;
   mass = 0;
};

datablock ShapeBaseImageData(VWeightP0) : MasterWeight
{
   mass = 0;
};

datablock ShapeBaseImageData(VWeightP5) : MasterWeight
{
   mass = 5;
};

datablock ShapeBaseImageData(VWeightP10) : MasterWeight
{
   mass = 10;
};

datablock ShapeBaseImageData(VWeightP15) : MasterWeight
{
   mass = 15;
};

datablock ShapeBaseImageData(VWeightP20) : MasterWeight
{
   mass = 20;
};

datablock ShapeBaseImageData(VWeightP25) : MasterWeight
{
   mass = 25;
};

datablock ShapeBaseImageData(VWeightP30) : MasterWeight
{
   mass = 30;
};

datablock ShapeBaseImageData(VWeightP35) : MasterWeight
{
   mass = 35;
};

datablock ShapeBaseImageData(VWeightP40) : MasterWeight
{
   mass = 40;
};

datablock ShapeBaseImageData(VWeightP45) : MasterWeight
{
   mass = 45;
};

datablock ShapeBaseImageData(VWeightP50) : MasterWeight
{
   mass = 50;
};

datablock ShapeBaseImageData(VWeightP55) : MasterWeight
{
   mass = 55;
};

datablock ShapeBaseImageData(VWeightP60) : MasterWeight
{
   mass = 60;
};

datablock ShapeBaseImageData(VWeightP65) : MasterWeight
{
   mass = 65;
};

datablock ShapeBaseImageData(VWeightP70) : MasterWeight
{
   mass = 70;
};

datablock ShapeBaseImageData(VWeightP75) : MasterWeight
{
   mass = 75;
};

datablock ShapeBaseImageData(VWeightP80) : MasterWeight
{
   mass = 80;
};

datablock ShapeBaseImageData(VWeightP85) : MasterWeight
{
   mass = 85;
};

datablock ShapeBaseImageData(VWeightP90) : MasterWeight
{
   mass = 90;
};

datablock ShapeBaseImageData(VWeightP95) : MasterWeight
{
   mass = 95;
};

datablock ShapeBaseImageData(VWeightP100) : MasterWeight
{
   mass = 100;
};

datablock ShapeBaseImageData(VWeightP105) : MasterWeight
{
   mass = 105;
};

datablock ShapeBaseImageData(VWeightP110) : MasterWeight
{
   mass = 110;
};

datablock ShapeBaseImageData(VWeightP115) : MasterWeight
{
   mass = 115;
};

datablock ShapeBaseImageData(VWeightP120) : MasterWeight
{
   mass = 120;
};

datablock ShapeBaseImageData(VWeightP125) : MasterWeight
{
   mass = 125;
};

datablock ShapeBaseImageData(VWeightP130) : MasterWeight
{
   mass = 130;
};

datablock ShapeBaseImageData(VWeightP135) : MasterWeight
{
   mass = 135;
};

datablock ShapeBaseImageData(VWeightP140) : MasterWeight
{
   mass = 140;
};

datablock ShapeBaseImageData(VWeightP145) : MasterWeight
{
   mass = 145;
};

datablock ShapeBaseImageData(VWeightP150) : MasterWeight
{
   mass = 150;
};

datablock ShapeBaseImageData(VWeightM5) : MasterWeight
{
   mass = -5;
};

datablock ShapeBaseImageData(VWeightM10) : MasterWeight
{
   mass = -10;
};

datablock ShapeBaseImageData(VWeightM15) : MasterWeight
{
   mass = -15;
};

datablock ShapeBaseImageData(VWeightM20) : MasterWeight
{
   mass = -20;
};

datablock ShapeBaseImageData(VWeightM25) : MasterWeight
{
   mass = -25;
};

datablock ShapeBaseImageData(VWeightM30) : MasterWeight
{
   mass = -30;
};

datablock ShapeBaseImageData(VWeightM35) : MasterWeight
{
   mass = -35;
};

datablock ShapeBaseImageData(VWeightM40) : MasterWeight
{
   mass = -40;
};

datablock ShapeBaseImageData(VWeightM45) : MasterWeight
{
   mass = -45;
};

datablock ShapeBaseImageData(VWeightM50) : MasterWeight
{
   mass = -50;
};

datablock ShapeBaseImageData(VWeightM55) : MasterWeight
{
   mass = -55;
};

datablock ShapeBaseImageData(VWeightM60) : MasterWeight
{
   mass = -60;
};

datablock ShapeBaseImageData(VWeightM65) : MasterWeight
{
   mass = -65;
};

datablock ShapeBaseImageData(VWeightM70) : MasterWeight
{
   mass = -70;
};

datablock ShapeBaseImageData(VWeightM75) : MasterWeight
{
   mass = -75;
};

datablock ShapeBaseImageData(VWeightM80) : MasterWeight
{
   mass = -80;
};

datablock ShapeBaseImageData(VWeightM85) : MasterWeight
{
   mass = -85;
};

datablock ShapeBaseImageData(VWeightM90) : MasterWeight
{
   mass = -90;
};

datablock ShapeBaseImageData(VWeightM95) : MasterWeight
{
   mass = -95;
};

datablock ShapeBaseImageData(VWeightM100) : MasterWeight
{
   mass = -100;
};

// Functions
function getWeightName(%data, %vehicle, %weight)
{
    %sign = %weight < 0 ? "M" : "P";
    %quantWeight = mRoundToNearest(mAbs(%weight), 5); // clamp to 5s
    %name = "VWeight" @ %sign @ %quantWeight;

    return %name;
}
