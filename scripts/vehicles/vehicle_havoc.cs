//**************************************************************
// HAVOC HEAVY TRANSPORT FLIER
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(HAPCFlyerEngineSound)
{
   filename    = "fx/vehicles/htransport_thrust.wav";
   description = AudioDefaultLooping3d;
};

datablock AudioProfile(HAPCFlyerThrustSound)
{
   filename    = "fx/vehicles/htransport_boost.wav";
   description = AudioDefaultLooping3d;
};

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************
datablock FlyingVehicleData(HAPCFlyer) : HavocDamageProfile
{
   spawnOffset = "0 0 6";
   renderWhenDestroyed = false;

   catagory = "Vehicles";
   shapeFile = "vehicle_air_hapc.dts";
   multipassenger = true;
   computeCRC = true;


   debrisShapeName = "vehicle_air_hapc_debris.dts";
   debris = ShapeDebris;

   drag = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
//   mountPose[1] = sitting;
   numMountPoints = 6;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[3] = true;
   isProtectedMountPoint[4] = true;
   isProtectedMountPoint[5] = true;

   cameraMaxDist = 17;
   cameraOffset = 2;
   cameraLag = 8.5;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 0.5;
	explosionRadius = 5.0;

   maxDamage = 7.5;
   destroyedLevel = 7.5;

   isShielded = false;
   rechargeRate = 0.9375; // z0dd - ZOD, 4/16/02. Was 0.8
   energyPerDamagePoint = 150; // z0dd - ZOD, 4/16/02. Was 200
   maxEnergy = 700; // z0dd - ZOD, 4/16/02. Was 550
   minDrag = 100;                // Linear Drag
   rotationalDrag = 2700;        // Anguler Drag

   // Auto stabilize speed
   maxAutoSpeed = 10;
   autoAngularForce = 3000;      // Angular stabilizer force
   autoLinearForce = 450;        // Linear stabilzer force
   autoInputDamping = 0.95;      // 
                                                        
   // Maneuvering
   maxSteeringAngle = 8;
   horizontalSurfaceForce = 10;  // Horizontal center "wing"
   verticalSurfaceForce = 10;    // Vertical center "wing"
   maneuveringForce = 6000;      // Horizontal jets // z0dd - ZOD, 4/25/02. Was 6000
   steeringForce = 1000;          // Steering jets
   steeringRollForce = 400;      // Steering jets
   rollForce = 12;               // Auto-roll
   hoverHeight = 8;         // Height off the ground at rest
   createHoverHeight = 6;   // Height off the ground when created
   maxForwardSpeed = 52.7;  // speed in which forward thrust force is no longer applied (meters/second) z0dd - ZOD, 4/25/02. Was 71

   // Turbo Jet
   jetForce = 4000; // z0dd - ZOD, 4/25/02. Was 5000
   minJetEnergy = 55;
   jetEnergyDrain = 75 / 32; // z0dd - ZOD, 4/16/02. Was 3.6
   vertThrustMultiple = 0.0; // havoc: 3.0


   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.6;
   damageLevelTolerance[1] = 0.8;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 688;
   bodyFriction = 0;
   bodyRestitution = 0.3;
   minRollSpeed = 0;
   softImpactSpeed = 12;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 15;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 25;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.060;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 28;
   collDamageMultiplier   = 0.020;

   //
   minTrailSpeed = 10;
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = HAPCFlyerThrustSound;
   engineSound = HAPCFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 5.0; 
   mediumSplashSoundVelocity = 8.0;   
   hardSplashSoundVelocity = 12.0;   
   exitSplashSoundVelocity = 8.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
   
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingHAPCIcon;
   cmdMiniIconName = "commander/MiniIcons/com_hapc_grey";
   targetNameTag = 'Imperator';
   targetTypeTag = 'Sky Fortress';

   sensorData = AWACPulseSensor;
   sensorRadius = AWACPulseSensor.detectRadius;
   sensorColor = "255 194 9";

   checkRadius = 7.8115;
   observeParameters = "1 15 15";

   stuckTimerTicks = 32;   // If the hapc spends more than 1 sec in contact with something
   stuckTimerAngle = 80;   //  with a > 80 deg. pitch, BOOM!

   shieldEffectScale = "1.0 0.9375 0.45";
   
   checkMinVelocity = 16;
   checkMinHeight = 20;
   checkMinReverseSpeed = 6;
   
   shieldGenerator = "HavocShield";
};

datablock ShapeBaseImageData(HavocPilotSection) : EngineAPEImage
{
   mountPoint = 7;
   offset = "0 0 1";
   shapeFile = "vehicle_air_scout.dts";
};

datablock StaticShapeData(ImperatorSensor)
{
   shapeFile = "sensor_pulse_medium.dts";

   dynamicType = $TypeMasks::VehicleObjectType;

   maxDamage = HAPCFlyer.maxDamage;
   destroyedLevel = HAPCFlyer.destroyedLevel;

   targetNameTag = 'Imperator';
   targetTypeTag = 'Sky Fortress';
};

function ImperatorSensor::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function ImperatorSensor::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function ImperatorSensor::doDismount(%this)
{
    // prevent console spam for some wacky reason
}

datablock TurretImageData(ImperatorTurretParam)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 750;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 100;
   degPerSecTheta    = 600;
   degPerSecPhi      = 600;

   attackRadius      = 75;
};

function HAPCFlyer::hasDismountOverrides(%data, %obj)
{
   return true;
}

function HAPCFlyer::getDismountOverride(%data, %obj, %mounted)
{
   %node = -1;
   for (%i = 0; %i < %data.numMountPoints; %i++)
   {
      if (%obj.getMountNodeObject(%i) == %mounted)
      {
         %node = %i;
         break;
      }
   }
   if (%node == -1)
   {
      warning("Couldn't find object mount point");
      return "0 0 1";
   }

   if (%node == 3 || %node == 2)
   {
      return "-1 0 1";
   }
   else if (%node == 5 || %node == 4)
   {
      return "1 0 1";
   }
   else
   {
      return "0 0 1";
   }
}

//**************************************************************
// WEAPONS
//**************************************************************

datablock TurretData(ImperatorTurret) : VTurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_base_large.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = HAPCFlyer.maxDamage;
   destroyedLevel          = HAPCFlyer.destroyedLevel;

   thetaMin                = 0;
   thetaMax                = 165;

   // capacitor
   maxCapacitorEnergy      = 300;
   capacitorRechargeRate   = 1.171875;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
//   numWeapons              = 2;

   targetNameTag           = 'Imperator';
   targetTypeTag           = 'Turret';
};

function ImperatorTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
         
   %obj.lastDamageVal = %newDamageVal;
}

function ImperatorTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function ImperatorTurret::onTrigger(%data, %obj, %trigger, %state)
{
   switch (%trigger) {
      case 0:
         %obj.fireTrigger = %state;
         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else
         {
            %obj.setImageTrigger(2, false);
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function ImperatorTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(0, false);
//   %obj.setImageTrigger(4, false);
   %client = %obj.getControllingClient();
// %client.setControlObject(%client.player);
   %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   %client.player.mountVehicle = false;
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
//   %client.player.getDataBlock().doDismount(%client.player);
}

function ImperatorTurret::doDismount()
{

}

function ImperatorTurret::selectTarget(%this, %turret)
{
    %veh = %turret.getObjectMount();
    
    if(%veh.gunshipBeacon > 0)
        %turret.setTargetObject(%veh.gunshipBeacon);
    else
        %turret.clearTarget();
}

function HAPCFlyer::triggerMine(%data, %obj, %player, %slot, %state)
{
    if(!%state)
        return;
        
    if(%obj.gunshipBeacon > 0)
    {
        %obj.gunshipBeacon.delete();
        %obj.gunshipBeacon = 0;
//        %obj.turretR.setImageAmmo(0, true);
//        %obj.turretL.setImageAmmo(0, true);
        %obj.turretR.setAutoFire(false);
        %obj.turretL.setAutoFire(false);
        %imgL = %obj.turretL.getMountedImage(2);
        %imgR = %obj.turretR.getMountedImage(2);
        %obj.turretR.unmountImage(2);
        %obj.turretR.mountImage(%imgR, 0);
        %obj.turretL.unmountImage(2);
        %obj.turretL.mountImage(%imgL, 0);
        return;
    }

    %obj.gunshipBeacon = new WayPoint()
    {
        position = "0 0 10000";
        rotation = "1 0 0 0";
        scale = "1 1 1";
        name = "";
        dataBlock = "WayPointMarker";
        lockCount = "0";
        homingCount = "0";
        team = %obj.team;
    };

    MissionCleanup.add(%obj.gunshipBeacon);
    %imgL = %obj.turretL.getMountedImage(0);
    %imgR = %obj.turretR.getMountedImage(0);
    %obj.turretR.unmountImage(0);
    %obj.turretR.mountImage(AssaultTurretParam, 0); // ImperatorTurretParam
    %obj.turretR.mountImage(%imgR, 2);
    %obj.turretL.unmountImage(0);
    %obj.turretL.mountImage(AssaultTurretParam, 0);
    %obj.turretL.mountImage(%imgL, 2);
    %obj.turretR.setAutoFire(true);
    %obj.turretL.setAutoFire(true);
//    %obj.turretR.setTargetObject(%obj.gunshipBeacon);
//    %obj.turretL.setTargetObject(%obj.gunshipBeacon);
//    %obj.turretR.setImageTrigger(0, false);
//    %obj.turretL.setImageTrigger(0, false);
    %obj.turretR.setImageAmmo(2, true);
    %obj.turretL.setImageAmmo(2, true);
    %data.trackBeaconPos(%obj, %player);
}

function HAPCFlyer::trackBeaconPos(%data, %obj, %pilot)
{
    if(%obj.gunshipBeacon > 0)
    {
        %muzzlePoint = %obj.getMuzzlePoint(0);
        %muzzleVector = %pilot.getEyeVector();
        %point = castRay(%muzzlePoint, %muzzleVector, 300, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);

        if(%point)
            %obj.gunshipBeacon.setPosition(%point.hitPos);
        else
            %obj.gunshipBeacon.setPosition(vectorProject(%muzzlePoint, %muzzleVector, 300));
            
//        %obj.turretR.setTargetObject(%obj.gunshipBeacon);
//        %obj.turretL.setTargetObject(%obj.gunshipBeacon);
    }
    
    %data.schedule(32, "trackBeaconPos", %obj, %pilot);
}

function HAPCFlyer::onTrigger(%data, %obj, %trigger, %state)
{
//        %obj.turretR.setAutoFire(false);
//        %obj.turretL.setAutoFire(false);
    if(%trigger == 0)
    {
//        %obj.turretR.setImageAmmo(0, %state);
//        %obj.turretL.setImageAmmo(0, %state);
        %obj.turretR.setImageTrigger(2, %state);
        %obj.turretL.setImageTrigger(2, %state);
    }
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
}

function HAPCFlyer::playerDismounted(%data, %obj, %player)
{
    if(!isObject(%obj.getMountNodeObject(0)) && %obj.gunshipBeacon > 0)
    {
        %obj.gunshipBeacon.delete();
        %obj.gunshipBeacon = 0;
        
        %obj.turretR.setAutoFire(false);
        %obj.turretL.setAutoFire(false);
        %imgL = %obj.turretL.getMountedImage(2);
        %imgR = %obj.turretR.getMountedImage(2);
        %obj.turretR.unmountImage(2);
        %obj.turretR.mountImage(%imgR, 0);
        %obj.turretL.unmountImage(2);
        %obj.turretL.mountImage(%imgL, 0);
        
//        %obj.turretR.setImageTrigger(0, false);
//        %obj.turretL.setImageTrigger(0, false);
//        %obj.turretR.setImageAmmo(0, true);
//        %obj.turretL.setImageAmmo(0, true);
    }

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}
