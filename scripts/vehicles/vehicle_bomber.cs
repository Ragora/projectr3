//**************************************************************
// THUNDERSWORD BOMBER
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************
datablock AudioProfile(BomberFlyerEngineSound)
{
   filename    = "fx/vehicles/bomber_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberFlyerThrustSound)
{
   filename    = "fx/vehicles/bomber_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(FusionExpSound)
// Sound played when mortar impacts on target
{
   filename    = "fx/powered/turret_mortar_explode.wav";
   description = "AudioBIGExplosion3d";
   preload = true;
};

datablock AudioProfile(BomberTurretFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(BomberTurretActivateSound)
{
   filename    = "fx/vehicles/bomber_turret_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretReloadSound)
{
   filename    = "fx/vehicles/bomber_turret_reload.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretIdleSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(BomberTurretDryFireSound)
{
   filename    = "fx/vehicles/bomber_turret_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};
   
datablock AudioProfile(BomberBombReloadSound)
{
   filename    = "fx/vehicles/bomber_bomb_reload.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombProjectileSound)
{
   filename    = "fx/vehicles/bomber_bomb_projectile.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberBombDryFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_reload.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombIdleSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = ClosestLooping3d;
   preload = true;
};

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock FlyingVehicleData(BomberFlyer) : BomberDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_air_bomber.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_air_bomber_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 3;  
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;
   
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 0.5;
	explosionRadius = 5.0;

   maxDamage = 5;
   destroyedLevel = 5;

   isShielded = false;
   energyPerDamagePoint = 150;
   maxEnergy = 360;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.78125;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds
   
   // Maneuvering
   maxSteeringAngle = 5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 8;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 4725;      // Horizontal jets (W,S,D,A key thrust) // z0dd - ZOD, 9/8/02. Was 4700
   steeringForce = 1100;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 120;  // speed in which forward thrust force is no longer applied (meters/second) // z0dd - ZOD, 9/8/02. Was 85

   // Turbo Jet
   jetForce = 3100;      // Afterburner thrust (this is in addition to normal thrust) // z0dd - ZOD, 9/8/02. Was 3000
   minJetEnergy = 25.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 56 / 32;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.5; // z0dd - ZOD, 9/8/02. Was 3.0

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 350;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 20;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 20;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.060;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 25;
   collDamageMultiplier   = 0.020;

   //
   minTrailSpeed = 15;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = BomberFlyerThrustSound;
   engineSound = BomberFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0; 
   mediumSplashSoundVelocity = 20.0;   
   hardSplashSoundVelocity = 30.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
  
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Firestorm';
   targetTypeTag = 'VTOL Escort';
   sensorData = VehiclePulseSensor;
   sensorRadius = VehiclePulseSensor.detectRadius; // z0dd - ZOD, 4/25/02. Allows sensor to be shown on CC

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   shieldEffectScale = "0.75 0.975 0.375";
   showPilotInfo = 1;
   
   shieldGenerator = "BomberShield";
};

datablock ShapeBaseImageData(FirestormPilotTurret) : EngineAPEImage
{
   shapeFile = "turret_belly_base.dts";
   offset = "0.0 1.675 -1.5";
   rotation = "0 0 1 0"; //degreesToRotation("0 -45 180"); //"0 1 0 -45"; // 90

   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

datablock StaticShapeData(FirestormFrame)
{
   shapeFile = "vehicle_air_bomber.dts";

   dynamicType = $TypeMasks::VehicleObjectType;

   maxDamage = BomberFlyer.maxDamage;
   destroyedLevel = BomberFlyer.destroyedLevel;

   targetNameTag = 'Frame';
   targetTypeTag = 'ShouldntSeeMe';
};

//**************************************************************
// WEAPONS
//**************************************************************

datablock TracerProjectileData(BomberFusionBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.35;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::BellyTurret;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;
   sound               = ShrikeBlasterProjectile;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BellyTurret;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 35;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   dryVelocity       = 675.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 370;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/blueSpark";//"special/tracer00";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.8;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

//-------------------------------------
// BOMBER BELLY TURRET CHARACTERISTICS
//-------------------------------------

datablock TurretData(BomberTurret) : VTurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = BomberFlyer.maxDamage;
   destroyedLevel          = BomberFlyer.destroyedLevel;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 0.8;
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 3;

   targetNameTag           = 'Thundersword Belly';
   targetTypeTag           = 'Turret';
};

datablock TurretImageData(BomberTurretBarrel)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;

   projectile                       = BomberFusionBolt;
   projectileType                   = TracerProjectile;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 16.0;
   minEnergy                        = 16.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;
   
   attackRadius                     = 75;

   // State transitions
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "WaitFire1";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = BomberTurretActivateSound;

   stateName[1]                     = "WaitFire1";
   stateTransitionOnTriggerDown[1]  = "Fire1";
   stateTransitionOnNoAmmo[1]       = "NoAmmo1";

   stateName[2]                     = "Fire1";
   stateTransitionOnTimeout[2]      = "Reload1";
   stateTimeoutValue[2]             = 0.13;
   stateFire[2]                     = true;
   stateRecoil[2]                   = LightRecoil;
   stateAllowImageChange[2]         = false;
   stateSequence[2]                 = "Fire";
   stateScript[2]                   = "onFire";
   stateSound[2]                    = BomberTurretFireSound;

   stateName[3]                     = "Reload1";
   stateSequence[3]                 = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateAllowImageChange[3]         = false;
   stateTransitionOnTimeout[3]      = "WaitFire2";
   stateTransitionOnNoAmmo[3]       = "NoAmmo1";

   stateName[4]                     = "NoAmmo1";
   stateTransitionOnAmmo[4]         = "Reload1";
   stateSequence[4]                 = "NoAmmo1"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo
   stateTransitionOnTriggerDown[4]  = "DryFire1";

   stateName[5]                     = "DryFire1";
   stateSound[5]                    = BomberTurretDryFireSound;
   stateTimeoutValue[5]             = 0.5;
   stateTransitionOnTimeout[5]      = "NoAmmo1";
   
   stateName[6]                     = "WaitFire2";
   stateTransitionOnTriggerDown[6]  = "Fire2";
   stateTransitionOnNoAmmo[6]       = "NoAmmo2"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo

   stateName[7]                     = "Fire2";
   stateTransitionOnTimeout[7]      = "Reload2";
   stateTimeoutValue[7]             = 0.13;
   stateScript[7]                   = "FirePair";

   stateName[8]                     = "Reload2";
   stateSequence[8]                 = "Reload";
   stateTimeoutValue[8]             = 0.1;
   stateAllowImageChange[8]         = false;
   stateTransitionOnTimeout[8]      = "WaitFire1";
   stateTransitionOnNoAmmo[8]       = "NoAmmo2";

   stateName[9]                     = "NoAmmo2";
   stateTransitionOnAmmo[9]         = "Reload2";
   stateSequence[9]                 = "NoAmmo2"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo
   stateTransitionOnTriggerDown[9]  = "DryFire2";

   stateName[10]                     = "DryFire2";
   stateSound[10]                    = BomberTurretDryFireSound;
   stateTimeoutValue[10]             = 0.5;
   stateTransitionOnTimeout[10]      = "NoAmmo2";
                                              
};

datablock TurretImageData(BomberTurretBarrelPair) 
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;
   
   projectile                       = BomberFusionBolt;
   projectileType                   = TracerProjectile;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 16.0;
   minEnergy                        = 16.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;
   
   attackRadius                     = 75;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 0.13;
   stateFire[1]                     = true;
   stateRecoil[1]                   = LightRecoil;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
   stateSound[1]                    = BomberTurretFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
};

datablock TurretImageData(AIAimingTurretBarrel) 
{
   shapeFile            = "turret_muzzlepoint.dts";
   mountPoint           = 3;

   projectile           = BomberFusionBolt;

   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 1500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 500;
   degPerSecPhi         = 800;
   
   attackRadius         = 75;
};

//-------------------------------------
// BOMBER BOMB PROJECTILE
//-------------------------------------

datablock BombProjectileData(BomberBomb)
{
   projectileShapeName  = "bomb.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 2.0;
   damageRadius         = 20;
   radiusDamageType     = $DamageType::BomberBombs;
   kickBackStrength     = 3500;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::BomberBombs;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 300;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion            = "VehicleBombExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 0.1;
   drag                 = 0.3;
   gravityMod		= 20.0 / mabs($Classic::gravSetting); // z0dd - ZOD, 8/28/02. Compensate for our grav change. Math: base grav / our grav

   minRotSpeed          = "60.0 0.0 0.0";
   maxRotSpeed          = "80.0 0.0 0.0";
   scale                = "1.0 1.0 1.0";
   
   sound                = BomberBombProjectileSound;
};

//-------------------------------------
// BOMBER BOMB CHARACTERISTICS
//-------------------------------------

datablock ItemData(BombAmmo)
{
   className         = Ammo;
   catagory          = "Ammo";
   shapeFile         = "repair_kit.dts";
   mass              = 1;
   elasticity        = 0.2;
   friction          = 0.6;
   pickupRadius      = 1;
   computeCRC        = true;
};

datablock StaticShapeData(DropBombs)
{
   catagory             = "Turrets";
   shapeFile            = "bombers_eye.dts";
   maxDamage            = 1.0;
   disabledLevel        = 0.6;
   destroyedLevel       = 0.8;
};

datablock TurretImageData(BomberBombImage)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "2 -4 -0.5";
   mountPoint                       = 10;

   projectile                       = BomberBomb;
   projectileType                   = BombProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VMBombAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;
   
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "WaitFire1";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";

   stateName[1]                     = "WaitFire1";
   stateTransitionOnTriggerDown[1]  = "Fire1";
   stateTransitionOnNoAmmo[1]       = "NoAmmo1";

   stateName[2]                     = "Fire1";
   stateTransitionOnTimeout[2]      = "Reload1";
   stateTimeoutValue[2]             = 0.5;
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateSequence[2]                 = "Fire";
   stateScript[2]                   = "onFire";
   stateSound[2]                    = BomberBombFireSound;

   stateName[3]                     = "Reload1";
   stateSequence[3]                 = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateAllowImageChange[3]         = false;
   stateTransitionOnTimeout[3]      = "WaitFire2";
   stateTransitionOnNoAmmo[3]       = "NoAmmo1";

   stateName[4]                     = "NoAmmo1";
   stateTransitionOnAmmo[4]         = "Reload1";
   stateSequence[4]                 = "NoAmmo1"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo
   stateTransitionOnTriggerDown[4]  = "DryFire1";

   stateName[5]                     = "DryFire1";
   stateSound[5]                    = BomberBombDryFireSound;
   stateTimeoutValue[5]             = 0.5;
   stateTransitionOnTimeout[5]      = "NoAmmo1";
   
   stateName[6]                     = "WaitFire2";
   stateTransitionOnTriggerDown[6]  = "Fire2";
   stateTransitionOnNoAmmo[6]       = "NoAmmo2"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo

   stateName[7]                     = "Fire2";
   stateTransitionOnTimeout[7]      = "Reload2";
   stateTimeoutValue[7]             = 0.5;
   stateScript[7]                   = "FirePair";

   stateName[8]                     = "Reload2";
   stateSequence[8]                 = "Reload";
   stateTimeoutValue[8]             = 0.1;
   stateAllowImageChange[8]         = false;
   stateTransitionOnTimeout[8]      = "WaitFire1";
   stateTransitionOnNoAmmo[8]       = "NoAmmo2";

   stateName[9]                     = "NoAmmo2";
   stateTransitionOnAmmo[9]         = "Reload2";
   stateSequence[9]                 = "NoAmmo2"; // z0dd - ZOD, 5/12/02. Was wrong: NoAmmo
   stateTransitionOnTriggerDown[9]  = "DryFire2";

   stateName[10]                     = "DryFire2";
   stateSound[10]                    = BomberBombDryFireSound;
   stateTimeoutValue[10]             = 0.5;
   stateTransitionOnTimeout[10]      = "NoAmmo2";
};

datablock TurretImageData(BomberBombPairImage)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;

   projectile                       = BomberBomb;
   projectileType                   = BombProjectile;

   sharedResourcePool               = true;
   updatePilotAmmo                  = true;
   ammo                             = "VMBombAmmo";
   usesEnergy                       = false;
   useMountEnergy                   = false;
   useCapacitor                     = false;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 0.5;
   stateFire[1]                     = true;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
   stateSound[1]                    = BomberBombFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
                         
};

//**************************************************************
// WEAPONS SPECIAL EFFECTS
//**************************************************************

//-------------------------------------
// BOMBER BELLY TURRET GUN (explosion)
//-------------------------------------

datablock ParticleData(FusionExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]            = "0.56 0.36 0.26 1.0";
   colors[1]            = "0.56 0.36 0.26 0.0";
   sizes[0]             = 1;
   sizes[1]             = 2;
};

datablock ParticleEmitterData(FusionExplosionEmitter)
{
   ejectionPeriodMS     = 7;
   periodVarianceMS     = 0;
   ejectionVelocity     = 12;
   velocityVariance     = 1.75;
   ejectionOffset       = 0.0;
   thetaMin             = 0;
   thetaMax             = 60;
   phiReferenceVel      = 0;
   phiVariance          = 360;
   overrideAdvances     = false;
   particles            = "FusionExplosionParticle";
};

datablock ExplosionData(FusionBoltExplosion)
{
   explosionShape       = "effect_plasma_explosion.dts";
   soundProfile         = FusionExpSound;
   particleEmitter      = FusionExplosionEmitter;
   particleDensity      = 250;
   particleRadius       = 1.25;
   faceViewer           = true;
};
                                    
//--------------------------------------------------------------------------
// BOMBER TARGETING LASER
//--------------------------------------------------------------------------

datablock AudioProfile(BomberTargetingSwitchSound)
{
   filename    = "fx/weapons/generic_switch.wav";
   description = AudioClosest3d;
   preload     = true;
};

datablock AudioProfile(BomberTargetingPaintSound)
{
   filename    = "fx/weapons/targetinglaser_paint.wav";
   description = CloseLooping3d;
   preload     = true;
};

//--------------------------------------
// BOMBER TARGETING PROJECTILE
//--------------------------------------
datablock TargetProjectileData(BomberTargeter)
{
   directDamage         = 0.0;
   hasDamageRadius      = false;
   indirectDamage       = 0.0;
   damageRadius         = 0.0;
   velInheritFactor     = 1.0;

   maxRifleRange        = 1000;
   beamColor            = "0.1 1.0 0.1";
                        
   startBeamWidth       = 0.20;
   pulseBeamWidth       = 0.15;
   beamFlareAngle       = 3.0;
   minFlareSize         = 0.0;
   maxFlareSize         = 400.0;
   pulseSpeed           = 6.0;
   pulseLength          = 0.150;

   textureName[0]       = "special/nonlingradient";
   textureName[1]       = "special/flare";
   textureName[2]       = "special/pulse";
   textureName[3]      	= "special/expFlare";
};

//-------------------------------------
// BOMBER TARGETING CHARACTERISTICS
//-------------------------------------
datablock ShapeBaseImageData(BomberTargetingImage)
{
   className                           = WeaponImage;

   shapeFile                           = "turret_muzzlepoint.dts";
   offset                              = "0 -0.04 -0.01";
   mountPoint                          = 2;

   projectile                          = BomberTargeter;
   projectileType                      = TargetProjectile;
   deleteLastProjectile                = true;

   usesEnergy                          = true;
   minEnergy                           = 3;

   stateName[0]                        = "Activate";
   stateSequence[0]                    = "Activate";
   stateSound[0]                       = BomberTargetingSwitchSound;
   stateTimeoutValue[0]                = 0.5;
   stateTransitionOnTimeout[0]         = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateTransitionOnAmmo[1]            = "Ready";
   stateTransitionOnNoAmmo[1]          = "NoAmmo";

   stateName[2]                        = "Ready";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateEnergyDrain[3]                 = 3;
   stateFire[3]                        = true;
   stateAllowImageChange[3]            = false;
   stateScript[3]                      = "onFire";
   stateTransitionOnTriggerUp[3]       = "Deconstruction";
   stateTransitionOnNoAmmo[3]          = "Deconstruction";
   stateSound[3]                       = BomberTargetingPaintSound;

   stateName[4]                        = "NoAmmo";
   stateTransitionOnAmmo[4]            = "Ready";

   stateName[5]                        = "Deconstruction";
   stateTransitionOnTimeout[5]         = "ActivateReady";
   stateTimeoutValue[5]                = 0.05;
};

function BomberTargetingImage::onFire(%data,%obj,%slot)
{
   %bomber = %obj.getObjectMount();
   if(%bomber.beacon)
   {
      %bomber.beacon.delete();
      %bomber.beacon = "";
   }   
   %p = Parent::onFire(%data, %obj, %slot);
   %p.setTarget(%obj.team);
}

function BomberTargetingImage::deconstruct(%data, %obj, %slot)
{
   %pos = %obj.lastProjectile.getTargetPoint();
   %bomber = %obj.getObjectMount();
   
   if(%bomber.beacon)
   {
      %bomber.beacon.delete();
      %bomber.beacon = "";
   }
   %bomber.beacon = new BeaconObject() {
      dataBlock = "BomberBeacon";
      beaconType = "vehicle";
      position = %pos;
   };

   %bomber.beacon.playThread($AmbientThread, "ambient");
   %bomber.beacon.team = %bomber.team;
   %bomber.beacon.sourceObject = %bomber;

   // give it a team target
   %bomber.beacon.setTarget(%bomber.team);                  
   MissionCleanup.add(%bomber.beacon);
   
   Parent::deconstruct(%data, %obj, %slot);
}

//-------------------------------------
// BOMBER BEACON
//-------------------------------------
datablock StaticShapeData(BomberBeacon)
{
   shapeFile = "turret_muzzlepoint.dts";
   targetNameTag = 'beacon';
   isInvincible = true;
   
   dynamicType = $TypeMasks::SensorObjectType;
};

