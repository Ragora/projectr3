datablock ShapeBaseImageData(MBeamLaserFIR1)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_shocklance.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   minEnergy = 40;
   fireEnergy = 40;
   
   projectile = VMBeamLaser;
   projectileType = SniperProjectile;
   emap = true;

   isLaser = true;
   laserOpacity = 0.85;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "activate";
   stateSound[4]            = SniperRifleFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 2.0;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MBeamLaserFIR1T) : MBeamLaserFIR1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;

   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = true;

   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "0 0 0";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBeamLaserGUA1) : MBeamLaserFIR1
{
   mountPoint = 1;
   
   usesEnergy = true;
   useMountEnergy = false;
   useCapacitor = false;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBeamLaserSKY1) : MBeamLaserFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBeamLaserSKY2) : MBeamLaserFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MBeamLaserOUT1) : MBeamLaserFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   minEnergy = 10;
   fireEnergy = 10;

   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBeamLaserOUT2) : MBeamLaserFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   minEnergy = 10;
   fireEnergy = 10;

   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

function MBeamLaser::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "SniperRifleFireSound";
    %this.projectileType = "SniperProjectile";
    %this.projectile = "VMBeamLaser";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 2000;
    %this.heatPerShot = 25;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
    
    // Laser
    %this.isLaser = true;
    %this.laserOpacity = 0.85;
}

function MBeamLaser::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Laser", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MBeamLaserSKY1, 2);
            %vehicle.mountImage(MBeamLaserSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MBeamLaserOUT1, 2);
            %vehicle.mountImage(MBeamLaserOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MBeamLaserGUA1, 0);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MBeamLaserFIR1, 0);
            else
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MBeamLaserFIR1T, 2 * %hardpoint);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBeamLaser", "Scorcher Beam Laser", "50kj beam laser mount", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Energy);
