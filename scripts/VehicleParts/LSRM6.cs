datablock ShapeBaseImageData(LSRM6IMP)
{
   className = WeaponImage;
   shapeFile = "stackable2m.dts";

   usesEnergy = false;
   sharedResourcePool = true;
   ammo = "VLSRM6Ammo";
   updatePilotAmmo = true;
   minEnergy = 0;
   fireEnergy = 0;
   fireTimeout = 750;
   staggerCount = 5;
   staggerDelay = 150;
   
   projectile = VSRMMissile;
   projectileType = SeekerProjectile;
   useForwardVector = true;
   
   isSeeker     = true;
   seekRadius   = 250;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 15;
   
   offset = "0 0 0";
   rotation = degreesToRotation("90 0 0");
   
   mountPoint = 0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = MBLFireSound;
   stateTimeoutValue[4]          = 6.0;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(LSRM6FIR2T) : LSRM6IMP
{
   offset = "0 1.0 0.1";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(LSRM6GUA1) : LSRM6IMP
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "1 0 0 90";
};

function LSRM6::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLSRM6Ammo";
    %this.refImage = LSRM6IMP;
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MILFireSound";
    %this.projectileType = "SeekerProjectile";
    %this.projectile = "VSRMMissile";
    %this.projectileSpread = 6;
    %this.fireEnergy = 0;
    %this.ammoUse = 6;
    %this.ammoAmount = 60;
    %this.fireTimeout = 6000;
    %this.heatPerShot = 48;
    %this.useForwardVector = true;
    %this.defaultFireGroup = $VehicleFiregroup::Gamma;
    %this.isSeeker = true;

    // Fires multiple shots
    %this.staggerCount = 5;
    %this.staggerDelay = 150;
}

function LSRM6::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "ClusterMissile", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LSRM6GUA1, 4);
            
        case $VehicleID::Firestorm:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LSRM6FIR2T, 4);
            
        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LSRM6IMP, 0);

        case $VehicleID::Imperator:
            %vehicle.mountImage(LSRM6IMP, 0);
    }
    
//    %vehicle.setInventory(%this.ammo, %data.max[%this.ammo]);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LSRM6", "SRM-6 Pod", "Short-range IR tracking missile pod, 4 count", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Missile);
