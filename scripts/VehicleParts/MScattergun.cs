datablock ShapeBaseImageData(MScattergunFIR1)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   ammo = "VMScattergunAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   projectile = ScattergunBolt;
   projectileType = TracerProjectile;
   emap = true;

   projectileSpread = 7.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0
   staggerCount = 4;
   staggerDelay = 0;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Recoil";
   stateSound[4]            = GrenadeFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.8;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.5;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MScattergunGUA1) : MScattergunFIR1
{
//   shapeFile = "turret_tank_barrelchain.dts";
   mountPoint = 1;
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
   pilotHeadTracking = true;
};

datablock ShapeBaseImageData(MScattergunSKY1) : MScattergunFIR1
{
   offset = "1 1.5 0.6";
   rotation = "0 1 0 0";
   pilotHeadTracking = false;
};

datablock ShapeBaseImageData(MScattergunSKY2) : MScattergunFIR1
{
   offset = "-1.0 1.5 0.8";
   rotation = "0 1 0 -180";
   pilotHeadTracking = false;
   
   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MScattergunOUT1) : MScattergunFIR1
{
   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
   pilotHeadTracking = false;
};

datablock ShapeBaseImageData(MScattergunOUT2) : MScattergunFIR1
{
   offset = "-0.775 6.75 -1.3";
   rotation = "0 1 0 -180";
   pilotHeadTracking = false;

   stateSound[4]                    = "";
   stateTimeoutValue[4]          = 0.1;
};

datablock ShapeBaseImageData(MScattergunFIR1T) : MScattergunFIR1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 0";
   pilotHeadTracking = false;
};

function MScattergun::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VMScattergunAmmo";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "GrenadeFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "ScattergunBolt";
    %this.projectileSpread = 7;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 60;
    %this.fireTimeout = 800;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
    
    // Fires multiple shots
    %this.staggerCount = 4;
    %this.staggerDelay = 0;
}

function MScattergun::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Plasma", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MScattergunSKY1, 2);
            %vehicle.mountImage(MScattergunSKY2, 3);

        case $VehicleID::Guardian:
            %vehicle.mountImage(MScattergunGUA1, 0);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MScattergunOUT1, 2);
            %vehicle.mountImage(MScattergunOUT2, 3);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MScattergunFIR1, 0);
            else
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MScattergunFIR1T, 2 * %hardpoint);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MScattergun", "Scattergun Mount", "High-powered lead spread, pelt their face off!", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Ballistic);
