function MOAblativeArmor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function MOAblativeArmor::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.bonusWeight += %data.mass * 0.33;
    %vehicle.maxHitPoints = mCeil(%vehicle.maxHitPoints * 1.33);
    %vehicle.hitPoints = mCeil(%vehicle.hitPoints * 1.33);
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOAblativeArmor", "Ablative Hull Plating", "Increases maximum HP and weight by 33%", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
