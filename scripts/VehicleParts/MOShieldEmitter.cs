function MOShieldEmitter::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Passive;
}

function MOShieldEmitter::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    
    %this.schedule(1000, "startShield", %data, %vehicle);
}

function MOShieldEmitter::startShield(%this, %data, %vehicle)
{
    %vehicle.isShielded = true;
    %vehicle.playShieldEffect("0 0 1");
    %vehicle.maxShieldStrength = 500;
    %vehicle.shieldRechargeRate = 100 / 32;
    %vehicle.shieldRechargeDelay = 15000;
    
    %data.walkerStartShield(%vehicle);
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOShieldEmitter", "Shield Emitter Array", "Strength: 500 HP, Recharge delay: 15 sec", $VehicleList::Stormguard, $VHardpointSize::Internal, $VHardpointType::Omni);
