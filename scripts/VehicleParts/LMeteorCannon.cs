datablock LinearFlareProjectileData(VMeteorCannonBlast) : MeteorCannonBlast
{
   damageRadius        = 6.0;
   kickBackStrength    = 3000;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MeteorCannon;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 250;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;
};

function VMeteorCannonBlast::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount % 8 == 0)
        createRemoteProjectile("LinearFlareProjectile", "MeteorTrailCharge", %proj.initialDirection, %proj.position, 0, %proj.instigator);
}

datablock TurretImageData(LMeteorCannonSTD)
{
   shapeFile = "turret_tank_barrelmortar.dts";
   mountPoint = 0;

   projectile = VMeteorCannonBlast;
   projectileType = LinearFlareProjectile;

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 75.0;
   minEnergy = 75.0;
   useCapacitor = true;
   sharedResourcePool = true;
   
   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 1.0;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = AAFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 1.25;
   stateAllowImageChange[4]            = false;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

datablock TurretImageData(LMeteorCannonIMP) : LMeteorCannonSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

function LMeteorCannonSTD::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);
     
     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "BluePowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

function LMeteorCannonIMP::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "BluePowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);
}

function LMeteorCannon::spawnProjectile(%this, %walker, %slot)
{
     %p = Parent::spawnProjectile(%this, %walker, %slot);

     if(%p)
        createRemoteProjectile("LinearFlareProjectile", "BluePowerDisplayCharge", %p.initialDirection, %p.position, 0, %p.instigator);

    return %p;
}

function LMeteorCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.refImage = LMeteorCannonSTD;

    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "AAFireSound";
    %this.projectileType = "LinearFlareProjectile";
    %this.projectile = "MeteorCannonBlast";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 2250;
    %this.heatPerShot = 50;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LMeteorCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "MainCannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LMeteorCannonSTD, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LMeteorCannonSTD, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LMeteorCannonIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LMeteorCannonIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LMeteorCannonIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LMeteorCannon", "Meteor Cannon", "Main gun switched to Meteor Cannon", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Energy);
