datablock TurretImageData(LMortarGUA1) : MortarBarrelLarge
{
//   shapeFile = "turret_tank_barrelmortar.dts";
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 180";

   usesEnergy = false;
   useCapacitor = false;
   useMountEnergy = false;
   sharedResourcePool = true;
   updatePilotAmmo = true;
   ammo = "VLMortarAmmo";
};

datablock TurretImageData(LMortarIMP) : LMortarGUA1
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   useCapacitor = false;
   useMountEnergy = false;
};

datablock TurretImageData(LMortarFIR2T) : LMortarGUA1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0.1 -1.5 -0.265";
   rotation = "0 1 0 180";

   useCapacitor = false;
   useMountEnergy = false;
};

function LMortar::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "VLMortarAmmo";
    %this.refImage = MortarBarrelLarge;
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "MBLFireSound";
    %this.projectileType = "GrenadeProjectile";
    %this.projectile = "MortarTurretShot";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 60;
    %this.fireTimeout = 3500;
    %this.heatPerShot = 5;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LMortar::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Cannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LMortarGUA1, 4);

        case $VehicleID::Firestorm:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]).mountImage(LMortarFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LMortarIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LMortarIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "MortarBarrelLarge";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LMortar", "Mortar Turret Mount", "Turret-level Mortar Turret mount", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Ballistic);

