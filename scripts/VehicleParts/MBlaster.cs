datablock TracerProjectileData(MBlasterAABolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.3;
   kickBackStrength    = 0;
   directDamageType    = $DamageType::AATurret;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;
   sound               = ShrikeBlasterProjectile;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::AATurret;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 30;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   dryVelocity       = 675.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 500;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 370;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/tracer00";//"special/";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.2;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

//datablock AudioProfile(MBlasterFireSound)
//{
//   filename    = "fx/vehicles/bomber_turret_fire.wav";
//   description = AudioDefaultLooping3d;
//   preload = true;
//};

datablock ShapeBaseImageData(MBlasterFIR1)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   projectile = MBlasterAABolt;
   projectileType = TracerProjectile;
   emap = true;

   projectileSpread = 1.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Deploy";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = BomberTurretFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.3;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MBlasterFIR1T) : MBlasterFIR1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;

   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = true;
   pilotHeadTracking = false;
   
   offset = "0 0 0";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBlasterGUA1) : MBlasterFIR1
{
   mountPoint = 1;
   
   usesEnergy = true;
   useMountEnergy = false;
   useCapacitor = false;
   pilotHeadTracking = true;

   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBlasterSKY1) : MBlasterFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   stateTimeoutValue[4]          = 0.3;
   
   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBlasterSKY2) : MBlasterFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   stateTimeoutValue[4]          = 0.3;
   
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MBlasterOUT1) : MBlasterFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   stateTimeoutValue[4]          = 0.3;

   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MBlasterOUT2) : MBlasterFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   stateTimeoutValue[4]          = 0.3;

   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

function MBlaster::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "BomberTurretFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "MBlasterAABolt";
    %this.projectileSpread = 1;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 300;
    %this.heatPerShot = 1;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function MBlaster::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Energy", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MBlasterSKY1, 2);
            %vehicle.mountImage(MBlasterSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MBlasterOUT1, 2);
            %vehicle.mountImage(MBlasterOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MBlasterGUA1, 0);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MBlasterFIR1, 0);
            else
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MBlasterFIR1T, 2 * %hardpoint);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBlaster", "Heavy Blaster Mount", "Higher damage version of the Blaster", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Energy);
