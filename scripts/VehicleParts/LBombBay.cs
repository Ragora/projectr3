function LBombBay::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLBombAmmo";
    %this.ammoAmount = 70;
}

function LBombBay::installPart(%this, %data, %vehicle, %player)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(BomberBombImage, 4);
            %turret.mountImage(BomberBombPairImage, 5);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 3, "mountPoint"]);
            %turret.mountImage(BomberBombImage, 4);
            %turret.mountImage(BomberBombPairImage, 5);
    }
    
    %vehicle.isBombing = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LBombBay", "'Stomper' 300kt Bombs", "These are gonna hurt when they land", $VehicleList::Firestorm, $VHardpointSize::Large, $VHardpointType::Bombs); // Imperator comes later
