datablock TracerProjectileData(ScattergunBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.15;
   directDamageType    = $DamageType::Bullet;
   explosion           = "ACChaingunExplosion";
   splash              = ChaingunSplash;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Scattergun;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[0]   = 15;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   kickBackStrength  = 0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 500.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 400;
   lifetimeMS        = 400;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 400;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.20;
   crossSize       = 0.35;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

datablock TurretImageData(LScattergunSTD)
{
   shapeFile = "weapon_mortar.dts";
   mountPoint = 0;

//   ammo = StarHammerAmmo;
   projectile = ScattergunBolt;
   projectileType = TracerProjectile;
   updatePilotAmmo = true;
   sharedResourcePool = true;
   ammo = "VLScattergunAmmo";

   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   projectileSpread = 8.0;
   
   staggerCount = 4;
   staggerDelay = 0;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = true;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Recoil";
   stateSound[4]            = GrenadeFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 1.0;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.25;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock TurretImageData(LScattergunIMP) : LScattergunSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

datablock TurretImageData(LScattergunFIR2T) : LScattergunSTD
{
   shapeFile = "turret_muzzlepoint.dts";
   
   useMountEnergy = false;
   useCapacitor = false;
};

function LScattergun::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLScattergunAmmo";
    %this.refImage = LScattergunSTD;
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "GrenadeFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "ScattergunBolt";
    %this.projectileSpread = 8;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 50;
    %this.fireTimeout = 1000;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
    
    // Fires multiple shots
    %this.staggerCount = 5;
    %this.staggerDelay = 0;
}

function LScattergun::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Plasma", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LScattergunSTD, 4);

        case $VehicleID::Firestorm:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(LScattergunFIR2T, 2 * %hardpoint);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LScattergunIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LScattergunIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LScattergunIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LScattergun", "Clustergun Mount", "High-powered lead spread, pelt their face off!", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Ballistic);
