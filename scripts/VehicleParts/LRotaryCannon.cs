datablock TracerProjectileData(LRotaryBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.3;
   directDamageType    = $DamageType::RotaryCannon;
   hasDamageRadius     = false;
   kickBackStrength    = 1000.0;
   sound          	   = BlasterProjectileSound;
   explosion           = SpikeExplosion;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::RotaryCannon;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 20;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 10;
   mdDamageRadius[1]   = false;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   dryVelocity       = 425.0;
   wetVelocity       = 225.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 900;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = 0;

   tracerLength    = 15;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0.647 0.0 1";
	tracerTex[0]  	 = "special/crescent3";
	tracerTex[1]  	 = "special/expFlare";
	tracerWidth     = 1.2;
   crossSize       = 0.8;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;

   decalData[0] = SpikeHitDecal;
};

datablock TurretImageData(LRotaryCannonSTD)
{
   shapeFile = "TR2weapon_chaingun.dts";
   mountPoint = 0;

//   ammo = StarHammerAmmo;
   projectile = LRotaryBolt;
   projectileType = TracerProjectile;
   updatePilotAmmo = true;
   sharedResourcePool = true;
   ammo = "VLRotaryCannonAmmo";

   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   projectileSpread = 2.0;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.3;
   stateWaitForTimeout[3]        = true;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = GrenadeFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.333;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.5;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock TurretImageData(LRotaryCannonIMP) : LRotaryCannonSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

datablock TurretImageData(LRotaryCannonFIR2T) : LRotaryCannonSTD
{
   shapeFile = "turret_muzzlepoint.dts";
   
   useMountEnergy = false;
   useCapacitor = false;
};

function LRotaryCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VLRotaryCannonAmmo";
    %this.refImage = LRotaryCannonSTD;
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "GrenadeFireSound";
    %this.projectileType = "TracerProjectile";
    %this.projectile = "LRotaryBolt";
    %this.projectileSpread = 2;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 300;
    %this.fireTimeout = 333;
    %this.heatPerShot = 0;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LRotaryCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "AC", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LRotaryCannonSTD, 4);

        case $VehicleID::Firestorm:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(LRotaryCannonFIR2T, 2 * %hardpoint);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LRotaryCannonIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LRotaryCannonIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LRotaryCannonIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LRotaryCannon", "M327 Rotary Cannon", "Heavy caliber gattling weapon", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Ballistic);
