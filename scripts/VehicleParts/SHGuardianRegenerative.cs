function SHGuardianRegenerative::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHGuardianRegenerative::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 405, 6000, (81 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHGuardianRegenerative", "Regenerative Shield", "Strength: 405 HP, Recharge delay: 6 sec, Power drain: 60%", $VehicleList::Guardian, $VHardpointSize::Internal, $VHardpointType::Omni);
