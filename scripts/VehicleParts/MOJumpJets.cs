function MOJumpJets::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Passive;
}

function MOJumpJets::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %block = %vehicle.shortName @ "WalkerJet";

    %vehicle.setDatablock(%block);
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOJumpJets", "Jump Jets", "Equips thrusters to the feet of the walker.", $VehicleList::Fury, $VHardpointSize::Internal, $VHardpointType::Omni);
