function SHFirestormStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHFirestormStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 540, 12000, (108 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHFirestormStandard", "Standard Shield", "Strength: 540 HP, Recharge delay: 12 sec, Power drain: 60%", $VehicleList::Firestorm, $VHardpointSize::Internal, $VHardpointType::Omni);
