function SHOutlawLight::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHOutlawLight::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 230, 8000, (46 / $g_TickTime), 0.4, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHOutlawLight", "Light Shield", "Strength: 230 HP, Recharge delay: 8 sec, Power drain: 40%", $VehicleList::Outlaw, $VHardpointSize::Internal, $VHardpointType::Omni);
