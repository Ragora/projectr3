function SHImperatorStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHImperatorStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 800, 12000, (160 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHImperatorStandard", "Standard Shield", "Strength: 800 HP, Recharge delay: 12 sec, Power drain: 60%", $VehicleList::Imperator, $VHardpointSize::Internal, $VHardpointType::Omni);
