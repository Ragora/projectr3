function MOTurboEngine::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Passive;
}

function MOTurboEngine::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %block = %vehicle.shortName @ "WalkerSpeed";

    %vehicle.setDatablock(%block);
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOTurboEngine", "Engine Upgrade", "Increases movement speed by 50%", $VehicleList::Walkers, $VHardpointSize::Internal, $VHardpointType::Omni);
