datablock TurretImageData(LLaserCannonSTD)
{
   shapeFile = "weapon_missile.dts";
   mountPoint = 0;
   rotation = "0 1 0 90";

   projectile = VLBeamLaser;
   projectileType = SniperProjectile;

   isLaser = true;
   laserOpacity = 0.85;

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 100.0;
   minEnergy = 100.0;
   useCapacitor = true;
   sharedResourcePool = true;
   
   // Turret parameters
   activationMS                        = 1000;
   deactivateDelayMS                   = 1500;
   thinkTimeMS                         = 200;
   degPerSecTheta                      = 500;
   degPerSecPhi                        = 500;
   attackRadius                        = 75;

   // State transitions
   stateName[0]                        = "Activate";
   stateTransitionOnNotLoaded[0]       = "Dead";
   stateTransitionOnLoaded[0]          = "ActivateReady";

   stateName[1]                        = "ActivateReady";
   stateSequence[1]                    = "Activate";
   stateSound[1]                       = AssaultTurretActivateSound;
   stateTimeoutValue[1]                = 1.0;
   stateTransitionOnTimeout[1]         = "Ready";
   stateTransitionOnNotLoaded[1]       = "Deactivate";

   stateName[2]                        = "Ready";
   stateTransitionOnNotLoaded[2]       = "Deactivate";
   stateTransitionOnNoAmmo[2]          = "NoAmmo";
   stateTransitionOnTriggerDown[2]     = "Fire";

   stateName[3]                        = "Fire";
   stateSequence[3]                    = "Fire";
   stateTransitionOnTimeout[3]         = "Reload";
   stateTimeoutValue[3]                = 1.0;
   stateFire[3]                        = true;
   stateRecoil[3]                      = LightRecoil;
   stateAllowImageChange[3]            = false;
   stateSound[3]                       = SniperRifleFireSound;
   stateScript[3]                      = "onFire";

   stateName[4]                        = "Reload";
   stateSequence[4]                    = "Reload";
   stateTimeoutValue[4]                = 1.0;
   stateAllowImageChange[4]            = false;
   stateTransitionOnTimeout[4]         = "Ready";
   //stateTransitionOnNoAmmo[4]          = "NoAmmo";
   stateWaitForTimeout[4]              = true;

   stateName[5]                        = "Deactivate";
   stateDirection[5]                   = false;
   stateSequence[5]                    = "Activate";
   stateTimeoutValue[5]                = 1.0;
   stateTransitionOnLoaded[5]          = "ActivateReady";
   stateTransitionOnTimeout[5]         = "Dead";

   stateName[6]                        = "Dead";
   stateTransitionOnLoaded[6]          = "ActivateReady";
   stateTransitionOnTriggerDown[6]     = "DryFire";

   stateName[7]                        = "DryFire";
   stateSound[7]                       = AssaultMortarDryFireSound;
   stateTimeoutValue[7]                = 1.0;
   stateTransitionOnTimeout[7]         = "NoAmmo";

   stateName[8]                        = "NoAmmo";
   stateSequence[8]                    = "NoAmmo";
   stateTransitionOnAmmo[8]            = "Reload";
   stateTransitionOnTriggerDown[8]     = "DryFire";
};

datablock TurretImageData(LLaserCannonIMP) : LLaserCannonSTD
{
   useMountEnergy = false;
   useCapacitor = false;
};

function LLaserCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.refImage = LLaserCannonSTD;

    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "SniperRifleFireSound";
    %this.projectileType = "SniperProjectile";
    %this.projectile = "VLBeamLaser";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 3000;
    %this.heatPerShot = 50;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
    
    // Laser
    %this.isLaser = true;
    %this.laserOpacity = 0.85;
}

function LLaserCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Laser", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LLaserCannonSTD, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(LLaserCannonSTD, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LLaserCannonIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LLaserCannonIMP, 0);
            
        case $VehicleID::MPB:
            %vehicle.barrel = "LLaserCannonIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LLaserCannon", "Laser Cannon", "100kj laser cannon, let's melt face!", $VehicleList::All, $VHardpointSize::Large, $VHardpointType::Energy);
