function MOAmmoCache::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function MOAmmoCache::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.bonusWeight += %data.mass * 0.15;
    %vehicle.maxAmmoCapacityFactor += 1.0;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOAmmoCache", "Ammo Cache", "Doubles ammo capacity, increases weight slightly", $VehicleList::All, $VHardpointSize::Internal, $VHardpointType::Omni);
