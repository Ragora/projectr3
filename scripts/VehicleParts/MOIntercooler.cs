function MOIntercooler::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Passive;
}

function MOIntercooler::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.heatDrain += 3;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOIntercooler", "Intercooler", "Dissipates an additional 6k/s heat.", $VehicleList::Walkers, $VHardpointSize::Internal, $VHardpointType::Omni);
