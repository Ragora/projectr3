datablock ShapeBaseImageData(SHailstorm3SKY1)
{
   className = WeaponImage;
   shapeFile = "stackable2m.dts";

   usesEnergy = false;
   ammo = "VSHailstorm3Ammo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   minEnergy = 0;
   fireEnergy = 0;
   fireTimeout = 750;
   staggerCount = 2;
   staggerDelay = 200;
   
   projectile = HailstormRocket;
   projectileType = LinearProjectile;
   useForwardVector = true;
   
   offset = "4.25 -1 -1.75";
   rotation = degreesToRotation("90 0 0");
   
   mountPoint = 10;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = MBLFireSound;
   stateTimeoutValue[4]          = 4.0;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(SHailstorm3SHA1) : SHailstorm3SKY1
{
   offset = "0 1.0 0.1";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(SHailstorm3GUA1) : SHailstorm3SKY1
{
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "1 0 0 90";
};

function SHailstorm3::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VSHailstorm3Ammo";
    %this.ammoUse = 1;
    %this.ammoAmount = 60;
}

function SHailstorm3::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Shadow:
            %vehicle.mountImage(SHailstorm3SHA1, 0);

        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(SHailstorm3GUA1, 2);
    }
    
//    %vehicle.setInventory(%this.ammo, %data.max[%this.ammo]);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "SHailstorm3", "Hailstorm Rocket 3-Pak", "3-burst dumbfire rocket launcher", $VehicleList::General, $VHardpointSize::Small, $VHardpointType::Missile);
