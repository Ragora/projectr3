function MOFusionGen::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
}

function MOFusionGen::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
    %vehicle.rechargeTick += 10 / 32;
    %vehicle.bonusWeight += 100;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOFusionGen", "Secondary Fusion Generator", "Increases power regen by 10KW, adds +100 KG weight", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
