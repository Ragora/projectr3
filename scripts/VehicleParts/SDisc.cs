datablock LinearProjectileData(VDiscProjectile) : DiscProjectile
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamage        = 0.25;
   directDamageType    = $DamageType::Disc;
   hasDamageRadius     = true;
   indirectDamage      = 0.50;
   damageRadius        = 7.5;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 2000;  // z0dd - ZOD, 4/24/02. Was 1750

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Disc;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 75;
   mdDamageRadius[0]   = true;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 25;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::PlayerFragment | $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound 				= discProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 180; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 90
   wetVelocity       = 55; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 50
   velInheritFactor  = 0.75; // z0dd - ZOD, 3/30/02. was 0.5
   fizzleTimeMS      = 4000;
   lifetimeMS        = 4000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 20.0; // z0dd - ZOD, 4/24/02. Was 0.0. 20 degrees skips off water
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock ShapeBaseImageData(SDiscSHA1)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_disc.dts";
   mountPoint = "10";
   offset = "0 1 -0.15";
   rotation = "0 1 0 0";

   ammo = "VSDiscAmmo";
   updatePilotAmmo = true;
   sharedResourcePool = true;
   
   projectile = VDiscProjectile;
   projectileType = LinearProjectile;
   emap = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.75;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(SDiscGUA1) : SDiscSHA1
{
   mountPoint = 1;
   offset = "0 -0.3 -0.15";
   rotation = "0 1 0 0";
};

function SDisc::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VSDiscAmmo";
    %this.ammoUse = 1;
    %this.ammoAmount = 50;
}

function SDisc::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Shadow:
            %vehicle.mountImage(SDiscSHA1, 0);

        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(SDiscGUA1, 2);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "SDisc", "Disc Launcher Mount", "A standard Disc Launcher mounted to the vehicle", $VehicleList::General, $VHardpointSize::Small, $VHardpointType::Ballistic);
