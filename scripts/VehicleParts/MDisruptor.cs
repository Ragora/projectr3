datablock LinearFlareProjectileData(VMPDisruptorBolt) : MPDisruptorBolt
{
   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Blaster;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 120;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound | $Projectile::CountMAs | $Projectile::CanHeadshot;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 750;
   lifetimeMS        = 1000;
   fizzleUnderwaterMS        = 1000;

   numFlares         = 25;
   size              = 0.7;
};

//datablock AudioProfile(MDisruptorFireSound)
//{
//   filename    = "fx/vehicles/bomber_turret_fire.wav";
//   description = AudioDefaultLooping3d;
//   preload = true;
//};

datablock ShapeBaseImageData(MDisruptorFIR1)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   mountPoint = "10";
   offset = "-0.1 4.25 -0.8";
   rotation = "1 0 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   sharedResourcePool = true;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 40;
   
   projectile = VMPDisruptorBolt;
   projectileType = LinearFlareProjectile;
   emap = true;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Deploy";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AAFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 1.5;
   stateTransitionOnTimeout[4]   = "Spinup";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MDisruptorFIR1T) : MDisruptorFIR1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;

   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = true;

   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "0 0 0";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorGUA1) : MDisruptorFIR1
{
   mountPoint = 1;
   
   usesEnergy = true;
   useMountEnergy = false;
   useCapacitor = false;
   pilotHeadTracking = true;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   offset = "-0.25 4 1.4";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorSKY1) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.5;
   
   offset = "1 1.5 0.65";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorSKY2) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;
   
   minEnergy = 10;
   fireEnergy = 10;
   
   stateTimeoutValue[4]          = 1.5;
   
   offset = "-1 1.5 0.65";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

datablock ShapeBaseImageData(MDisruptorOUT1) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.5;

   offset = "0.6 6.75 -1.5";
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MDisruptorOUT2) : MDisruptorFIR1
{
   usesEnergy = true;
   useMountEnergy = true;
   useCapacitor = false;
   pilotHeadTracking = false;

   minEnergy = 10;
   fireEnergy = 10;

   stateTimeoutValue[4]          = 1.5;

   offset = "-0.775 6.75 -1.5";
   rotation = "0 1 0 0";

   stateSound[4]                    = "";
};

function MDisruptor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "";
    
    // Walker prefs
    %this.dryFireSound = "ChaingunDryFireSound";
    %this.fireSound = "AAFireSound";
    %this.projectileType = "LinearFlareProjectile";
    %this.projectile = "VMPDisruptorBolt";
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 0;
    %this.ammoAmount = 0;
    %this.fireTimeout = 1000;
    %this.heatPerShot = 15;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha;
    %this.isSeeker = false;
}

function MDisruptor::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Energy", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MDisruptorSKY1, 2);
            %vehicle.mountImage(MDisruptorSKY2, 3);

        case $VehicleID::Outlaw:
            %vehicle.mountImage(MDisruptorOUT1, 2);
            %vehicle.mountImage(MDisruptorOUT2, 3);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MDisruptorGUA1, 0);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MDisruptorFIR1, 0);
            else
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MDisruptorFIR1T, 2 * %hardpoint);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MDisruptor", "MegaPulse Disrutor Mount", "Significantly more powerful version of the handheld Disruptor", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Energy);
