function SHOSolveilTMF::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHOSolveilTMF::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.setHeat(0.0);
    
    %data.installShield(%vehicle, 0, 0, 0, 0.5, "VehicleShieldEmitter");
    %this.tickShield(%vehicle);
}

function SHOSolveilTMF::tickShield(%this, %vehicle)
{
    if(!isObject(%vehicle))
        return;
        
    %val = %vehicle.getDamagePct() < 0.25 ? 1.0 : 0.0;
    %vehicle.setHeat(%val);

    %this.schedule(500, "tickShield", %vehicle);
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHOSolveilTMF", "Thermal Masking Field", "Prevents missile locks if armor > 25%, Power drain: 50%", $VehicleList::Skycutter | $VehicleList::Outlaw | $VehicleList::Firestorm, $VHardpointSize::Internal, $VHardpointType::Omni);
