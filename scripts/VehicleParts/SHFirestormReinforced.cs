function SHFirestormReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHFirestormReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 755, 14000, (150 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHFirestormReinforced", "Reinforced Shield", "Strength: 755 HP, Recharge delay: 14 sec, Power drain: 80%", $VehicleList::Firestorm, $VHardpointSize::Internal, $VHardpointType::Omni);
