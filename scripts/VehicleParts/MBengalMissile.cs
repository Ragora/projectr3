datablock ShapeBaseImageData(MBengalSKY4) : EngineAPEImage
{
   mountPoint = 10;

   shapeFile = "ammo_missile.dts";

   offset = "4.25 -2 -1.75";
   rotation = "0 1 0 -125";
};

datablock ShapeBaseImageData(MBengalSKY5) : MBengalSKY4
{
   offset = "-4.25 -2 -1.75";
   rotation = "0 1 0 125";
};

datablock ShapeBaseImageData(MBengalSKY6) : MBengalSKY4
{
   offset = "4.25 -1.75 3";
   rotation = "0 1 0 -55";
};

datablock ShapeBaseImageData(MBengalSKY7) : MBengalSKY4
{
   offset = "-4.25 -1.75 3";
   rotation = "0 1 0 55";
};

datablock BombProjectileData(BengalMissileDeploy)
{
   projectileShapeName  = "ammo_missile.dts";
   emitterDelay         = -1;
   directDamage         = 0.0;
   hasDamageRadius      = true;
   indirectDamage       = 1.5;
   damageRadius         = 10;
   radiusDamageType     = $DamageType::Missile;
   kickBackStrength     = 2250;  // z0dd - ZOD, 4/25/02. Was 2500

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion            = "LargeMissileExplosion";
   velInheritFactor     = 1.0;

   grenadeElasticity    = 0.25;
   grenadeFriction      = 0.4;
   armingDelayMS        = 2000;
   muzzleVelocity       = 15.0;
   drag                 = 0.3;
   gravityMod		    = 1.0;

   minRotSpeed          = "0.0 45.0 0.0";
   maxRotSpeed          = "0.0 45.0 0.0";

   scale                = "1.0 1.0 1.0";

   sound                = "";
};

datablock LinearProjectileData(BengalMissileDumbfire)
{
   scale = "1.5 1.5 1.5";
   projectileShapeName = "ammo_missile.dts";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 8;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "LargeMissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 350;
   wetVelocity       = 200;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 6000;
   lifetimeMS        = 6000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 6000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";
};

datablock SeekerProjectileData(BengalMissile) : ShoulderMissile
{
   scale = "5.0 5.0 5.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "ammo_missile.dts";
   directDamage        = 0.5;
   directDamageType    = $DamageType::Missile;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 8;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3200;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 225;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "LargeMissileExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = WildcatJetEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 7000;
   muzzleVelocity      = 200.0;
   maxVelocity         = 350.0;
   turningSpeed        = 240.0;
   acceleration        = 100.0;

   proximityRadius     = 4;

   flareDistance = 20;
   flareAngle    = 10;

   sound = ScoutThrustSound;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   explodeOnWaterImpact = false;

   // Used for vehicle-mounted missile system
   isVehicleMissile = true;
};

function MBengalMissile::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
    %this.firesMissiles = true;
}

function MBengalMissile::installPart(%this, %data, %vehicle, %player, %pid)
{
    %vehicle.mountImage(SeekingParamImage, 0);
}

function MBengalMissile::reArm(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.addMissile(4, MBengalSKY4, BengalMissileDeploy, BengalMissile);
            %vehicle.addMissile(5, MBengalSKY5, BengalMissileDeploy, BengalMissile);
            %vehicle.addMissile(6, MBengalSKY6, BengalMissileDeploy, BengalMissile);
            %vehicle.addMissile(7, MBengalSKY7, BengalMissileDeploy, BengalMissile);
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MBengalMissile", "Bengal Missiles", "Standard explosive rack-mounted missiles", $VehicleList::Skycutter, $VHardpointSize::Medium, $VHardpointType::Rack);
