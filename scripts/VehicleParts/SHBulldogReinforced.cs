function SHBulldogReinforced::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHBulldogReinforced::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 1400, 15000, (190 / $g_TickTime), 0.8, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHBulldogReinforced", "Reinforced Shield", "Strength: 1400 HP, Recharge delay: 15 sec, Power drain: 80%", $VehicleList::Bulldog, $VHardpointSize::Internal, $VHardpointType::Omni);
