function MOShieldPulse::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
    %this.triggerType = $VMTrigger::Push;
    %this.cooldownTime = 5000;
}

function MOShieldPulse::triggerPush(%this, %data, %vehicle, %player)
{
    if(%vehicle.getEnergyLevel() > 50)
    {
        %strength = %vehicle.shieldSource.strength;
        
        if(%strength < 1)
        {
            messageClient(%player.client, 'MsgVShieldPulseFailS', '\c2Shield strength too low, cannot ignite shield pulse.');
            return;
        }
    
        %vehicle.useEnergy(50);
        %vehicle.play3D(LargeExplosionSound);
        %vehicle.lastHitFlags = 0;
        %this.initShieldPulse(%vehicle, %player, %strength);
        %vehicle.shieldSource.strength = 0;
        %vehicle.shieldSource.getDatablock().restartRecharge(%vehicle.shieldSource, true);
    }
    else
        messageClient(%player.client, 'MsgVShieldPulseFail', '\c2Not enough energy to ignite shield pulse (50kj).');
}

function MOShieldPulse::initShieldPulse(%this, %vehicle, %player, %strength)
{
    %radius = 75;
    %impulse = %strength * 10;
    %position = %vehicle.getWorldBoxCenter();
    %damage = %strength / 100;
    
   InitContainerRadiusSearch(%position, %radius, ($TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::TurretObjectType));
   %numTargets = 0;

   while((%targetObject = containerSearchNext()) != 0)
   {
      if(%targetObject == %vehicle)
         continue;
        
      %dist = containerSearchCurrRadDamageDist();

      if(%dist > %radius)
         continue;

      if(%targetObject.isMounted())
        continue;

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for(%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %amount = (1.0 - (%dist / %radius)) * %coverage * %damage;

//      echo("found target:" SPC %targetObject SPC %coverage);

      %data = %targetObject.getDataBlock();
      %className = %data.className;

      if(%data.shouldApplyImpulse(%targetObject))
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);

         //------------------------------------------------------------------------------

         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));
         %doImpulse = true;
      }
      else if(%targetObject.isVehicle())
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);

         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));

         if( getWord( %momVec, 2 ) < -0.5 )
            %momVec = "0 0 1";

         // Add obj's velocity into the momentum vector
         %velocity = %targetObject.getVelocity();
         %doImpulse = true;
      }
      else
      {
         %momVec = "0 0 1";
         %doImpulse = false;
      }

      if(%amount > 0)
         %data.damageObject(%targetObject, %player, %position, %amount, $DamageType::ShieldPulse, %momVec, %player.client, 0);

      if(%doImpulse)
      {
         %targetObject.applyImpulse(%position, %impulseVec);
         %targetObject.playShieldEffect("0 0 1");
      }
   }
}

function MOShieldPulse::installPart(%this, %data, %vehicle, %player)
{
    %vehicle.installedModule = true;
}

VehiclePart.registerVehiclePart($VehiclePartType::Module, "MOShieldPulse", "Shield Pulse", "Projects a damaging force wave out from the shields, drains shields fully", $VehicleList::AllExceptWalkers, $VHardpointSize::Internal, $VHardpointType::Omni);
