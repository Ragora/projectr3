datablock ShapeBaseImageData(MLRM4SKY1)
{
   className = WeaponImage;
   shapeFile = "stackable2m.dts";

   usesEnergy = false;
   ammo = "VMLRM4Ammo";
   updatePilotAmmo = true;
   minEnergy = 0;
   fireEnergy = 0;
   fireTimeout = 750;
   staggerCount = 3;
   staggerDelay = 200;
   sharedResourcePool = true;

   projectile = VLRMMissile;
   projectileType = SeekerProjectile;
   useForwardVector = true;
   
   offset = "2 -0.5 0.6";
   rotation = "1 0 0 90";
   
   mountPoint = 10;

   isSeeker     = true;
   seekRadius   = 250;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 15;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = MILFireSound;
   stateTimeoutValue[4]          = 6.0;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(MLRM4SKY2) : MLRM4SKY1
{
   offset = "-2 -0.5 0.6";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MLRM4OUT1) : MLRM4SKY1
{
   offset = "0.6 6.75 -1.5";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MLRM4OUT2) : MLRM4SKY1
{
   offset = "-0.775 6.75 -1.5";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MLRM4FIR1) : MLRM4SKY1
{
   pilotHeadTracking = true;
   
   offset = "0 4.5 -0.8";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(MLRM4FIR1T) : MLRM4SKY1
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "1 0 0 0";
};

datablock ShapeBaseImageData(MLRM4GUA1) : MLRM4SKY1
{
   pilotHeadTracking = true;
   mountPoint = 1;
   offset = "-0.1 4.25 1.45";
   rotation = "1 0 0 90";
};

function MLRM4::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = true;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "VMLRM4Ammo";
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MILFireSound";
    %this.projectileType = "SeekerProjectile";
    %this.projectile = "VLRMMissile";
    %this.projectileSpread = 4.5;
    %this.fireEnergy = 0;
    %this.ammoUse = 4;
    %this.ammoAmount = 40;
    %this.fireTimeout = 6000;
    %this.heatPerShot = 36;
    %this.useForwardVector = true;
    %this.defaultFireGroup = $VehicleFiregroup::Gamma;
    %this.isSeeker = true;

    // Fires multiple shots
    %this.staggerCount = 3;
    %this.staggerDelay = 200;
}

function MLRM4::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "ClusterMissile", %this);
        return;
    }

    switch(%vehicle.vid)
    {
        case $VehicleID::Skycutter:
            %vehicle.mountImage(MLRM4SKY1, 2);
            %vehicle.mountImage(MLRM4SKY2, 3);
            %vehicle.mountImage(SeekingParamImage, 0);
            
        case $VehicleID::Outlaw:
            %vehicle.mountImage(MLRM4OUT1, 2);
            %vehicle.mountImage(MLRM4OUT2, 3);
            %vehicle.mountImage(SeekingParamImage, 0);
            
        case $VehicleID::Guardian:
            %vehicle.mountImage(MLRM4GUA1, 0);

        case $VehicleID::Firestorm:
            if(%hardpoint == 0)
                %vehicle.mountImage(MLRM4FIR1, 0);
            else
            {
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(MLRM4FIR1T, 2 * %hardpoint);
                %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]).mountImage(SeekingTurretParam, 0);
            }
    }
    
//    %vehicle.setInventory(%this.ammo, %data.max[%this.ammo]);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "MLRM4", "LRM-4 Pod", "Long-range IR tracking missile pod, 4 count", $VehicleList::General, $VHardpointSize::Medium, $VHardpointType::Missile);
