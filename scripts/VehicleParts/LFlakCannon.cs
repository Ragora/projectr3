datablock GrenadeProjectileData(LFlakShell)
{
   scale = "3.0 3.0 3.0";
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   hasDamageRadius     = true;
   indirectDamage      = 0.06;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Missile;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.0;
   bubbleEmitTime      = 1.0;

   sound               = GrenadeProjectileSound;
   explosion           = "GrenadeExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";
   velInheritFactor    = 0.85; // z0dd - ZOD, 3/30/02. Was 0.5
   splash              = GrenadeSplash;

   baseEmitter         = "";
   bubbleEmitter       = "";

   grenadeElasticity = 0.01; // z0dd - ZOD, 9/13/02. Was 0.35
   grenadeFriction   = 0.99;
   numBounces        = 0;
   armingDelayMS     = 0; // z0dd - ZOD, 9/13/02. Was 1000
   muzzleVelocity    = 150.00; // z0dd - ZOD, 3/30/02. GL projectile is faster. Was 47.00
   //drag = 0.1; // z0dd - ZOD, 3/30/02. No drag.
   gravityMod        = 0.2; // z0dd - ZOD, 5/18/02. Make GL projectile heavier, less floaty
};

function LFlakShell::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount > 96)
        return %this.detonate(%proj);
        
    InitContainerRadiusSearch(%proj.position, 12, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return %this.detonate(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.85)
                return %this.detonate(%proj);
        }
    }
}

function LFlakShell::detonate(%this, %proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "LFlakShellBurst", %proj.position, %proj.initialDirection);
}

datablock LinearFlareProjectileData(LFlakShellBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.24;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 500;

   sound               = GrenadeProjectileSound;
   explosion           = "HandGrenadeExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   flags               = $Projectile::PlayerFragment;

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock TurretImageData(LFlakCannonGUA1) : MortarBarrelLarge
{
   projectileType = GrenadeProjectile;
   projectile = LFlakShell;
   
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 180";

   usesEnergy = false;
   useCapacitor = false;
   useMountEnergy = false;
   sharedResourcePool = true;
   updatePilotAmmo = true;
   ammo = "VLFlakCannonAmmo";

   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.3;
   stateSequence[3]            = "Fire";
   stateSound[3]               = MBLFireSound;

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.033;
   stateSequence[4]              = "Reload";
};

datablock TurretImageData(LFlakCannonFIR2T) : LFlakCannonGUA1
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 1;
   offset = "0.1 -1.5 -0.265";
   rotation = "0 1 0 180";

   useCapacitor = false;
   useMountEnergy = false;
};

datablock TurretImageData(LFlakCannonIMP) : LFlakCannonGUA1
{
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   useCapacitor = false;
   useMountEnergy = false;
};

function LFlakCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "VLFlakCannonAmmo";
    %this.refImage = MissileBarrelLarge;
    
    // Walker prefs
    %this.dryFireSound = "MissileDryFireSound";
    %this.fireSound = "MBLFireSound";
    %this.projectileType = "GrenadeProjectile";
    %this.projectile = "LFlakShell";
    %this.projectileSpread = 12;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 300;
    %this.fireTimeout = 333;
    %this.heatPerShot = 1;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Beta;
}

function LFlakCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker)
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "Cannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LFlakCannonGUA1, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(SeekingTurretParam, 0);
            %turret.mountImage(LFlakCannonFIR2T, 4);

        case $VehicleID::Outlaw:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(LFlakCannonIMP, 0);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(LFlakCannonIMP, 0);

        case $VehicleID::MPB:
            %vehicle.barrel = "LFlakCannonIMP";
    }
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LFlakCannon", "Flak Cannon", "Repeating thermal proximity explosive launcher", $VehicleList::General, $VHardpointSize::Large, $VHardpointType::Ballistic);
