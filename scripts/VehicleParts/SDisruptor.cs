datablock LinearFlareProjectileData(VDisruptorBolt)
{
   directDamage        = 0.3;
   directDamageType    = $DamageType::Blaster;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::Blaster;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 50;
   mdDamageRadius[0]   = false;

   flags               = $Projectile::PlaysHitSound | $Projectile::CountMAs | $Projectile::CanHeadshot;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   scale              = "2 2 2";

   explosion          = "MegaPulseExplosion";
   baseEmitter        = MegaPulseTrailEmitter;

   dryVelocity       = 200.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   numFlares         = 16;
   size              = 0.6;
   flareColor        = "1 0.25 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1.0 0.25 0.25";
};

datablock ShapeBaseImageData(SDisruptorSKY1)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_grenade_launcher.dts";

   projectileType = LinearFlareProjectile;
   projectile = VDisruptorBolt;
   mountPoint = 10;

   offset = "3.25 -1.4 -1.0";
   rotation = "0 1 0 -125";

   usesEnergy = true;
   useMountEnergy = true;

   minEnergy = 12;
   fireEnergy = 12;
   fireTimeout = 175;
   
   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Deploy";
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.5;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = BlasterFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 1.9;
   stateAllowImageChange[4]      = false;
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

datablock ShapeBaseImageData(SDisruptorSHA1) : SDisruptorSKY1
{
   offset = "0 1.1 0.2";
   rotation = "1 0 0 0";

   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.5;
   stateSequence[3]            = "deploy";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.1;
   stateAllowImageChange[4]      = false;
   
   stateSound[3]                    = "";
   stateSound[5]                    = "";
};

datablock ShapeBaseImageData(SDisruptorGUA1) : SDisruptorSKY1
{
   mountPoint = 1;
   offset = "0 -0.3 -0.15";
   rotation = "1 0 0 0";
   
   useCapacitor = true;
   useMountEnergy = true;
   
   stateName[3]                = "Fire";
   stateTimeoutValue[3]        = 0.5;
   stateSequence[3]            = "deploy";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.1;
   stateAllowImageChange[4]      = false;
};

function SDisruptor::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "";
}

function SDisruptor::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    switch(%vehicle.vid)
    {
        case $VehicleID::Shadow:
            %vehicle.mountImage(SDisruptorSHA1, 0);

        case $VehicleID::Guardian:
            %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]).mountImage(SDisruptorGUA1, 2);
    }

    %vehicle.alternateFlipFlop = false;
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "SDisruptor", "Disruptor Mount", "Vehicle-grade disruptor mount", $VehicleList::General, $VHardpointSize::Small, $VHardpointType::Energy);
