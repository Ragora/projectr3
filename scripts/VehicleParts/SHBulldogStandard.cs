function SHBulldogStandard::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 0;
    %this.ammo = "none";
}

function SHBulldogStandard::installPart(%this, %data, %vehicle, %player)
{
    %data.installShield(%vehicle, 1000, 13000, (200 / $g_TickTime), 0.6, "VehicleShieldEmitter");
}

VehiclePart.registerVehiclePart($VehiclePartType::Shield, "SHBulldogStandard", "Standard Shield", "Strength: 1000 HP, Recharge delay: 13 sec, Power drain: 60%", $VehicleList::Bulldog, $VHardpointSize::Internal, $VHardpointType::Omni);
