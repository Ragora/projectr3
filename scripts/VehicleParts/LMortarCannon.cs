function xAssaultMortarTurretBarrel::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);

     %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);
}

function LMortarCannon::onInit(%this)
{
    // Set prefs here
    %this.usesAmmo = false;
    %this.reloadTime = 0.0;
    %this.clipSize = 0;
    %this.ammoCount = 1;
    %this.ammo = "AssaultMortarAmmo";
    %this.refImage = AssaultMortarTurretBarrel;
    
    // Walker prefs
    %this.dryFireSound = "MortarDryFireSound";
    %this.fireSound = "AssaultMortarFireSound";
    %this.projectileType = "GrenadeProjectile";
    %this.projectile = "AssaultMortar"; // MortarTurretShot
    %this.projectileSpread = 0;
    %this.fireEnergy = 0;
    %this.ammoUse = 1;
    %this.ammoAmount = 40;
    %this.fireTimeout = 3000;
    %this.heatPerShot = 10;
    %this.useForwardVector = false;
    %this.defaultFireGroup = $VehicleFiregroup::Alpha | $VehicleFiregroup::Beta;
    %this.isSeeker = false;
}

function LMortarCannon::installPart(%this, %data, %vehicle, %player, %hardpoint)
{
    if(%vehicle.isWalker !$= "")
    {
        %vehicle.walkerMountWeapon($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"], "MainCannon", %this);
        return;
    }
    
    switch(%vehicle.vid)
    {
        case $VehicleID::Guardian:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 1, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(AssaultMortarTurretBarrel, 4);

        case $VehicleID::Firestorm:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, 2, "mountPoint"]);
            %turret.mountImage(AssaultTurretParam, 0);
            %turret.mountImage(AssaultMortarTurretBarrel, 4);

        case $VehicleID::Imperator:
            %turret = %vehicle.getMountNodeObject($VehicleHardpoints[%vehicle.vid, %hardpoint, "mountPoint"]);
            %turret.mountImage(AssaultMortarBarrelI, 0);
    }

//    %vehicle.turretObject.setInventory(%this.ammo, %vehicle.turretObject.max[%this.ammo]);
}

VehiclePart.registerVehiclePart($VehiclePartType::Weapon, "LMortarCannon", "Artillery Mortar", "Main gun switched to Artillery Mortar", 0, $VHardpointSize::Large, $VHardpointType::Ballistic); // $VehicleList::All - disable for now
