<?php 
function generatePassword($user, $pass)
{
	return hash('sha512', base64_encode(sprintf("%s%s%s", $user, md5($user), $pass)));
}

function transactionFail($dbc)
{
	$dbc->rollback();
	$dbc->autocommit(true);
	
	die("TRANSACTION FAILED");
}

function generateServerID($len = 16)
{
	$id = "";
	
	for($i = 0; $i < $len; $i++)
		$id .= rand(0, 1) ? chr(rand(65, 90)) : rand(0, 9);
		
	return $id;
}

function getIPAddr()
{
	if(!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
    else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
		return $_SERVER['REMOTE_ADDR'];
}
?>